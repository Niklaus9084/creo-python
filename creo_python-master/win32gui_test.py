# -*- coding:utf-8 -*-
import win32gui
import win32con
import re





def active_creo():
    hwnd_title = {}

    def get_all_hwnd(hwnd, mouse):
        if (win32gui.IsWindow(hwnd) and win32gui.IsWindowEnabled(hwnd) and win32gui.IsWindowVisible(hwnd)):
            hwnd_title.update({hwnd: win32gui.GetWindowText(hwnd)})

    win32gui.EnumWindows(get_all_hwnd, 0)
    for h, t in hwnd_title.items():
        if re.match(r'.*?\(活动的\) - Creo Parametric.*?', t):
            hwnd = win32gui.FindWindow(None, t)
            win32gui.ShowWindow(hwnd, win32con.SW_SHOWMAXIMIZED)
            win32gui.SetWindowPos(hwnd, win32con.HWND_TOPMOST, 0, 0, 1920, 1080, win32con.SWP_SHOWWINDOW)
            win32gui.SetWindowPos(hwnd, win32con.HWND_NOTOPMOST, 0, 0, 1920, 1080, win32con.SWP_SHOWWINDOW)
# hwnd = win32gui.FindWindow(None, "T_1001000-12 (活动的) - Creo Parametric 6.0")
# win32gui.ShowWindow(hwnd, win32con.SW_SHOWMAXIMIZED)
# win32gui.SetWindowPos(hwnd, win32con.HWND_TOPMOST, 0, 0, 0, 0,
#                       win32con.SWP_NOMOVE | win32con.SWP_NOACTIVATE | win32con.SWP_NOOWNERZORDER | win32con.SWP_SHOWWINDOW | win32con.SWP_NOSIZE)

if __name__ == '__main__':
    active_creo()