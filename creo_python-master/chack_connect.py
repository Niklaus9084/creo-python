import VBAPI
from win32com import client

CREO_APP = 'C:/Program Files/PTC/Creo 6.0.4.0/Parametric/bin/parametric.exe'
INPUT_DIR = 'E:/python/test_data/'

cAC = client.Dispatch(VBAPI.CCpfcAsyncConnection)
#AsyncConnection = cAC.GetActiveConnection()
AsyncConnection = cAC.Start(CREO_APP, '')
print(AsyncConnection.Session.ConnectionId)
AsyncConnection.End()
