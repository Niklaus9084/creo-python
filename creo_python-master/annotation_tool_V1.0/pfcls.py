# -*- coding:utf-8 -*-
from win32com.client import Dispatch, DispatchBaseClass
from enum import Enum


class ICpfcACIS3DExportInstructions(object):
    com = 'pfcls.pfcACIS3DExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inConfiguration=None, inGeometry=None):
        return cls.disp.Create(inConfiguration, inGeometry)


class ICpfcAngleDimensionSense(object):
    com = 'pfcls.pfcAngleDimensionSense.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, AngleOptions=None):
        return cls.disp.Create(AngleOptions)


class ICpfcAngularDimOptions(object):
    com = 'pfcls.pfcAngularDimOptions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, IsFirst=None, ShouldFlip=None):
        return cls.disp.Create(IsFirst, ShouldFlip)


class ICpfcAngularDimSense(object):
    com = 'pfcls.pfcAngularDimSense.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Options=None):
        return cls.disp.Create(Options)


class ICpfcAnnotationTextStyle(object):
    com = 'pfcls.pfcAnnotationTextStyle.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcAppInfo(object):
    com = 'pfcls.pfcAppInfo.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Compatibility=None):
        return cls.disp.Create(Compatibility)


class ICpfcArcDescriptor(object):
    com = 'pfcls.pfcArcDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Vector1=None, Vector2=None, Center=None, StartAngle=None, EndAngle=None, Radius=None):
        return cls.disp.Create(Vector1, Vector2, Center, StartAngle, EndAngle, Radius)


class ICpfcArgument(object):
    com = 'pfcls.pfcArgument.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inLabel=None, inValue=None):
        return cls.disp.Create(inLabel, inValue)


class ICpfcArrowDescriptor(object):
    com = 'pfcls.pfcArrowDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, End1=None, End2=None):
        return cls.disp.Create(End1, End2)


class ICpfcASSEMTreeCFGImportInstructions(object):
    com = 'pfcls.pfcASSEMTreeCFGImportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcAsyncConnection(object):
    com = 'pfcls.pfcAsyncConnection.1'
    disp = Dispatch(com)

    @classmethod
    def Connect(cls, Display=None, UserID=None, TextPath=None, TimeoutSec=None):
        return cls.disp.Connect(Display, UserID, TextPath, TimeoutSec)

    @classmethod
    def ConnectById(cls, Id=None, TextPath=None, TimeoutSec=None):
        return cls.disp.ConnectById(Id, TextPath, TimeoutSec)

    @classmethod
    def ConnectWS(cls, Display=None, UserID=None, WSName=None, TextPath=None, TimeoutSec=None):
        return cls.disp.ConnectWS(Display, UserID, WSName, TextPath, TimeoutSec)

    @classmethod
    def GetActiveConnection(cls):
        return cls.disp.GetActiveConnection()

    @classmethod
    def Spawn(cls, CmdLine=None, TextPath=None):
        return cls.disp.Spawn(CmdLine, TextPath)

    @classmethod
    def Start(cls, CmdLine=None, TextPath=None):
        return cls.disp.Start(CmdLine, TextPath)


class ICpfcBitmapImageExportInstructions(object):
    com = 'pfcls.pfcBitmapImageExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, ImageWidth=None, ImageHeight=None):
        return cls.disp.Create(ImageWidth, ImageHeight)


class ICpfcBOMExportInstructions(object):
    com = 'pfcls.pfcBOMExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcBSplineDescriptor(object):
    com = 'pfcls.pfcBSplineDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Degree=None, Points=None, Knots=None):
        return cls.disp.Create(Degree, Points, Knots)


class ICpfcBSplinePoint(object):
    com = 'pfcls.pfcBSplinePoint.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Point=None, Weight=None):
        return cls.disp.Create(Point, Weight)


class ICpfcCableParamsFileInstructions(object):
    com = 'pfcls.pfcCableParamsFileInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Mdl=None, CableName=None):
        return cls.disp.Create(Mdl, CableName)


class ICpfcCableParamsImportInstructions(object):
    com = 'pfcls.pfcCableParamsImportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Harness=None, CableName=None):
        return cls.disp.Create(Harness, CableName)


class ICpfcCADDSExportInstructions(object):
    com = 'pfcls.pfcCADDSExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcCatiaCGR3DExportInstructions(object):
    com = 'pfcls.pfcCatiaCGR3DExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inConfiguration=None, inGeometry=None):
        return cls.disp.Create(inConfiguration, inGeometry)


class ICpfcCATIAFacetsExportInstructions(object):
    com = 'pfcls.pfcCATIAFacetsExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, CsysName=None):
        return cls.disp.Create(CsysName)


class ICpfcCATIAModel3DExportInstructions(object):
    com = 'pfcls.pfcCATIAModel3DExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inConfiguration=None, inGeometry=None):
        return cls.disp.Create(inConfiguration, inGeometry)


class ICpfcCatiaPart3DExportInstructions(object):
    com = 'pfcls.pfcCatiaPart3DExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inConfiguration=None, inGeometry=None):
        return cls.disp.Create(inConfiguration, inGeometry)


class ICpfcCatiaProduct3DExportInstructions(object):
    com = 'pfcls.pfcCatiaProduct3DExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inConfiguration=None, inGeometry=None):
        return cls.disp.Create(inConfiguration, inGeometry)


class ICpfcCATIASession3DExportInstructions(object):
    com = 'pfcls.pfcCATIASession3DExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inConfiguration=None, inGeometry=None):
        return cls.disp.Create(inConfiguration, inGeometry)


class ICpfcCGMFILEExportInstructions(object):
    com = 'pfcls.pfcCGMFILEExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, ExportType=None, WhichScale=None):
        return cls.disp.Create(ExportType, WhichScale)


class ICpfcCheckinOptions(object):
    com = 'pfcls.pfcCheckinOptions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcCheckoutOptions(object):
    com = 'pfcls.pfcCheckoutOptions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcCircleDescriptor(object):
    com = 'pfcls.pfcCircleDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Center=None, Radius=None, UnitNormal=None):
        return cls.disp.Create(Center, Radius, UnitNormal)


class ICpfcColorRGB(object):
    com = 'pfcls.pfcColorRGB.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inRed=None, inGreen=None, inBlue=None):
        return cls.disp.Create(inRed, inGreen, inBlue)


class ICpfcColumnCreateOption(object):
    com = 'pfcls.pfcColumnCreateOption.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Justification=None, ColumnWidth=None):
        return cls.disp.Create(Justification, ColumnWidth)


class ICpfcComponentConstraint(object):
    com = 'pfcls.pfcComponentConstraint.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Type=None):
        return cls.disp.Create(Type)


class ICpfcComponentDimensionShowInstructions(object):
    com = 'pfcls.pfcComponentDimensionShowInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Path=None):
        return cls.disp.Create(Path)


class ICpfcCompositeCurveDescriptor(object):
    com = 'pfcls.pfcCompositeCurveDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Elements=None):
        return cls.disp.Create(Elements)


class ICpfcConeDescriptor(object):
    com = 'pfcls.pfcConeDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Extents=None, Orientation=None, Origin=None, Alpha=None):
        return cls.disp.Create(Extents, Orientation, Origin, Alpha)


class ICpfcConfigImportInstructions(object):
    com = 'pfcls.pfcConfigImportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcConnectionId(object):
    com = 'pfcls.pfcConnectionId.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, ExternalRep=None):
        return cls.disp.Create(ExternalRep)


class ICpfcConnectorParamExportInstructions(object):
    com = 'pfcls.pfcConnectorParamExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, MembIdTab=None):
        return cls.disp.Create(MembIdTab)


class ICpfcConnectorParamsImportInstructions(object):
    com = 'pfcls.pfcConnectorParamsImportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, MembIdTab=None):
        return cls.disp.Create(MembIdTab)


class ICpfcConstraintAttributes(object):
    com = 'pfcls.pfcConstraintAttributes.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Force=None, Ignore=None):
        return cls.disp.Create(Force, Ignore)


class ICpfcCoonsPatchDescriptor(object):
    com = 'pfcls.pfcCoonsPatchDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Extents=None, Orientation=None, U0Profile=None, U1Profile=None, V0Profile=None, V1Profile=None,
               CornerPoints=None, UVDerivatives=None):
        return cls.disp.Create(Extents, Orientation, U0Profile, U1Profile, V0Profile, V1Profile, CornerPoints,
                               UVDerivatives)


class ICpfcCreateNewSimpRepInstructions(object):
    com = 'pfcls.pfcCreateNewSimpRepInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, NewSimpName=None):
        return cls.disp.Create(NewSimpName)


class ICpfcCustomCheckInstructions(object):
    com = 'pfcls.pfcCustomCheckInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, CheckName=None, CheckLabel=None, Listener=None):
        return cls.disp.Create(CheckName, CheckLabel, Listener)


class ICpfcCustomCheckResults(object):
    com = 'pfcls.pfcCustomCheckResults.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, ResultsCount=None):
        return cls.disp.Create(ResultsCount)


class ICpfcCylinderDescriptor(object):
    com = 'pfcls.pfcCylinderDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Extents=None, Orientation=None, Origin=None, Radius=None):
        return cls.disp.Create(Extents, Orientation, Origin, Radius)


class ICpfcCylindricalSplineSurfaceDescriptor(object):
    com = 'pfcls.pfcCylindricalSplineSurfaceDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Extents=None, Orientation=None, Origin=None, SplineSurfaceData=None):
        return cls.disp.Create(Extents, Orientation, Origin, SplineSurfaceData)


class ICpfcDatumAxisConstraint(object):
    com = 'pfcls.pfcDatumAxisConstraint.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, ConstraintType=None, ConstraintRef=None):
        return cls.disp.Create(ConstraintType, ConstraintRef)


class ICpfcDatumAxisDimensionConstraint(object):
    com = 'pfcls.pfcDatumAxisDimensionConstraint.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, DimRef=None, DimOffset=None):
        return cls.disp.Create(DimRef, DimOffset)


class ICpfcDatumCsysDimensionConstraint(object):
    com = 'pfcls.pfcDatumCsysDimensionConstraint.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, DimRef=None, DimConstraintType=None, DimValue=None):
        return cls.disp.Create(DimRef, DimConstraintType, DimValue)


class ICpfcDatumCsysOrientMoveConstraint(object):
    com = 'pfcls.pfcDatumCsysOrientMoveConstraint.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, OrientMoveConstraintType=None, OrientMoveValue=None):
        return cls.disp.Create(OrientMoveConstraintType, OrientMoveValue)


class ICpfcDatumCsysOriginConstraint(object):
    com = 'pfcls.pfcDatumCsysOriginConstraint.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, OriginRef=None):
        return cls.disp.Create(OriginRef)


class ICpfcDatumPlaneAngleConstraint(object):
    com = 'pfcls.pfcDatumPlaneAngleConstraint.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, AngleRef=None, AngleValue=None):
        return cls.disp.Create(AngleRef, AngleValue)


class ICpfcDatumPlaneDefaultXConstraint(object):
    com = 'pfcls.pfcDatumPlaneDefaultXConstraint.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcDatumPlaneDefaultYConstraint(object):
    com = 'pfcls.pfcDatumPlaneDefaultYConstraint.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcDatumPlaneDefaultZConstraint(object):
    com = 'pfcls.pfcDatumPlaneDefaultZConstraint.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcDatumPlaneNormalConstraint(object):
    com = 'pfcls.pfcDatumPlaneNormalConstraint.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, NormalRef=None):
        return cls.disp.Create(NormalRef)


class ICpfcDatumPlaneOffsetConstraint(object):
    com = 'pfcls.pfcDatumPlaneOffsetConstraint.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, OffsetRef=None, OffsetValue=None):
        return cls.disp.Create(OffsetRef, OffsetValue)


class ICpfcDatumPlaneOffsetCoordSysConstraint(object):
    com = 'pfcls.pfcDatumPlaneOffsetCoordSysConstraint.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, CsysAxis=None, OffsetRef=None, OffsetValue=None):
        return cls.disp.Create(CsysAxis, OffsetRef, OffsetValue)


class ICpfcDatumPlaneParallelConstraint(object):
    com = 'pfcls.pfcDatumPlaneParallelConstraint.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, ParallelRef=None):
        return cls.disp.Create(ParallelRef)


class ICpfcDatumPlaneSectionConstraint(object):
    com = 'pfcls.pfcDatumPlaneSectionConstraint.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, SectionRef=None, SectionIndex=None):
        return cls.disp.Create(SectionRef, SectionIndex)


class ICpfcDatumPlaneTangentConstraint(object):
    com = 'pfcls.pfcDatumPlaneTangentConstraint.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, TangentRef=None):
        return cls.disp.Create(TangentRef)


class ICpfcDatumPlaneThroughConstraint(object):
    com = 'pfcls.pfcDatumPlaneThroughConstraint.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, ThroughRef=None):
        return cls.disp.Create(ThroughRef)


class ICpfcDatumPointDimensionConstraint(object):
    com = 'pfcls.pfcDatumPointDimensionConstraint.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, ConstraintRef=None, Type=None, value=None):
        return cls.disp.Create(ConstraintRef, Type, value)


class ICpfcDatumPointPlacementConstraint(object):
    com = 'pfcls.pfcDatumPointPlacementConstraint.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, ConstraintRef=None, Type=None, value=None):
        return cls.disp.Create(ConstraintRef, Type, value)


class ICpfcDetailAttachment(object):
    com = 'pfcls.pfcDetailAttachment.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, NoteAttach=None, LeaderAttach=None):
        return cls.disp.Create(NoteAttach, LeaderAttach)


class ICpfcDetailEntityInstructions(object):
    com = 'pfcls.pfcDetailEntityInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inGeometry=None, inView=None):
        return cls.disp.Create(inGeometry, inView)


class ICpfcDetailGroupInstructions(object):
    com = 'pfcls.pfcDetailGroupInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inName=None, inElements=None):
        return cls.disp.Create(inName, inElements)


class ICpfcDetailLeaders(object):
    com = 'pfcls.pfcDetailLeaders.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcDetailNoteInstructions(object):
    com = 'pfcls.pfcDetailNoteInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inTextLines=None):
        return cls.disp.Create(inTextLines)


class ICpfcDetailSymbolDefInstructions(object):
    com = 'pfcls.pfcDetailSymbolDefInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inFullPath=None):
        return cls.disp.Create(inFullPath)


class ICpfcDetailSymbolGroupInstructions(object):
    com = 'pfcls.pfcDetailSymbolGroupInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Name=None, Items=None):
        return cls.disp.Create(Name, Items)


class ICpfcDetailSymbolInstInstructions(object):
    com = 'pfcls.pfcDetailSymbolInstInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inSymbolDef=None):
        return cls.disp.Create(inSymbolDef)


class ICpfcDetailText(object):
    com = 'pfcls.pfcDetailText.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inText=None):
        return cls.disp.Create(inText)


class ICpfcDetailTextLine(object):
    com = 'pfcls.pfcDetailTextLine.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inTexts=None):
        return cls.disp.Create(inTexts)


class ICpfcDetailVariantText(object):
    com = 'pfcls.pfcDetailVariantText.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inPrompt=None, inValue=None):
        return cls.disp.Create(inPrompt, inValue)


class ICpfcDimensionAngleOptions(object):
    com = 'pfcls.pfcDimensionAngleOptions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcDimTolISODIN(object):
    com = 'pfcls.pfcDimTolISODIN.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inTolTableType=None, inTableName=None, inTableColumn=None):
        return cls.disp.Create(inTolTableType, inTableName, inTableColumn)


class ICpfcDimTolLimits(object):
    com = 'pfcls.pfcDimTolLimits.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inUpperLimit=None, inLowerLimit=None):
        return cls.disp.Create(inUpperLimit, inLowerLimit)


class ICpfcDimTolPlusMinus(object):
    com = 'pfcls.pfcDimTolPlusMinus.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inPlus=None, inMinus=None):
        return cls.disp.Create(inPlus, inMinus)


class ICpfcDimTolSymmetric(object):
    com = 'pfcls.pfcDimTolSymmetric.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inValue=None):
        return cls.disp.Create(inValue)


class ICpfcDimTolSymSuperscript(object):
    com = 'pfcls.pfcDimTolSymSuperscript.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inValue=None):
        return cls.disp.Create(inValue)


class ICpfcDirectorySelectionOptions(object):
    com = 'pfcls.pfcDirectorySelectionOptions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcDrawingDimCreateInstructions(object):
    com = 'pfcls.pfcDrawingDimCreateInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Attachments=None, Senses=None, TextLocation=None, Hint=None):
        return cls.disp.Create(Attachments, Senses, TextLocation, Hint)


class ICpfcDrawingDimensionShowInstructions(object):
    com = 'pfcls.pfcDrawingDimensionShowInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, View=None, Path=None):
        return cls.disp.Create(View, Path)


class ICpfcDWG3DExportInstructions(object):
    com = 'pfcls.pfcDWG3DExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcDWGImport2DInstructions(object):
    com = 'pfcls.pfcDWGImport2DInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcDWGSetupExportInstructions(object):
    com = 'pfcls.pfcDWGSetupExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcDWGSetupImportInstructions(object):
    com = 'pfcls.pfcDWGSetupImportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcDXF3DExportInstructions(object):
    com = 'pfcls.pfcDXF3DExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcDXFExportInstructions(object):
    com = 'pfcls.pfcDXFExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcDXFImport2DInstructions(object):
    com = 'pfcls.pfcDXFImport2DInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcEllipseDescriptor(object):
    com = 'pfcls.pfcEllipseDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Center=None, UnitMajorAxis=None, UnitNormal=None, MajorLength=None, MinorLength=None,
               StartAngle=None, EndAngle=None):
        return cls.disp.Create(Center, UnitMajorAxis, UnitNormal, MajorLength, MinorLength, StartAngle, EndAngle)


class ICpfcEmptyDimensionSense(object):
    com = 'pfcls.pfcEmptyDimensionSense.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcEmptyDimSense(object):
    com = 'pfcls.pfcEmptyDimSense.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcEPSImageExportInstructions(object):
    com = 'pfcls.pfcEPSImageExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, ImageWidth=None, ImageHeight=None):
        return cls.disp.Create(ImageWidth, ImageHeight)


class ICpfcExport2DOption(object):
    com = 'pfcls.pfcExport2DOption.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcFeatInfoExportInstructions(object):
    com = 'pfcls.pfcFeatInfoExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FeatId=None):
        return cls.disp.Create(FeatId)


class ICpfcFIATExportInstructions(object):
    com = 'pfcls.pfcFIATExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Flags=None):
        return cls.disp.Create(Flags)


class ICpfcFileOpenOptions(object):
    com = 'pfcls.pfcFileOpenOptions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FilterString=None):
        return cls.disp.Create(FilterString)


class ICpfcFileOpenRegisterOptions(object):
    com = 'pfcls.pfcFileOpenRegisterOptions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FileType=None, FileDescription=None):
        return cls.disp.Create(FileType, FileDescription)


class ICpfcFileOpenShortcut(object):
    com = 'pfcls.pfcFileOpenShortcut.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, ShorcutPath=None, ShortcutName=None):
        return cls.disp.Create(ShorcutPath, ShortcutName)


class ICpfcFileSaveOptions(object):
    com = 'pfcls.pfcFileSaveOptions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FilterString=None):
        return cls.disp.Create(FilterString)


class ICpfcFileSaveRegisterOptions(object):
    com = 'pfcls.pfcFileSaveRegisterOptions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FileType=None, FileDescription=None):
        return cls.disp.Create(FileType, FileDescription)


class ICpfcFilletSurfaceDescriptor(object):
    com = 'pfcls.pfcFilletSurfaceDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Extents=None, Orientation=None, U0Profile=None, CenterProfile=None, TangentProfile=None):
        return cls.disp.Create(Extents, Orientation, U0Profile, CenterProfile, TangentProfile)


class ICpfcFolderAssignment(object):
    com = 'pfcls.pfcFolderAssignment.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Folder=None, ModelName=None):
        return cls.disp.Create(Folder, ModelName)


class ICpfcForeignSurfaceDescriptor(object):
    com = 'pfcls.pfcForeignSurfaceDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Extents=None, Orientation=None, Origin=None, ForeignID=None):
        return cls.disp.Create(Extents, Orientation, Origin, ForeignID)


class ICpfcFreeAttachment(object):
    com = 'pfcls.pfcFreeAttachment.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inAttachmentPoint=None):
        return cls.disp.Create(inAttachmentPoint)


class ICpfcGeneralViewCreateInstructions(object):
    com = 'pfcls.pfcGeneralViewCreateInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, ViewModel=None, SheetNumber=None, Location=None, Orientation=None):
        return cls.disp.Create(ViewModel, SheetNumber, Location, Orientation)


class ICpfcGeometryFlags(object):
    com = 'pfcls.pfcGeometryFlags.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcGeomExportFlags(object):
    com = 'pfcls.pfcGeomExportFlags.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcIGES3DExportInstructions(object):
    com = 'pfcls.pfcIGES3DExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Flags=None):
        return cls.disp.Create(Flags)


class ICpfcIGES3DNewExportInstructions(object):
    com = 'pfcls.pfcIGES3DNewExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inConfiguration=None, inGeometry=None):
        return cls.disp.Create(inConfiguration, inGeometry)


class ICpfcIGESFileExportInstructions(object):
    com = 'pfcls.pfcIGESFileExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcIGESImport2DInstructions(object):
    com = 'pfcls.pfcIGESImport2DInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcIGESSectionImportInstructions(object):
    com = 'pfcls.pfcIGESSectionImportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcImportFeatAttr(object):
    com = 'pfcls.pfcImportFeatAttr.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcInclusionFlags(object):
    com = 'pfcls.pfcInclusionFlags.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcIntfACIS(object):
    com = 'pfcls.pfcIntfACIS.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FileName=None):
        return cls.disp.Create(FileName)


class ICpfcIntfAI(object):
    com = 'pfcls.pfcIntfAI.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FileName=None):
        return cls.disp.Create(FileName)


class ICpfcIntfCatiaCGR(object):
    com = 'pfcls.pfcIntfCatiaCGR.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FileName=None):
        return cls.disp.Create(FileName)


class ICpfcIntfCatiaPart(object):
    com = 'pfcls.pfcIntfCatiaPart.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FileName=None):
        return cls.disp.Create(FileName)


class ICpfcIntfCatiaProduct(object):
    com = 'pfcls.pfcIntfCatiaProduct.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FileName=None):
        return cls.disp.Create(FileName)


class ICpfcIntfCDRS(object):
    com = 'pfcls.pfcIntfCDRS.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FileName=None):
        return cls.disp.Create(FileName)


class ICpfcIntfDXF(object):
    com = 'pfcls.pfcIntfDXF.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FileName=None):
        return cls.disp.Create(FileName)


class ICpfcIntfICEM(object):
    com = 'pfcls.pfcIntfICEM.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FileName=None):
        return cls.disp.Create(FileName)


class ICpfcIntfIges(object):
    com = 'pfcls.pfcIntfIges.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FileName=None):
        return cls.disp.Create(FileName)


class ICpfcIntfJT(object):
    com = 'pfcls.pfcIntfJT.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FileName=None):
        return cls.disp.Create(FileName)


class ICpfcIntfNeutral(object):
    com = 'pfcls.pfcIntfNeutral.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FileName=None):
        return cls.disp.Create(FileName)


class ICpfcIntfNeutralFile(object):
    com = 'pfcls.pfcIntfNeutralFile.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FileName=None):
        return cls.disp.Create(FileName)


class ICpfcIntfParaSolid(object):
    com = 'pfcls.pfcIntfParaSolid.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FileName=None):
        return cls.disp.Create(FileName)


class ICpfcIntfProductView(object):
    com = 'pfcls.pfcIntfProductView.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FileName=None):
        return cls.disp.Create(FileName)


class ICpfcIntfStep(object):
    com = 'pfcls.pfcIntfStep.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FileName=None):
        return cls.disp.Create(FileName)


class ICpfcIntfSTL(object):
    com = 'pfcls.pfcIntfSTL.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FileName=None):
        return cls.disp.Create(FileName)


class ICpfcIntfUG(object):
    com = 'pfcls.pfcIntfUG.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FileName=None):
        return cls.disp.Create(FileName)


class ICpfcIntfVDA(object):
    com = 'pfcls.pfcIntfVDA.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FileName=None):
        return cls.disp.Create(FileName)


class ICpfcIntfVRML(object):
    com = 'pfcls.pfcIntfVRML.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FileName=None):
        return cls.disp.Create(FileName)


class ICpfcInventorExportInstructions(object):
    com = 'pfcls.pfcInventorExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, CsysName=None):
        return cls.disp.Create(CsysName)


class ICpfcJPEGImageExportInstructions(object):
    com = 'pfcls.pfcJPEGImageExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, ImageWidth=None, ImageHeight=None):
        return cls.disp.Create(ImageWidth, ImageHeight)


class ICpfcJT3DExportInstructions(object):
    com = 'pfcls.pfcJT3DExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inConfiguration=None, inGeometry=None):
        return cls.disp.Create(inConfiguration, inGeometry)


class ICpfcLayerExportOptions(object):
    com = 'pfcls.pfcLayerExportOptions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcLeaderAttachment(object):
    com = 'pfcls.pfcLeaderAttachment.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, LeaderAttachs=None):
        return cls.disp.Create(LeaderAttachs)


class ICpfcLinAOCTangentDimensionSense(object):
    com = 'pfcls.pfcLinAOCTangentDimensionSense.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, TangentType=None):
        return cls.disp.Create(TangentType)


class ICpfcLineAOCTangentDimSense(object):
    com = 'pfcls.pfcLineAOCTangentDimSense.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, TangentType=None):
        return cls.disp.Create(TangentType)


class ICpfcLineDescriptor(object):
    com = 'pfcls.pfcLineDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, End1=None, End2=None):
        return cls.disp.Create(End1, End2)


class ICpfcMassProperty(object):
    com = 'pfcls.pfcMassProperty.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Volume=None, SurfaceArea=None, Density=None, Mass=None, GravityCenter=None, CoordSysInertia=None,
               CoordSysInertiaTensor=None, CenterGravityInertiaTensor=None, PrincipalMoments=None, PrincipalAxes=None):
        return cls.disp.Create(Volume, SurfaceArea, Density, Mass, GravityCenter, CoordSysInertia,
                               CoordSysInertiaTensor, CenterGravityInertiaTensor, PrincipalMoments, PrincipalAxes)


class ICpfcMaterialExportInstructions(object):
    com = 'pfcls.pfcMaterialExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcMaterialProperty(object):
    com = 'pfcls.pfcMaterialProperty.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, value=None, Units=None):
        return cls.disp.Create(value, Units)


class ICpfcMedusaExportInstructions(object):
    com = 'pfcls.pfcMedusaExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inOption=None):
        return cls.disp.Create(inOption)


class ICpfcMessageDialogOptions(object):
    com = 'pfcls.pfcMessageDialogOptions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcMFGFeatCLExportInstructions(object):
    com = 'pfcls.pfcMFGFeatCLExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FeatId=None):
        return cls.disp.Create(FeatId)


class ICpfcMFGOperCLExportInstructions(object):
    com = 'pfcls.pfcMFGOperCLExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FeatId=None):
        return cls.disp.Create(FeatId)


class ICpfcModelCheckInstructions(object):
    com = 'pfcls.pfcModelCheckInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcModelDescriptor(object):
    com = 'pfcls.pfcModelDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Type=None, InstanceName=None, GenericName=None):
        return cls.disp.Create(Type, InstanceName, GenericName)

    @classmethod
    def CreateFromFileName(cls, FileName=None):
        return cls.disp.CreateFromFileName(FileName)


class ICpfcModelInfoExportInstructions(object):
    com = 'pfcls.pfcModelInfoExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcModelItemOId(object):
    com = 'pfcls.pfcModelItemOId.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Type=None, Id=None):
        return cls.disp.Create(Type, Id)


class ICpfcModelOId(object):
    com = 'pfcls.pfcModelOId.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Type=None, Name=None):
        return cls.disp.Create(Type, Name)


class ICpfcMouseStatus(object):
    com = 'pfcls.pfcMouseStatus.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Position=None):
        return cls.disp.Create(Position)


class ICpfcNEUTRALFileExportInstructions(object):
    com = 'pfcls.pfcNEUTRALFileExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcNormalLeaderAttachment(object):
    com = 'pfcls.pfcNormalLeaderAttachment.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, LeaderAttachs=None):
        return cls.disp.Create(LeaderAttachs)


class ICpfcNURBSSurfaceDescriptor(object):
    com = 'pfcls.pfcNURBSSurfaceDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Extents=None, Orientation=None, UDegree=None, VDegree=None, UKnots=None, VKnots=None, Points=None):
        return cls.disp.Create(Extents, Orientation, UDegree, VDegree, UKnots, VKnots, Points)


class ICpfcOffsetAttachment(object):
    com = 'pfcls.pfcOffsetAttachment.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inAttachedGeometry=None, inAttachmentPoint=None):
        return cls.disp.Create(inAttachedGeometry, inAttachmentPoint)


class ICpfcParameterSelectionOptions(object):
    com = 'pfcls.pfcParameterSelectionOptions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcParametricAttachment(object):
    com = 'pfcls.pfcParametricAttachment.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inAttachedGeometry=None):
        return cls.disp.Create(inAttachedGeometry)


class ICpfcParamOId(object):
    com = 'pfcls.pfcParamOId.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Name=None):
        return cls.disp.Create(Name)


class ICpfcParaSolid3DExportInstructions(object):
    com = 'pfcls.pfcParaSolid3DExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inConfiguration=None, inGeometry=None):
        return cls.disp.Create(inConfiguration, inGeometry)


class ICpfcPDFExportInstructions(object):
    com = 'pfcls.pfcPDFExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcPDFOption(object):
    com = 'pfcls.pfcPDFOption.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcPlaneDescriptor(object):
    com = 'pfcls.pfcPlaneDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Extents=None, Orientation=None, Origin=None):
        return cls.disp.Create(Extents, Orientation, Origin)


class ICpfcPlotInstructions(object):
    com = 'pfcls.pfcPlotInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, PlotterName=None):
        return cls.disp.Create(PlotterName)


class ICpfcPointDescriptor(object):
    com = 'pfcls.pfcPointDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Point=None):
        return cls.disp.Create(Point)


class ICpfcPointDimensionSense(object):
    com = 'pfcls.pfcPointDimensionSense.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, PointType=None):
        return cls.disp.Create(PointType)


class ICpfcPointDimSense(object):
    com = 'pfcls.pfcPointDimSense.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, PointType=None):
        return cls.disp.Create(PointType)


class ICpfcPointToAngleDimensionSense(object):
    com = 'pfcls.pfcPointToAngleDimensionSense.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, PointType=None, AngleOptions=None):
        return cls.disp.Create(PointType, AngleOptions)


class ICpfcPointToAngleDimSense(object):
    com = 'pfcls.pfcPointToAngleDimSense.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, PointType=None, Options=None):
        return cls.disp.Create(PointType, Options)


class ICpfcPolygonDescriptor(object):
    com = 'pfcls.pfcPolygonDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Vertices=None):
        return cls.disp.Create(Vertices)


class ICpfcPopupmenuOptions(object):
    com = 'pfcls.pfcPopupmenuOptions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Name=None):
        return cls.disp.Create(Name)


class ICpfcPrinterInstructions(object):
    com = 'pfcls.pfcPrinterInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcPrinterPCFOptions(object):
    com = 'pfcls.pfcPrinterPCFOptions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcPrintMdlOption(object):
    com = 'pfcls.pfcPrintMdlOption.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcPrintPlacementOption(object):
    com = 'pfcls.pfcPrintPlacementOption.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcPrintPrinterOption(object):
    com = 'pfcls.pfcPrintPrinterOption.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcPrintSize(object):
    com = 'pfcls.pfcPrintSize.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcProductViewExportInstructions(object):
    com = 'pfcls.pfcProductViewExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcProductViewExportOptions(object):
    com = 'pfcls.pfcProductViewExportOptions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inPVFormat=None):
        return cls.disp.Create(inPVFormat)


class ICpfcProgramExportInstructions(object):
    com = 'pfcls.pfcProgramExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcProgramImportInstructions(object):
    com = 'pfcls.pfcProgramImportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcProjectionViewCreateInstructions(object):
    com = 'pfcls.pfcProjectionViewCreateInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, ParentView=None, Location=None):
        return cls.disp.Create(ParentView, Location)


class ICpfcRegenInstructions(object):
    com = 'pfcls.pfcRegenInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, AllowFixUI=None, ForceRegen=None, FromFeat=None):
        return cls.disp.Create(AllowFixUI, ForceRegen, FromFeat)


class ICpfcRelationExportInstructions(object):
    com = 'pfcls.pfcRelationExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcRelationFunctionArgument(object):
    com = 'pfcls.pfcRelationFunctionArgument.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Type=None):
        return cls.disp.Create(Type)


class ICpfcRelationFunctionOptions(object):
    com = 'pfcls.pfcRelationFunctionOptions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcRelationImportInstructions(object):
    com = 'pfcls.pfcRelationImportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcRenderExportInstructions(object):
    com = 'pfcls.pfcRenderExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, CsysName=None):
        return cls.disp.Create(CsysName)


class ICpfcRetrieveExistingSimpRepInstructions(object):
    com = 'pfcls.pfcRetrieveExistingSimpRepInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, ExistSimpName=None):
        return cls.disp.Create(ExistSimpName)


class ICpfcRetrieveModelOptions(object):
    com = 'pfcls.pfcRetrieveModelOptions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcRevolvedSurfaceDescriptor(object):
    com = 'pfcls.pfcRevolvedSurfaceDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Extents=None, Orientation=None, Origin=None, Profile=None):
        return cls.disp.Create(Extents, Orientation, Origin, Profile)


class ICpfcRuledSurfaceDescriptor(object):
    com = 'pfcls.pfcRuledSurfaceDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Extents=None, Orientation=None, Origin=None, Profile1=None, Profile2=None):
        return cls.disp.Create(Extents, Orientation, Origin, Profile1, Profile2)


class ICpfcScreenTransform(object):
    com = 'pfcls.pfcScreenTransform.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, PanX=None, PanY=None, Zoom=None):
        return cls.disp.Create(PanX, PanY, Zoom)


class ICpfcSelectionOptions(object):
    com = 'pfcls.pfcSelectionOptions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inOptionKeywords=None):
        return cls.disp.Create(inOptionKeywords)


class ICpfcSelectionPair(object):
    com = 'pfcls.pfcSelectionPair.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inSel1=None, inSel2=None):
        return cls.disp.Create(inSel1, inSel2)


class ICpfcShrinkwrapFacetedPartInstructions(object):
    com = 'pfcls.pfcShrinkwrapFacetedPartInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, OutputModel=None, Lightweight=None):
        return cls.disp.Create(OutputModel, Lightweight)


class ICpfcShrinkwrapMergedSolidInstructions(object):
    com = 'pfcls.pfcShrinkwrapMergedSolidInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, OutputModel=None):
        return cls.disp.Create(OutputModel)


class ICpfcShrinkwrapSTLInstructions(object):
    com = 'pfcls.pfcShrinkwrapSTLInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, OutputFile=None):
        return cls.disp.Create(OutputFile)


class ICpfcShrinkwrapSurfaceSubsetInstructions(object):
    com = 'pfcls.pfcShrinkwrapSurfaceSubsetInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, OutputModel=None):
        return cls.disp.Create(OutputModel)


class ICpfcShrinkwrapVRMLInstructions(object):
    com = 'pfcls.pfcShrinkwrapVRMLInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, OutputFile=None):
        return cls.disp.Create(OutputFile)


class ICpfcSimpRepAutomatic(object):
    com = 'pfcls.pfcSimpRepAutomatic.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcSimpRepBoundBox(object):
    com = 'pfcls.pfcSimpRepBoundBox.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcSimpRepCompItemPath(object):
    com = 'pfcls.pfcSimpRepCompItemPath.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, ItemPath=None):
        return cls.disp.Create(ItemPath)


class ICpfcSimpRepDefaultEnvelope(object):
    com = 'pfcls.pfcSimpRepDefaultEnvelope.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcSimpRepExclude(object):
    com = 'pfcls.pfcSimpRepExclude.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcSimpRepFeatItemPath(object):
    com = 'pfcls.pfcSimpRepFeatItemPath.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FeatId=None):
        return cls.disp.Create(FeatId)


class ICpfcSimpRepGeom(object):
    com = 'pfcls.pfcSimpRepGeom.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcSimpRepGraphics(object):
    com = 'pfcls.pfcSimpRepGraphics.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcSimpRepInclude(object):
    com = 'pfcls.pfcSimpRepInclude.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcSimpRepItem(object):
    com = 'pfcls.pfcSimpRepItem.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, ItemPath=None):
        return cls.disp.Create(ItemPath)


class ICpfcSimpRepLightWeightGraphics(object):
    com = 'pfcls.pfcSimpRepLightWeightGraphics.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcSimpRepNone(object):
    com = 'pfcls.pfcSimpRepNone.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcSimpRepReverse(object):
    com = 'pfcls.pfcSimpRepReverse.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcSimpRepSubstitute(object):
    com = 'pfcls.pfcSimpRepSubstitute.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, SubsData=None):
        return cls.disp.Create(SubsData)


class ICpfcSimpRepSymb(object):
    com = 'pfcls.pfcSimpRepSymb.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcSliceExportData(object):
    com = 'pfcls.pfcSliceExportData.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, CompIds=None):
        return cls.disp.Create(CompIds)


class ICpfcSphericalSplineSurfaceDescriptor(object):
    com = 'pfcls.pfcSphericalSplineSurfaceDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Extents=None, Orientation=None, Origin=None, SplineSurfaceData=None):
        return cls.disp.Create(Extents, Orientation, Origin, SplineSurfaceData)


class ICpfcSplineDescriptor(object):
    com = 'pfcls.pfcSplineDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Points=None):
        return cls.disp.Create(Points)


class ICpfcSplinePoint(object):
    com = 'pfcls.pfcSplinePoint.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Parameter=None, Point=None, Tangent=None):
        return cls.disp.Create(Parameter, Point, Tangent)


class ICpfcSplinePointDimensionSense(object):
    com = 'pfcls.pfcSplinePointDimensionSense.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, SplinePointIndex=None):
        return cls.disp.Create(SplinePointIndex)


class ICpfcSplinePointDimSense(object):
    com = 'pfcls.pfcSplinePointDimSense.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, SplinePointIndex=None):
        return cls.disp.Create(SplinePointIndex)


class ICpfcSplineSurfaceDescriptor(object):
    com = 'pfcls.pfcSplineSurfaceDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Extents=None, Orientation=None, Points=None):
        return cls.disp.Create(Extents, Orientation, Points)


class ICpfcSplineSurfacePoint(object):
    com = 'pfcls.pfcSplineSurfacePoint.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Parameter=None, Point=None, UTangent=None, VTangent=None, UVDerivative=None):
        return cls.disp.Create(Parameter, Point, UTangent, VTangent, UVDerivative)


class ICpfcSpoolImportInstructions(object):
    com = 'pfcls.pfcSpoolImportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcSTEP2DExportInstructions(object):
    com = 'pfcls.pfcSTEP2DExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcSTEP3DExportInstructions(object):
    com = 'pfcls.pfcSTEP3DExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inConfiguration=None, inGeometry=None):
        return cls.disp.Create(inConfiguration, inGeometry)


class ICpfcSTEPExportInstructions(object):
    com = 'pfcls.pfcSTEPExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Flags=None):
        return cls.disp.Create(Flags)


class ICpfcSTEPImport2DInstructions(object):
    com = 'pfcls.pfcSTEPImport2DInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcSTLASCIIExportInstructions(object):
    com = 'pfcls.pfcSTLASCIIExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, CsysName=None):
        return cls.disp.Create(CsysName)


class ICpfcSTLBinaryExportInstructions(object):
    com = 'pfcls.pfcSTLBinaryExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, CsysName=None):
        return cls.disp.Create(CsysName)


class ICpfcSubstAsmRep(object):
    com = 'pfcls.pfcSubstAsmRep.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, SubstPath=None, AsmRep=None):
        return cls.disp.Create(SubstPath, AsmRep)


class ICpfcSubstEnvelope(object):
    com = 'pfcls.pfcSubstEnvelope.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, SubstPath=None):
        return cls.disp.Create(SubstPath)


class ICpfcSubstInterchg(object):
    com = 'pfcls.pfcSubstInterchg.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, SubstPath=None):
        return cls.disp.Create(SubstPath)


class ICpfcSubstPrtRep(object):
    com = 'pfcls.pfcSubstPrtRep.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, SubstPath=None, PartRep=None):
        return cls.disp.Create(SubstPath, PartRep)


class ICpfcSurfaceExtents(object):
    com = 'pfcls.pfcSurfaceExtents.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, UVExtents=None):
        return cls.disp.Create(UVExtents)


class ICpfcSWAsm3DExportInstructions(object):
    com = 'pfcls.pfcSWAsm3DExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inConfiguration=None, inGeometry=None):
        return cls.disp.Create(inConfiguration, inGeometry)


class ICpfcSWPart3DExportInstructions(object):
    com = 'pfcls.pfcSWPart3DExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inConfiguration=None, inGeometry=None):
        return cls.disp.Create(inConfiguration, inGeometry)


class ICpfcSymbolDefAttachment(object):
    com = 'pfcls.pfcSymbolDefAttachment.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inType=None, inAttachmentPoint=None):
        return cls.disp.Create(inType, inAttachmentPoint)


class ICpfcTableCell(object):
    com = 'pfcls.pfcTableCell.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, RowNumber=None, ColumnNumber=None):
        return cls.disp.Create(RowNumber, ColumnNumber)


class ICpfcTableCreateInstructions(object):
    com = 'pfcls.pfcTableCreateInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Origin=None):
        return cls.disp.Create(Origin)


class ICpfcTableRetrieveInstructions(object):
    com = 'pfcls.pfcTableRetrieveInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, FileName=None, Position=None):
        return cls.disp.Create(FileName, Position)


class ICpfcTabulatedCylinderDescriptor(object):
    com = 'pfcls.pfcTabulatedCylinderDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Extents=None, Orientation=None, Origin=None, Profile=None):
        return cls.disp.Create(Extents, Orientation, Origin, Profile)


class ICpfcTangentIndexDimensionSense(object):
    com = 'pfcls.pfcTangentIndexDimensionSense.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, TangentIndex=None):
        return cls.disp.Create(TangentIndex)


class ICpfcTangentIndexDimSense(object):
    com = 'pfcls.pfcTangentIndexDimSense.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, TangentIndex=None):
        return cls.disp.Create(TangentIndex)


class ICpfcTangentLeaderAttachment(object):
    com = 'pfcls.pfcTangentLeaderAttachment.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, LeaderAttachs=None):
        return cls.disp.Create(LeaderAttachs)


class ICpfcTextDescriptor(object):
    com = 'pfcls.pfcTextDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, TextString=None, Point=None, Style=None):
        return cls.disp.Create(TextString, Point, Style)


class ICpfcTextReference(object):
    com = 'pfcls.pfcTextReference.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inTextRefNote=None, inTextLineNumber=None, inTextIndexNumber=None):
        return cls.disp.Create(inTextRefNote, inTextLineNumber, inTextIndexNumber)


class ICpfcTextStyle(object):
    com = 'pfcls.pfcTextStyle.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcTIFFImageExportInstructions(object):
    com = 'pfcls.pfcTIFFImageExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, ImageWidth=None, ImageHeight=None):
        return cls.disp.Create(ImageWidth, ImageHeight)


class ICpfcTorusDescriptor(object):
    com = 'pfcls.pfcTorusDescriptor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Extents=None, Orientation=None, Origin=None, Radius1=None, Radius2=None):
        return cls.disp.Create(Extents, Orientation, Origin, Radius1, Radius2)


class ICpfcTransform3D(object):
    com = 'pfcls.pfcTransform3D.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Matrix=None):
        return cls.disp.Create(Matrix)


class ICpfcTriangulationInstructions(object):
    com = 'pfcls.pfcTriangulationInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, AngleControl=None, ChordHeight=None):
        return cls.disp.Create(AngleControl, ChordHeight)


class ICpfcUDFAssemblyIntersection(object):
    com = 'pfcls.pfcUDFAssemblyIntersection.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, ComponentPath=None, VisibilityLevel=None):
        return cls.disp.Create(ComponentPath, VisibilityLevel)


class ICpfcUDFCustomCreateInstructions(object):
    com = 'pfcls.pfcUDFCustomCreateInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Name=None):
        return cls.disp.Create(Name)


class ICpfcUDFExternalReference(object):
    com = 'pfcls.pfcUDFExternalReference.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Model=None):
        return cls.disp.Create(Model)


class ICpfcUDFPromptCreateInstructions(object):
    com = 'pfcls.pfcUDFPromptCreateInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Name=None):
        return cls.disp.Create(Name)


class ICpfcUDFReference(object):
    com = 'pfcls.pfcUDFReference.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, PromptForReference=None, ReferenceItem=None):
        return cls.disp.Create(PromptForReference, ReferenceItem)


class ICpfcUDFVariantDimension(object):
    com = 'pfcls.pfcUDFVariantDimension.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Name=None, DimensionValue=None):
        return cls.disp.Create(Name, DimensionValue)


class ICpfcUDFVariantPatternParam(object):
    com = 'pfcls.pfcUDFVariantPatternParam.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Name=None, PatternParam=None):
        return cls.disp.Create(Name, PatternParam)


class ICpfcUG3DExportInstructions(object):
    com = 'pfcls.pfcUG3DExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inConfiguration=None, inGeometry=None):
        return cls.disp.Create(inConfiguration, inGeometry)


class ICpfcUnitConversionFactor(object):
    com = 'pfcls.pfcUnitConversionFactor.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Scale=None):
        return cls.disp.Create(Scale)


class ICpfcUnitConversionOptions(object):
    com = 'pfcls.pfcUnitConversionOptions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, DimensionOption=None):
        return cls.disp.Create(DimensionOption)


class ICpfcUploadOptions(object):
    com = 'pfcls.pfcUploadOptions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcVDA3DExportInstructions(object):
    com = 'pfcls.pfcVDA3DExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, inConfiguration=None, inGeometry=None):
        return cls.disp.Create(inConfiguration, inGeometry)


class ICpfcVDAExportInstructions(object):
    com = 'pfcls.pfcVDAExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Flags=None):
        return cls.disp.Create(Flags)


class ICpfcViewDisplay(object):
    com = 'pfcls.pfcViewDisplay.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Style=None, TangentStyle=None, CableStyle=None, RemoveQuiltHiddenLines=None, ShowConceptModel=None,
               ShowWeldXSection=None):
        return cls.disp.Create(Style, TangentStyle, CableStyle, RemoveQuiltHiddenLines, ShowConceptModel,
                               ShowWeldXSection)


class ICpfcViewOId(object):
    com = 'pfcls.pfcViewOId.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Name=None):
        return cls.disp.Create(Name)


class ICpfcVRMLDirectExportInstructions(object):
    com = 'pfcls.pfcVRMLDirectExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, OutputFile=None, InputFile=None):
        return cls.disp.Create(OutputFile, InputFile)


class ICpfcVRMLModelExportInstructions(object):
    com = 'pfcls.pfcVRMLModelExportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, OutputFile=None):
        return cls.disp.Create(OutputFile)


class ICpfcWindowOId(object):
    com = 'pfcls.pfcWindowOId.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, Id=None):
        return cls.disp.Create(Id)


class ICpfcWireListImportInstructions(object):
    com = 'pfcls.pfcWireListImportInstructions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class ICpfcWorkspaceDefinition(object):
    com = 'pfcls.pfcWorkspaceDefinition.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls, WorkspaceName=None, WorkspaceContext=None):
        return cls.disp.Create(WorkspaceName, WorkspaceContext)


class ICpfcWSExportOptions(object):
    com = 'pfcls.pfcWSExportOptions.1'
    disp = Dispatch(com)

    @classmethod
    def Create(cls):
        return cls.disp.Create()


class IMpfcArgument(object):
    com = 'pfcls.MpfcArgument.1'
    disp = Dispatch(com)

    @classmethod
    def CreateASCIIStringArgValue(cls, value=None):
        return cls.disp.CreateASCIIStringArgValue(value)

    @classmethod
    def CreateBoolArgValue(cls, value=None):
        return cls.disp.CreateBoolArgValue(value)

    @classmethod
    def CreateDoubleArgValue(cls, value=None):
        return cls.disp.CreateDoubleArgValue(value)

    @classmethod
    def CreateIntArgValue(cls, value=None):
        return cls.disp.CreateIntArgValue(value)

    @classmethod
    def CreateSelectionArgValue(cls, value=None):
        return cls.disp.CreateSelectionArgValue(value)

    @classmethod
    def CreateStringArgValue(cls, value=None):
        return cls.disp.CreateStringArgValue(value)

    @classmethod
    def CreateTransformArgValue(cls, value=None):
        return cls.disp.CreateTransformArgValue(value)

    @classmethod
    def FindArgumentByLabel(cls, ArgList=None, Label=None):
        return cls.disp.FindArgumentByLabel(ArgList, Label)


class IMpfcAssembly(object):
    com = 'pfcls.MpfcAssembly.1'
    disp = Dispatch(com)

    @classmethod
    def CreateComponentPath(cls, Root=None, Ids=None):
        return cls.disp.CreateComponentPath(Root, Ids)


class IMpfcExternal(object):
    com = 'pfcls.MpfcExternal.1'
    disp = Dispatch(com)

    @classmethod
    def CreateDoubleExternalData(cls, value=None):
        return cls.disp.CreateDoubleExternalData(value)

    @classmethod
    def CreateIntExternalData(cls, value=None):
        return cls.disp.CreateIntExternalData(value)

    @classmethod
    def CreateStringExternalData(cls, value=None):
        return cls.disp.CreateStringExternalData(value)


class IMpfcInterference(object):
    com = 'pfcls.MpfcInterference.1'
    disp = Dispatch(com)

    @classmethod
    def CreateGlobalEvaluator(cls, Assem=None):
        return cls.disp.CreateGlobalEvaluator(Assem)

    @classmethod
    def CreateSelectionEvaluator(cls, Selections=None):
        return cls.disp.CreateSelectionEvaluator(Selections)


class IMpfcModelItem(object):
    com = 'pfcls.MpfcModelItem.1'
    disp = Dispatch(com)

    @classmethod
    def CreateBoolParamValue(cls, value=None):
        return cls.disp.CreateBoolParamValue(value)

    @classmethod
    def CreateDoubleParamValue(cls, value=None):
        return cls.disp.CreateDoubleParamValue(value)

    @classmethod
    def CreateIntParamValue(cls, value=None):
        return cls.disp.CreateIntParamValue(value)

    @classmethod
    def CreateNoteParamValue(cls, value=None):
        return cls.disp.CreateNoteParamValue(value)

    @classmethod
    def CreateStringParamValue(cls, value=None):
        return cls.disp.CreateStringParamValue(value)


class IMpfcSelect(object):
    com = 'pfcls.MpfcSelect.1'
    disp = Dispatch(com)

    @classmethod
    def CreateComponentSelection(cls, Path=None):
        return cls.disp.CreateComponentSelection(Path)

    @classmethod
    def CreateModelItemSelection(cls, SelItem=None, Path=None):
        return cls.disp.CreateModelItemSelection(SelItem, Path)

    @classmethod
    def CreateModelSelection(cls, Model=None):
        return cls.disp.CreateModelSelection(Model)

    @classmethod
    def CreateSelectionFromString(cls, SelectionString=None):
        return cls.disp.CreateSelectionFromString(SelectionString)


class IMpfcSession(object):
    com = 'pfcls.MpfcSession.1'
    disp = Dispatch(com)

    @classmethod
    def GetCurrentSession(cls):
        return cls.disp.GetCurrentSession()

    @classmethod
    def GetCurrentSessionWithCompatibility(cls, Compatibility=None):
        return cls.disp.GetCurrentSessionWithCompatibility(Compatibility)

    @classmethod
    def SetSessionUndo(cls, setFlag=None):
        return cls.disp.SetSessionUndo(setFlag)


class IpfcActionListeners(object):
    com = 'pfcls.pfcActionListeners.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcActionSources(object):
    com = 'pfcls.pfcActionSources.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcActionTypes(object):
    com = 'pfcls.pfcActionTypes.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcArguments(object):
    com = 'pfcls.pfcArguments.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcAttachments(object):
    com = 'pfcls.pfcAttachments.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcBaseDimensions(object):
    com = 'pfcls.pfcBaseDimensions.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcBSplinePoints(object):
    com = 'pfcls.pfcBSplinePoints.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcColumnCreateOptions(object):
    com = 'pfcls.pfcColumnCreateOptions.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcComponentConstraints(object):
    com = 'pfcls.pfcComponentConstraints.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcComponentFeats(object):
    com = 'pfcls.pfcComponentFeats.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcComponentPaths(object):
    com = 'pfcls.pfcComponentPaths.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcContours(object):
    com = 'pfcls.pfcContours.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcCoonsCornerPoints(object):
    com = 'pfcls.pfcCoonsCornerPoints.1'
    disp = Dispatch(com)

    @classmethod
    def Item(cls, IIndex0=None, IIndex1=None):
        return cls.disp.Item(IIndex0, IIndex1)

    @classmethod
    def Set(cls, IIndex0=None, IIndex1=None, Item=None):
        return cls.disp.Set(IIndex0, IIndex1, Item)


class IpfcCoonsUVDerivatives(object):
    com = 'pfcls.pfcCoonsUVDerivatives.1'
    disp = Dispatch(com)

    @classmethod
    def Item(cls, IIndex0=None, IIndex1=None):
        return cls.disp.Item(IIndex0, IIndex1)

    @classmethod
    def Set(cls, IIndex0=None, IIndex1=None, Item=None):
        return cls.disp.Set(IIndex0, IIndex1, Item)


class IpfcCurveDescriptors(object):
    com = 'pfcls.pfcCurveDescriptors.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcCurves(object):
    com = 'pfcls.pfcCurves.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDatumAxisConstraints(object):
    com = 'pfcls.pfcDatumAxisConstraints.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDatumAxisDimensionConstraints(object):
    com = 'pfcls.pfcDatumAxisDimensionConstraints.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDatumCsysDimensionConstraints(object):
    com = 'pfcls.pfcDatumCsysDimensionConstraints.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDatumCsysOrientMoveConstraints(object):
    com = 'pfcls.pfcDatumCsysOrientMoveConstraints.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDatumCsysOriginConstraints(object):
    com = 'pfcls.pfcDatumCsysOriginConstraints.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDatumPlaneConstraints(object):
    com = 'pfcls.pfcDatumPlaneConstraints.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDatumPointDimensionConstraints(object):
    com = 'pfcls.pfcDatumPointDimensionConstraints.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDatumPointPlacementConstraints(object):
    com = 'pfcls.pfcDatumPointPlacementConstraints.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDependencies(object):
    com = 'pfcls.pfcDependencies.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDetailItems(object):
    com = 'pfcls.pfcDetailItems.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDetailLeaderAttachments(object):
    com = 'pfcls.pfcDetailLeaderAttachments.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDetailSymbolDefItems(object):
    com = 'pfcls.pfcDetailSymbolDefItems.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDetailSymbolGroups(object):
    com = 'pfcls.pfcDetailSymbolGroups.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDetailSymbolInstItems(object):
    com = 'pfcls.pfcDetailSymbolInstItems.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDetailTextLines(object):
    com = 'pfcls.pfcDetailTextLines.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDetailTexts(object):
    com = 'pfcls.pfcDetailTexts.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDetailVariantTexts(object):
    com = 'pfcls.pfcDetailVariantTexts.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDimension2Ds(object):
    com = 'pfcls.pfcDimension2Ds.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDimensionAttachment(object):
    com = 'pfcls.pfcDimensionAttachment.1'
    disp = Dispatch(com)

    @classmethod
    def Item(cls, IIndex0=None):
        return cls.disp.Item(IIndex0)

    @classmethod
    def Set(cls, IIndex0=None, Item=None):
        return cls.disp.Set(IIndex0, Item)


class IpfcDimensionAttachments(object):
    com = 'pfcls.pfcDimensionAttachments.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDimensions(object):
    com = 'pfcls.pfcDimensions.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDimensionSenses(object):
    com = 'pfcls.pfcDimensionSenses.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDimSenses(object):
    com = 'pfcls.pfcDimSenses.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDisplayStatuses(object):
    com = 'pfcls.pfcDisplayStatuses.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDrawingCreateErrors(object):
    com = 'pfcls.pfcDrawingCreateErrors.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcDrawingCreateOptions(object):
    com = 'pfcls.pfcDrawingCreateOptions.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcEdges(object):
    com = 'pfcls.pfcEdges.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcEnvelope2D(object):
    com = 'pfcls.pfcEnvelope2D.1'
    disp = Dispatch(com)

    @classmethod
    def Item(cls, IIndex0=None):
        return cls.disp.Item(IIndex0)

    @classmethod
    def Set(cls, IIndex0=None, Item=None):
        return cls.disp.Set(IIndex0, Item)


class IpfcExternalDataClasses(object):
    com = 'pfcls.pfcExternalDataClasses.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcExternalDataSlots(object):
    com = 'pfcls.pfcExternalDataSlots.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcFacetControlFlags(object):
    com = 'pfcls.pfcFacetControlFlags.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcFamilyTableColumns(object):
    com = 'pfcls.pfcFamilyTableColumns.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcFamilyTableRows(object):
    com = 'pfcls.pfcFamilyTableRows.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcFeatureGroups(object):
    com = 'pfcls.pfcFeatureGroups.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcFeatureOperations(object):
    com = 'pfcls.pfcFeatureOperations.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcFeatures(object):
    com = 'pfcls.pfcFeatures.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcFileOpenShortcuts(object):
    com = 'pfcls.pfcFileOpenShortcuts.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcFolderAssignments(object):
    com = 'pfcls.pfcFolderAssignments.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcGeneralDatumPoints(object):
    com = 'pfcls.pfcGeneralDatumPoints.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcGlobalInterferences(object):
    com = 'pfcls.pfcGlobalInterferences.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcInertia(object):
    com = 'pfcls.pfcInertia.1'
    disp = Dispatch(com)

    @classmethod
    def Item(cls, IIndex0=None, IIndex1=None):
        return cls.disp.Item(IIndex0, IIndex1)

    @classmethod
    def Set(cls, IIndex0=None, IIndex1=None, Item=None):
        return cls.disp.Set(IIndex0, IIndex1, Item)


class IpfcLayers(object):
    com = 'pfcls.pfcLayers.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcLeaderAttachments(object):
    com = 'pfcls.pfcLeaderAttachments.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcMaterials(object):
    com = 'pfcls.pfcMaterials.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcMatrix3D(object):
    com = 'pfcls.pfcMatrix3D.1'
    disp = Dispatch(com)

    def __new__(cls, *args, **kwargs):
        return cls.disp

    @classmethod
    def Item(cls, IIndex0=None, IIndex1=None):
        return cls.disp.Item(IIndex0, IIndex1)

    @classmethod
    def Set(cls, IIndex0=None, IIndex1=None, Item=None):
        return cls.disp.Set(IIndex0, IIndex1, Item)


class IpfcMessageButtons(object):
    com = 'pfcls.pfcMessageButtons.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcModelDescriptors(object):
    com = 'pfcls.pfcModelDescriptors.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcModelItems(object):
    com = 'pfcls.pfcModelItems.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcModelItemTypes(object):
    com = 'pfcls.pfcModelItemTypes.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcModels(object):
    com = 'pfcls.pfcModels.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcOutline2D(object):
    com = 'pfcls.pfcOutline2D.1'
    disp = Dispatch(com)

    @classmethod
    def Item(cls, IIndex0=None):
        return cls.disp.Item(IIndex0)

    @classmethod
    def Set(cls, IIndex0=None, Item=None):
        return cls.disp.Set(IIndex0, Item)


class IpfcOutline3D(object):
    com = 'pfcls.pfcOutline3D.1'
    disp = Dispatch(com)

    @classmethod
    def Item(cls, IIndex0=None):
        return cls.disp.Item(IIndex0)

    @classmethod
    def Set(cls, IIndex0=None, Item=None):
        return cls.disp.Set(IIndex0, Item)


class IpfcParameters(object):
    com = 'pfcls.pfcParameters.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcParameterSelectionContexts(object):
    com = 'pfcls.pfcParameterSelectionContexts.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcParamValues(object):
    com = 'pfcls.pfcParamValues.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcPDFOptions(object):
    com = 'pfcls.pfcPDFOptions.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcPoint2D(object):
    com = 'pfcls.pfcPoint2D.1'
    disp = Dispatch(com)

    @classmethod
    def Item(cls, IIndex0=None):
        return cls.disp.Item(IIndex0)

    @classmethod
    def Set(cls, IIndex0=None, Item=None):
        return cls.disp.Set(IIndex0, Item)


class IpfcPoint2Ds(object):
    com = 'pfcls.pfcPoint2Ds.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class baseclass(type):
    com = ''

    def __new__(cls, *args, **kwargs):
        cls.com = args[2]['com']
        return Dispatch(cls.com)

    def __get__(self, instance, owner):


class IpfcPoint3D(DispatchBaseClass, metaclass=baseclass):
    com = 'pfcls.pfcPoint3D.1'

    def __new__(cls, *args, **kwargs):
        return super().__new__(cls)

    @classmethod
    def Item(cls, IIndex0=None):
        return cls.disp.Item(IIndex0)

    @classmethod
    def Set(cls, IIndex0=None, Item=None):
        return cls.Set(IIndex0, Item)


class IpfcPoint3Ds(object):
    com = 'pfcls.pfcPoint3Ds.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcPrincipalAxes(object):
    com = 'pfcls.pfcPrincipalAxes.1'
    disp = Dispatch(com)

    @classmethod
    def Item(cls, IIndex0=None):
        return cls.disp.Item(IIndex0)

    @classmethod
    def Set(cls, IIndex0=None, Item=None):
        return cls.disp.Set(IIndex0, Item)


class IpfcRelationFunctionArguments(object):
    com = 'pfcls.pfcRelationFunctionArguments.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcSelections(object):
    com = 'pfcls.pfcSelections.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcServers(object):
    com = 'pfcls.pfcServers.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcSimpRepItems(object):
    com = 'pfcls.pfcSimpRepItems.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcSimpReps(object):
    com = 'pfcls.pfcSimpReps.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcSplinePoints(object):
    com = 'pfcls.pfcSplinePoints.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcSplineSurfacePoints(object):
    com = 'pfcls.pfcSplineSurfacePoints.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcStdColors(object):
    com = 'pfcls.pfcStdColors.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcSurfaceDescriptors(object):
    com = 'pfcls.pfcSurfaceDescriptors.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcSurfaces(object):
    com = 'pfcls.pfcSurfaces.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcSymbolDefAttachments(object):
    com = 'pfcls.pfcSymbolDefAttachments.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcTables(object):
    com = 'pfcls.pfcTables.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcTransform3Ds(object):
    com = 'pfcls.pfcTransform3Ds.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcUDFAssemblyIntersections(object):
    com = 'pfcls.pfcUDFAssemblyIntersections.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcUDFDimensions(object):
    com = 'pfcls.pfcUDFDimensions.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcUDFExternalReferences(object):
    com = 'pfcls.pfcUDFExternalReferences.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcUDFOrientations(object):
    com = 'pfcls.pfcUDFOrientations.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcUDFReferences(object):
    com = 'pfcls.pfcUDFReferences.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcUDFVariantValues(object):
    com = 'pfcls.pfcUDFVariantValues.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcUnits(object):
    com = 'pfcls.pfcUnits.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcUnitSystems(object):
    com = 'pfcls.pfcUnitSystems.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcUVOutline(object):
    com = 'pfcls.pfcUVOutline.1'
    disp = Dispatch(com)

    @classmethod
    def Item(cls, IIndex0=None):
        return cls.disp.Item(IIndex0)

    @classmethod
    def Set(cls, IIndex0=None, Item=None):
        return cls.disp.Set(IIndex0, Item)


class IpfcUVParams(object):
    com = 'pfcls.pfcUVParams.1'
    disp = Dispatch(com)

    @classmethod
    def Item(cls, IIndex0=None):
        return cls.disp.Item(IIndex0)

    @classmethod
    def Set(cls, IIndex0=None, Item=None):
        return cls.disp.Set(IIndex0, Item)


class IpfcUVParamsSequence(object):
    com = 'pfcls.pfcUVParamsSequence.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcUVVector(object):
    com = 'pfcls.pfcUVVector.1'
    disp = Dispatch(com)

    @classmethod
    def Item(cls, IIndex0=None):
        return cls.disp.Item(IIndex0)

    @classmethod
    def Set(cls, IIndex0=None, Item=None):
        return cls.disp.Set(IIndex0, Item)


class IpfcVector2D(object):
    com = 'pfcls.pfcVector2D.1'
    disp = Dispatch(com)

    @classmethod
    def Item(cls, IIndex0=None):
        return cls.disp.Item(IIndex0)

    @classmethod
    def Set(cls, IIndex0=None, Item=None):
        return cls.disp.Set(IIndex0, Item)


class IpfcVector3D(object):
    com = 'pfcls.pfcVector3D.1'
    disp = Dispatch(com)

    @classmethod
    def Item(cls, IIndex0=None):
        return cls.disp.Item(IIndex0)

    @classmethod
    def Set(cls, IIndex0=None, Item=None):
        return cls.disp.Set(IIndex0, Item)


class IpfcVector3Ds(object):
    com = 'pfcls.pfcVector3Ds.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcView2Ds(object):
    com = 'pfcls.pfcView2Ds.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcViews(object):
    com = 'pfcls.pfcViews.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcWindows(object):
    com = 'pfcls.pfcWindows.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcWorkspaceDefinitions(object):
    com = 'pfcls.pfcWorkspaceDefinitions.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcWSImportExportMessages(object):
    com = 'pfcls.pfcWSImportExportMessages.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class IpfcXSections(object):
    com = 'pfcls.pfcXSections.1'
    disp = Dispatch(com)
    Count = disp.Count

    @classmethod
    def Append(cls, item=None):
        return cls.disp.Append(item)

    @classmethod
    def Clear(cls):
        return cls.disp.Clear()

    @classmethod
    def Insert(cls, lIndex=None, item=None):
        return cls.disp.Insert(lIndex, item)

    @classmethod
    def InsertSeq(cls, lAtIndex=None, pSeq=None):
        return cls.disp.InsertSeq(lAtIndex, pSeq)

    @classmethod
    def Item(cls, lIndex=None):
        return cls.disp.Item(lIndex)

    @classmethod
    def Remove(cls, lFromIncl=None, lToExcl=None):
        return cls.disp.Remove(lFromIncl, lToExcl)

    @classmethod
    def Set(cls, lIndex=None, item=None):
        return cls.disp.Set(lIndex, item)


class ICpfcXCancelProEAction(object):
    com = 'pfcls.pfcXCancelProEAction.1'
    disp = Dispatch(com)

    @classmethod
    def Throw(cls):
        return cls.disp.Throw()


class EpfcStdColor(Enum):
    EpfcCOLOR_BACKGROUND = 3
    EpfcCOLOR_CURVE = 10
    EpfcCOLOR_DATUM = 16
    EpfcCOLOR_DIMMED = 6
    EpfcCOLOR_DRAWING = 2
    EpfcCOLOR_EDGE_HIGHLIGHT = 5
    EpfcCOLOR_ERROR = 7
    EpfcCOLOR_HALF_TONE = 4
    EpfcCOLOR_HIGHLIGHT = 1
    EpfcCOLOR_LETTER = 0
    EpfcCOLOR_LWW = 18
    EpfcCOLOR_MAX = 19
    EpfcCOLOR_PRESEL_HIGHLIGHT = 11
    EpfcCOLOR_PREVIEW = 14
    EpfcCOLOR_QUILT = 17
    EpfcCOLOR_SECONDARY_PREVIEW = 15
    EpfcCOLOR_SECONDARY_SELECTED = 13
    EpfcCOLOR_SELECTED = 12
    EpfcCOLOR_SHADED_EDGE = 20
    EpfcCOLOR_SHEETMETAL = 9
    EpfcCOLOR_SS_CHILD_OF_MODIFIED_IN_DMA = 38
    EpfcCOLOR_SS_CONF_ASM_NODES = 37
    EpfcCOLOR_SS_CURRENT_DESIGN_SOL = 34
    EpfcCOLOR_SS_DRIVEN_BY_PROPROGRAM = 32
    EpfcCOLOR_SS_FAILED_ITEMS = 23
    EpfcCOLOR_SS_FROZEN_COMPONENT = 22
    EpfcCOLOR_SS_FT_GENERICS = 29
    EpfcCOLOR_SS_FT_INSTANCES = 28
    EpfcCOLOR_SS_HAS_FROZEN_FEAT = 21
    EpfcCOLOR_SS_HAS_PROPROGRAM = 30
    EpfcCOLOR_SS_HAS_PROPROGRAM_INPUT = 31
    EpfcCOLOR_SS_INTCH_GROUP_MEMBERS = 26
    EpfcCOLOR_SS_MODULE_NODES = 33
    EpfcCOLOR_SS_NONCURRENT_DESIGN_SOL = 35
    EpfcCOLOR_SS_PACKAGED = 24
    EpfcCOLOR_SS_RECENT_SEARCH = 25
    EpfcCOLOR_SS_REPLACED_COMPS = 27
    EpfcCOLOR_SS_REPRESENTATIVE_DESIGN_SOL = 36
    EpfcCOLOR_WARNING = 8
    EpfcStdColor_nil = 39
    com = 'pfcls.pfcStdColor.1'


class EpfcStdLineStyle(Enum):
    EpfcLINE_CENTERLINE = 2
    EpfcLINE_CTRL_L_L = 6
    EpfcLINE_CTRL_MID_L = 10
    EpfcLINE_CTRL_S_L = 5
    EpfcLINE_CTRL_S_S = 7
    EpfcLINE_DASH = 4
    EpfcLINE_DASH_S_S = 8
    EpfcLINE_DOTTED = 1
    EpfcLINE_INTMIT_LWW_HIDDEN = 11
    EpfcLINE_PDFHIDDEN_LINESTYLE = 12
    EpfcLINE_PHANTOM = 3
    EpfcLINE_PHANTOM_S_S = 9
    EpfcLINE_SOLID = 0
    EpfcStdLineStyle_nil = 13
    com = 'pfcls.pfcStdLineStyle.1'


class EpfcCoordAxis(Enum):
    EpfcCOORD_AXIS_X = 0
    EpfcCOORD_AXIS_Y = 1
    EpfcCOORD_AXIS_Z = 2
    EpfcCoordAxis_nil = 3
    com = 'pfcls.pfcCoordAxis.1'


class EpfcPlacement(Enum):
    EpfcPLACE_INSIDE = 0
    EpfcPLACE_ON_BOUNDARY = 1
    EpfcPLACE_OUTSIDE = 2
    EpfcPlacement_nil = 3
    com = 'pfcls.pfcPlacement.1'


class EpfcActionType(Enum):
    EpfcActionType_nil = 51
    EpfcDIM_MODIFY_VALUE_PRE = 12
    EpfcDIRECTORY_CHANGE_POST = 41
    EpfcFEATURE_COPY_POST = 20
    EpfcFEATURE_CREATE_POST = 37
    EpfcFEATURE_CREATE_PRE = 36
    EpfcFEATURE_DELETE_POST = 38
    EpfcFEATURE_DELETE_PRE = 13
    EpfcFEATURE_REDEFINE_PRE = 19
    EpfcFEATURE_REGEN_FAILURE = 18
    EpfcFEATURE_REGEN_POST = 15
    EpfcFEATURE_REGEN_PRE = 14
    EpfcFEATURE_SUPPRESS_POST = 17
    EpfcFEATURE_SUPPRESS_PRE = 16
    EpfcMDL_CREATE_POST = 33
    EpfcMDL_DELETE_POST = 25
    EpfcMDL_DELETE_POST_ALL = 29
    EpfcMDL_DELETE_PRE = 50
    EpfcMDL_DISPLAY_POST = 43
    EpfcMDL_DISPLAY_PRE = 32
    EpfcMDL_ERASE_PRE = 47
    EpfcMDL_PURGE_POST = 49
    EpfcMDL_PURGE_PRE = 48
    EpfcMODEL_COPY_POST = 21
    EpfcMODEL_COPY_POST_ALL = 22
    EpfcMODEL_COPY_PRE = 45
    EpfcMODEL_ERASE_POST = 24
    EpfcMODEL_ERASE_POST_ALL = 28
    EpfcMODEL_RENAME_POST = 23
    EpfcMODEL_RENAME_POST_ALL = 11
    EpfcMODEL_RENAME_PRE = 46
    EpfcMODEL_REPLACE_POST = 8
    EpfcMODEL_RETRIEVE_POST = 30
    EpfcMODEL_RETRIEVE_POST_ALL = 31
    EpfcMODEL_RETRIEVE_PRE = 9
    EpfcMODEL_SAVE_POST = 26
    EpfcMODEL_SAVE_POST_ALL = 27
    EpfcMODEL_SAVE_PRE = 44
    EpfcMODEL_SAVE_PRE_ALL = 10
    EpfcPARAM_CREATE_PRE = 1
    EpfcPARAM_CREATE_W_UNITS_PRE = 4
    EpfcPARAM_DELETE_PRE = 3
    EpfcPARAM_DELETE_W_UNITS_POST = 7
    EpfcPARAM_MODIFY_PRE = 2
    EpfcPARAM_MODIFY_W_UNITS_POST = 6
    EpfcPARAM_MODIFY_W_UNITS_PRE = 5
    EpfcPOPUPMENU_CREATE_POST = 0
    EpfcSOLID_REGEN_POST = 35
    EpfcSOLID_REGEN_PRE = 34
    EpfcSOLID_UNIT_CONVERT_POST = 40
    EpfcSOLID_UNIT_CONVERT_PRE = 39
    EpfcWINDOW_CHANGE_POST = 42
    com = 'pfcls.pfcActionType.1'


class EpfcUnitType(Enum):
    EpfcUNIT_ANGLE = 5
    EpfcUNIT_FORCE = 2
    EpfcUNIT_LENGTH = 0
    EpfcUNIT_MASS = 1
    EpfcUNIT_TEMPERATURE = 4
    EpfcUNIT_TIME = 3
    EpfcUnitType_nil = 6
    com = 'pfcls.pfcUnitType.1'


class EpfcLengthUnitType(Enum):
    EpfcLENGTHUNIT_CM = 3
    EpfcLENGTHUNIT_FOOT = 1
    EpfcLENGTHUNIT_INCH = 0
    EpfcLENGTHUNIT_KM = 7
    EpfcLENGTHUNIT_M = 4
    EpfcLENGTHUNIT_MCM = 5
    EpfcLENGTHUNIT_MILE = 8
    EpfcLENGTHUNIT_MM = 2
    EpfcLENGTHUNIT_NM = 6
    EpfcLengthUnitType_nil = 9
    com = 'pfcls.pfcLengthUnitType.1'


class EpfcMassUnitType(Enum):
    EpfcMASSUNIT_GRAM = 3
    EpfcMASSUNIT_KILOGRAM = 4
    EpfcMASSUNIT_OUNCE = 0
    EpfcMASSUNIT_POUND = 1
    EpfcMASSUNIT_TON = 2
    EpfcMASSUNIT_TONNE = 5
    EpfcMassUnitType_nil = 6
    com = 'pfcls.pfcMassUnitType.1'


class EpfcDisplayStyle(Enum):
    EpfcDISPSTYLE_DEFAULT = 0
    EpfcDISPSTYLE_FOLLOW_ENVIRONMENT = 5
    EpfcDISPSTYLE_HIDDEN_LINE = 2
    EpfcDISPSTYLE_NO_HIDDEN = 3
    EpfcDISPSTYLE_SHADED = 4
    EpfcDISPSTYLE_SHADED_WITH_EDGES = 6
    EpfcDISPSTYLE_WIREFRAME = 1
    EpfcDisplayStyle_nil = 7
    com = 'pfcls.pfcDisplayStyle.1'


class EpfcTangentEdgeDisplayStyle(Enum):
    EpfcTANEDGE_CENTERLINE = 2
    EpfcTANEDGE_DEFAULT = 0
    EpfcTANEDGE_DIMMED = 4
    EpfcTANEDGE_NONE = 1
    EpfcTANEDGE_PHANTOM = 3
    EpfcTANEDGE_SOLID = 5
    EpfcTangentEdgeDisplayStyle_nil = 6
    com = 'pfcls.pfcTangentEdgeDisplayStyle.1'


class EpfcCableDisplayStyle(Enum):
    EpfcCABLEDISP_CENTERLINE = 1
    EpfcCABLEDISP_DEFAULT = 0
    EpfcCABLEDISP_THICK = 2
    EpfcCableDisplayStyle_nil = 3
    com = 'pfcls.pfcCableDisplayStyle.1'


class EpfcDatumSide(Enum):
    EpfcDATUM_SIDE_NONE = 0
    EpfcDATUM_SIDE_RED = 1
    EpfcDATUM_SIDE_YELLOW = 2
    EpfcDatumSide_nil = 3
    com = 'pfcls.pfcDatumSide.1'


class EpfcDimDisplayMode(Enum):
    EpfcDIM_DISPLAY_NUMERIC = 0
    EpfcDIM_DISPLAY_SYMBOLIC = 1
    EpfcDimDisplayMode_nil = 2
    com = 'pfcls.pfcDimDisplayMode.1'


class EpfcUnitSystemType(Enum):
    EpfcUNIT_SYSTEM_FORCE_LENGTH_TIME = 1
    EpfcUNIT_SYSTEM_MASS_LENGTH_TIME = 0
    EpfcUnitSystemType_nil = 2
    com = 'pfcls.pfcUnitSystemType.1'


class EpfcUnitDimensionConversion(Enum):
    EpfcUNITCONVERT_SAME_DIMS = 0
    EpfcUNITCONVERT_SAME_SIZE = 1
    EpfcUnitDimensionConversion_nil = 2
    com = 'pfcls.pfcUnitDimensionConversion.1'


class EpfcParamValueType(Enum):
    EpfcPARAM_BOOLEAN = 2
    EpfcPARAM_DOUBLE = 3
    EpfcPARAM_INTEGER = 1
    EpfcPARAM_NOTE = 4
    EpfcPARAM_NOT_SET = 6
    EpfcPARAM_STRING = 0
    EpfcPARAM_VOID = 5
    EpfcParamValueType_nil = 7
    com = 'pfcls.pfcParamValueType.1'


class EpfcParameterDriverType(Enum):
    EpfcPARAMDRIVER_FUNCTION = 1
    EpfcPARAMDRIVER_PARAM = 0
    EpfcPARAMDRIVER_RELATION = 2
    EpfcParameterDriverType_nil = 3
    com = 'pfcls.pfcParameterDriverType.1'


class EpfcParameterLimitType(Enum):
    EpfcPARAMLIMIT_GREATER_THAN = 2
    EpfcPARAMLIMIT_GREATER_THAN_OR_EQUAL = 3
    EpfcPARAMLIMIT_LESS_THAN = 0
    EpfcPARAMLIMIT_LESS_THAN_OR_EQUAL = 1
    EpfcPARAMLIMIT_UNLIMITED = 4
    EpfcParameterLimitType_nil = 5
    com = 'pfcls.pfcParameterLimitType.1'


class EpfcParameterSelectionContext(Enum):
    EpfcPARAMSELECT_ALLOW_SUBITEM_SELECTION = 12
    EpfcPARAMSELECT_ASM = 2
    EpfcPARAMSELECT_COMPONENT = 11
    EpfcPARAMSELECT_COMPOSITE_CURVE = 8
    EpfcPARAMSELECT_CURVE = 7
    EpfcPARAMSELECT_EDGE = 4
    EpfcPARAMSELECT_FEATURE = 3
    EpfcPARAMSELECT_INHERITED = 9
    EpfcPARAMSELECT_MODEL = 0
    EpfcPARAMSELECT_PART = 1
    EpfcPARAMSELECT_QUILT = 6
    EpfcPARAMSELECT_SKELETON = 10
    EpfcPARAMSELECT_SURFACE = 5
    EpfcParameterSelectionContext_nil = 13
    com = 'pfcls.pfcParameterSelectionContext.1'


class EpfcRestrictionType(Enum):
    EpfcPARAMSELECT_ENUMERATION = 0
    EpfcPARAMSELECT_RANGE = 1
    EpfcRestrictionType_nil = 2
    com = 'pfcls.pfcRestrictionType.1'


class EpfcModelItemType(Enum):
    EpfcITEM_ANNOTATION_ELEM = 33
    EpfcITEM_ANNOT_PLANE = 32
    EpfcITEM_AXIS = 4
    EpfcITEM_COMBINED_STATE = 27
    EpfcITEM_COORD_SYS = 3
    EpfcITEM_CURVE = 7
    EpfcITEM_DIMENSION = 10
    EpfcITEM_DTL_ENTITY = 15
    EpfcITEM_DTL_GROUP = 17
    EpfcITEM_DTL_NOTE = 16
    EpfcITEM_DTL_OLE_OBJECT = 20
    EpfcITEM_DTL_SYM_DEFINITION = 18
    EpfcITEM_DTL_SYM_INSTANCE = 19
    EpfcITEM_EDGE = 2
    EpfcITEM_EDGE_END = 24
    EpfcITEM_EDGE_START = 22
    EpfcITEM_EXPLODED_STATE = 21
    EpfcITEM_FEATURE = 0
    EpfcITEM_GTOL = 35
    EpfcITEM_LAYER = 8
    EpfcITEM_LAYER_STATE = 26
    EpfcITEM_LOG_EDGE = 23
    EpfcITEM_NOTE = 9
    EpfcITEM_POINT = 5
    EpfcITEM_QUILT = 6
    EpfcITEM_REF_DIMENSION = 11
    EpfcITEM_RP_MATERIAL = 29
    EpfcITEM_SET_DATUM_TAG = 34
    EpfcITEM_SIMPREP = 12
    EpfcITEM_SOLID_GEOMETRY = 13
    EpfcITEM_STYLE_STATE = 28
    EpfcITEM_SURFACE = 1
    EpfcITEM_SURF_FIN = 31
    EpfcITEM_TABLE = 14
    EpfcITEM_VIEW = 30
    EpfcITEM_XSEC = 25
    EpfcModelItemType_nil = 36
    com = 'pfcls.pfcModelItemType.1'


class EpfcDisplayStatus(Enum):
    EpfcDisplayStatus_nil = 4
    EpfcLAYER_BLANK = 2
    EpfcLAYER_DISPLAY = 1
    EpfcLAYER_HIDDEN = 3
    EpfcLAYER_NORMAL = 0
    com = 'pfcls.pfcDisplayStatus.1'


class EpfcExternalDataType(Enum):
    EpfcEXTDATA_CHAPTER = 4
    EpfcEXTDATA_DOUBLE = 1
    EpfcEXTDATA_INTEGER = 0
    EpfcEXTDATA_STREAM = 3
    EpfcEXTDATA_STRING = 2
    EpfcExternalDataType_nil = 5
    com = 'pfcls.pfcExternalDataType.1'


class EpfcDimensionType(Enum):
    EpfcDIM_ANGULAR = 3
    EpfcDIM_DIAMETER = 2
    EpfcDIM_LINEAR = 0
    EpfcDIM_RADIAL = 1
    EpfcDimensionType_nil = 4
    com = 'pfcls.pfcDimensionType.1'


class EpfcDimToleranceType(Enum):
    EpfcDIMTOL_ISODIN = 4
    EpfcDIMTOL_LIMITS = 0
    EpfcDIMTOL_PLUS_MINUS = 1
    EpfcDIMTOL_SYMMETRIC = 2
    EpfcDIMTOL_SYMMETRIC_SUPERSCRIPT = 3
    EpfcDimToleranceType_nil = 5
    com = 'pfcls.pfcDimToleranceType.1'


class EpfcToleranceTableType(Enum):
    EpfcTOLTABLE_BROKEN_EDGE = 1
    EpfcTOLTABLE_GENERAL = 0
    EpfcTOLTABLE_HOLES = 3
    EpfcTOLTABLE_SHAFTS = 2
    EpfcToleranceTableType_nil = 4
    com = 'pfcls.pfcToleranceTableType.1'


class EpfcDimSenseType(Enum):
    EpfcDIMENSION_SENSE_ANGLE = 5
    EpfcDIMENSION_SENSE_LINEAR_TO_ARC_OR_CIRCLE_TANGENT = 4
    EpfcDIMENSION_SENSE_NONE = 0
    EpfcDIMENSION_SENSE_POINT = 1
    EpfcDIMENSION_SENSE_POINT_TO_ANGLE = 6
    EpfcDIMENSION_SENSE_SPLINE_PT = 2
    EpfcDIMENSION_SENSE_TANGENT_INDEX = 3
    EpfcDimSenseType_nil = 7
    com = 'pfcls.pfcDimSenseType.1'


class EpfcDimPointType(Enum):
    EpfcDIMENSION_POINT_CENTER = 2
    EpfcDIMENSION_POINT_END1 = 0
    EpfcDIMENSION_POINT_END2 = 1
    EpfcDIMENSION_POINT_MIDPOINT = 4
    EpfcDIMENSION_POINT_NONE = 3
    EpfcDimPointType_nil = 5
    com = 'pfcls.pfcDimPointType.1'


class EpfcDimLineAOCTangentType(Enum):
    EpfcDimLineAOCTangentType_nil = 4
    EpfcTANGENT_LEFT_OTHER_SIDE = 2
    EpfcTANGENT_LEFT_SAME_SIDE = 0
    EpfcTANGENT_RIGHT_OTHER_SIDE = 3
    EpfcTANGENT_RIGHT_SAME_SIDE = 1
    com = 'pfcls.pfcDimLineAOCTangentType.1'


class EpfcDimOrientationHint(Enum):
    EpfcDimOrientationHint_nil = 10
    EpfcORIENTATION_HINT_ARC_ANGLE = 6
    EpfcORIENTATION_HINT_ARC_LENGTH = 7
    EpfcORIENTATION_HINT_ELLIPSE_RADIUS1 = 4
    EpfcORIENTATION_HINT_ELLIPSE_RADIUS2 = 5
    EpfcORIENTATION_HINT_HORIZONTAL = 1
    EpfcORIENTATION_HINT_LINE_TO_TANGENT_CURVE_ANGLE = 8
    EpfcORIENTATION_HINT_NONE = 0
    EpfcORIENTATION_HINT_RADIAL_DIFF = 9
    EpfcORIENTATION_HINT_SLANTED = 3
    EpfcORIENTATION_HINT_VERTICAL = 2
    com = 'pfcls.pfcDimOrientationHint.1'


class EpfcFeatureStatus(Enum):
    EpfcFEAT_ACTIVE = 0
    EpfcFEAT_FAMILY_TABLE_SUPPRESSED = 2
    EpfcFEAT_INACTIVE = 1
    EpfcFEAT_PROGRAM_SUPPRESSED = 4
    EpfcFEAT_SIMP_REP_SUPPRESSED = 3
    EpfcFEAT_SUPPRESSED = 5
    EpfcFEAT_UNREGENERATED = 6
    EpfcFeatureStatus_nil = 7
    com = 'pfcls.pfcFeatureStatus.1'


class EpfcFeatureType(Enum):
    EpfcFEATTYPE_ACCEPT_CRITERIA = 268
    EpfcFEATTYPE_ANALYSIS = 168
    EpfcFEATTYPE_ANALYT_GEOM = 261
    EpfcFEATTYPE_ANNOTATION = 235
    EpfcFEATTYPE_AREA_NIBBLE = 70
    EpfcFEATTYPE_ARTWORK = 246
    EpfcFEATTYPE_ASSEMBLY_CUT = 37
    EpfcFEATTYPE_ASSEMBLY_CUT_COPY = 145
    EpfcFEATTYPE_ASSY_MERGE = 209
    EpfcFEATTYPE_ASSY_WELD_NOTCH = 215
    EpfcFEATTYPE_ATTACH = 81
    EpfcFEATTYPE_ATTACH_VOLUME = 130
    EpfcFEATTYPE_AUTO_ROUND = 243
    EpfcFEATTYPE_AUXILIARY = 112
    EpfcFEATTYPE_BEAM_SECTION = 151
    EpfcFEATTYPE_BEND = 42
    EpfcFEATTYPE_BEND_BACK = 47
    EpfcFEATTYPE_BLD_OPERATION = 131
    EpfcFEATTYPE_BULK_OBJECT = 116
    EpfcFEATTYPE_CABLE = 67
    EpfcFEATTYPE_CABLE_COSMETIC = 87
    EpfcFEATTYPE_CABLE_LOCATION = 65
    EpfcFEATTYPE_CABLE_SEGMENT = 66
    EpfcFEATTYPE_CAV_DEVIATION = 160
    EpfcFEATTYPE_CAV_FIT = 159
    EpfcFEATTYPE_CAV_SCAN_SET = 158
    EpfcFEATTYPE_CE_COMP = 264
    EpfcFEATTYPE_CE_GEOM = 260
    EpfcFEATTYPE_CE_SKET = 273
    EpfcFEATTYPE_CHAMFER = 4
    EpfcFEATTYPE_CHANNEL = 69
    EpfcFEATTYPE_CMMCONSTR = 156
    EpfcFEATTYPE_CMMMEASURE_STEP = 155
    EpfcFEATTYPE_CMMVERIFY = 157
    EpfcFEATTYPE_COMPONENT = 89
    EpfcFEATTYPE_COMP_INTERFACE = 211
    EpfcFEATTYPE_CONTACT3D = 248
    EpfcFEATTYPE_CONT_MAP = 93
    EpfcFEATTYPE_COORD_SYS = 68
    EpfcFEATTYPE_CORE = 73
    EpfcFEATTYPE_CORNER_CHAMFER = 20
    EpfcFEATTYPE_COSMETIC = 23
    EpfcFEATTYPE_CROSS_SECTION = 82
    EpfcFEATTYPE_CURVE = 39
    EpfcFEATTYPE_CUSTOMIZE = 99
    EpfcFEATTYPE_CUT = 6
    EpfcFEATTYPE_CUT_MOTION = 98
    EpfcFEATTYPE_DAMPER = 241
    EpfcFEATTYPE_DATUM_AXIS = 16
    EpfcFEATTYPE_DATUM_PLANE = 13
    EpfcFEATTYPE_DATUM_POINT = 21
    EpfcFEATTYPE_DATUM_QUILT = 36
    EpfcFEATTYPE_DATUM_SURFACE = 32
    EpfcFEATTYPE_DECLARE = 123
    EpfcFEATTYPE_DEFORM_AREA = 146
    EpfcFEATTYPE_DERIVED_MEMBER = 267
    EpfcFEATTYPE_DESIGNATED_AREA = 244
    EpfcFEATTYPE_DOME = 12
    EpfcFEATTYPE_DOME2 = 19
    EpfcFEATTYPE_DRAFT = 17
    EpfcFEATTYPE_DRAFT_LINE = 127
    EpfcFEATTYPE_DRILL = 30
    EpfcFEATTYPE_DRILL_GROUP = 85
    EpfcFEATTYPE_DRVD = 182
    EpfcFEATTYPE_DRV_TOOL_CURVE = 102
    EpfcFEATTYPE_DRV_TOOL_EDGE = 101
    EpfcFEATTYPE_DRV_TOOL_PROFILE = 165
    EpfcFEATTYPE_DRV_TOOL_SKETCH = 100
    EpfcFEATTYPE_DRV_TOOL_SURF = 103
    EpfcFEATTYPE_DRV_TOOL_TURN = 186
    EpfcFEATTYPE_DRV_TOOL_TWO_CNTR = 120
    EpfcFEATTYPE_DS_OPTIMIZE = 210
    EpfcFEATTYPE_EAR = 11
    EpfcFEATTYPE_EDGE_BEND = 164
    EpfcFEATTYPE_ETCH = 24
    EpfcFEATTYPE_EXPLODE_LINE = 166
    EpfcFEATTYPE_EXP_RATIO = 94
    EpfcFEATTYPE_EXTEND = 78
    EpfcFEATTYPE_EXTRACT = 74
    EpfcFEATTYPE_FIRST = 0
    EpfcFEATTYPE_FIXTURE_SETUP = 91
    EpfcFEATTYPE_FLANGE = 9
    EpfcFEATTYPE_FLATQLT = 185
    EpfcFEATTYPE_FLATTEN = 55
    EpfcFEATTYPE_FLAT_PAT = 92
    EpfcFEATTYPE_FLAT_RIBBON_SEGMENT = 148
    EpfcFEATTYPE_FLEXMOVE = 255
    EpfcFEATTYPE_FLEXSUBST = 258
    EpfcFEATTYPE_FLEX_MUTATOR = 234
    EpfcFEATTYPE_FLXATTACH = 257
    EpfcFEATTYPE_FLX_OGF = 263
    EpfcFEATTYPE_FLX_SOLVER = 272
    EpfcFEATTYPE_FORM = 45
    EpfcFEATTYPE_FREEFORM = 202
    EpfcFEATTYPE_FREE_FORM = 135
    EpfcFEATTYPE_FR_SYS = 198
    EpfcFEATTYPE_GENERAL_MERGE = 240
    EpfcFEATTYPE_GEOM_COPY = 167
    EpfcFEATTYPE_GLOBAL_MODEL = 229
    EpfcFEATTYPE_GRANITE_REMOVE_SRFS = 236
    EpfcFEATTYPE_GRANITE_TAPER_EXTR = 237
    EpfcFEATTYPE_GRANITE_TOOL_COMP = 238
    EpfcFEATTYPE_GRAPH = 52
    EpfcFEATTYPE_GROOVE = 34
    EpfcFEATTYPE_GROUP_HEAD = 181
    EpfcFEATTYPE_HOLE = 1
    EpfcFEATTYPE_HULL_BEAM = 191
    EpfcFEATTYPE_HULL_BRACKET = 196
    EpfcFEATTYPE_HULL_COLLAR = 194
    EpfcFEATTYPE_HULL_COMPT = 199
    EpfcFEATTYPE_HULL_CUTOUT = 189
    EpfcFEATTYPE_HULL_DRAW = 195
    EpfcFEATTYPE_HULL_ENDCUT = 192
    EpfcFEATTYPE_HULL_FOLDED_FLG = 197
    EpfcFEATTYPE_HULL_HOLE = 188
    EpfcFEATTYPE_HULL_MITRE = 230
    EpfcFEATTYPE_HULL_PAD = 227
    EpfcFEATTYPE_HULL_PLATE = 187
    EpfcFEATTYPE_HULL_REP_TMP = 205
    EpfcFEATTYPE_HULL_SLOTCUT = 231
    EpfcFEATTYPE_HULL_SPLIT_BOUND = 224
    EpfcFEATTYPE_HULL_STIFFENER = 190
    EpfcFEATTYPE_HULL_WELD_NOTCH = 217
    EpfcFEATTYPE_HULL_WLD_FLANGE = 193
    EpfcFEATTYPE_IMPORT = 22
    EpfcFEATTYPE_INSULATION = 206
    EpfcFEATTYPE_INTERNAL_UDF = 50
    EpfcFEATTYPE_INTERSECT = 80
    EpfcFEATTYPE_IPMQUILT = 179
    EpfcFEATTYPE_ISEGMENT = 86
    EpfcFEATTYPE_JOIN_WALLS = 271
    EpfcFEATTYPE_KERNEL = 203
    EpfcFEATTYPE_LINE_STOCK = 114
    EpfcFEATTYPE_LIP = 60
    EpfcFEATTYPE_LOC_PUSH = 14
    EpfcFEATTYPE_MANUAL_MILL = 61
    EpfcFEATTYPE_MATERIAL_REMOVAL = 104
    EpfcFEATTYPE_MEASURE = 126
    EpfcFEATTYPE_MERGE = 25
    EpfcFEATTYPE_MFGGATHER = 62
    EpfcFEATTYPE_MFGMERGE = 90
    EpfcFEATTYPE_MFGPTM_MATREM = 265
    EpfcFEATTYPE_MFGREFINE = 75
    EpfcFEATTYPE_MFGTRIM = 63
    EpfcFEATTYPE_MFGUSE_VOLUME = 64
    EpfcFEATTYPE_MILL = 29
    EpfcFEATTYPE_MOD_CHAMFER = 276
    EpfcFEATTYPE_MOD_ROUND = 259
    EpfcFEATTYPE_MOLD = 26
    EpfcFEATTYPE_MOLD_SHUTOFF_SRF = 269
    EpfcFEATTYPE_MOLD_SKIRT_EXT_FEAT = 274
    EpfcFEATTYPE_MOLD_SKIRT_FILL_FEAT = 275
    EpfcFEATTYPE_MOLD_SLIDER = 226
    EpfcFEATTYPE_NECK = 8
    EpfcFEATTYPE_OFFSET = 31
    EpfcFEATTYPE_OLE = 212
    EpfcFEATTYPE_OPERATION = 96
    EpfcFEATTYPE_OPERATION_COMPONENT = 125
    EpfcFEATTYPE_PATCH = 71
    EpfcFEATTYPE_PATTERN = 232
    EpfcFEATTYPE_PATTERN_HEAD = 233
    EpfcFEATTYPE_PIPE = 35
    EpfcFEATTYPE_PIPE_BRANCH = 119
    EpfcFEATTYPE_PIPE_EXTEND = 108
    EpfcFEATTYPE_PIPE_FOLLOW = 110
    EpfcFEATTYPE_PIPE_JOIN = 111
    EpfcFEATTYPE_PIPE_JOINT = 118
    EpfcFEATTYPE_PIPE_LINE = 113
    EpfcFEATTYPE_PIPE_POINT_TO_POINT = 107
    EpfcFEATTYPE_PIPE_SET_START = 106
    EpfcFEATTYPE_PIPE_TRIM = 109
    EpfcFEATTYPE_PLASTIC_RIB = 251
    EpfcFEATTYPE_PLY = 72
    EpfcFEATTYPE_PM_BELT_CURVE = 252
    EpfcFEATTYPE_PM_BUSHING_LD = 256
    EpfcFEATTYPE_POSITION_FOLD = 149
    EpfcFEATTYPE_PROCESS_STEP = 163
    EpfcFEATTYPE_PROTRUSION = 7
    EpfcFEATTYPE_RCG_CHAMFER = 278
    EpfcFEATTYPE_RCG_ROUND = 277
    EpfcFEATTYPE_REFERENCE = 200
    EpfcFEATTYPE_REMOVE_SURFACE = 245
    EpfcFEATTYPE_REMOVE_SURFACES = 128
    EpfcFEATTYPE_REPLACE_SURFACE = 33
    EpfcFEATTYPE_RIB = 10
    EpfcFEATTYPE_RIBBON_CABLE = 129
    EpfcFEATTYPE_RIBBON_EXTEND = 144
    EpfcFEATTYPE_RIBBON_PATH = 143
    EpfcFEATTYPE_RIBBON_SOLID = 147
    EpfcFEATTYPE_RIP = 95
    EpfcFEATTYPE_ROUND = 3
    EpfcFEATTYPE_ROUTE_MANAGER = 216
    EpfcFEATTYPE_ROUTE_PATH = 223
    EpfcFEATTYPE_ROUTE_SPOOL = 228
    EpfcFEATTYPE_RS_GENERATOR = 249
    EpfcFEATTYPE_RS_TRJ_MOVE = 250
    EpfcFEATTYPE_SAW = 27
    EpfcFEATTYPE_SET = 56
    EpfcFEATTYPE_SHAFT = 2
    EpfcFEATTYPE_SHEETMETAL_CLAMP = 162
    EpfcFEATTYPE_SHEETMETAL_CONVERSION = 154
    EpfcFEATTYPE_SHEETMETAL_CUT = 44
    EpfcFEATTYPE_SHEETMETAL_OPTIMIZE = 122
    EpfcFEATTYPE_SHEETMETAL_POPULATE = 124
    EpfcFEATTYPE_SHEETMETAL_PUNCH_POINT = 59
    EpfcFEATTYPE_SHEETMETAL_SHEAR = 142
    EpfcFEATTYPE_SHEETMETAL_ZONE = 161
    EpfcFEATTYPE_SHELL = 18
    EpfcFEATTYPE_SHELL_EXP = 201
    EpfcFEATTYPE_SHRINKAGE = 117
    EpfcFEATTYPE_SHRINK_DIM = 152
    EpfcFEATTYPE_SILHOUETTE_TRIM = 76
    EpfcFEATTYPE_SLDBEND = 184
    EpfcFEATTYPE_SLD_PIP_INSUL = 207
    EpfcFEATTYPE_SLOT = 5
    EpfcFEATTYPE_SMMFGAPPROACH = 176
    EpfcFEATTYPE_SMMFGCOSMETIC = 175
    EpfcFEATTYPE_SMMFGCUT = 54
    EpfcFEATTYPE_SMMFGFORM = 58
    EpfcFEATTYPE_SMMFGMATERIAL_REMOVE = 174
    EpfcFEATTYPE_SMMFGOFFSET = 173
    EpfcFEATTYPE_SMMFGPUNCH = 53
    EpfcFEATTYPE_SMMFGSHAPE = 178
    EpfcFEATTYPE_SMMFGSLOT = 177
    EpfcFEATTYPE_SMM_ETCH = 222
    EpfcFEATTYPE_SMM_GROOVE = 221
    EpfcFEATTYPE_SMM_HOLE = 219
    EpfcFEATTYPE_SMM_NEST = 220
    EpfcFEATTYPE_SMM_SLIT = 218
    EpfcFEATTYPE_SMT_CRN_REL = 183
    EpfcFEATTYPE_SMT_EXTRACT = 208
    EpfcFEATTYPE_SMT_SKETCH_FORM = 270
    EpfcFEATTYPE_SOLIDIFY = 79
    EpfcFEATTYPE_SOLID_PIPE = 115
    EpfcFEATTYPE_SPINAL_BEND = 133
    EpfcFEATTYPE_SPLIT = 77
    EpfcFEATTYPE_SPLIT_SRF_RGN = 266
    EpfcFEATTYPE_SPLIT_SURFACE = 51
    EpfcFEATTYPE_SPOOL = 88
    EpfcFEATTYPE_SPRING = 242
    EpfcFEATTYPE_SPRING_BACK = 150
    EpfcFEATTYPE_STAMPED_AREA = 262
    EpfcFEATTYPE_STYLE = 239
    EpfcFEATTYPE_SUBDIVISION = 254
    EpfcFEATTYPE_SUB_HARNESS = 121
    EpfcFEATTYPE_SUPERQUILT = 225
    EpfcFEATTYPE_SURFACE_MODEL = 40
    EpfcFEATTYPE_TERMINATOR = 213
    EpfcFEATTYPE_THICKEN = 46
    EpfcFEATTYPE_THREAD = 153
    EpfcFEATTYPE_TORUS = 105
    EpfcFEATTYPE_TURN = 28
    EpfcFEATTYPE_TWIST = 134
    EpfcFEATTYPE_UDF = 15
    EpfcFEATTYPE_UDFCLAMP = 84
    EpfcFEATTYPE_UDFNOTCH = 48
    EpfcFEATTYPE_UDFPUNCH = 49
    EpfcFEATTYPE_UDFRMDT = 170
    EpfcFEATTYPE_UDFTHREAD = 38
    EpfcFEATTYPE_UDFWORK_REGION = 132
    EpfcFEATTYPE_UDFZONE = 83
    EpfcFEATTYPE_UNBEND = 43
    EpfcFEATTYPE_UNRCG_ROUND = 279
    EpfcFEATTYPE_USER = 180
    EpfcFEATTYPE_VDA = 57
    EpfcFEATTYPE_VOL_SPLIT = 171
    EpfcFEATTYPE_VPDD = 253
    EpfcFEATTYPE_WALL = 41
    EpfcFEATTYPE_WATER_LINE = 169
    EpfcFEATTYPE_WELDING_ROD = 137
    EpfcFEATTYPE_WELD_COMBINE = 247
    EpfcFEATTYPE_WELD_EDGE_PREP = 172
    EpfcFEATTYPE_WELD_FILLET = 138
    EpfcFEATTYPE_WELD_GROOVE = 139
    EpfcFEATTYPE_WELD_NOTCH = 214
    EpfcFEATTYPE_WELD_PLUG_SLOT = 140
    EpfcFEATTYPE_WELD_PROCESS = 204
    EpfcFEATTYPE_WELD_SPOT = 141
    EpfcFEATTYPE_WORKCELL = 97
    EpfcFEATTYPE_ZONE = 136
    EpfcFEAT_CUSTOM = 282
    EpfcFEAT_CUSTOM_GRANITE = 283
    EpfcFEAT_ECAD_CUTS = 288
    EpfcFEAT_ECAD_CU_AREAS = 281
    EpfcFEAT_EDIT_BEND = 291
    EpfcFEAT_EDIT_BEND_RELIEF = 292
    EpfcFEAT_EDIT_CORNER_RELIEF = 293
    EpfcFEAT_EDIT_CORNER_SEAM = 295
    EpfcFEAT_HULL_BLOCK = 299
    EpfcFEAT_HULL_BLOCK_DEF = 300
    EpfcFEAT_LATTICE = 290
    EpfcFEAT_MOLD_INSERT = 294
    EpfcFEAT_MOVE_COMP = 302
    EpfcFEAT_PART_COMP = 297
    EpfcFEAT_PM_BELT = 301
    EpfcFEAT_PM_MOTOR = 286
    EpfcFEAT_PRTSPLIT = 287
    EpfcFEAT_PULL_WALL = 298
    EpfcFEAT_PUNCH_QUILT = 284
    EpfcFEAT_REFPART_CUTOUT = 285
    EpfcFEAT_SENSOR = 296
    EpfcFEAT_SMT_RECOGNITION = 289
    EpfcFEAT_UNRCG_CHAMFER = 280
    EpfcFeatureType_nil = 303
    com = 'pfcls.pfcFeatureType.1'


class EpfcFeatureCopyType(Enum):
    EpfcFEATCOPY_MIRROR = 2
    EpfcFEATCOPY_MOVE = 3
    EpfcFEATCOPY_NEWREFS = 0
    EpfcFEATCOPY_SAMEREFS = 1
    EpfcFeatureCopyType_nil = 4
    com = 'pfcls.pfcFeatureCopyType.1'


class EpfcContourTraversal(Enum):
    EpfcCONTOUR_TRAV_EXTERNAL = 2
    EpfcCONTOUR_TRAV_INTERNAL = 1
    EpfcCONTOUR_TRAV_NONE = 0
    EpfcContourTraversal_nil = 3
    com = 'pfcls.pfcContourTraversal.1'


class EpfcSurfaceType(Enum):
    EpfcSURFACE_CONE = 2
    EpfcSURFACE_COONS_PATCH = 8
    EpfcSURFACE_CYLINDER = 1
    EpfcSURFACE_CYLINDRICAL_SPLINE = 11
    EpfcSURFACE_FILLET = 7
    EpfcSURFACE_FOREIGN = 13
    EpfcSURFACE_NURBS = 10
    EpfcSURFACE_PLANE = 0
    EpfcSURFACE_REVOLVED = 5
    EpfcSURFACE_RULED = 4
    EpfcSURFACE_SPHERICAL_SPLINE = 12
    EpfcSURFACE_SPLINE = 9
    EpfcSURFACE_TABULATED_CYLINDER = 6
    EpfcSURFACE_TORUS = 3
    EpfcSurfaceType_nil = 14
    com = 'pfcls.pfcSurfaceType.1'


class EpfcSurfaceOrientation(Enum):
    EpfcSURFACEORIENT_INWARD = 2
    EpfcSURFACEORIENT_NONE = 0
    EpfcSURFACEORIENT_OUTWARD = 1
    EpfcSurfaceOrientation_nil = 3
    com = 'pfcls.pfcSurfaceOrientation.1'


class EpfcCurveType(Enum):
    EpfcCURVE_ARC = 4
    EpfcCURVE_ARROW = 3
    EpfcCURVE_BSPLINE = 6
    EpfcCURVE_CIRCLE = 7
    EpfcCURVE_COMPOSITE = 0
    EpfcCURVE_ELLIPSE = 8
    EpfcCURVE_LINE = 2
    EpfcCURVE_POINT = 1
    EpfcCURVE_POLYGON = 9
    EpfcCURVE_SPLINE = 5
    EpfcCURVE_TEXT = 10
    EpfcCurveType_nil = 11
    com = 'pfcls.pfcCurveType.1'


class EpfcModelType(Enum):
    EpfcMDL_2D_SECTION = 3
    EpfcMDL_ASSEMBLY = 0
    EpfcMDL_CE_DRAWING = 12
    EpfcMDL_CE_SOLID = 11
    EpfcMDL_DIAGRAM = 9
    EpfcMDL_DRAWING = 2
    EpfcMDL_DWG_FORMAT = 5
    EpfcMDL_LAYOUT = 4
    EpfcMDL_MARKUP = 8
    EpfcMDL_MFG = 6
    EpfcMDL_PART = 1
    EpfcMDL_REPORT = 7
    EpfcMDL_UNSPECIFIED = 10
    EpfcModelType_nil = 13
    com = 'pfcls.pfcModelType.1'


class EpfcExportType(Enum):
    EpfcEXPORT_ACIS = 26
    EpfcEXPORT_BOM = 8
    EpfcEXPORT_CABLE_PARAMS = 21
    EpfcEXPORT_CADDS = 28
    EpfcEXPORT_CATIAFACETS = 22
    EpfcEXPORT_CATIA_CGR = 38
    EpfcEXPORT_CATIA_MODEL = 25
    EpfcEXPORT_CATIA_PART = 36
    EpfcEXPORT_CATIA_PRODUCT = 37
    EpfcEXPORT_CATIA_SESSION = 27
    EpfcEXPORT_CGM = 17
    EpfcEXPORT_CONNECTOR_PARAMS = 20
    EpfcEXPORT_DWG_SETUP = 9
    EpfcEXPORT_DXF = 4
    EpfcEXPORT_FEAT_INFO = 10
    EpfcEXPORT_FIAT = 19
    EpfcEXPORT_IGES = 3
    EpfcEXPORT_IGES_3D = 14
    EpfcEXPORT_INTF_3MF = 45
    EpfcEXPORT_INTF_DWG = 42
    EpfcEXPORT_INTF_DXF = 41
    EpfcEXPORT_INTF_SW_ASSEM = 44
    EpfcEXPORT_INTF_SW_PART = 43
    EpfcEXPORT_INVENTOR = 18
    EpfcEXPORT_JT = 34
    EpfcEXPORT_MATERIAL = 13
    EpfcEXPORT_MEDUSA = 30
    EpfcEXPORT_MFG_FEAT_CL = 12
    EpfcEXPORT_MFG_OPER_CL = 11
    EpfcEXPORT_MODEL_INFO = 1
    EpfcEXPORT_NEUTRAL = 31
    EpfcEXPORT_PARASOLID = 39
    EpfcEXPORT_PDF = 33
    EpfcEXPORT_PLOT = 23
    EpfcEXPORT_PRINT = 40
    EpfcEXPORT_PRODUCTVIEW = 32
    EpfcEXPORT_PROGRAM = 2
    EpfcEXPORT_RELATION = 0
    EpfcEXPORT_RENDER = 5
    EpfcEXPORT_STEP = 15
    EpfcEXPORT_STEP_2D = 29
    EpfcEXPORT_STL_ASCII = 6
    EpfcEXPORT_STL_BINARY = 7
    EpfcEXPORT_UG = 35
    EpfcEXPORT_VDA = 16
    EpfcEXPORT_VRML = 24
    EpfcExportType_nil = 46
    com = 'pfcls.pfcExportType.1'


class EpfcCGMExportType(Enum):
    EpfcCGMExportType_nil = 2
    EpfcEXPORT_CGM_CLEAR_TEXT = 0
    EpfcEXPORT_CGM_MIL_SPEC = 1
    com = 'pfcls.pfcCGMExportType.1'


class EpfcCGMScaleType(Enum):
    EpfcCGMScaleType_nil = 2
    EpfcEXPORT_CGM_ABSTRACT = 0
    EpfcEXPORT_CGM_METRIC = 1
    com = 'pfcls.pfcCGMScaleType.1'


class EpfcImportType(Enum):
    EpfcIMPORT_2D_CADAM = 14
    EpfcIMPORT_2D_DWG = 13
    EpfcIMPORT_2D_DXF = 12
    EpfcIMPORT_2D_IGES = 11
    EpfcIMPORT_2D_STEP = 10
    EpfcIMPORT_ASSEM_TREE_CFG = 8
    EpfcIMPORT_CABLE_PARAMS = 7
    EpfcIMPORT_CONFIG = 3
    EpfcIMPORT_CONNECTOR_PARAMS = 6
    EpfcIMPORT_DWG_SETUP = 4
    EpfcIMPORT_IGES_SECTION = 1
    EpfcIMPORT_PROGRAM = 2
    EpfcIMPORT_RELATION = 0
    EpfcIMPORT_SPOOL = 5
    EpfcIMPORT_WIRELIST = 9
    EpfcImportType_nil = 15
    com = 'pfcls.pfcImportType.1'


class EpfcPlotPaperSize(Enum):
    EpfcA0SIZEPLOT = 9
    EpfcA1SIZEPLOT = 8
    EpfcA2SIZEPLOT = 7
    EpfcA3SIZEPLOT = 6
    EpfcA4SIZEPLOT = 5
    EpfcASIZEPLOT = 0
    EpfcBSIZEPLOT = 1
    EpfcCEEMPTYPLOT = 13
    EpfcCEEMPTYPLOT_MM = 14
    EpfcCSIZEPLOT = 2
    EpfcDSIZEPLOT = 3
    EpfcESIZEPLOT = 4
    EpfcFSIZEPLOT = 10
    EpfcPlotPaperSize_nil = 15
    EpfcVARIABLESIZEPLOT = 11
    EpfcVARIABLESIZE_IN_MM_PLOT = 12
    com = 'pfcls.pfcPlotPaperSize.1'


class EpfcExport2DSheetOption(Enum):
    EpfcEXPORT_ALL = 2
    EpfcEXPORT_CURRENT_TO_MODEL_SPACE = 0
    EpfcEXPORT_CURRENT_TO_PAPER_SPACE = 1
    EpfcEXPORT_SELECTED = 3
    EpfcExport2DSheetOption_nil = 4
    com = 'pfcls.pfcExport2DSheetOption.1'


class EpfcFacetControlFlag(Enum):
    EpfcEXPORT_INCLUDE_ANNOTATIONS = 9
    EpfcFACET_ANGLE_CONTROL_DEFAULT = 4
    EpfcFACET_CHORD_HEIGHT_ADJUST = 1
    EpfcFACET_CHORD_HEIGHT_DEFAULT = 3
    EpfcFACET_FORCE_INTO_RANGE = 7
    EpfcFACET_INCLUDE_QUILTS = 8
    EpfcFACET_STEP_SIZE_ADJUST = 0
    EpfcFACET_STEP_SIZE_DEFAULT = 5
    EpfcFACET_STEP_SIZE_OFF = 6
    EpfcFACET_USE_CONFIG = 2
    EpfcFacetControlFlag_nil = 10
    com = 'pfcls.pfcFacetControlFlag.1'


class EpfcPlotPageRange(Enum):
    EpfcPLOT_RANGE_ALL = 0
    EpfcPLOT_RANGE_CURRENT = 1
    EpfcPLOT_RANGE_OF_PAGES = 2
    EpfcPlotPageRange_nil = 3
    com = 'pfcls.pfcPlotPageRange.1'


class EpfcIntfType(Enum):
    EpfcINTF_3MF = 25
    EpfcINTF_ACIS = 6
    EpfcINTF_AI = 12
    EpfcINTF_CATIA_CGR = 17
    EpfcINTF_CATIA_PART = 13
    EpfcINTF_CATIA_PRODUCT = 16
    EpfcINTF_CDRS = 8
    EpfcINTF_DXF = 7
    EpfcINTF_IBL = 21
    EpfcINTF_ICEM = 5
    EpfcINTF_IGES = 2
    EpfcINTF_INVENTOR_ASM = 20
    EpfcINTF_INVENTOR_PART = 19
    EpfcINTF_JT = 18
    EpfcINTF_NEUTRAL = 0
    EpfcINTF_NEUTRAL_FILE = 1
    EpfcINTF_PARASOLID = 11
    EpfcINTF_PRODUCTVIEW = 15
    EpfcINTF_PTS = 22
    EpfcINTF_SE_PART = 23
    EpfcINTF_SE_SHEETMETAL_PART = 24
    EpfcINTF_STEP = 3
    EpfcINTF_STL = 9
    EpfcINTF_UG = 14
    EpfcINTF_VDA = 4
    EpfcINTF_VRML = 10
    EpfcIntfType_nil = 26
    com = 'pfcls.pfcIntfType.1'


class EpfcOperationType(Enum):
    EpfcADD_OPERATION = 1
    EpfcCUT_OPERATION = 0
    EpfcOperationType_nil = 2
    com = 'pfcls.pfcOperationType.1'


class EpfcFamilyColumnType(Enum):
    EpfcFAM_ANNOT_ELEM_PARAM = 24
    EpfcFAM_ASMCOMP = 4
    EpfcFAM_ASMCOMP_MODEL = 6
    EpfcFAM_COMP_CURVE_PARAM = 22
    EpfcFAM_CONNECTION_PARAM = 25
    EpfcFAM_CURVE_PARAM = 21
    EpfcFAM_DIMENSION = 1
    EpfcFAM_EDGE_PARAM = 19
    EpfcFAM_EXTERNAL_REFERENCE = 12
    EpfcFAM_FEATURE = 3
    EpfcFAM_FEATURE_PARAM = 18
    EpfcFAM_GTOL = 7
    EpfcFAM_INH_PART_REF = 16
    EpfcFAM_IPAR_NOTE = 2
    EpfcFAM_MASS_PROPS_SOURCE = 15
    EpfcFAM_MASS_PROPS_USER_PARAM = 14
    EpfcFAM_MERGE_PART_REF = 13
    EpfcFAM_QUILT_PARAM = 23
    EpfcFAM_SIM_OBJ = 17
    EpfcFAM_SURFACE_PARAM = 20
    EpfcFAM_SYSTEM_PARAM = 11
    EpfcFAM_TOL_MINUS = 9
    EpfcFAM_TOL_PLUS = 8
    EpfcFAM_TOL_PLUSMINUS = 10
    EpfcFAM_UDF = 5
    EpfcFAM_USER_PARAM = 0
    EpfcFamilyColumnType_nil = 26
    com = 'pfcls.pfcFamilyColumnType.1'


class EpfcFaminstanceVerifyStatus(Enum):
    EpfcFAMINST_FAILED_VERIFIED = 1
    EpfcFAMINST_NOT_VERIFIED = 2
    EpfcFAMINST_SUCCESS_VERIFIED = 0
    EpfcFaminstanceVerifyStatus_nil = 3
    com = 'pfcls.pfcFaminstanceVerifyStatus.1'


class EpfcSimpRepActionType(Enum):
    EpfcSIMPREP_ASSEM_ONLY = 12
    EpfcSIMPREP_AUTO = 11
    EpfcSIMPREP_BOUNDBOX = 8
    EpfcSIMPREP_DEFENV = 9
    EpfcSIMPREP_EXCLUDE = 2
    EpfcSIMPREP_GEOM = 4
    EpfcSIMPREP_GRAPHICS = 5
    EpfcSIMPREP_INCLUDE = 1
    EpfcSIMPREP_LIGHT_GRAPH = 10
    EpfcSIMPREP_NONE = 7
    EpfcSIMPREP_REVERSE = 0
    EpfcSIMPREP_SUBSTITUTE = 3
    EpfcSIMPREP_SYMB = 6
    EpfcSimpRepActionType_nil = 13
    com = 'pfcls.pfcSimpRepActionType.1'


class EpfcSubstType(Enum):
    EpfcSUBST_ASM_REP = 2
    EpfcSUBST_ENVELOPE = 3
    EpfcSUBST_INTERCHG = 0
    EpfcSUBST_NONE = 4
    EpfcSUBST_PRT_REP = 1
    EpfcSubstType_nil = 5
    com = 'pfcls.pfcSubstType.1'


class EpfcSimpRepType(Enum):
    EpfcSIMPREP_BOUNDBOX_REP = 5
    EpfcSIMPREP_DEFENV_REP = 6
    EpfcSIMPREP_GEOM_REP = 3
    EpfcSIMPREP_GRAPHICS_REP = 2
    EpfcSIMPREP_LIGHT_GRAPH_REP = 7
    EpfcSIMPREP_MASTER_REP = 0
    EpfcSIMPREP_SYMB_REP = 4
    EpfcSIMPREP_USER_DEFINED = 1
    EpfcSimpRepType_nil = 9
    com = 'pfcls.pfcSimpRepType.1'


class EpfcXSecCutType(Enum):
    EpfcXSEC_OFFSET = 1
    EpfcXSEC_PLANAR = 0
    EpfcXSecCutType_nil = 2
    com = 'pfcls.pfcXSecCutType.1'


class EpfcXSecCutobjType(Enum):
    EpfcXSECTYPE_MODEL = 0
    EpfcXSECTYPE_MODELQUILTS = 2
    EpfcXSECTYPE_ONEPART = 3
    EpfcXSECTYPE_QUILTS = 1
    EpfcXSecCutobjType_nil = 4
    com = 'pfcls.pfcXSecCutobjType.1'


class EpfcMaterialType(Enum):
    EpfcMTL_ISOTROPIC = 0
    EpfcMTL_ORTHOTROPIC = 1
    EpfcMTL_TRANSVERSELY_ISOTROPIC = 2
    EpfcMaterialType_nil = 3
    com = 'pfcls.pfcMaterialType.1'


class EpfcMaterialPropertyType(Enum):
    EpfcMTL_PROP_COMPRESSION_ULTIMATE_STRESS = 29
    EpfcMTL_PROP_COMPRESSION_ULTIMATE_STRESS_SC1 = 5
    EpfcMTL_PROP_COMPRESSION_ULTIMATE_STRESS_SC2 = 6
    EpfcMTL_PROP_COST_TYPE = 51
    EpfcMTL_PROP_EMISSIVITY = 38
    EpfcMTL_PROP_EXP_LAW_EXPONENT = 60
    EpfcMTL_PROP_FATIGUE_CUT_OFF_CYCLES = 52
    EpfcMTL_PROP_FATIGUE_STRENGTH_REDUCTION_FACTOR = 0
    EpfcMTL_PROP_HARDENING = 56
    EpfcMTL_PROP_HARDENING_LIMIT = 61
    EpfcMTL_PROP_HARDNESS = 33
    EpfcMTL_PROP_INITIAL_BEND_Y_FACTOR = 34
    EpfcMTL_PROP_MASS_DENSITY = 26
    EpfcMTL_PROP_MECHANISMS_DAMPING = 63
    EpfcMTL_PROP_MODEL_COEF_C01 = 41
    EpfcMTL_PROP_MODEL_COEF_C02 = 42
    EpfcMTL_PROP_MODEL_COEF_C10 = 43
    EpfcMTL_PROP_MODEL_COEF_C11 = 44
    EpfcMTL_PROP_MODEL_COEF_C20 = 45
    EpfcMTL_PROP_MODEL_COEF_C30 = 46
    EpfcMTL_PROP_MODEL_COEF_D = 47
    EpfcMTL_PROP_MODEL_COEF_D1 = 48
    EpfcMTL_PROP_MODEL_COEF_D2 = 49
    EpfcMTL_PROP_MODEL_COEF_D3 = 50
    EpfcMTL_PROP_MODEL_COEF_LM = 40
    EpfcMTL_PROP_MODEL_COEF_MU = 39
    EpfcMTL_PROP_MODIFIED_MODULUS = 58
    EpfcMTL_PROP_POISSON_RATIO = 25
    EpfcMTL_PROP_POISSON_RATIO_NU21 = 7
    EpfcMTL_PROP_POISSON_RATIO_NU31 = 8
    EpfcMTL_PROP_POISSON_RATIO_NU32 = 9
    EpfcMTL_PROP_POWER_LAW_EXPONENT = 59
    EpfcMTL_PROP_SHEAR_MODULUS = 35
    EpfcMTL_PROP_SHEAR_MODULUS_G12 = 13
    EpfcMTL_PROP_SHEAR_MODULUS_G13 = 14
    EpfcMTL_PROP_SHEAR_MODULUS_G23 = 15
    EpfcMTL_PROP_SHEAR_ULTIMATE_STRESS = 30
    EpfcMTL_PROP_SPECIFIC_HEAT = 32
    EpfcMTL_PROP_STRESS_LIMIT_FOR_COMPRESSION = 54
    EpfcMTL_PROP_STRESS_LIMIT_FOR_FATIGUE = 22
    EpfcMTL_PROP_STRESS_LIMIT_FOR_SHEAR = 55
    EpfcMTL_PROP_STRESS_LIMIT_FOR_TENSION = 53
    EpfcMTL_PROP_STRUCTURAL_DAMPING_COEFFICIENT = 37
    EpfcMTL_PROP_TANGENT_MODULUS = 57
    EpfcMTL_PROP_TEMPERATURE = 23
    EpfcMTL_PROP_TENSILE_ULTIMATE_STRESS = 28
    EpfcMTL_PROP_TENSILE_ULTIMATE_STRESS_ST1 = 3
    EpfcMTL_PROP_TENSILE_ULTIMATE_STRESS_ST2 = 4
    EpfcMTL_PROP_TENSILE_YIELD_STRESS = 2
    EpfcMTL_PROP_THERMAL_CONDUCTIVITY = 31
    EpfcMTL_PROP_THERMAL_CONDUCTIVITY_K1 = 19
    EpfcMTL_PROP_THERMAL_CONDUCTIVITY_K2 = 20
    EpfcMTL_PROP_THERMAL_CONDUCTIVITY_K3 = 21
    EpfcMTL_PROP_THERMAL_EXPANSION_COEFFICIENT = 27
    EpfcMTL_PROP_THERMAL_EXPANSION_COEFFICIENT_A1 = 16
    EpfcMTL_PROP_THERMAL_EXPANSION_COEFFICIENT_A2 = 17
    EpfcMTL_PROP_THERMAL_EXPANSION_COEFFICIENT_A3 = 18
    EpfcMTL_PROP_THERMAL_SOFTENING_COEF = 62
    EpfcMTL_PROP_THERM_EXPANSION_REF_TEMPERATURE = 36
    EpfcMTL_PROP_TSAI_WU_INTERACTION_TERM_F12 = 1
    EpfcMTL_PROP_YOUNG_MODULUS = 24
    EpfcMTL_PROP_YOUNG_MODULUS_E1 = 10
    EpfcMTL_PROP_YOUNG_MODULUS_E2 = 11
    EpfcMTL_PROP_YOUNG_MODULUS_E3 = 12
    EpfcMaterialPropertyType_nil = 64
    com = 'pfcls.pfcMaterialPropertyType.1'


class EpfcGraphicsMode(Enum):
    EpfcDRAW_GRAPHICS_COMPLEMENT = 1
    EpfcDRAW_GRAPHICS_NORMAL = 0
    EpfcGraphicsMode_nil = 2
    com = 'pfcls.pfcGraphicsMode.1'


class EpfcColumnJustification(Enum):
    EpfcCOL_JUSTIFY_CENTER = 1
    EpfcCOL_JUSTIFY_LEFT = 0
    EpfcCOL_JUSTIFY_RIGHT = 2
    EpfcColumnJustification_nil = 3
    com = 'pfcls.pfcColumnJustification.1'


class EpfcTableSizeType(Enum):
    EpfcTABLESIZE_BY_LENGTH = 1
    EpfcTABLESIZE_BY_NUM_CHARS = 0
    EpfcTableSizeType_nil = 2
    com = 'pfcls.pfcTableSizeType.1'


class EpfcParamMode(Enum):
    EpfcDWGTABLE_FULL = 1
    EpfcDWGTABLE_NORMAL = 0
    EpfcParamMode_nil = 2
    com = 'pfcls.pfcParamMode.1'


class EpfcRotationDegree(Enum):
    EpfcROTATE_180 = 1
    EpfcROTATE_270 = 2
    EpfcROTATE_90 = 0
    EpfcRotationDegree_nil = 3
    com = 'pfcls.pfcRotationDegree.1'


class EpfcView2DType(Enum):
    EpfcDRAWVIEW_AUXILIARY = 2
    EpfcDRAWVIEW_COPY_AND_ALIGN = 5
    EpfcDRAWVIEW_DETAIL = 3
    EpfcDRAWVIEW_GENERAL = 0
    EpfcDRAWVIEW_OF_FLAT_TYPE = 6
    EpfcDRAWVIEW_PROJECTION = 1
    EpfcDRAWVIEW_REVOLVE = 4
    EpfcView2DType_nil = 7
    com = 'pfcls.pfcView2DType.1'


class EpfcRasterDepth(Enum):
    EpfcRASTERDEPTH_24 = 1
    EpfcRASTERDEPTH_8 = 0
    EpfcRasterDepth_nil = 2
    com = 'pfcls.pfcRasterDepth.1'


class EpfcDotsPerInch(Enum):
    EpfcDotsPerInch_nil = 6
    EpfcRASTERDPI_100 = 0
    EpfcRASTERDPI_200 = 1
    EpfcRASTERDPI_300 = 2
    EpfcRASTERDPI_400 = 3
    EpfcRASTERDPI_500 = 4
    EpfcRASTERDPI_600 = 5
    com = 'pfcls.pfcDotsPerInch.1'


class EpfcRasterType(Enum):
    EpfcRASTER_BMP = 0
    EpfcRASTER_EPS = 2
    EpfcRASTER_JPEG = 3
    EpfcRASTER_TIFF = 1
    EpfcRasterType_nil = 4
    com = 'pfcls.pfcRasterType.1'


class EpfcComponentConstraintType(Enum):
    EpfcASM_CONSTRAINT_ALIGN = 2
    EpfcASM_CONSTRAINT_ALIGN_ANG_OFF = 15
    EpfcASM_CONSTRAINT_ALIGN_NODEP_ANGLE = 30
    EpfcASM_CONSTRAINT_ALIGN_OFF = 3
    EpfcASM_CONSTRAINT_AUTO = 14
    EpfcASM_CONSTRAINT_CSYS = 6
    EpfcASM_CONSTRAINT_CSYS_PNT = 17
    EpfcASM_CONSTRAINT_DEF_PLACEMENT = 10
    EpfcASM_CONSTRAINT_EDGE_ON_SRF = 9
    EpfcASM_CONSTRAINT_EDGE_ON_SRF_ANG = 28
    EpfcASM_CONSTRAINT_EDGE_ON_SRF_DIST = 27
    EpfcASM_CONSTRAINT_EDGE_ON_SRF_NORMAL = 29
    EpfcASM_CONSTRAINT_EDGE_ON_SRF_PARL = 33
    EpfcASM_CONSTRAINT_EXPLICIT = 35
    EpfcASM_CONSTRAINT_FIX = 13
    EpfcASM_CONSTRAINT_INSERT = 4
    EpfcASM_CONSTRAINT_INSERT_NORM = 23
    EpfcASM_CONSTRAINT_INSERT_PARL = 24
    EpfcASM_CONSTRAINT_LINE_ANGLE = 32
    EpfcASM_CONSTRAINT_LINE_COPLANAR = 19
    EpfcASM_CONSTRAINT_LINE_DIST = 21
    EpfcASM_CONSTRAINT_LINE_NORMAL = 18
    EpfcASM_CONSTRAINT_LINE_PARL = 20
    EpfcASM_CONSTRAINT_MATE = 0
    EpfcASM_CONSTRAINT_MATE_ANG_OFF = 16
    EpfcASM_CONSTRAINT_MATE_NODEP_ANGLE = 31
    EpfcASM_CONSTRAINT_MATE_OFF = 1
    EpfcASM_CONSTRAINT_ORIENT = 5
    EpfcASM_CONSTRAINT_PNT_DIST = 22
    EpfcASM_CONSTRAINT_PNT_ON_LINE = 12
    EpfcASM_CONSTRAINT_PNT_ON_LINE_DIST = 25
    EpfcASM_CONSTRAINT_PNT_ON_SRF = 8
    EpfcASM_CONSTRAINT_PNT_ON_SRF_DIST = 26
    EpfcASM_CONSTRAINT_SRF_NORMAL = 34
    EpfcASM_CONSTRAINT_SUBSTITUTE = 11
    EpfcASM_CONSTRAINT_TANGENT = 7
    EpfcComponentConstraintType_nil = 36
    com = 'pfcls.pfcComponentConstraintType.1'


class EpfcComponentType(Enum):
    EpfcCOMPONENT_CAST_ASSEM = 7
    EpfcCOMPONENT_CAST_RESULT = 11
    EpfcCOMPONENT_DIE_BLOCK = 8
    EpfcCOMPONENT_DIE_COMP = 9
    EpfcCOMPONENT_FIXTURE = 2
    EpfcCOMPONENT_FROM_MOTION = 12
    EpfcCOMPONENT_GEN_ASSEM = 6
    EpfcCOMPONENT_MOLD_ASSEM = 5
    EpfcCOMPONENT_MOLD_BASE = 3
    EpfcCOMPONENT_MOLD_COMP = 4
    EpfcCOMPONENT_NONE = 14
    EpfcCOMPONENT_NO_DEF_ASSUM = 13
    EpfcCOMPONENT_REF_MODEL = 1
    EpfcCOMPONENT_SAND_CORE = 10
    EpfcCOMPONENT_WORKPIECE = 0
    EpfcComponentType_nil = 15
    com = 'pfcls.pfcComponentType.1'


class EpfcSheetOrientation(Enum):
    EpfcORIENT_LANDSCAPE = 1
    EpfcORIENT_PORTRAIT = 0
    EpfcSheetOrientation_nil = 2
    com = 'pfcls.pfcSheetOrientation.1'


class EpfcDetailType(Enum):
    EpfcDETAIL_DRAFT_GROUP = 4
    EpfcDETAIL_ENTITY = 0
    EpfcDETAIL_NOTE = 1
    EpfcDETAIL_OLE_OBJECT = 5
    EpfcDETAIL_SYM_DEFINITION = 2
    EpfcDETAIL_SYM_INSTANCE = 3
    EpfcDetailType_nil = 6
    com = 'pfcls.pfcDetailType.1'


class EpfcAttachmentType(Enum):
    EpfcATTACH_FREE = 0
    EpfcATTACH_OFFSET = 3
    EpfcATTACH_PARAMETRIC = 1
    EpfcATTACH_TYPE_UNSUPPORTED = 2
    EpfcAttachmentType_nil = 4
    com = 'pfcls.pfcAttachmentType.1'


class EpfcHorizontalJustification(Enum):
    EpfcH_JUSTIFY_CENTER = 1
    EpfcH_JUSTIFY_DEFAULT = 3
    EpfcH_JUSTIFY_LEFT = 0
    EpfcH_JUSTIFY_RIGHT = 2
    EpfcHorizontalJustification_nil = 4
    com = 'pfcls.pfcHorizontalJustification.1'


class EpfcVerticalJustification(Enum):
    EpfcV_JUSTIFY_BOTTOM = 2
    EpfcV_JUSTIFY_DEFAULT = 3
    EpfcV_JUSTIFY_MIDDLE = 1
    EpfcV_JUSTIFY_TOP = 0
    EpfcVerticalJustification_nil = 4
    com = 'pfcls.pfcVerticalJustification.1'


class EpfcSymbolDefHeight(Enum):
    EpfcSYMDEF_FIXED = 0
    EpfcSYMDEF_MODEL_UNITS = 3
    EpfcSYMDEF_RELATIVE_TO_TEXT = 2
    EpfcSYMDEF_VARIABLE = 1
    EpfcSymbolDefHeight_nil = 4
    com = 'pfcls.pfcSymbolDefHeight.1'


class EpfcSymbolDefAttachmentType(Enum):
    EpfcSYMDEFATTACH_FREE = 0
    EpfcSYMDEFATTACH_LEFT_LEADER = 1
    EpfcSYMDEFATTACH_NORMAL_TO_ITEM = 5
    EpfcSYMDEFATTACH_ON_ITEM = 4
    EpfcSYMDEFATTACH_RADIAL_LEADER = 3
    EpfcSYMDEFATTACH_RIGHT_LEADER = 2
    EpfcSymbolDefAttachmentType_nil = 6
    com = 'pfcls.pfcSymbolDefAttachmentType.1'


class EpfcSymbolGroupFilter(Enum):
    EpfcDTLSYMINST_ACTIVE_GROUPS = 1
    EpfcDTLSYMINST_ALL_GROUPS = 0
    EpfcDTLSYMINST_INACTIVE_GROUPS = 2
    EpfcSymbolGroupFilter_nil = 3
    com = 'pfcls.pfcSymbolGroupFilter.1'


class EpfcDetailSymbolGroupOption(Enum):
    EpfcDETAIL_SYMBOL_GROUP_ALL = 1
    EpfcDETAIL_SYMBOL_GROUP_CUSTOM = 3
    EpfcDETAIL_SYMBOL_GROUP_INTERACTIVE = 0
    EpfcDETAIL_SYMBOL_GROUP_NONE = 2
    EpfcDetailSymbolGroupOption_nil = 4
    com = 'pfcls.pfcDetailSymbolGroupOption.1'


class EpfcDetailTextDisplayOption(Enum):
    EpfcDISPMODE_NUMERIC = 0
    EpfcDISPMODE_SYMBOLIC = 1
    EpfcDetailTextDisplayOption_nil = 2
    com = 'pfcls.pfcDetailTextDisplayOption.1'


class EpfcDetailLeaderAttachmentType(Enum):
    EpfcDetailLeaderAttachmentType_nil = 3
    EpfcLEADER_ATTACH_NONE = 0
    EpfcLEADER_ATTACH_NORMAL = 1
    EpfcLEADER_ATTACH_TANGENT = 2
    com = 'pfcls.pfcDetailLeaderAttachmentType.1'


class EpfcDetailSymbolDefItemSource(Enum):
    EpfcDTLSYMDEF_SRC_PATH = 3
    EpfcDTLSYMDEF_SRC_SURF_FINISH_DIR = 1
    EpfcDTLSYMDEF_SRC_SYMBOL_DIR = 2
    EpfcDTLSYMDEF_SRC_SYSTEM = 0
    EpfcDetailSymbolDefItemSource_nil = 4
    com = 'pfcls.pfcDetailSymbolDefItemSource.1'


class EpfcDimensionSenseType(Enum):
    EpfcDIMSENSE_ANGLE = 5
    EpfcDIMSENSE_LINEAR_TO_ARC_OR_CIRCLE_TANGENT = 4
    EpfcDIMSENSE_NONE = 0
    EpfcDIMSENSE_POINT = 1
    EpfcDIMSENSE_POINT_TO_ANGLE = 6
    EpfcDIMSENSE_SPLINE_PT = 2
    EpfcDIMSENSE_TANGENT_INDEX = 3
    EpfcDimensionSenseType_nil = 7
    com = 'pfcls.pfcDimensionSenseType.1'


class EpfcDimensionPointType(Enum):
    EpfcDIMPOINT_CENTER = 2
    EpfcDIMPOINT_END1 = 0
    EpfcDIMPOINT_END2 = 1
    EpfcDIMPOINT_MIDPOINT = 4
    EpfcDIMPOINT_NONE = 3
    EpfcDimensionPointType_nil = 5
    com = 'pfcls.pfcDimensionPointType.1'


class EpfcDimensionLinAOCTangentType(Enum):
    EpfcDIMLINAOCTANGENT_LEFT0 = 0
    EpfcDIMLINAOCTANGENT_LEFT1 = 2
    EpfcDIMLINAOCTANGENT_RIGHT0 = 1
    EpfcDIMLINAOCTANGENT_RIGHT1 = 3
    EpfcDimensionLinAOCTangentType_nil = 4
    com = 'pfcls.pfcDimensionLinAOCTangentType.1'


class EpfcOrientationHint(Enum):
    EpfcORIENTHINT_ARC_ANGLE = 6
    EpfcORIENTHINT_ARC_LENGTH = 7
    EpfcORIENTHINT_ELLIPSE_RADIUS1 = 4
    EpfcORIENTHINT_ELLIPSE_RADIUS2 = 5
    EpfcORIENTHINT_HORIZONTAL = 1
    EpfcORIENTHINT_LINE_TO_TANGENT_CURVE_ANGLE = 8
    EpfcORIENTHINT_NONE = 0
    EpfcORIENTHINT_SLANTED = 3
    EpfcORIENTHINT_VERTICAL = 2
    EpfcOrientationHint_nil = 10
    com = 'pfcls.pfcOrientationHint.1'


class EpfcDrawingCreateOption(Enum):
    EpfcDRAWINGCREATE_DISPLAY_DRAWING = 0
    EpfcDRAWINGCREATE_PROMPT_UNKNOWN_PARAMS = 3
    EpfcDRAWINGCREATE_SHOW_ERROR_DIALOG = 1
    EpfcDRAWINGCREATE_WRITE_ERROR_FILE = 2
    EpfcDrawingCreateOption_nil = 4
    com = 'pfcls.pfcDrawingCreateOption.1'


class EpfcDatumAxisConstraintType(Enum):
    EpfcDTMAXIS_CENTER = 3
    EpfcDTMAXIS_NORMAL = 0
    EpfcDTMAXIS_TANGENT = 2
    EpfcDTMAXIS_THRU = 1
    EpfcDatumAxisConstraintType_nil = 4
    com = 'pfcls.pfcDatumAxisConstraintType.1'


class EpfcSplitCurveSide(Enum):
    EpfcCURVE_BOTH_SIDES = 2
    EpfcCURVE_SIDE_ONE = 0
    EpfcCURVE_SIDE_TWO = 1
    EpfcSplitCurveSide_nil = 3
    com = 'pfcls.pfcSplitCurveSide.1'


class EpfcOffsetCurveDirection(Enum):
    EpfcOFFSET_SIDE_ONE = 0
    EpfcOFFSET_SIDE_TWO = 1
    EpfcOffsetCurveDirection_nil = 2
    com = 'pfcls.pfcOffsetCurveDirection.1'


class EpfcCurveStartPoint(Enum):
    EpfcCURVE_END = 1
    EpfcCURVE_START = 0
    EpfcCurveStartPoint_nil = 2
    com = 'pfcls.pfcCurveStartPoint.1'


class EpfcDatumCsysDimConstraintType(Enum):
    EpfcDTMCSYS_DIM_ALIGN = 1
    EpfcDTMCSYS_DIM_OFFSET = 0
    EpfcDatumCsysDimConstraintType_nil = 2
    com = 'pfcls.pfcDatumCsysDimConstraintType.1'


class EpfcDatumCsysOrientMoveConstraintType(Enum):
    EpfcDTMCSYS_MOVE_PHI = 7
    EpfcDTMCSYS_MOVE_RAD = 6
    EpfcDTMCSYS_MOVE_ROT_X = 3
    EpfcDTMCSYS_MOVE_ROT_Y = 4
    EpfcDTMCSYS_MOVE_ROT_Z = 5
    EpfcDTMCSYS_MOVE_THETA = 9
    EpfcDTMCSYS_MOVE_TRAN_X = 0
    EpfcDTMCSYS_MOVE_TRAN_Y = 1
    EpfcDTMCSYS_MOVE_TRAN_Z = 2
    EpfcDTMCSYS_MOVE_ZI = 8
    EpfcDatumCsysOrientMoveConstraintType_nil = 10
    com = 'pfcls.pfcDatumCsysOrientMoveConstraintType.1'


class EpfcDatumCsysOffsetType(Enum):
    EpfcDTMCSYS_OFFSET_CARTESIAN = 0
    EpfcDTMCSYS_OFFSET_CYLINDRICAL = 1
    EpfcDTMCSYS_OFFSET_SPHERICAL = 2
    EpfcDatumCsysOffsetType_nil = 3
    com = 'pfcls.pfcDatumCsysOffsetType.1'


class EpfcDatumCsysOnSurfaceType(Enum):
    EpfcDTMCSYS_ONSURF_DIAMETER = 2
    EpfcDTMCSYS_ONSURF_LINEAR = 0
    EpfcDTMCSYS_ONSURF_RADIAL = 1
    EpfcDatumCsysOnSurfaceType_nil = 3
    com = 'pfcls.pfcDatumCsysOnSurfaceType.1'


class EpfcDatumCsysOrientByMethod(Enum):
    EpfcDTMCSYS_ORIENT_BY_SEL_CSYS_AXES = 1
    EpfcDTMCSYS_ORIENT_BY_SEL_REFS = 0
    EpfcDatumCsysOrientByMethod_nil = 2
    com = 'pfcls.pfcDatumCsysOrientByMethod.1'


class EpfcDatumPlaneConstraintType(Enum):
    EpfcDTMPLN_ANG = 4
    EpfcDTMPLN_DEF_X = 7
    EpfcDTMPLN_DEF_Y = 8
    EpfcDTMPLN_DEF_Z = 9
    EpfcDTMPLN_NORM = 1
    EpfcDTMPLN_OFFS = 3
    EpfcDTMPLN_PRL = 2
    EpfcDTMPLN_SEC = 6
    EpfcDTMPLN_TANG = 5
    EpfcDTMPLN_THRU = 0
    EpfcDatumPlaneConstraintType_nil = 10
    com = 'pfcls.pfcDatumPlaneConstraintType.1'


class EpfcDatumPointConstraintType(Enum):
    EpfcDTMPNT_ALONG_X = 9
    EpfcDTMPNT_ALONG_Y = 10
    EpfcDTMPNT_ALONG_Z = 11
    EpfcDTMPNT_CARTESIAN = 12
    EpfcDTMPNT_CENTER = 2
    EpfcDTMPNT_CYLINDRICAL = 13
    EpfcDTMPNT_LENGTH = 5
    EpfcDTMPNT_LENGTH_END = 6
    EpfcDTMPNT_NORMAL = 3
    EpfcDTMPNT_OFFSET = 1
    EpfcDTMPNT_ON = 0
    EpfcDTMPNT_PARALLEL = 4
    EpfcDTMPNT_RATIO = 7
    EpfcDTMPNT_RATIO_END = 8
    EpfcDTMPNT_SPHERICAL = 14
    EpfcDatumPointConstraintType_nil = 15
    com = 'pfcls.pfcDatumPointConstraintType.1'


class EpfcArgValueType(Enum):
    EpfcARG_V_ASCII_STRING = 3
    EpfcARG_V_BOOLEAN = 2
    EpfcARG_V_DOUBLE = 1
    EpfcARG_V_INTEGER = 0
    EpfcARG_V_POINTER = 7
    EpfcARG_V_SELECTION = 5
    EpfcARG_V_STRING = 4
    EpfcARG_V_TRANSFORM = 6
    EpfcARG_V_WSTRING = 8
    EpfcArgValueType_nil = 9
    com = 'pfcls.pfcArgValueType.1'


class EpfcDrawingCreateErrorType(Enum):
    EpfcDWGCREATE_ERR_CANT_GET_PROJ_PARENT = 11
    EpfcDWGCREATE_ERR_EXPLODE_DOESNT_EXIST = 2
    EpfcDWGCREATE_ERR_FIRST_REGION_USED = 6
    EpfcDWGCREATE_ERR_MODEL_NOT_EXPLODABLE = 3
    EpfcDWGCREATE_ERR_NOT_PROCESS_ASSEM = 7
    EpfcDWGCREATE_ERR_NO_PARENT_VIEW_FOR_PROJ = 10
    EpfcDWGCREATE_ERR_NO_RPT_REGIONS = 5
    EpfcDWGCREATE_ERR_NO_STEP_NUM = 8
    EpfcDWGCREATE_ERR_SAVED_VIEW_DOESNT_EXIST = 0
    EpfcDWGCREATE_ERR_SEC_NOT_PARALLEL = 12
    EpfcDWGCREATE_ERR_SEC_NOT_PERP = 4
    EpfcDWGCREATE_ERR_SIMP_REP_DOESNT_EXIST = 13
    EpfcDWGCREATE_ERR_TEMPLATE_USED = 9
    EpfcDWGCREATE_ERR_X_SEC_DOESNT_EXIST = 1
    EpfcDWGCRTERR_COMB_STATE_DOESNT_EXIST = 14
    EpfcDWGCRTERR_NOT_ALL_BALLOONS_CLEANED_WRN = 16
    EpfcDWGCRTERR_TOOL_DOESNT_EXIST = 15
    EpfcDrawingCreateErrorType_nil = 17
    com = 'pfcls.pfcDrawingCreateErrorType.1'


class EpfcCommandAccess(Enum):
    EpfcACCESS_AVAILABLE = 0
    EpfcACCESS_DISALLOW = 4
    EpfcACCESS_INVISIBLE = 2
    EpfcACCESS_REMOVE = 3
    EpfcACCESS_UNAVAILABLE = 1
    EpfcCommandAccess_nil = 5
    com = 'pfcls.pfcCommandAccess.1'


class EpfcUDFDependencyType(Enum):
    EpfcUDFDEP_DRIVEN = 1
    EpfcUDFDEP_INDEPENDENT = 0
    EpfcUDFDependencyType_nil = 2
    com = 'pfcls.pfcUDFDependencyType.1'


class EpfcUDFScaleType(Enum):
    EpfcUDFSCALE_CUSTOM = 2
    EpfcUDFSCALE_SAME_DIMS = 1
    EpfcUDFSCALE_SAME_SIZE = 0
    EpfcUDFScaleType_nil = 3
    com = 'pfcls.pfcUDFScaleType.1'


class EpfcUDFVariantValueType(Enum):
    EpfcUDFVARIANT_DIMENSION = 0
    EpfcUDFVARIANT_PATTERN_PARAM = 1
    EpfcUDFVariantValueType_nil = 2
    com = 'pfcls.pfcUDFVariantValueType.1'


class EpfcUDFDimensionDisplayType(Enum):
    EpfcUDFDISPLAY_BLANK = 2
    EpfcUDFDISPLAY_NORMAL = 0
    EpfcUDFDISPLAY_READ_ONLY = 1
    EpfcUDFDimensionDisplayType_nil = 3
    com = 'pfcls.pfcUDFDimensionDisplayType.1'


class EpfcUDFOrientation(Enum):
    EpfcUDFORIENT_FLIP = 2
    EpfcUDFORIENT_INTERACTIVE = 0
    EpfcUDFORIENT_NO_FLIP = 1
    EpfcUDFOrientation_nil = 3
    com = 'pfcls.pfcUDFOrientation.1'


class EpfcToolkitType(Enum):
    EpfcOTK = 1
    EpfcProtoolkit = 0
    EpfcToolkitType_nil = 2
    com = 'pfcls.pfcToolkitType.1'


class EpfcShrinkwrapFacetedFormat(Enum):
    EpfcSWFACETED_LIGHTWEIGHT_PART = 3
    EpfcSWFACETED_PART = 0
    EpfcSWFACETED_STL = 1
    EpfcSWFACETED_VRML = 2
    EpfcShrinkwrapFacetedFormat_nil = 4
    com = 'pfcls.pfcShrinkwrapFacetedFormat.1'


class EpfcShrinkwrapMethod(Enum):
    EpfcSWCREATE_FACETED_SOLID = 1
    EpfcSWCREATE_MERGED_SOLID = 2
    EpfcSWCREATE_SURF_SUBSET = 0
    EpfcShrinkwrapMethod_nil = 3
    com = 'pfcls.pfcShrinkwrapMethod.1'


class EpfcAssemblyConfiguration(Enum):
    EpfcAssemblyConfiguration_nil = 4
    EpfcEXPORT_ASM_ASSEMBLY_PARTS = 3
    EpfcEXPORT_ASM_FLAT_FILE = 0
    EpfcEXPORT_ASM_MULTI_FILES = 2
    EpfcEXPORT_ASM_SINGLE_FILE = 1
    com = 'pfcls.pfcAssemblyConfiguration.1'


class EpfcPDFSaveMode(Enum):
    EpfcPDFSaveMode_nil = 2
    EpfcPDF_ARCHIVE_1 = 0
    EpfcPDF_FULL = 1
    com = 'pfcls.pfcPDFSaveMode.1'


class EpfcPDFFontStrokeMode(Enum):
    EpfcPDFFontStrokeMode_nil = 2
    EpfcPDF_STROKE_ALL_FONTS = 0
    EpfcPDF_USE_TRUE_TYPE_FONTS = 1
    com = 'pfcls.pfcPDFFontStrokeMode.1'


class EpfcPDFColorDepth(Enum):
    EpfcPDFColorDepth_nil = 3
    EpfcPDF_CD_COLOR = 0
    EpfcPDF_CD_GRAY = 1
    EpfcPDF_CD_MONO = 2
    com = 'pfcls.pfcPDFColorDepth.1'


class EpfcPDFHiddenLineMode(Enum):
    EpfcPDFHiddenLineMode_nil = 2
    EpfcPDF_HLM_DASHED = 1
    EpfcPDF_HLM_SOLID = 0
    com = 'pfcls.pfcPDFHiddenLineMode.1'


class EpfcPDFLayerMode(Enum):
    EpfcPDFLayerMode_nil = 3
    EpfcPDF_LAYERS_ALL = 0
    EpfcPDF_LAYERS_NONE = 2
    EpfcPDF_LAYERS_VISIBLE = 1
    com = 'pfcls.pfcPDFLayerMode.1'


class EpfcPDFParameterMode(Enum):
    EpfcPDFParameterMode_nil = 3
    EpfcPDF_PARAMS_ALL = 0
    EpfcPDF_PARAMS_DESIGNATED = 1
    EpfcPDF_PARAMS_NONE = 2
    com = 'pfcls.pfcPDFParameterMode.1'


class EpfcPDFPrintingMode(Enum):
    EpfcPDFPrintingMode_nil = 2
    EpfcPDF_PRINTING_HIGH_RES = 1
    EpfcPDF_PRINTING_LOW_RES = 0
    com = 'pfcls.pfcPDFPrintingMode.1'


class EpfcPDFRestrictOperationsMode(Enum):
    EpfcPDFRestrictOperationsMode_nil = 5
    EpfcPDF_RESTRICT_COMMENT_FORM_SIGNING = 3
    EpfcPDF_RESTRICT_EXTRACTING = 4
    EpfcPDF_RESTRICT_FORMS_SIGNING = 1
    EpfcPDF_RESTRICT_INSERT_DELETE_ROTATE = 2
    EpfcPDF_RESTRICT_NONE = 0
    com = 'pfcls.pfcPDFRestrictOperationsMode.1'


class EpfcPDFLinecap(Enum):
    EpfcPDFLinecap_nil = 3
    EpfcPDF_LINECAP_BUTT = 0
    EpfcPDF_LINECAP_PROJECTING_SQUARE = 2
    EpfcPDF_LINECAP_ROUND = 1
    com = 'pfcls.pfcPDFLinecap.1'


class EpfcPDFLinejoin(Enum):
    EpfcPDFLinejoin_nil = 3
    EpfcPDF_LINEJOIN_BEVEL = 2
    EpfcPDF_LINEJOIN_MITER = 0
    EpfcPDF_LINEJOIN_ROUND = 1
    com = 'pfcls.pfcPDFLinejoin.1'


class EpfcPDFU3DLightingMode(Enum):
    EpfcPDFU3DLightingMode_nil = 11
    EpfcPDF_U3D_LIGHT_BLUE = 6
    EpfcPDF_U3D_LIGHT_BRIGHT = 3
    EpfcPDF_U3D_LIGHT_CAD = 9
    EpfcPDF_U3D_LIGHT_CUBE = 8
    EpfcPDF_U3D_LIGHT_DAY = 2
    EpfcPDF_U3D_LIGHT_HEADLAMP = 10
    EpfcPDF_U3D_LIGHT_NIGHT = 5
    EpfcPDF_U3D_LIGHT_NONE = 0
    EpfcPDF_U3D_LIGHT_PRIMARY = 4
    EpfcPDF_U3D_LIGHT_RED = 7
    EpfcPDF_U3D_LIGHT_WHITE = 1
    com = 'pfcls.pfcPDFU3DLightingMode.1'


class EpfcPDFU3DRenderMode(Enum):
    EpfcPDFU3DRenderMode_nil = 15
    EpfcPDF_U3D_RENDER_BOUNDING_BOX = 0
    EpfcPDF_U3D_RENDER_HIDDEN_WIREFRAME = 14
    EpfcPDF_U3D_RENDER_ILLUSTRATION = 11
    EpfcPDF_U3D_RENDER_SHADED_ILLUSTRATION = 13
    EpfcPDF_U3D_RENDER_SHADED_VERTICES = 4
    EpfcPDF_U3D_RENDER_SHADED_WIREFRAME = 6
    EpfcPDF_U3D_RENDER_SOLID = 7
    EpfcPDF_U3D_RENDER_SOLID_OUTLINE = 12
    EpfcPDF_U3D_RENDER_SOLID_WIREFRAME = 9
    EpfcPDF_U3D_RENDER_TRANSPARENT = 8
    EpfcPDF_U3D_RENDER_TRANSPARENT_BOUNDING_BOX = 1
    EpfcPDF_U3D_RENDER_TRANSPARENT_BOUNDING_BOX_OUTLINE = 2
    EpfcPDF_U3D_RENDER_TRANSPARENT_WIREFRAME = 10
    EpfcPDF_U3D_RENDER_VERTICES = 3
    EpfcPDF_U3D_RENDER_WIREFRAME = 5
    com = 'pfcls.pfcPDFU3DRenderMode.1'


class EpfcPDFSelectedViewMode(Enum):
    EpfcPDFSelectedViewMode_nil = 3
    EpfcPDF_VIEW_SELECT_ALL = 1
    EpfcPDF_VIEW_SELECT_BY_NAME = 2
    EpfcPDF_VIEW_SELECT_CURRENT = 0
    com = 'pfcls.pfcPDFSelectedViewMode.1'


class EpfcPDFExportMode(Enum):
    EpfcPDFExportMode_nil = 4
    EpfcPDF_2D_DRAWING = 0
    EpfcPDF_3D_AS_NAMED_VIEWS = 1
    EpfcPDF_3D_AS_U3D = 3
    EpfcPDF_3D_AS_U3D_PDF = 2
    com = 'pfcls.pfcPDFExportMode.1'


class EpfcPrintSheets(Enum):
    EpfcPRINT_ALL_SHEETS = 1
    EpfcPRINT_CURRENT_SHEET = 0
    EpfcPRINT_SELECTED_SHEETS = 2
    EpfcPrintSheets_nil = 3
    com = 'pfcls.pfcPrintSheets.1'


class EpfcPDFAnnotMode(Enum):
    EpfcPDFAnnotMode_nil = 2
    EpfcPDF_EXCLUDE_ANNOTATION = 0
    EpfcPDF_INCLUDE_ANNOTATION = 1
    com = 'pfcls.pfcPDFAnnotMode.1'


class EpfcPDFOptionType(Enum):
    EpfcPDFOPT_ADD_VIEWS = 42
    EpfcPDFOPT_ALLOW_ACCESSIBILITY = 24
    EpfcPDFOPT_ALLOW_COPYING = 23
    EpfcPDFOPT_ALLOW_MODE = 22
    EpfcPDFOPT_ALLOW_PRINTING = 20
    EpfcPDFOPT_ALLOW_PRINTING_MODE = 21
    EpfcPDFOPT_AUTHOR = 14
    EpfcPDFOPT_BACKGROUND_COLOR_BLUE = 41
    EpfcPDFOPT_BACKGROUND_COLOR_GREEN = 40
    EpfcPDFOPT_BACKGROUND_COLOR_RED = 39
    EpfcPDFOPT_BOOKMARK_FLAG_NOTES = 12
    EpfcPDFOPT_BOOKMARK_SHEETS = 11
    EpfcPDFOPT_BOOKMARK_VIEWS = 10
    EpfcPDFOPT_BOOKMARK_ZONES = 9
    EpfcPDFOPT_COLOR_DEPTH = 1
    EpfcPDFOPT_EXPORT_MODE = 30
    EpfcPDFOPT_FONT_STROKE = 0
    EpfcPDFOPT_HEIGHT = 34
    EpfcPDFOPT_HIDDENLINE_MODE = 2
    EpfcPDFOPT_HYPERLINKS = 8
    EpfcPDFOPT_INCL_ANNOT = 46
    EpfcPDFOPT_KEYWORDS = 16
    EpfcPDFOPT_LAUNCH_VIEWER = 5
    EpfcPDFOPT_LAYER_MODE = 6
    EpfcPDFOPT_LEFT_MARGIN = 38
    EpfcPDFOPT_LIGHT_DEFAULT = 31
    EpfcPDFOPT_LINECAP = 26
    EpfcPDFOPT_LINEJOIN = 27
    EpfcPDFOPT_MASTER_PASSWORD = 18
    EpfcPDFOPT_ORIENTATION = 36
    EpfcPDFOPT_PARAM_MODE = 7
    EpfcPDFOPT_PASSWORD_TO_OPEN = 17
    EpfcPDFOPT_PDF_SAVE = 45
    EpfcPDFOPT_PENTABLE = 25
    EpfcPDFOPT_RASTER_DPI = 4
    EpfcPDFOPT_RENDER_STYLE_DEFAULT = 32
    EpfcPDFOPT_RESTRICT_OPERATIONS = 19
    EpfcPDFOPT_SEARCHABLE_TEXT = 3
    EpfcPDFOPT_SELECTED_VIEW = 44
    EpfcPDFOPT_SHEETS = 28
    EpfcPDFOPT_SHEET_RANGE = 29
    EpfcPDFOPT_SIZE = 33
    EpfcPDFOPT_SUBJECT = 15
    EpfcPDFOPT_TITLE = 13
    EpfcPDFOPT_TOP_MARGIN = 37
    EpfcPDFOPT_VIEW_TO_EXPORT = 43
    EpfcPDFOPT_WIDTH = 35
    EpfcPDFOptionType_nil = 47
    com = 'pfcls.pfcPDFOptionType.1'


class EpfcPrintSaveMethod(Enum):
    EpfcPRINT_SAVE_APPEND_TO_FILE = 3
    EpfcPRINT_SAVE_MULTIPLE_FILE = 2
    EpfcPRINT_SAVE_NULL = 0
    EpfcPRINT_SAVE_SINGLE_FILE = 1
    EpfcPrintSaveMethod_nil = 4
    com = 'pfcls.pfcPrintSaveMethod.1'


class EpfcProductViewFormat(Enum):
    EpfcPV_FORMAT_ED = 1
    EpfcPV_FORMAT_EDZ = 2
    EpfcPV_FORMAT_PVS = 0
    EpfcPV_FORMAT_PVZ = 3
    EpfcProductViewFormat_nil = 4
    com = 'pfcls.pfcProductViewFormat.1'


class EpfcNewModelImportType(Enum):
    EpfcIMPORT_NEW_3MF = 28
    EpfcIMPORT_NEW_ACIS = 11
    EpfcIMPORT_NEW_CADDS = 3
    EpfcIMPORT_NEW_CATIA_CGR = 18
    EpfcIMPORT_NEW_CATIA_MODEL = 9
    EpfcIMPORT_NEW_CATIA_PART = 14
    EpfcIMPORT_NEW_CATIA_PRODUCT = 15
    EpfcIMPORT_NEW_CATIA_SESSION = 8
    EpfcIMPORT_NEW_CC = 24
    EpfcIMPORT_NEW_DXF = 10
    EpfcIMPORT_NEW_ICEM = 13
    EpfcIMPORT_NEW_IGES = 0
    EpfcIMPORT_NEW_INVENTOR_ASSEM = 23
    EpfcIMPORT_NEW_INVENTOR_PART = 22
    EpfcIMPORT_NEW_JT = 19
    EpfcIMPORT_NEW_NEUTRAL = 2
    EpfcIMPORT_NEW_PARASOLID = 12
    EpfcIMPORT_NEW_POLTXT = 7
    EpfcIMPORT_NEW_PRODUCTVIEW = 17
    EpfcIMPORT_NEW_SEDGE_ASSEMBLY = 26
    EpfcIMPORT_NEW_SEDGE_PART = 25
    EpfcIMPORT_NEW_SEDGE_SHEETMETAL_PART = 27
    EpfcIMPORT_NEW_STEP = 4
    EpfcIMPORT_NEW_STL = 5
    EpfcIMPORT_NEW_SW_ASSEM = 21
    EpfcIMPORT_NEW_SW_PART = 20
    EpfcIMPORT_NEW_UG = 16
    EpfcIMPORT_NEW_VDA = 1
    EpfcIMPORT_NEW_VRML = 6
    EpfcNewModelImportType_nil = 29
    com = 'pfcls.pfcNewModelImportType.1'


class EpfcImportAction(Enum):
    EpfcIMPORT_LAYER_BLANK = 2
    EpfcIMPORT_LAYER_DISPLAY = 0
    EpfcIMPORT_LAYER_HIDDEN = 5
    EpfcIMPORT_LAYER_IGNORE = 3
    EpfcIMPORT_LAYER_NORMAL = 4
    EpfcIMPORT_LAYER_SKIP = 1
    EpfcImportAction_nil = 6
    com = 'pfcls.pfcImportAction.1'


class EpfcMessageDialogType(Enum):
    EpfcMESSAGE_ERROR = 0
    EpfcMESSAGE_INFO = 1
    EpfcMESSAGE_QUESTION = 3
    EpfcMESSAGE_WARNING = 2
    EpfcMessageDialogType_nil = 4
    com = 'pfcls.pfcMessageDialogType.1'


class EpfcMessageButton(Enum):
    EpfcMESSAGE_BUTTON_ABORT = 4
    EpfcMESSAGE_BUTTON_CANCEL = 1
    EpfcMESSAGE_BUTTON_CONFIRM = 7
    EpfcMESSAGE_BUTTON_IGNORE = 6
    EpfcMESSAGE_BUTTON_NO = 3
    EpfcMESSAGE_BUTTON_OK = 0
    EpfcMESSAGE_BUTTON_RETRY = 5
    EpfcMESSAGE_BUTTON_YES = 2
    EpfcMessageButton_nil = 8
    com = 'pfcls.pfcMessageButton.1'


class EpfcServerDependency(Enum):
    EpfcSERVER_DEPENDENCY_ALL = 0
    EpfcSERVER_DEPENDENCY_NONE = 2
    EpfcSERVER_DEPENDENCY_REQUIRED = 1
    EpfcServerDependency_nil = 3
    com = 'pfcls.pfcServerDependency.1'


class EpfcServerIncludeInstances(Enum):
    EpfcSERVER_INCLUDE_ALL = 0
    EpfcSERVER_INCLUDE_NONE = 2
    EpfcSERVER_INCLUDE_SELECTED = 1
    EpfcServerIncludeInstances_nil = 3
    com = 'pfcls.pfcServerIncludeInstances.1'


class EpfcServerAutoresolveOption(Enum):
    EpfcSERVER_AUTORESOLVE_IGNORE = 1
    EpfcSERVER_AUTORESOLVE_UPDATE_IGNORE = 2
    EpfcSERVER_DONT_AUTORESOLVE = 0
    EpfcServerAutoresolveOption_nil = 3
    com = 'pfcls.pfcServerAutoresolveOption.1'


class EpfcModelCheckMode(Enum):
    EpfcMODELCHECK_GRAPHICS = 0
    EpfcMODELCHECK_NO_GRAPHICS = 1
    EpfcModelCheckMode_nil = 2
    com = 'pfcls.pfcModelCheckMode.1'


class EpfcWSImportExportMessageType(Enum):
    EpfcWSIMPEX_MSG_CONFLICT = 2
    EpfcWSIMPEX_MSG_ERROR = 3
    EpfcWSIMPEX_MSG_INFO = 0
    EpfcWSIMPEX_MSG_WARNING = 1
    EpfcWSImportExportMessageType_nil = 4
    com = 'pfcls.pfcWSImportExportMessageType.1'


class EpfcParamType(Enum):
    EpfcALL_PARAMS = 5
    EpfcDIMTOL_PARAM = 3
    EpfcDIM_PARAM = 1
    EpfcGTOL_PARAM = 6
    EpfcPATTERN_PARAM = 2
    EpfcParamType_nil = 8
    EpfcREFDIM_PARAM = 4
    EpfcSURFFIN_PARAM = 7
    EpfcUSER_PARAM = 0
    com = 'pfcls.pfcParamType.1'


class EpfcFileListOpt(Enum):
    EpfcFILE_LIST_ALL = 0
    EpfcFILE_LIST_ALL_INST = 2
    EpfcFILE_LIST_LATEST = 1
    EpfcFILE_LIST_LATEST_INST = 3
    EpfcFileListOpt_nil = 4
    com = 'pfcls.pfcFileListOpt.1'


class EpfcRelCriterion(Enum):
    EpfcFILE_INCLUDE_ALL = 0
    EpfcFILE_INCLUDE_NONE = 2
    EpfcFILE_INCLUDE_REQUIRED = 1
    EpfcRelCriterion_nil = 3
    com = 'pfcls.pfcRelCriterion.1'


class EpfcMouseButton(Enum):
    EpfcMOUSE_BTN_ANY = 4
    EpfcMOUSE_BTN_LEFT = 0
    EpfcMOUSE_BTN_LEFT_DOUBLECLICK = 3
    EpfcMOUSE_BTN_MIDDLE = 1
    EpfcMOUSE_BTN_MOVE = 5
    EpfcMOUSE_BTN_RIGHT = 2
    EpfcMouseButton_nil = 6
    com = 'pfcls.pfcMouseButton.1'


class EpfcCreoCompatibility(Enum):
    EpfcC3Compatible = 3
    EpfcC4Compatible = 4
    EpfcCompatibilityUndefined = -1
    EpfcCreoCompatibility_nil = 5
    com = 'pfcls.pfcCreoCompatibility.1'


class EpfcTerminationStatus(Enum):
    EpfcTERM_ABNORMAL = 1
    EpfcTERM_EXIT = 0
    EpfcTERM_SIGNAL = 2
    EpfcTerminationStatus_nil = 3
    com = 'pfcls.pfcTerminationStatus.1'
