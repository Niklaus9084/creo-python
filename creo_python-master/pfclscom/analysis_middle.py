# -*- coding:utf-8 -*-

import re
from win32com.client import Dispatch, constants
import PfclsAPI

def middle_creatable():
    with open('middle.py', 'r') as fi:
        files = fi.readlines()
        fi.close()
    with open('middle_creatable.txt', 'w') as fo:
        fo.write('coclass name --->creatable name \n')
        name_pattern = re.compile(r"^# This CoClass is known by the name '(.*)'")
        coclass_pattern = re.compile(r'^class (.*)\(CoClassBaseClass\)')
        for index, line in enumerate(files):
            if name_pattern.match(line):
                fo.write(coclass_pattern.match(files[index+1]).group(1) + '--->')
                fo.write(name_pattern.match(line).group(1) + '\n')
def gen_PfclsAPI():
    with open('middle.py', 'r') as fi:
        files = fi.readlines()
        fi.close()
    with open('PfclsAPI.py', 'w') as fo:
        name_pattern = re.compile(r"^# This CoClass is known by the name (.*)")
        coclass_pattern = re.compile(r'^class (.*)\(CoClassBaseClass\)')
        for index, line in enumerate(files):
            if name_pattern.match(line):
                fo.write(coclass_pattern.match(files[index+1]).group(1) + ' = ')
                fo.write(name_pattern.match(line).group(1) + '\n')

def middle_noncreatable():
    with open('middle.py', 'r') as fi:
        files = fi.readlines()
        fi.close()
    with open('middle_noncreatable.txt', 'w') as fo:
        fo.write('coclass name --->noncreatable \n')
        name_pattern = re.compile(r"^# This CoClass is known by the name '(.*)'")
        coclass_pattern = re.compile(r'^class (.*)\(CoClassBaseClass\)')
        for index, line in enumerate(files):
            match = coclass_pattern.match(line)
            if match and name_pattern.match(files[index-1]) is None:
                fo.write(match.group(1) + '\n')


    fo.close()

def pfcls_creatable():
    dic = read_pfcls()
    middle_creatable()
    with open('pfcls_creatable.txt', 'w') as fo:
        with open('middle_creatable.txt', 'r') as fm:
            coclass_pattern = re.compile(r'(.*)--->(.*)')
            for line in fm:
                match = coclass_pattern.match(line)
                coclass_name = match.group(1)
                if coclass_name in dic.keys():
                    fo.write(dic[coclass_name] + match.group(2) + '\n')
    fm.close()
    fo.close()
def pfcls_noncratable():
    dic = read_pfcls()
    middle_noncreatable()
    with open('pfcls_noncreatable.txt', 'w') as fo:
        with open('middle_noncreatable.txt') as fm:
            for line in fm:
                coclass_name = line[:-1]
                if coclass_name in dic.keys():
                    fo.write(dic[coclass_name] + '\n')

    fm.close()
    fo.close()
def read_pfcls():
    with open('pfclscom.IDL', 'r') as f:
        pfcls = f.read()
        pattern = re.compile(r'    \[.*?\};', re.S)
        list = pattern.findall(pfcls)
        co_pattern = re.compile(r'.*coclass (.*?) .*', re.S)
        dic = {}
        for item in list:
            co_name = co_pattern.match(item)
            if co_name:
                dic[co_name.group(1)] = item
    f.close()
    return dic



if __name__ == '__main__':
    pass
