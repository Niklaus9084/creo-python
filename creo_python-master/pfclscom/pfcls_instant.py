# -*- coding:utf-8 -*-
from enum import Enum
class Enumerated(Enum):
	EpfcStdColor = {
			'type': 'Enumerated Types',
			'value': {
				'COLOR_BACKGROUND': 3,
				'COLOR_CURVE': 10,
				'COLOR_DATUM': 16,
				'COLOR_DIMMED': 6,
				'COLOR_DRAWING': 2,
				'COLOR_EDGE_HIGHLIGHT': 5,
				'COLOR_ERROR': 7,
				'COLOR_HALF_TONE': 4,
				'COLOR_HIGHLIGHT': 1,
				'COLOR_LETTER': 0,
				'COLOR_LWW': 18,
				'COLOR_MAX': 19,
				'COLOR_PRESEL_HIGHLIGHT': 11,
				'COLOR_PREVIEW': 14,
				'COLOR_QUILT': 17,
				'COLOR_SECONDARY_PREVIEW': 15,
				'COLOR_SECONDARY_SELECTED': 13,
				'COLOR_SELECTED': 12,
				'COLOR_SHADED_EDGE': 20,
				'COLOR_SHEETMETAL': 9,
				'COLOR_SS_CHILD_OF_MODIFIED_IN_DMA': 38,
				'COLOR_SS_CONF_ASM_NODES': 37,
				'COLOR_SS_CURRENT_DESIGN_SOL': 34,
				'COLOR_SS_DRIVEN_BY_PROPROGRAM': 32,
				'COLOR_SS_FAILED_ITEMS': 23,
				'COLOR_SS_FROZEN_COMPONENT': 22,
				'COLOR_SS_FT_GENERICS': 29,
				'COLOR_SS_FT_INSTANCES': 28,
				'COLOR_SS_HAS_FROZEN_FEAT': 21,
				'COLOR_SS_HAS_PROPROGRAM': 30,
				'COLOR_SS_HAS_PROPROGRAM_INPUT': 31,
				'COLOR_SS_INTCH_GROUP_MEMBERS': 26,
				'COLOR_SS_MODULE_NODES': 33,
				'COLOR_SS_NONCURRENT_DESIGN_SOL': 35,
				'COLOR_SS_PACKAGED': 24,
				'COLOR_SS_RECENT_SEARCH': 25,
				'COLOR_SS_REPLACED_COMPS': 27,
				'COLOR_SS_REPRESENTATIVE_DESIGN_SOL': 36,
				'COLOR_WARNING': 8,
				'StdColor_nil': 39,
			},
			'com': 'pfcls.pfcStdColor.1',
	}
	EpfcStdLineStyle = {
			'type': 'Enumerated Types',
			'value': {
				'LINE_CENTERLINE': 2,
				'LINE_CTRL_L_L': 6,
				'LINE_CTRL_MID_L': 10,
				'LINE_CTRL_S_L': 5,
				'LINE_CTRL_S_S': 7,
				'LINE_DASH': 4,
				'LINE_DASH_S_S': 8,
				'LINE_DOTTED': 1,
				'LINE_INTMIT_LWW_HIDDEN': 11,
				'LINE_PDFHIDDEN_LINESTYLE': 12,
				'LINE_PHANTOM': 3,
				'LINE_PHANTOM_S_S': 9,
				'LINE_SOLID': 0,
				'StdLineStyle_nil': 13,
			},
			'com': 'pfcls.pfcStdLineStyle.1',
	}
	EpfcCoordAxis = {
			'type': 'Enumerated Types',
			'value': {
				'COORD_AXIS_X': 0,
				'COORD_AXIS_Y': 1,
				'COORD_AXIS_Z': 2,
				'CoordAxis_nil': 3,
			},
			'com': 'pfcls.pfcCoordAxis.1',
	}
	EpfcPlacement = {
			'type': 'Enumerated Types',
			'value': {
				'PLACE_INSIDE': 0,
				'PLACE_ON_BOUNDARY': 1,
				'PLACE_OUTSIDE': 2,
				'Placement_nil': 3,
			},
			'com': 'pfcls.pfcPlacement.1',
	}
	EpfcActionType = {
			'type': 'Enumerated Types',
			'value': {
				'ActionType_nil': 51,
				'DIM_MODIFY_VALUE_PRE': 12,
				'DIRECTORY_CHANGE_POST': 41,
				'FEATURE_COPY_POST': 20,
				'FEATURE_CREATE_POST': 37,
				'FEATURE_CREATE_PRE': 36,
				'FEATURE_DELETE_POST': 38,
				'FEATURE_DELETE_PRE': 13,
				'FEATURE_REDEFINE_PRE': 19,
				'FEATURE_REGEN_FAILURE': 18,
				'FEATURE_REGEN_POST': 15,
				'FEATURE_REGEN_PRE': 14,
				'FEATURE_SUPPRESS_POST': 17,
				'FEATURE_SUPPRESS_PRE': 16,
				'MDL_CREATE_POST': 33,
				'MDL_DELETE_POST': 25,
				'MDL_DELETE_POST_ALL': 29,
				'MDL_DELETE_PRE': 50,
				'MDL_DISPLAY_POST': 43,
				'MDL_DISPLAY_PRE': 32,
				'MDL_ERASE_PRE': 47,
				'MDL_PURGE_POST': 49,
				'MDL_PURGE_PRE': 48,
				'MODEL_COPY_POST': 21,
				'MODEL_COPY_POST_ALL': 22,
				'MODEL_COPY_PRE': 45,
				'MODEL_ERASE_POST': 24,
				'MODEL_ERASE_POST_ALL': 28,
				'MODEL_RENAME_POST': 23,
				'MODEL_RENAME_POST_ALL': 11,
				'MODEL_RENAME_PRE': 46,
				'MODEL_REPLACE_POST': 8,
				'MODEL_RETRIEVE_POST': 30,
				'MODEL_RETRIEVE_POST_ALL': 31,
				'MODEL_RETRIEVE_PRE': 9,
				'MODEL_SAVE_POST': 26,
				'MODEL_SAVE_POST_ALL': 27,
				'MODEL_SAVE_PRE': 44,
				'MODEL_SAVE_PRE_ALL': 10,
				'PARAM_CREATE_PRE': 1,
				'PARAM_CREATE_W_UNITS_PRE': 4,
				'PARAM_DELETE_PRE': 3,
				'PARAM_DELETE_W_UNITS_POST': 7,
				'PARAM_MODIFY_PRE': 2,
				'PARAM_MODIFY_W_UNITS_POST': 6,
				'PARAM_MODIFY_W_UNITS_PRE': 5,
				'POPUPMENU_CREATE_POST': 0,
				'SOLID_REGEN_POST': 35,
				'SOLID_REGEN_PRE': 34,
				'SOLID_UNIT_CONVERT_POST': 40,
				'SOLID_UNIT_CONVERT_PRE': 39,
				'WINDOW_CHANGE_POST': 42,
			},
			'com': 'pfcls.pfcActionType.1',
	}
	EpfcUnitType = {
			'type': 'Enumerated Types',
			'value': {
				'UNIT_ANGLE': 5,
				'UNIT_FORCE': 2,
				'UNIT_LENGTH': 0,
				'UNIT_MASS': 1,
				'UNIT_TEMPERATURE': 4,
				'UNIT_TIME': 3,
				'UnitType_nil': 6,
			},
			'com': 'pfcls.pfcUnitType.1',
	}
	EpfcLengthUnitType = {
			'type': 'Enumerated Types',
			'value': {
				'LENGTHUNIT_CM': 3,
				'LENGTHUNIT_FOOT': 1,
				'LENGTHUNIT_INCH': 0,
				'LENGTHUNIT_KM': 7,
				'LENGTHUNIT_M': 4,
				'LENGTHUNIT_MCM': 5,
				'LENGTHUNIT_MILE': 8,
				'LENGTHUNIT_MM': 2,
				'LENGTHUNIT_NM': 6,
				'LengthUnitType_nil': 9,
			},
			'com': 'pfcls.pfcLengthUnitType.1',
	}
	EpfcMassUnitType = {
			'type': 'Enumerated Types',
			'value': {
				'MASSUNIT_GRAM': 3,
				'MASSUNIT_KILOGRAM': 4,
				'MASSUNIT_OUNCE': 0,
				'MASSUNIT_POUND': 1,
				'MASSUNIT_TON': 2,
				'MASSUNIT_TONNE': 5,
				'MassUnitType_nil': 6,
			},
			'com': 'pfcls.pfcMassUnitType.1',
	}
	EpfcDisplayStyle = {
			'type': 'Enumerated Types',
			'value': {
				'DISPSTYLE_DEFAULT': 0,
				'DISPSTYLE_FOLLOW_ENVIRONMENT': 5,
				'DISPSTYLE_HIDDEN_LINE': 2,
				'DISPSTYLE_NO_HIDDEN': 3,
				'DISPSTYLE_SHADED': 4,
				'DISPSTYLE_SHADED_WITH_EDGES': 6,
				'DISPSTYLE_WIREFRAME': 1,
				'DisplayStyle_nil': 7,
			},
			'com': 'pfcls.pfcDisplayStyle.1',
	}
	EpfcTangentEdgeDisplayStyle = {
			'type': 'Enumerated Types',
			'value': {
				'TANEDGE_CENTERLINE': 2,
				'TANEDGE_DEFAULT': 0,
				'TANEDGE_DIMMED': 4,
				'TANEDGE_NONE': 1,
				'TANEDGE_PHANTOM': 3,
				'TANEDGE_SOLID': 5,
				'TangentEdgeDisplayStyle_nil': 6,
			},
			'com': 'pfcls.pfcTangentEdgeDisplayStyle.1',
	}
	EpfcCableDisplayStyle = {
			'type': 'Enumerated Types',
			'value': {
				'CABLEDISP_CENTERLINE': 1,
				'CABLEDISP_DEFAULT': 0,
				'CABLEDISP_THICK': 2,
				'CableDisplayStyle_nil': 3,
			},
			'com': 'pfcls.pfcCableDisplayStyle.1',
	}
	EpfcDatumSide = {
			'type': 'Enumerated Types',
			'value': {
				'DATUM_SIDE_NONE': 0,
				'DATUM_SIDE_RED': 1,
				'DATUM_SIDE_YELLOW': 2,
				'DatumSide_nil': 3,
			},
			'com': 'pfcls.pfcDatumSide.1',
	}
	EpfcDimDisplayMode = {
			'type': 'Enumerated Types',
			'value': {
				'DIM_DISPLAY_NUMERIC': 0,
				'DIM_DISPLAY_SYMBOLIC': 1,
				'DimDisplayMode_nil': 2,
			},
			'com': 'pfcls.pfcDimDisplayMode.1',
	}
	EpfcUnitSystemType = {
			'type': 'Enumerated Types',
			'value': {
				'UNIT_SYSTEM_FORCE_LENGTH_TIME': 1,
				'UNIT_SYSTEM_MASS_LENGTH_TIME': 0,
				'UnitSystemType_nil': 2,
			},
			'com': 'pfcls.pfcUnitSystemType.1',
	}
	EpfcUnitDimensionConversion = {
			'type': 'Enumerated Types',
			'value': {
				'UNITCONVERT_SAME_DIMS': 0,
				'UNITCONVERT_SAME_SIZE': 1,
				'UnitDimensionConversion_nil': 2,
			},
			'com': 'pfcls.pfcUnitDimensionConversion.1',
	}
	EpfcParamValueType = {
			'type': 'Enumerated Types',
			'value': {
				'PARAM_BOOLEAN': 2,
				'PARAM_DOUBLE': 3,
				'PARAM_INTEGER': 1,
				'PARAM_NOTE': 4,
				'PARAM_NOT_SET': 6,
				'PARAM_STRING': 0,
				'PARAM_VOID': 5,
				'ParamValueType_nil': 7,
			},
			'com': 'pfcls.pfcParamValueType.1',
	}
	EpfcParameterDriverType = {
			'type': 'Enumerated Types',
			'value': {
				'PARAMDRIVER_FUNCTION': 1,
				'PARAMDRIVER_PARAM': 0,
				'PARAMDRIVER_RELATION': 2,
				'ParameterDriverType_nil': 3,
			},
			'com': 'pfcls.pfcParameterDriverType.1',
	}
	EpfcParameterLimitType = {
			'type': 'Enumerated Types',
			'value': {
				'PARAMLIMIT_GREATER_THAN': 2,
				'PARAMLIMIT_GREATER_THAN_OR_EQUAL': 3,
				'PARAMLIMIT_LESS_THAN': 0,
				'PARAMLIMIT_LESS_THAN_OR_EQUAL': 1,
				'PARAMLIMIT_UNLIMITED': 4,
				'ParameterLimitType_nil': 5,
			},
			'com': 'pfcls.pfcParameterLimitType.1',
	}
	EpfcParameterSelectionContext = {
			'type': 'Enumerated Types',
			'value': {
				'PARAMSELECT_ALLOW_SUBITEM_SELECTION': 12,
				'PARAMSELECT_ASM': 2,
				'PARAMSELECT_COMPONENT': 11,
				'PARAMSELECT_COMPOSITE_CURVE': 8,
				'PARAMSELECT_CURVE': 7,
				'PARAMSELECT_EDGE': 4,
				'PARAMSELECT_FEATURE': 3,
				'PARAMSELECT_INHERITED': 9,
				'PARAMSELECT_MODEL': 0,
				'PARAMSELECT_PART': 1,
				'PARAMSELECT_QUILT': 6,
				'PARAMSELECT_SKELETON': 10,
				'PARAMSELECT_SURFACE': 5,
				'ParameterSelectionContext_nil': 13,
			},
			'com': 'pfcls.pfcParameterSelectionContext.1',
	}
	EpfcRestrictionType = {
			'type': 'Enumerated Types',
			'value': {
				'PARAMSELECT_ENUMERATION': 0,
				'PARAMSELECT_RANGE': 1,
				'RestrictionType_nil': 2,
			},
			'com': 'pfcls.pfcRestrictionType.1',
	}
	EpfcModelItemType = {
			'type': 'Enumerated Types',
			'value': {
				'ITEM_ANNOTATION_ELEM': 33,
				'ITEM_ANNOT_PLANE': 32,
				'ITEM_AXIS': 4,
				'ITEM_COMBINED_STATE': 27,
				'ITEM_COORD_SYS': 3,
				'ITEM_CURVE': 7,
				'ITEM_DIMENSION': 10,
				'ITEM_DTL_ENTITY': 15,
				'ITEM_DTL_GROUP': 17,
				'ITEM_DTL_NOTE': 16,
				'ITEM_DTL_OLE_OBJECT': 20,
				'ITEM_DTL_SYM_DEFINITION': 18,
				'ITEM_DTL_SYM_INSTANCE': 19,
				'ITEM_EDGE': 2,
				'ITEM_EDGE_END': 24,
				'ITEM_EDGE_START': 22,
				'ITEM_EXPLODED_STATE': 21,
				'ITEM_FEATURE': 0,
				'ITEM_GTOL': 35,
				'ITEM_LAYER': 8,
				'ITEM_LAYER_STATE': 26,
				'ITEM_LOG_EDGE': 23,
				'ITEM_NOTE': 9,
				'ITEM_POINT': 5,
				'ITEM_QUILT': 6,
				'ITEM_REF_DIMENSION': 11,
				'ITEM_RP_MATERIAL': 29,
				'ITEM_SET_DATUM_TAG': 34,
				'ITEM_SIMPREP': 12,
				'ITEM_SOLID_GEOMETRY': 13,
				'ITEM_STYLE_STATE': 28,
				'ITEM_SURFACE': 1,
				'ITEM_SURF_FIN': 31,
				'ITEM_TABLE': 14,
				'ITEM_VIEW': 30,
				'ITEM_XSEC': 25,
				'ModelItemType_nil': 36,
			},
			'com': 'pfcls.pfcModelItemType.1',
	}
	EpfcDisplayStatus = {
			'type': 'Enumerated Types',
			'value': {
				'DisplayStatus_nil': 4,
				'LAYER_BLANK': 2,
				'LAYER_DISPLAY': 1,
				'LAYER_HIDDEN': 3,
				'LAYER_NORMAL': 0,
			},
			'com': 'pfcls.pfcDisplayStatus.1',
	}
	EpfcExternalDataType = {
			'type': 'Enumerated Types',
			'value': {
				'EXTDATA_CHAPTER': 4,
				'EXTDATA_DOUBLE': 1,
				'EXTDATA_INTEGER': 0,
				'EXTDATA_STREAM': 3,
				'EXTDATA_STRING': 2,
				'ExternalDataType_nil': 5,
			},
			'com': 'pfcls.pfcExternalDataType.1',
	}
	EpfcDimensionType = {
			'type': 'Enumerated Types',
			'value': {
				'DIM_ANGULAR': 3,
				'DIM_DIAMETER': 2,
				'DIM_LINEAR': 0,
				'DIM_RADIAL': 1,
				'DimensionType_nil': 4,
			},
			'com': 'pfcls.pfcDimensionType.1',
	}
	EpfcDimToleranceType = {
			'type': 'Enumerated Types',
			'value': {
				'DIMTOL_ISODIN': 4,
				'DIMTOL_LIMITS': 0,
				'DIMTOL_PLUS_MINUS': 1,
				'DIMTOL_SYMMETRIC': 2,
				'DIMTOL_SYMMETRIC_SUPERSCRIPT': 3,
				'DimToleranceType_nil': 5,
			},
			'com': 'pfcls.pfcDimToleranceType.1',
	}
	EpfcToleranceTableType = {
			'type': 'Enumerated Types',
			'value': {
				'TOLTABLE_BROKEN_EDGE': 1,
				'TOLTABLE_GENERAL': 0,
				'TOLTABLE_HOLES': 3,
				'TOLTABLE_SHAFTS': 2,
				'ToleranceTableType_nil': 4,
			},
			'com': 'pfcls.pfcToleranceTableType.1',
	}
	EpfcDimSenseType = {
			'type': 'Enumerated Types',
			'value': {
				'DIMENSION_SENSE_ANGLE': 5,
				'DIMENSION_SENSE_LINEAR_TO_ARC_OR_CIRCLE_TANGENT': 4,
				'DIMENSION_SENSE_NONE': 0,
				'DIMENSION_SENSE_POINT': 1,
				'DIMENSION_SENSE_POINT_TO_ANGLE': 6,
				'DIMENSION_SENSE_SPLINE_PT': 2,
				'DIMENSION_SENSE_TANGENT_INDEX': 3,
				'DimSenseType_nil': 7,
			},
			'com': 'pfcls.pfcDimSenseType.1',
	}
	EpfcDimPointType = {
			'type': 'Enumerated Types',
			'value': {
				'DIMENSION_POINT_CENTER': 2,
				'DIMENSION_POINT_END1': 0,
				'DIMENSION_POINT_END2': 1,
				'DIMENSION_POINT_MIDPOINT': 4,
				'DIMENSION_POINT_NONE': 3,
				'DimPointType_nil': 5,
			},
			'com': 'pfcls.pfcDimPointType.1',
	}
	EpfcDimLineAOCTangentType = {
			'type': 'Enumerated Types',
			'value': {
				'DimLineAOCTangentType_nil': 4,
				'TANGENT_LEFT_OTHER_SIDE': 2,
				'TANGENT_LEFT_SAME_SIDE': 0,
				'TANGENT_RIGHT_OTHER_SIDE': 3,
				'TANGENT_RIGHT_SAME_SIDE': 1,
			},
			'com': 'pfcls.pfcDimLineAOCTangentType.1',
	}
	EpfcDimOrientationHint = {
			'type': 'Enumerated Types',
			'value': {
				'DimOrientationHint_nil': 10,
				'ORIENTATION_HINT_ARC_ANGLE': 6,
				'ORIENTATION_HINT_ARC_LENGTH': 7,
				'ORIENTATION_HINT_ELLIPSE_RADIUS1': 4,
				'ORIENTATION_HINT_ELLIPSE_RADIUS2': 5,
				'ORIENTATION_HINT_HORIZONTAL': 1,
				'ORIENTATION_HINT_LINE_TO_TANGENT_CURVE_ANGLE': 8,
				'ORIENTATION_HINT_NONE': 0,
				'ORIENTATION_HINT_RADIAL_DIFF': 9,
				'ORIENTATION_HINT_SLANTED': 3,
				'ORIENTATION_HINT_VERTICAL': 2,
			},
			'com': 'pfcls.pfcDimOrientationHint.1',
	}
	EpfcFeatureStatus = {
			'type': 'Enumerated Types',
			'value': {
				'FEAT_ACTIVE': 0,
				'FEAT_FAMILY_TABLE_SUPPRESSED': 2,
				'FEAT_INACTIVE': 1,
				'FEAT_PROGRAM_SUPPRESSED': 4,
				'FEAT_SIMP_REP_SUPPRESSED': 3,
				'FEAT_SUPPRESSED': 5,
				'FEAT_UNREGENERATED': 6,
				'FeatureStatus_nil': 7,
			},
			'com': 'pfcls.pfcFeatureStatus.1',
	}
	EpfcFeatureType = {
			'type': 'Enumerated Types',
			'value': {
				'FEATTYPE_ACCEPT_CRITERIA': 268,
				'FEATTYPE_ANALYSIS': 168,
				'FEATTYPE_ANALYT_GEOM': 261,
				'FEATTYPE_ANNOTATION': 235,
				'FEATTYPE_AREA_NIBBLE': 70,
				'FEATTYPE_ARTWORK': 246,
				'FEATTYPE_ASSEMBLY_CUT': 37,
				'FEATTYPE_ASSEMBLY_CUT_COPY': 145,
				'FEATTYPE_ASSY_MERGE': 209,
				'FEATTYPE_ASSY_WELD_NOTCH': 215,
				'FEATTYPE_ATTACH': 81,
				'FEATTYPE_ATTACH_VOLUME': 130,
				'FEATTYPE_AUTO_ROUND': 243,
				'FEATTYPE_AUXILIARY': 112,
				'FEATTYPE_BEAM_SECTION': 151,
				'FEATTYPE_BEND': 42,
				'FEATTYPE_BEND_BACK': 47,
				'FEATTYPE_BLD_OPERATION': 131,
				'FEATTYPE_BULK_OBJECT': 116,
				'FEATTYPE_CABLE': 67,
				'FEATTYPE_CABLE_COSMETIC': 87,
				'FEATTYPE_CABLE_LOCATION': 65,
				'FEATTYPE_CABLE_SEGMENT': 66,
				'FEATTYPE_CAV_DEVIATION': 160,
				'FEATTYPE_CAV_FIT': 159,
				'FEATTYPE_CAV_SCAN_SET': 158,
				'FEATTYPE_CE_COMP': 264,
				'FEATTYPE_CE_GEOM': 260,
				'FEATTYPE_CE_SKET': 273,
				'FEATTYPE_CHAMFER': 4,
				'FEATTYPE_CHANNEL': 69,
				'FEATTYPE_CMMCONSTR': 156,
				'FEATTYPE_CMMMEASURE_STEP': 155,
				'FEATTYPE_CMMVERIFY': 157,
				'FEATTYPE_COMPONENT': 89,
				'FEATTYPE_COMP_INTERFACE': 211,
				'FEATTYPE_CONTACT3D': 248,
				'FEATTYPE_CONT_MAP': 93,
				'FEATTYPE_COORD_SYS': 68,
				'FEATTYPE_CORE': 73,
				'FEATTYPE_CORNER_CHAMFER': 20,
				'FEATTYPE_COSMETIC': 23,
				'FEATTYPE_CROSS_SECTION': 82,
				'FEATTYPE_CURVE': 39,
				'FEATTYPE_CUSTOMIZE': 99,
				'FEATTYPE_CUT': 6,
				'FEATTYPE_CUT_MOTION': 98,
				'FEATTYPE_DAMPER': 241,
				'FEATTYPE_DATUM_AXIS': 16,
				'FEATTYPE_DATUM_PLANE': 13,
				'FEATTYPE_DATUM_POINT': 21,
				'FEATTYPE_DATUM_QUILT': 36,
				'FEATTYPE_DATUM_SURFACE': 32,
				'FEATTYPE_DECLARE': 123,
				'FEATTYPE_DEFORM_AREA': 146,
				'FEATTYPE_DERIVED_MEMBER': 267,
				'FEATTYPE_DESIGNATED_AREA': 244,
				'FEATTYPE_DOME': 12,
				'FEATTYPE_DOME2': 19,
				'FEATTYPE_DRAFT': 17,
				'FEATTYPE_DRAFT_LINE': 127,
				'FEATTYPE_DRILL': 30,
				'FEATTYPE_DRILL_GROUP': 85,
				'FEATTYPE_DRVD': 182,
				'FEATTYPE_DRV_TOOL_CURVE': 102,
				'FEATTYPE_DRV_TOOL_EDGE': 101,
				'FEATTYPE_DRV_TOOL_PROFILE': 165,
				'FEATTYPE_DRV_TOOL_SKETCH': 100,
				'FEATTYPE_DRV_TOOL_SURF': 103,
				'FEATTYPE_DRV_TOOL_TURN': 186,
				'FEATTYPE_DRV_TOOL_TWO_CNTR': 120,
				'FEATTYPE_DS_OPTIMIZE': 210,
				'FEATTYPE_EAR': 11,
				'FEATTYPE_EDGE_BEND': 164,
				'FEATTYPE_ETCH': 24,
				'FEATTYPE_EXPLODE_LINE': 166,
				'FEATTYPE_EXP_RATIO': 94,
				'FEATTYPE_EXTEND': 78,
				'FEATTYPE_EXTRACT': 74,
				'FEATTYPE_FIRST': 0,
				'FEATTYPE_FIXTURE_SETUP': 91,
				'FEATTYPE_FLANGE': 9,
				'FEATTYPE_FLATQLT': 185,
				'FEATTYPE_FLATTEN': 55,
				'FEATTYPE_FLAT_PAT': 92,
				'FEATTYPE_FLAT_RIBBON_SEGMENT': 148,
				'FEATTYPE_FLEXMOVE': 255,
				'FEATTYPE_FLEXSUBST': 258,
				'FEATTYPE_FLEX_MUTATOR': 234,
				'FEATTYPE_FLXATTACH': 257,
				'FEATTYPE_FLX_OGF': 263,
				'FEATTYPE_FLX_SOLVER': 272,
				'FEATTYPE_FORM': 45,
				'FEATTYPE_FREEFORM': 202,
				'FEATTYPE_FREE_FORM': 135,
				'FEATTYPE_FR_SYS': 198,
				'FEATTYPE_GENERAL_MERGE': 240,
				'FEATTYPE_GEOM_COPY': 167,
				'FEATTYPE_GLOBAL_MODEL': 229,
				'FEATTYPE_GRANITE_REMOVE_SRFS': 236,
				'FEATTYPE_GRANITE_TAPER_EXTR': 237,
				'FEATTYPE_GRANITE_TOOL_COMP': 238,
				'FEATTYPE_GRAPH': 52,
				'FEATTYPE_GROOVE': 34,
				'FEATTYPE_GROUP_HEAD': 181,
				'FEATTYPE_HOLE': 1,
				'FEATTYPE_HULL_BEAM': 191,
				'FEATTYPE_HULL_BRACKET': 196,
				'FEATTYPE_HULL_COLLAR': 194,
				'FEATTYPE_HULL_COMPT': 199,
				'FEATTYPE_HULL_CUTOUT': 189,
				'FEATTYPE_HULL_DRAW': 195,
				'FEATTYPE_HULL_ENDCUT': 192,
				'FEATTYPE_HULL_FOLDED_FLG': 197,
				'FEATTYPE_HULL_HOLE': 188,
				'FEATTYPE_HULL_MITRE': 230,
				'FEATTYPE_HULL_PAD': 227,
				'FEATTYPE_HULL_PLATE': 187,
				'FEATTYPE_HULL_REP_TMP': 205,
				'FEATTYPE_HULL_SLOTCUT': 231,
				'FEATTYPE_HULL_SPLIT_BOUND': 224,
				'FEATTYPE_HULL_STIFFENER': 190,
				'FEATTYPE_HULL_WELD_NOTCH': 217,
				'FEATTYPE_HULL_WLD_FLANGE': 193,
				'FEATTYPE_IMPORT': 22,
				'FEATTYPE_INSULATION': 206,
				'FEATTYPE_INTERNAL_UDF': 50,
				'FEATTYPE_INTERSECT': 80,
				'FEATTYPE_IPMQUILT': 179,
				'FEATTYPE_ISEGMENT': 86,
				'FEATTYPE_JOIN_WALLS': 271,
				'FEATTYPE_KERNEL': 203,
				'FEATTYPE_LINE_STOCK': 114,
				'FEATTYPE_LIP': 60,
				'FEATTYPE_LOC_PUSH': 14,
				'FEATTYPE_MANUAL_MILL': 61,
				'FEATTYPE_MATERIAL_REMOVAL': 104,
				'FEATTYPE_MEASURE': 126,
				'FEATTYPE_MERGE': 25,
				'FEATTYPE_MFGGATHER': 62,
				'FEATTYPE_MFGMERGE': 90,
				'FEATTYPE_MFGPTM_MATREM': 265,
				'FEATTYPE_MFGREFINE': 75,
				'FEATTYPE_MFGTRIM': 63,
				'FEATTYPE_MFGUSE_VOLUME': 64,
				'FEATTYPE_MILL': 29,
				'FEATTYPE_MOD_CHAMFER': 276,
				'FEATTYPE_MOD_ROUND': 259,
				'FEATTYPE_MOLD': 26,
				'FEATTYPE_MOLD_SHUTOFF_SRF': 269,
				'FEATTYPE_MOLD_SKIRT_EXT_FEAT': 274,
				'FEATTYPE_MOLD_SKIRT_FILL_FEAT': 275,
				'FEATTYPE_MOLD_SLIDER': 226,
				'FEATTYPE_NECK': 8,
				'FEATTYPE_OFFSET': 31,
				'FEATTYPE_OLE': 212,
				'FEATTYPE_OPERATION': 96,
				'FEATTYPE_OPERATION_COMPONENT': 125,
				'FEATTYPE_PATCH': 71,
				'FEATTYPE_PATTERN': 232,
				'FEATTYPE_PATTERN_HEAD': 233,
				'FEATTYPE_PIPE': 35,
				'FEATTYPE_PIPE_BRANCH': 119,
				'FEATTYPE_PIPE_EXTEND': 108,
				'FEATTYPE_PIPE_FOLLOW': 110,
				'FEATTYPE_PIPE_JOIN': 111,
				'FEATTYPE_PIPE_JOINT': 118,
				'FEATTYPE_PIPE_LINE': 113,
				'FEATTYPE_PIPE_POINT_TO_POINT': 107,
				'FEATTYPE_PIPE_SET_START': 106,
				'FEATTYPE_PIPE_TRIM': 109,
				'FEATTYPE_PLASTIC_RIB': 251,
				'FEATTYPE_PLY': 72,
				'FEATTYPE_PM_BELT_CURVE': 252,
				'FEATTYPE_PM_BUSHING_LD': 256,
				'FEATTYPE_POSITION_FOLD': 149,
				'FEATTYPE_PROCESS_STEP': 163,
				'FEATTYPE_PROTRUSION': 7,
				'FEATTYPE_RCG_CHAMFER': 278,
				'FEATTYPE_RCG_ROUND': 277,
				'FEATTYPE_REFERENCE': 200,
				'FEATTYPE_REMOVE_SURFACE': 245,
				'FEATTYPE_REMOVE_SURFACES': 128,
				'FEATTYPE_REPLACE_SURFACE': 33,
				'FEATTYPE_RIB': 10,
				'FEATTYPE_RIBBON_CABLE': 129,
				'FEATTYPE_RIBBON_EXTEND': 144,
				'FEATTYPE_RIBBON_PATH': 143,
				'FEATTYPE_RIBBON_SOLID': 147,
				'FEATTYPE_RIP': 95,
				'FEATTYPE_ROUND': 3,
				'FEATTYPE_ROUTE_MANAGER': 216,
				'FEATTYPE_ROUTE_PATH': 223,
				'FEATTYPE_ROUTE_SPOOL': 228,
				'FEATTYPE_RS_GENERATOR': 249,
				'FEATTYPE_RS_TRJ_MOVE': 250,
				'FEATTYPE_SAW': 27,
				'FEATTYPE_SET': 56,
				'FEATTYPE_SHAFT': 2,
				'FEATTYPE_SHEETMETAL_CLAMP': 162,
				'FEATTYPE_SHEETMETAL_CONVERSION': 154,
				'FEATTYPE_SHEETMETAL_CUT': 44,
				'FEATTYPE_SHEETMETAL_OPTIMIZE': 122,
				'FEATTYPE_SHEETMETAL_POPULATE': 124,
				'FEATTYPE_SHEETMETAL_PUNCH_POINT': 59,
				'FEATTYPE_SHEETMETAL_SHEAR': 142,
				'FEATTYPE_SHEETMETAL_ZONE': 161,
				'FEATTYPE_SHELL': 18,
				'FEATTYPE_SHELL_EXP': 201,
				'FEATTYPE_SHRINKAGE': 117,
				'FEATTYPE_SHRINK_DIM': 152,
				'FEATTYPE_SILHOUETTE_TRIM': 76,
				'FEATTYPE_SLDBEND': 184,
				'FEATTYPE_SLD_PIP_INSUL': 207,
				'FEATTYPE_SLOT': 5,
				'FEATTYPE_SMMFGAPPROACH': 176,
				'FEATTYPE_SMMFGCOSMETIC': 175,
				'FEATTYPE_SMMFGCUT': 54,
				'FEATTYPE_SMMFGFORM': 58,
				'FEATTYPE_SMMFGMATERIAL_REMOVE': 174,
				'FEATTYPE_SMMFGOFFSET': 173,
				'FEATTYPE_SMMFGPUNCH': 53,
				'FEATTYPE_SMMFGSHAPE': 178,
				'FEATTYPE_SMMFGSLOT': 177,
				'FEATTYPE_SMM_ETCH': 222,
				'FEATTYPE_SMM_GROOVE': 221,
				'FEATTYPE_SMM_HOLE': 219,
				'FEATTYPE_SMM_NEST': 220,
				'FEATTYPE_SMM_SLIT': 218,
				'FEATTYPE_SMT_CRN_REL': 183,
				'FEATTYPE_SMT_EXTRACT': 208,
				'FEATTYPE_SMT_SKETCH_FORM': 270,
				'FEATTYPE_SOLIDIFY': 79,
				'FEATTYPE_SOLID_PIPE': 115,
				'FEATTYPE_SPINAL_BEND': 133,
				'FEATTYPE_SPLIT': 77,
				'FEATTYPE_SPLIT_SRF_RGN': 266,
				'FEATTYPE_SPLIT_SURFACE': 51,
				'FEATTYPE_SPOOL': 88,
				'FEATTYPE_SPRING': 242,
				'FEATTYPE_SPRING_BACK': 150,
				'FEATTYPE_STAMPED_AREA': 262,
				'FEATTYPE_STYLE': 239,
				'FEATTYPE_SUBDIVISION': 254,
				'FEATTYPE_SUB_HARNESS': 121,
				'FEATTYPE_SUPERQUILT': 225,
				'FEATTYPE_SURFACE_MODEL': 40,
				'FEATTYPE_TERMINATOR': 213,
				'FEATTYPE_THICKEN': 46,
				'FEATTYPE_THREAD': 153,
				'FEATTYPE_TORUS': 105,
				'FEATTYPE_TURN': 28,
				'FEATTYPE_TWIST': 134,
				'FEATTYPE_UDF': 15,
				'FEATTYPE_UDFCLAMP': 84,
				'FEATTYPE_UDFNOTCH': 48,
				'FEATTYPE_UDFPUNCH': 49,
				'FEATTYPE_UDFRMDT': 170,
				'FEATTYPE_UDFTHREAD': 38,
				'FEATTYPE_UDFWORK_REGION': 132,
				'FEATTYPE_UDFZONE': 83,
				'FEATTYPE_UNBEND': 43,
				'FEATTYPE_UNRCG_ROUND': 279,
				'FEATTYPE_USER': 180,
				'FEATTYPE_VDA': 57,
				'FEATTYPE_VOL_SPLIT': 171,
				'FEATTYPE_VPDD': 253,
				'FEATTYPE_WALL': 41,
				'FEATTYPE_WATER_LINE': 169,
				'FEATTYPE_WELDING_ROD': 137,
				'FEATTYPE_WELD_COMBINE': 247,
				'FEATTYPE_WELD_EDGE_PREP': 172,
				'FEATTYPE_WELD_FILLET': 138,
				'FEATTYPE_WELD_GROOVE': 139,
				'FEATTYPE_WELD_NOTCH': 214,
				'FEATTYPE_WELD_PLUG_SLOT': 140,
				'FEATTYPE_WELD_PROCESS': 204,
				'FEATTYPE_WELD_SPOT': 141,
				'FEATTYPE_WORKCELL': 97,
				'FEATTYPE_ZONE': 136,
				'FEAT_CUSTOM': 282,
				'FEAT_CUSTOM_GRANITE': 283,
				'FEAT_ECAD_CUTS': 288,
				'FEAT_ECAD_CU_AREAS': 281,
				'FEAT_EDIT_BEND': 291,
				'FEAT_EDIT_BEND_RELIEF': 292,
				'FEAT_EDIT_CORNER_RELIEF': 293,
				'FEAT_EDIT_CORNER_SEAM': 295,
				'FEAT_HULL_BLOCK': 299,
				'FEAT_HULL_BLOCK_DEF': 300,
				'FEAT_LATTICE': 290,
				'FEAT_MOLD_INSERT': 294,
				'FEAT_MOVE_COMP': 302,
				'FEAT_PART_COMP': 297,
				'FEAT_PM_BELT': 301,
				'FEAT_PM_MOTOR': 286,
				'FEAT_PRTSPLIT': 287,
				'FEAT_PULL_WALL': 298,
				'FEAT_PUNCH_QUILT': 284,
				'FEAT_REFPART_CUTOUT': 285,
				'FEAT_SENSOR': 296,
				'FEAT_SMT_RECOGNITION': 289,
				'FEAT_UNRCG_CHAMFER': 280,
				'FeatureType_nil': 303,
			},
			'com': 'pfcls.pfcFeatureType.1',
	}
	EpfcFeatureCopyType = {
			'type': 'Enumerated Types',
			'value': {
				'FEATCOPY_MIRROR': 2,
				'FEATCOPY_MOVE': 3,
				'FEATCOPY_NEWREFS': 0,
				'FEATCOPY_SAMEREFS': 1,
				'FeatureCopyType_nil': 4,
			},
			'com': 'pfcls.pfcFeatureCopyType.1',
	}
	EpfcContourTraversal = {
			'type': 'Enumerated Types',
			'value': {
				'CONTOUR_TRAV_EXTERNAL': 2,
				'CONTOUR_TRAV_INTERNAL': 1,
				'CONTOUR_TRAV_NONE': 0,
				'ContourTraversal_nil': 3,
			},
			'com': 'pfcls.pfcContourTraversal.1',
	}
	EpfcSurfaceType = {
			'type': 'Enumerated Types',
			'value': {
				'SURFACE_CONE': 2,
				'SURFACE_COONS_PATCH': 8,
				'SURFACE_CYLINDER': 1,
				'SURFACE_CYLINDRICAL_SPLINE': 11,
				'SURFACE_FILLET': 7,
				'SURFACE_FOREIGN': 13,
				'SURFACE_NURBS': 10,
				'SURFACE_PLANE': 0,
				'SURFACE_REVOLVED': 5,
				'SURFACE_RULED': 4,
				'SURFACE_SPHERICAL_SPLINE': 12,
				'SURFACE_SPLINE': 9,
				'SURFACE_TABULATED_CYLINDER': 6,
				'SURFACE_TORUS': 3,
				'SurfaceType_nil': 14,
			},
			'com': 'pfcls.pfcSurfaceType.1',
	}
	EpfcSurfaceOrientation = {
			'type': 'Enumerated Types',
			'value': {
				'SURFACEORIENT_INWARD': 2,
				'SURFACEORIENT_NONE': 0,
				'SURFACEORIENT_OUTWARD': 1,
				'SurfaceOrientation_nil': 3,
			},
			'com': 'pfcls.pfcSurfaceOrientation.1',
	}
	EpfcCurveType = {
			'type': 'Enumerated Types',
			'value': {
				'CURVE_ARC': 4,
				'CURVE_ARROW': 3,
				'CURVE_BSPLINE': 6,
				'CURVE_CIRCLE': 7,
				'CURVE_COMPOSITE': 0,
				'CURVE_ELLIPSE': 8,
				'CURVE_LINE': 2,
				'CURVE_POINT': 1,
				'CURVE_POLYGON': 9,
				'CURVE_SPLINE': 5,
				'CURVE_TEXT': 10,
				'CurveType_nil': 11,
			},
			'com': 'pfcls.pfcCurveType.1',
	}
	EpfcModelType = {
			'type': 'Enumerated Types',
			'value': {
				'MDL_2D_SECTION': 3,
				'MDL_ASSEMBLY': 0,
				'MDL_CE_DRAWING': 12,
				'MDL_CE_SOLID': 11,
				'MDL_DIAGRAM': 9,
				'MDL_DRAWING': 2,
				'MDL_DWG_FORMAT': 5,
				'MDL_LAYOUT': 4,
				'MDL_MARKUP': 8,
				'MDL_MFG': 6,
				'MDL_PART': 1,
				'MDL_REPORT': 7,
				'MDL_UNSPECIFIED': 10,
				'ModelType_nil': 13,
			},
			'com': 'pfcls.pfcModelType.1',
	}
	EpfcExportType = {
			'type': 'Enumerated Types',
			'value': {
				'EXPORT_ACIS': 26,
				'EXPORT_BOM': 8,
				'EXPORT_CABLE_PARAMS': 21,
				'EXPORT_CADDS': 28,
				'EXPORT_CATIAFACETS': 22,
				'EXPORT_CATIA_CGR': 38,
				'EXPORT_CATIA_MODEL': 25,
				'EXPORT_CATIA_PART': 36,
				'EXPORT_CATIA_PRODUCT': 37,
				'EXPORT_CATIA_SESSION': 27,
				'EXPORT_CGM': 17,
				'EXPORT_CONNECTOR_PARAMS': 20,
				'EXPORT_DWG_SETUP': 9,
				'EXPORT_DXF': 4,
				'EXPORT_FEAT_INFO': 10,
				'EXPORT_FIAT': 19,
				'EXPORT_IGES': 3,
				'EXPORT_IGES_3D': 14,
				'EXPORT_INTF_3MF': 45,
				'EXPORT_INTF_DWG': 42,
				'EXPORT_INTF_DXF': 41,
				'EXPORT_INTF_SW_ASSEM': 44,
				'EXPORT_INTF_SW_PART': 43,
				'EXPORT_INVENTOR': 18,
				'EXPORT_JT': 34,
				'EXPORT_MATERIAL': 13,
				'EXPORT_MEDUSA': 30,
				'EXPORT_MFG_FEAT_CL': 12,
				'EXPORT_MFG_OPER_CL': 11,
				'EXPORT_MODEL_INFO': 1,
				'EXPORT_NEUTRAL': 31,
				'EXPORT_PARASOLID': 39,
				'EXPORT_PDF': 33,
				'EXPORT_PLOT': 23,
				'EXPORT_PRINT': 40,
				'EXPORT_PRODUCTVIEW': 32,
				'EXPORT_PROGRAM': 2,
				'EXPORT_RELATION': 0,
				'EXPORT_RENDER': 5,
				'EXPORT_STEP': 15,
				'EXPORT_STEP_2D': 29,
				'EXPORT_STL_ASCII': 6,
				'EXPORT_STL_BINARY': 7,
				'EXPORT_UG': 35,
				'EXPORT_VDA': 16,
				'EXPORT_VRML': 24,
				'ExportType_nil': 46,
			},
			'com': 'pfcls.pfcExportType.1',
	}
	EpfcCGMExportType = {
			'type': 'Enumerated Types',
			'value': {
				'CGMExportType_nil': 2,
				'EXPORT_CGM_CLEAR_TEXT': 0,
				'EXPORT_CGM_MIL_SPEC': 1,
			},
			'com': 'pfcls.pfcCGMExportType.1',
	}
	EpfcCGMScaleType = {
			'type': 'Enumerated Types',
			'value': {
				'CGMScaleType_nil': 2,
				'EXPORT_CGM_ABSTRACT': 0,
				'EXPORT_CGM_METRIC': 1,
			},
			'com': 'pfcls.pfcCGMScaleType.1',
	}
	EpfcImportType = {
			'type': 'Enumerated Types',
			'value': {
				'IMPORT_2D_CADAM': 14,
				'IMPORT_2D_DWG': 13,
				'IMPORT_2D_DXF': 12,
				'IMPORT_2D_IGES': 11,
				'IMPORT_2D_STEP': 10,
				'IMPORT_ASSEM_TREE_CFG': 8,
				'IMPORT_CABLE_PARAMS': 7,
				'IMPORT_CONFIG': 3,
				'IMPORT_CONNECTOR_PARAMS': 6,
				'IMPORT_DWG_SETUP': 4,
				'IMPORT_IGES_SECTION': 1,
				'IMPORT_PROGRAM': 2,
				'IMPORT_RELATION': 0,
				'IMPORT_SPOOL': 5,
				'IMPORT_WIRELIST': 9,
				'ImportType_nil': 15,
			},
			'com': 'pfcls.pfcImportType.1',
	}
	EpfcPlotPaperSize = {
			'type': 'Enumerated Types',
			'value': {
				'A0SIZEPLOT': 9,
				'A1SIZEPLOT': 8,
				'A2SIZEPLOT': 7,
				'A3SIZEPLOT': 6,
				'A4SIZEPLOT': 5,
				'ASIZEPLOT': 0,
				'BSIZEPLOT': 1,
				'CEEMPTYPLOT': 13,
				'CEEMPTYPLOT_MM': 14,
				'CSIZEPLOT': 2,
				'DSIZEPLOT': 3,
				'ESIZEPLOT': 4,
				'FSIZEPLOT': 10,
				'PlotPaperSize_nil': 15,
				'VARIABLESIZEPLOT': 11,
				'VARIABLESIZE_IN_MM_PLOT': 12,
			},
			'com': 'pfcls.pfcPlotPaperSize.1',
	}
	EpfcExport2DSheetOption = {
			'type': 'Enumerated Types',
			'value': {
				'EXPORT_ALL': 2,
				'EXPORT_CURRENT_TO_MODEL_SPACE': 0,
				'EXPORT_CURRENT_TO_PAPER_SPACE': 1,
				'EXPORT_SELECTED': 3,
				'Export2DSheetOption_nil': 4,
			},
			'com': 'pfcls.pfcExport2DSheetOption.1',
	}
	EpfcFacetControlFlag = {
			'type': 'Enumerated Types',
			'value': {
				'EXPORT_INCLUDE_ANNOTATIONS': 9,
				'FACET_ANGLE_CONTROL_DEFAULT': 4,
				'FACET_CHORD_HEIGHT_ADJUST': 1,
				'FACET_CHORD_HEIGHT_DEFAULT': 3,
				'FACET_FORCE_INTO_RANGE': 7,
				'FACET_INCLUDE_QUILTS': 8,
				'FACET_STEP_SIZE_ADJUST': 0,
				'FACET_STEP_SIZE_DEFAULT': 5,
				'FACET_STEP_SIZE_OFF': 6,
				'FACET_USE_CONFIG': 2,
				'FacetControlFlag_nil': 10,
			},
			'com': 'pfcls.pfcFacetControlFlag.1',
	}
	EpfcPlotPageRange = {
			'type': 'Enumerated Types',
			'value': {
				'PLOT_RANGE_ALL': 0,
				'PLOT_RANGE_CURRENT': 1,
				'PLOT_RANGE_OF_PAGES': 2,
				'PlotPageRange_nil': 3,
			},
			'com': 'pfcls.pfcPlotPageRange.1',
	}
	EpfcIntfType = {
			'type': 'Enumerated Types',
			'value': {
				'INTF_3MF': 25,
				'INTF_ACIS': 6,
				'INTF_AI': 12,
				'INTF_CATIA_CGR': 17,
				'INTF_CATIA_PART': 13,
				'INTF_CATIA_PRODUCT': 16,
				'INTF_CDRS': 8,
				'INTF_DXF': 7,
				'INTF_IBL': 21,
				'INTF_ICEM': 5,
				'INTF_IGES': 2,
				'INTF_INVENTOR_ASM': 20,
				'INTF_INVENTOR_PART': 19,
				'INTF_JT': 18,
				'INTF_NEUTRAL': 0,
				'INTF_NEUTRAL_FILE': 1,
				'INTF_PARASOLID': 11,
				'INTF_PRODUCTVIEW': 15,
				'INTF_PTS': 22,
				'INTF_SE_PART': 23,
				'INTF_SE_SHEETMETAL_PART': 24,
				'INTF_STEP': 3,
				'INTF_STL': 9,
				'INTF_UG': 14,
				'INTF_VDA': 4,
				'INTF_VRML': 10,
				'IntfType_nil': 26,
			},
			'com': 'pfcls.pfcIntfType.1',
	}
	EpfcOperationType = {
			'type': 'Enumerated Types',
			'value': {
				'ADD_OPERATION': 1,
				'CUT_OPERATION': 0,
				'OperationType_nil': 2,
			},
			'com': 'pfcls.pfcOperationType.1',
	}
	EpfcFamilyColumnType = {
			'type': 'Enumerated Types',
			'value': {
				'FAM_ANNOT_ELEM_PARAM': 24,
				'FAM_ASMCOMP': 4,
				'FAM_ASMCOMP_MODEL': 6,
				'FAM_COMP_CURVE_PARAM': 22,
				'FAM_CONNECTION_PARAM': 25,
				'FAM_CURVE_PARAM': 21,
				'FAM_DIMENSION': 1,
				'FAM_EDGE_PARAM': 19,
				'FAM_EXTERNAL_REFERENCE': 12,
				'FAM_FEATURE': 3,
				'FAM_FEATURE_PARAM': 18,
				'FAM_GTOL': 7,
				'FAM_INH_PART_REF': 16,
				'FAM_IPAR_NOTE': 2,
				'FAM_MASS_PROPS_SOURCE': 15,
				'FAM_MASS_PROPS_USER_PARAM': 14,
				'FAM_MERGE_PART_REF': 13,
				'FAM_QUILT_PARAM': 23,
				'FAM_SIM_OBJ': 17,
				'FAM_SURFACE_PARAM': 20,
				'FAM_SYSTEM_PARAM': 11,
				'FAM_TOL_MINUS': 9,
				'FAM_TOL_PLUS': 8,
				'FAM_TOL_PLUSMINUS': 10,
				'FAM_UDF': 5,
				'FAM_USER_PARAM': 0,
				'FamilyColumnType_nil': 26,
			},
			'com': 'pfcls.pfcFamilyColumnType.1',
	}
	EpfcFaminstanceVerifyStatus = {
			'type': 'Enumerated Types',
			'value': {
				'FAMINST_FAILED_VERIFIED': 1,
				'FAMINST_NOT_VERIFIED': 2,
				'FAMINST_SUCCESS_VERIFIED': 0,
				'FaminstanceVerifyStatus_nil': 3,
			},
			'com': 'pfcls.pfcFaminstanceVerifyStatus.1',
	}
	EpfcSimpRepActionType = {
			'type': 'Enumerated Types',
			'value': {
				'SIMPREP_ASSEM_ONLY': 12,
				'SIMPREP_AUTO': 11,
				'SIMPREP_BOUNDBOX': 8,
				'SIMPREP_DEFENV': 9,
				'SIMPREP_EXCLUDE': 2,
				'SIMPREP_GEOM': 4,
				'SIMPREP_GRAPHICS': 5,
				'SIMPREP_INCLUDE': 1,
				'SIMPREP_LIGHT_GRAPH': 10,
				'SIMPREP_NONE': 7,
				'SIMPREP_REVERSE': 0,
				'SIMPREP_SUBSTITUTE': 3,
				'SIMPREP_SYMB': 6,
				'SimpRepActionType_nil': 13,
			},
			'com': 'pfcls.pfcSimpRepActionType.1',
	}
	EpfcSubstType = {
			'type': 'Enumerated Types',
			'value': {
				'SUBST_ASM_REP': 2,
				'SUBST_ENVELOPE': 3,
				'SUBST_INTERCHG': 0,
				'SUBST_NONE': 4,
				'SUBST_PRT_REP': 1,
				'SubstType_nil': 5,
			},
			'com': 'pfcls.pfcSubstType.1',
	}
	EpfcSimpRepType = {
			'type': 'Enumerated Types',
			'value': {
				'SIMPREP_BOUNDBOX_REP': 5,
				'SIMPREP_DEFENV_REP': 6,
				'SIMPREP_GEOM_REP': 3,
				'SIMPREP_GRAPHICS_REP': 2,
				'SIMPREP_LIGHT_GRAPH_REP': 7,
				'SIMPREP_MASTER_REP': 0,
				'SIMPREP_SYMB_REP': 4,
				'SIMPREP_USER_DEFINED': 1,
				'SimpRepType_nil': 9,
			},
			'com': 'pfcls.pfcSimpRepType.1',
	}
	EpfcXSecCutType = {
			'type': 'Enumerated Types',
			'value': {
				'XSEC_OFFSET': 1,
				'XSEC_PLANAR': 0,
				'XSecCutType_nil': 2,
			},
			'com': 'pfcls.pfcXSecCutType.1',
	}
	EpfcXSecCutobjType = {
			'type': 'Enumerated Types',
			'value': {
				'XSECTYPE_MODEL': 0,
				'XSECTYPE_MODELQUILTS': 2,
				'XSECTYPE_ONEPART': 3,
				'XSECTYPE_QUILTS': 1,
				'XSecCutobjType_nil': 4,
			},
			'com': 'pfcls.pfcXSecCutobjType.1',
	}
	EpfcMaterialType = {
			'type': 'Enumerated Types',
			'value': {
				'MTL_ISOTROPIC': 0,
				'MTL_ORTHOTROPIC': 1,
				'MTL_TRANSVERSELY_ISOTROPIC': 2,
				'MaterialType_nil': 3,
			},
			'com': 'pfcls.pfcMaterialType.1',
	}
	EpfcMaterialPropertyType = {
			'type': 'Enumerated Types',
			'value': {
				'MTL_PROP_COMPRESSION_ULTIMATE_STRESS': 29,
				'MTL_PROP_COMPRESSION_ULTIMATE_STRESS_SC1': 5,
				'MTL_PROP_COMPRESSION_ULTIMATE_STRESS_SC2': 6,
				'MTL_PROP_COST_TYPE': 51,
				'MTL_PROP_EMISSIVITY': 38,
				'MTL_PROP_EXP_LAW_EXPONENT': 60,
				'MTL_PROP_FATIGUE_CUT_OFF_CYCLES': 52,
				'MTL_PROP_FATIGUE_STRENGTH_REDUCTION_FACTOR': 0,
				'MTL_PROP_HARDENING': 56,
				'MTL_PROP_HARDENING_LIMIT': 61,
				'MTL_PROP_HARDNESS': 33,
				'MTL_PROP_INITIAL_BEND_Y_FACTOR': 34,
				'MTL_PROP_MASS_DENSITY': 26,
				'MTL_PROP_MECHANISMS_DAMPING': 63,
				'MTL_PROP_MODEL_COEF_C01': 41,
				'MTL_PROP_MODEL_COEF_C02': 42,
				'MTL_PROP_MODEL_COEF_C10': 43,
				'MTL_PROP_MODEL_COEF_C11': 44,
				'MTL_PROP_MODEL_COEF_C20': 45,
				'MTL_PROP_MODEL_COEF_C30': 46,
				'MTL_PROP_MODEL_COEF_D': 47,
				'MTL_PROP_MODEL_COEF_D1': 48,
				'MTL_PROP_MODEL_COEF_D2': 49,
				'MTL_PROP_MODEL_COEF_D3': 50,
				'MTL_PROP_MODEL_COEF_LM': 40,
				'MTL_PROP_MODEL_COEF_MU': 39,
				'MTL_PROP_MODIFIED_MODULUS': 58,
				'MTL_PROP_POISSON_RATIO': 25,
				'MTL_PROP_POISSON_RATIO_NU21': 7,
				'MTL_PROP_POISSON_RATIO_NU31': 8,
				'MTL_PROP_POISSON_RATIO_NU32': 9,
				'MTL_PROP_POWER_LAW_EXPONENT': 59,
				'MTL_PROP_SHEAR_MODULUS': 35,
				'MTL_PROP_SHEAR_MODULUS_G12': 13,
				'MTL_PROP_SHEAR_MODULUS_G13': 14,
				'MTL_PROP_SHEAR_MODULUS_G23': 15,
				'MTL_PROP_SHEAR_ULTIMATE_STRESS': 30,
				'MTL_PROP_SPECIFIC_HEAT': 32,
				'MTL_PROP_STRESS_LIMIT_FOR_COMPRESSION': 54,
				'MTL_PROP_STRESS_LIMIT_FOR_FATIGUE': 22,
				'MTL_PROP_STRESS_LIMIT_FOR_SHEAR': 55,
				'MTL_PROP_STRESS_LIMIT_FOR_TENSION': 53,
				'MTL_PROP_STRUCTURAL_DAMPING_COEFFICIENT': 37,
				'MTL_PROP_TANGENT_MODULUS': 57,
				'MTL_PROP_TEMPERATURE': 23,
				'MTL_PROP_TENSILE_ULTIMATE_STRESS': 28,
				'MTL_PROP_TENSILE_ULTIMATE_STRESS_ST1': 3,
				'MTL_PROP_TENSILE_ULTIMATE_STRESS_ST2': 4,
				'MTL_PROP_TENSILE_YIELD_STRESS': 2,
				'MTL_PROP_THERMAL_CONDUCTIVITY': 31,
				'MTL_PROP_THERMAL_CONDUCTIVITY_K1': 19,
				'MTL_PROP_THERMAL_CONDUCTIVITY_K2': 20,
				'MTL_PROP_THERMAL_CONDUCTIVITY_K3': 21,
				'MTL_PROP_THERMAL_EXPANSION_COEFFICIENT': 27,
				'MTL_PROP_THERMAL_EXPANSION_COEFFICIENT_A1': 16,
				'MTL_PROP_THERMAL_EXPANSION_COEFFICIENT_A2': 17,
				'MTL_PROP_THERMAL_EXPANSION_COEFFICIENT_A3': 18,
				'MTL_PROP_THERMAL_SOFTENING_COEF': 62,
				'MTL_PROP_THERM_EXPANSION_REF_TEMPERATURE': 36,
				'MTL_PROP_TSAI_WU_INTERACTION_TERM_F12': 1,
				'MTL_PROP_YOUNG_MODULUS': 24,
				'MTL_PROP_YOUNG_MODULUS_E1': 10,
				'MTL_PROP_YOUNG_MODULUS_E2': 11,
				'MTL_PROP_YOUNG_MODULUS_E3': 12,
				'MaterialPropertyType_nil': 64,
			},
			'com': 'pfcls.pfcMaterialPropertyType.1',
	}
	EpfcGraphicsMode = {
			'type': 'Enumerated Types',
			'value': {
				'DRAW_GRAPHICS_COMPLEMENT': 1,
				'DRAW_GRAPHICS_NORMAL': 0,
				'GraphicsMode_nil': 2,
			},
			'com': 'pfcls.pfcGraphicsMode.1',
	}
	EpfcColumnJustification = {
			'type': 'Enumerated Types',
			'value': {
				'COL_JUSTIFY_CENTER': 1,
				'COL_JUSTIFY_LEFT': 0,
				'COL_JUSTIFY_RIGHT': 2,
				'ColumnJustification_nil': 3,
			},
			'com': 'pfcls.pfcColumnJustification.1',
	}
	EpfcTableSizeType = {
			'type': 'Enumerated Types',
			'value': {
				'TABLESIZE_BY_LENGTH': 1,
				'TABLESIZE_BY_NUM_CHARS': 0,
				'TableSizeType_nil': 2,
			},
			'com': 'pfcls.pfcTableSizeType.1',
	}
	EpfcParamMode = {
			'type': 'Enumerated Types',
			'value': {
				'DWGTABLE_FULL': 1,
				'DWGTABLE_NORMAL': 0,
				'ParamMode_nil': 2,
			},
			'com': 'pfcls.pfcParamMode.1',
	}
	EpfcRotationDegree = {
			'type': 'Enumerated Types',
			'value': {
				'ROTATE_180': 1,
				'ROTATE_270': 2,
				'ROTATE_90': 0,
				'RotationDegree_nil': 3,
			},
			'com': 'pfcls.pfcRotationDegree.1',
	}
	EpfcView2DType = {
			'type': 'Enumerated Types',
			'value': {
				'DRAWVIEW_AUXILIARY': 2,
				'DRAWVIEW_COPY_AND_ALIGN': 5,
				'DRAWVIEW_DETAIL': 3,
				'DRAWVIEW_GENERAL': 0,
				'DRAWVIEW_OF_FLAT_TYPE': 6,
				'DRAWVIEW_PROJECTION': 1,
				'DRAWVIEW_REVOLVE': 4,
				'View2DType_nil': 7,
			},
			'com': 'pfcls.pfcView2DType.1',
	}
	EpfcRasterDepth = {
			'type': 'Enumerated Types',
			'value': {
				'RASTERDEPTH_24': 1,
				'RASTERDEPTH_8': 0,
				'RasterDepth_nil': 2,
			},
			'com': 'pfcls.pfcRasterDepth.1',
	}
	EpfcDotsPerInch = {
			'type': 'Enumerated Types',
			'value': {
				'DotsPerInch_nil': 6,
				'RASTERDPI_100': 0,
				'RASTERDPI_200': 1,
				'RASTERDPI_300': 2,
				'RASTERDPI_400': 3,
				'RASTERDPI_500': 4,
				'RASTERDPI_600': 5,
			},
			'com': 'pfcls.pfcDotsPerInch.1',
	}
	EpfcRasterType = {
			'type': 'Enumerated Types',
			'value': {
				'RASTER_BMP': 0,
				'RASTER_EPS': 2,
				'RASTER_JPEG': 3,
				'RASTER_TIFF': 1,
				'RasterType_nil': 4,
			},
			'com': 'pfcls.pfcRasterType.1',
	}
	EpfcComponentConstraintType = {
			'type': 'Enumerated Types',
			'value': {
				'ASM_CONSTRAINT_ALIGN': 2,
				'ASM_CONSTRAINT_ALIGN_ANG_OFF': 15,
				'ASM_CONSTRAINT_ALIGN_NODEP_ANGLE': 30,
				'ASM_CONSTRAINT_ALIGN_OFF': 3,
				'ASM_CONSTRAINT_AUTO': 14,
				'ASM_CONSTRAINT_CSYS': 6,
				'ASM_CONSTRAINT_CSYS_PNT': 17,
				'ASM_CONSTRAINT_DEF_PLACEMENT': 10,
				'ASM_CONSTRAINT_EDGE_ON_SRF': 9,
				'ASM_CONSTRAINT_EDGE_ON_SRF_ANG': 28,
				'ASM_CONSTRAINT_EDGE_ON_SRF_DIST': 27,
				'ASM_CONSTRAINT_EDGE_ON_SRF_NORMAL': 29,
				'ASM_CONSTRAINT_EDGE_ON_SRF_PARL': 33,
				'ASM_CONSTRAINT_EXPLICIT': 35,
				'ASM_CONSTRAINT_FIX': 13,
				'ASM_CONSTRAINT_INSERT': 4,
				'ASM_CONSTRAINT_INSERT_NORM': 23,
				'ASM_CONSTRAINT_INSERT_PARL': 24,
				'ASM_CONSTRAINT_LINE_ANGLE': 32,
				'ASM_CONSTRAINT_LINE_COPLANAR': 19,
				'ASM_CONSTRAINT_LINE_DIST': 21,
				'ASM_CONSTRAINT_LINE_NORMAL': 18,
				'ASM_CONSTRAINT_LINE_PARL': 20,
				'ASM_CONSTRAINT_MATE': 0,
				'ASM_CONSTRAINT_MATE_ANG_OFF': 16,
				'ASM_CONSTRAINT_MATE_NODEP_ANGLE': 31,
				'ASM_CONSTRAINT_MATE_OFF': 1,
				'ASM_CONSTRAINT_ORIENT': 5,
				'ASM_CONSTRAINT_PNT_DIST': 22,
				'ASM_CONSTRAINT_PNT_ON_LINE': 12,
				'ASM_CONSTRAINT_PNT_ON_LINE_DIST': 25,
				'ASM_CONSTRAINT_PNT_ON_SRF': 8,
				'ASM_CONSTRAINT_PNT_ON_SRF_DIST': 26,
				'ASM_CONSTRAINT_SRF_NORMAL': 34,
				'ASM_CONSTRAINT_SUBSTITUTE': 11,
				'ASM_CONSTRAINT_TANGENT': 7,
				'ComponentConstraintType_nil': 36,
			},
			'com': 'pfcls.pfcComponentConstraintType.1',
	}
	EpfcComponentType = {
			'type': 'Enumerated Types',
			'value': {
				'COMPONENT_CAST_ASSEM': 7,
				'COMPONENT_CAST_RESULT': 11,
				'COMPONENT_DIE_BLOCK': 8,
				'COMPONENT_DIE_COMP': 9,
				'COMPONENT_FIXTURE': 2,
				'COMPONENT_FROM_MOTION': 12,
				'COMPONENT_GEN_ASSEM': 6,
				'COMPONENT_MOLD_ASSEM': 5,
				'COMPONENT_MOLD_BASE': 3,
				'COMPONENT_MOLD_COMP': 4,
				'COMPONENT_NONE': 14,
				'COMPONENT_NO_DEF_ASSUM': 13,
				'COMPONENT_REF_MODEL': 1,
				'COMPONENT_SAND_CORE': 10,
				'COMPONENT_WORKPIECE': 0,
				'ComponentType_nil': 15,
			},
			'com': 'pfcls.pfcComponentType.1',
	}
	EpfcSheetOrientation = {
			'type': 'Enumerated Types',
			'value': {
				'ORIENT_LANDSCAPE': 1,
				'ORIENT_PORTRAIT': 0,
				'SheetOrientation_nil': 2,
			},
			'com': 'pfcls.pfcSheetOrientation.1',
	}
	EpfcDetailType = {
			'type': 'Enumerated Types',
			'value': {
				'DETAIL_DRAFT_GROUP': 4,
				'DETAIL_ENTITY': 0,
				'DETAIL_NOTE': 1,
				'DETAIL_OLE_OBJECT': 5,
				'DETAIL_SYM_DEFINITION': 2,
				'DETAIL_SYM_INSTANCE': 3,
				'DetailType_nil': 6,
			},
			'com': 'pfcls.pfcDetailType.1',
	}
	EpfcAttachmentType = {
			'type': 'Enumerated Types',
			'value': {
				'ATTACH_FREE': 0,
				'ATTACH_OFFSET': 3,
				'ATTACH_PARAMETRIC': 1,
				'ATTACH_TYPE_UNSUPPORTED': 2,
				'AttachmentType_nil': 4,
			},
			'com': 'pfcls.pfcAttachmentType.1',
	}
	EpfcHorizontalJustification = {
			'type': 'Enumerated Types',
			'value': {
				'H_JUSTIFY_CENTER': 1,
				'H_JUSTIFY_DEFAULT': 3,
				'H_JUSTIFY_LEFT': 0,
				'H_JUSTIFY_RIGHT': 2,
				'HorizontalJustification_nil': 4,
			},
			'com': 'pfcls.pfcHorizontalJustification.1',
	}
	EpfcVerticalJustification = {
			'type': 'Enumerated Types',
			'value': {
				'V_JUSTIFY_BOTTOM': 2,
				'V_JUSTIFY_DEFAULT': 3,
				'V_JUSTIFY_MIDDLE': 1,
				'V_JUSTIFY_TOP': 0,
				'VerticalJustification_nil': 4,
			},
			'com': 'pfcls.pfcVerticalJustification.1',
	}
	EpfcSymbolDefHeight = {
			'type': 'Enumerated Types',
			'value': {
				'SYMDEF_FIXED': 0,
				'SYMDEF_MODEL_UNITS': 3,
				'SYMDEF_RELATIVE_TO_TEXT': 2,
				'SYMDEF_VARIABLE': 1,
				'SymbolDefHeight_nil': 4,
			},
			'com': 'pfcls.pfcSymbolDefHeight.1',
	}
	EpfcSymbolDefAttachmentType = {
			'type': 'Enumerated Types',
			'value': {
				'SYMDEFATTACH_FREE': 0,
				'SYMDEFATTACH_LEFT_LEADER': 1,
				'SYMDEFATTACH_NORMAL_TO_ITEM': 5,
				'SYMDEFATTACH_ON_ITEM': 4,
				'SYMDEFATTACH_RADIAL_LEADER': 3,
				'SYMDEFATTACH_RIGHT_LEADER': 2,
				'SymbolDefAttachmentType_nil': 6,
			},
			'com': 'pfcls.pfcSymbolDefAttachmentType.1',
	}
	EpfcSymbolGroupFilter = {
			'type': 'Enumerated Types',
			'value': {
				'DTLSYMINST_ACTIVE_GROUPS': 1,
				'DTLSYMINST_ALL_GROUPS': 0,
				'DTLSYMINST_INACTIVE_GROUPS': 2,
				'SymbolGroupFilter_nil': 3,
			},
			'com': 'pfcls.pfcSymbolGroupFilter.1',
	}
	EpfcDetailSymbolGroupOption = {
			'type': 'Enumerated Types',
			'value': {
				'DETAIL_SYMBOL_GROUP_ALL': 1,
				'DETAIL_SYMBOL_GROUP_CUSTOM': 3,
				'DETAIL_SYMBOL_GROUP_INTERACTIVE': 0,
				'DETAIL_SYMBOL_GROUP_NONE': 2,
				'DetailSymbolGroupOption_nil': 4,
			},
			'com': 'pfcls.pfcDetailSymbolGroupOption.1',
	}
	EpfcDetailTextDisplayOption = {
			'type': 'Enumerated Types',
			'value': {
				'DISPMODE_NUMERIC': 0,
				'DISPMODE_SYMBOLIC': 1,
				'DetailTextDisplayOption_nil': 2,
			},
			'com': 'pfcls.pfcDetailTextDisplayOption.1',
	}
	EpfcDetailLeaderAttachmentType = {
			'type': 'Enumerated Types',
			'value': {
				'DetailLeaderAttachmentType_nil': 3,
				'LEADER_ATTACH_NONE': 0,
				'LEADER_ATTACH_NORMAL': 1,
				'LEADER_ATTACH_TANGENT': 2,
			},
			'com': 'pfcls.pfcDetailLeaderAttachmentType.1',
	}
	EpfcDetailSymbolDefItemSource = {
			'type': 'Enumerated Types',
			'value': {
				'DTLSYMDEF_SRC_PATH': 3,
				'DTLSYMDEF_SRC_SURF_FINISH_DIR': 1,
				'DTLSYMDEF_SRC_SYMBOL_DIR': 2,
				'DTLSYMDEF_SRC_SYSTEM': 0,
				'DetailSymbolDefItemSource_nil': 4,
			},
			'com': 'pfcls.pfcDetailSymbolDefItemSource.1',
	}
	EpfcDimensionSenseType = {
			'type': 'Enumerated Types',
			'value': {
				'DIMSENSE_ANGLE': 5,
				'DIMSENSE_LINEAR_TO_ARC_OR_CIRCLE_TANGENT': 4,
				'DIMSENSE_NONE': 0,
				'DIMSENSE_POINT': 1,
				'DIMSENSE_POINT_TO_ANGLE': 6,
				'DIMSENSE_SPLINE_PT': 2,
				'DIMSENSE_TANGENT_INDEX': 3,
				'DimensionSenseType_nil': 7,
			},
			'com': 'pfcls.pfcDimensionSenseType.1',
	}
	EpfcDimensionPointType = {
			'type': 'Enumerated Types',
			'value': {
				'DIMPOINT_CENTER': 2,
				'DIMPOINT_END1': 0,
				'DIMPOINT_END2': 1,
				'DIMPOINT_MIDPOINT': 4,
				'DIMPOINT_NONE': 3,
				'DimensionPointType_nil': 5,
			},
			'com': 'pfcls.pfcDimensionPointType.1',
	}
	EpfcDimensionLinAOCTangentType = {
			'type': 'Enumerated Types',
			'value': {
				'DIMLINAOCTANGENT_LEFT0': 0,
				'DIMLINAOCTANGENT_LEFT1': 2,
				'DIMLINAOCTANGENT_RIGHT0': 1,
				'DIMLINAOCTANGENT_RIGHT1': 3,
				'DimensionLinAOCTangentType_nil': 4,
			},
			'com': 'pfcls.pfcDimensionLinAOCTangentType.1',
	}
	EpfcOrientationHint = {
			'type': 'Enumerated Types',
			'value': {
				'ORIENTHINT_ARC_ANGLE': 6,
				'ORIENTHINT_ARC_LENGTH': 7,
				'ORIENTHINT_ELLIPSE_RADIUS1': 4,
				'ORIENTHINT_ELLIPSE_RADIUS2': 5,
				'ORIENTHINT_HORIZONTAL': 1,
				'ORIENTHINT_LINE_TO_TANGENT_CURVE_ANGLE': 8,
				'ORIENTHINT_NONE': 0,
				'ORIENTHINT_SLANTED': 3,
				'ORIENTHINT_VERTICAL': 2,
				'OrientationHint_nil': 10,
			},
			'com': 'pfcls.pfcOrientationHint.1',
	}
	EpfcDrawingCreateOption = {
			'type': 'Enumerated Types',
			'value': {
				'DRAWINGCREATE_DISPLAY_DRAWING': 0,
				'DRAWINGCREATE_PROMPT_UNKNOWN_PARAMS': 3,
				'DRAWINGCREATE_SHOW_ERROR_DIALOG': 1,
				'DRAWINGCREATE_WRITE_ERROR_FILE': 2,
				'DrawingCreateOption_nil': 4,
			},
			'com': 'pfcls.pfcDrawingCreateOption.1',
	}
	EpfcDatumAxisConstraintType = {
			'type': 'Enumerated Types',
			'value': {
				'DTMAXIS_CENTER': 3,
				'DTMAXIS_NORMAL': 0,
				'DTMAXIS_TANGENT': 2,
				'DTMAXIS_THRU': 1,
				'DatumAxisConstraintType_nil': 4,
			},
			'com': 'pfcls.pfcDatumAxisConstraintType.1',
	}
	EpfcSplitCurveSide = {
			'type': 'Enumerated Types',
			'value': {
				'CURVE_BOTH_SIDES': 2,
				'CURVE_SIDE_ONE': 0,
				'CURVE_SIDE_TWO': 1,
				'SplitCurveSide_nil': 3,
			},
			'com': 'pfcls.pfcSplitCurveSide.1',
	}
	EpfcOffsetCurveDirection = {
			'type': 'Enumerated Types',
			'value': {
				'OFFSET_SIDE_ONE': 0,
				'OFFSET_SIDE_TWO': 1,
				'OffsetCurveDirection_nil': 2,
			},
			'com': 'pfcls.pfcOffsetCurveDirection.1',
	}
	EpfcCurveStartPoint = {
			'type': 'Enumerated Types',
			'value': {
				'CURVE_END': 1,
				'CURVE_START': 0,
				'CurveStartPoint_nil': 2,
			},
			'com': 'pfcls.pfcCurveStartPoint.1',
	}
	EpfcDatumCsysDimConstraintType = {
			'type': 'Enumerated Types',
			'value': {
				'DTMCSYS_DIM_ALIGN': 1,
				'DTMCSYS_DIM_OFFSET': 0,
				'DatumCsysDimConstraintType_nil': 2,
			},
			'com': 'pfcls.pfcDatumCsysDimConstraintType.1',
	}
	EpfcDatumCsysOrientMoveConstraintType = {
			'type': 'Enumerated Types',
			'value': {
				'DTMCSYS_MOVE_PHI': 7,
				'DTMCSYS_MOVE_RAD': 6,
				'DTMCSYS_MOVE_ROT_X': 3,
				'DTMCSYS_MOVE_ROT_Y': 4,
				'DTMCSYS_MOVE_ROT_Z': 5,
				'DTMCSYS_MOVE_THETA': 9,
				'DTMCSYS_MOVE_TRAN_X': 0,
				'DTMCSYS_MOVE_TRAN_Y': 1,
				'DTMCSYS_MOVE_TRAN_Z': 2,
				'DTMCSYS_MOVE_ZI': 8,
				'DatumCsysOrientMoveConstraintType_nil': 10,
			},
			'com': 'pfcls.pfcDatumCsysOrientMoveConstraintType.1',
	}
	EpfcDatumCsysOffsetType = {
			'type': 'Enumerated Types',
			'value': {
				'DTMCSYS_OFFSET_CARTESIAN': 0,
				'DTMCSYS_OFFSET_CYLINDRICAL': 1,
				'DTMCSYS_OFFSET_SPHERICAL': 2,
				'DatumCsysOffsetType_nil': 3,
			},
			'com': 'pfcls.pfcDatumCsysOffsetType.1',
	}
	EpfcDatumCsysOnSurfaceType = {
			'type': 'Enumerated Types',
			'value': {
				'DTMCSYS_ONSURF_DIAMETER': 2,
				'DTMCSYS_ONSURF_LINEAR': 0,
				'DTMCSYS_ONSURF_RADIAL': 1,
				'DatumCsysOnSurfaceType_nil': 3,
			},
			'com': 'pfcls.pfcDatumCsysOnSurfaceType.1',
	}
	EpfcDatumCsysOrientByMethod = {
			'type': 'Enumerated Types',
			'value': {
				'DTMCSYS_ORIENT_BY_SEL_CSYS_AXES': 1,
				'DTMCSYS_ORIENT_BY_SEL_REFS': 0,
				'DatumCsysOrientByMethod_nil': 2,
			},
			'com': 'pfcls.pfcDatumCsysOrientByMethod.1',
	}
	EpfcDatumPlaneConstraintType = {
			'type': 'Enumerated Types',
			'value': {
				'DTMPLN_ANG': 4,
				'DTMPLN_DEF_X': 7,
				'DTMPLN_DEF_Y': 8,
				'DTMPLN_DEF_Z': 9,
				'DTMPLN_NORM': 1,
				'DTMPLN_OFFS': 3,
				'DTMPLN_PRL': 2,
				'DTMPLN_SEC': 6,
				'DTMPLN_TANG': 5,
				'DTMPLN_THRU': 0,
				'DatumPlaneConstraintType_nil': 10,
			},
			'com': 'pfcls.pfcDatumPlaneConstraintType.1',
	}
	EpfcDatumPointConstraintType = {
			'type': 'Enumerated Types',
			'value': {
				'DTMPNT_ALONG_X': 9,
				'DTMPNT_ALONG_Y': 10,
				'DTMPNT_ALONG_Z': 11,
				'DTMPNT_CARTESIAN': 12,
				'DTMPNT_CENTER': 2,
				'DTMPNT_CYLINDRICAL': 13,
				'DTMPNT_LENGTH': 5,
				'DTMPNT_LENGTH_END': 6,
				'DTMPNT_NORMAL': 3,
				'DTMPNT_OFFSET': 1,
				'DTMPNT_ON': 0,
				'DTMPNT_PARALLEL': 4,
				'DTMPNT_RATIO': 7,
				'DTMPNT_RATIO_END': 8,
				'DTMPNT_SPHERICAL': 14,
				'DatumPointConstraintType_nil': 15,
			},
			'com': 'pfcls.pfcDatumPointConstraintType.1',
	}
	EpfcArgValueType = {
			'type': 'Enumerated Types',
			'value': {
				'ARG_V_ASCII_STRING': 3,
				'ARG_V_BOOLEAN': 2,
				'ARG_V_DOUBLE': 1,
				'ARG_V_INTEGER': 0,
				'ARG_V_POINTER': 7,
				'ARG_V_SELECTION': 5,
				'ARG_V_STRING': 4,
				'ARG_V_TRANSFORM': 6,
				'ARG_V_WSTRING': 8,
				'ArgValueType_nil': 9,
			},
			'com': 'pfcls.pfcArgValueType.1',
	}
	EpfcDrawingCreateErrorType = {
			'type': 'Enumerated Types',
			'value': {
				'DWGCREATE_ERR_CANT_GET_PROJ_PARENT': 11,
				'DWGCREATE_ERR_EXPLODE_DOESNT_EXIST': 2,
				'DWGCREATE_ERR_FIRST_REGION_USED': 6,
				'DWGCREATE_ERR_MODEL_NOT_EXPLODABLE': 3,
				'DWGCREATE_ERR_NOT_PROCESS_ASSEM': 7,
				'DWGCREATE_ERR_NO_PARENT_VIEW_FOR_PROJ': 10,
				'DWGCREATE_ERR_NO_RPT_REGIONS': 5,
				'DWGCREATE_ERR_NO_STEP_NUM': 8,
				'DWGCREATE_ERR_SAVED_VIEW_DOESNT_EXIST': 0,
				'DWGCREATE_ERR_SEC_NOT_PARALLEL': 12,
				'DWGCREATE_ERR_SEC_NOT_PERP': 4,
				'DWGCREATE_ERR_SIMP_REP_DOESNT_EXIST': 13,
				'DWGCREATE_ERR_TEMPLATE_USED': 9,
				'DWGCREATE_ERR_X_SEC_DOESNT_EXIST': 1,
				'DWGCRTERR_COMB_STATE_DOESNT_EXIST': 14,
				'DWGCRTERR_NOT_ALL_BALLOONS_CLEANED_WRN': 16,
				'DWGCRTERR_TOOL_DOESNT_EXIST': 15,
				'DrawingCreateErrorType_nil': 17,
			},
			'com': 'pfcls.pfcDrawingCreateErrorType.1',
	}
	EpfcCommandAccess = {
			'type': 'Enumerated Types',
			'value': {
				'ACCESS_AVAILABLE': 0,
				'ACCESS_DISALLOW': 4,
				'ACCESS_INVISIBLE': 2,
				'ACCESS_REMOVE': 3,
				'ACCESS_UNAVAILABLE': 1,
				'CommandAccess_nil': 5,
			},
			'com': 'pfcls.pfcCommandAccess.1',
	}
	EpfcUDFDependencyType = {
			'type': 'Enumerated Types',
			'value': {
				'UDFDEP_DRIVEN': 1,
				'UDFDEP_INDEPENDENT': 0,
				'UDFDependencyType_nil': 2,
			},
			'com': 'pfcls.pfcUDFDependencyType.1',
	}
	EpfcUDFScaleType = {
			'type': 'Enumerated Types',
			'value': {
				'UDFSCALE_CUSTOM': 2,
				'UDFSCALE_SAME_DIMS': 1,
				'UDFSCALE_SAME_SIZE': 0,
				'UDFScaleType_nil': 3,
			},
			'com': 'pfcls.pfcUDFScaleType.1',
	}
	EpfcUDFVariantValueType = {
			'type': 'Enumerated Types',
			'value': {
				'UDFVARIANT_DIMENSION': 0,
				'UDFVARIANT_PATTERN_PARAM': 1,
				'UDFVariantValueType_nil': 2,
			},
			'com': 'pfcls.pfcUDFVariantValueType.1',
	}
	EpfcUDFDimensionDisplayType = {
			'type': 'Enumerated Types',
			'value': {
				'UDFDISPLAY_BLANK': 2,
				'UDFDISPLAY_NORMAL': 0,
				'UDFDISPLAY_READ_ONLY': 1,
				'UDFDimensionDisplayType_nil': 3,
			},
			'com': 'pfcls.pfcUDFDimensionDisplayType.1',
	}
	EpfcUDFOrientation = {
			'type': 'Enumerated Types',
			'value': {
				'UDFORIENT_FLIP': 2,
				'UDFORIENT_INTERACTIVE': 0,
				'UDFORIENT_NO_FLIP': 1,
				'UDFOrientation_nil': 3,
			},
			'com': 'pfcls.pfcUDFOrientation.1',
	}
	EpfcToolkitType = {
			'type': 'Enumerated Types',
			'value': {
				'OTK': 1,
				'Protoolkit': 0,
				'ToolkitType_nil': 2,
			},
			'com': 'pfcls.pfcToolkitType.1',
	}
	EpfcShrinkwrapFacetedFormat = {
			'type': 'Enumerated Types',
			'value': {
				'SWFACETED_LIGHTWEIGHT_PART': 3,
				'SWFACETED_PART': 0,
				'SWFACETED_STL': 1,
				'SWFACETED_VRML': 2,
				'ShrinkwrapFacetedFormat_nil': 4,
			},
			'com': 'pfcls.pfcShrinkwrapFacetedFormat.1',
	}
	EpfcShrinkwrapMethod = {
			'type': 'Enumerated Types',
			'value': {
				'SWCREATE_FACETED_SOLID': 1,
				'SWCREATE_MERGED_SOLID': 2,
				'SWCREATE_SURF_SUBSET': 0,
				'ShrinkwrapMethod_nil': 3,
			},
			'com': 'pfcls.pfcShrinkwrapMethod.1',
	}
	EpfcAssemblyConfiguration = {
			'type': 'Enumerated Types',
			'value': {
				'AssemblyConfiguration_nil': 4,
				'EXPORT_ASM_ASSEMBLY_PARTS': 3,
				'EXPORT_ASM_FLAT_FILE': 0,
				'EXPORT_ASM_MULTI_FILES': 2,
				'EXPORT_ASM_SINGLE_FILE': 1,
			},
			'com': 'pfcls.pfcAssemblyConfiguration.1',
	}
	EpfcPDFSaveMode = {
			'type': 'Enumerated Types',
			'value': {
				'PDFSaveMode_nil': 2,
				'PDF_ARCHIVE_1': 0,
				'PDF_FULL': 1,
			},
			'com': 'pfcls.pfcPDFSaveMode.1',
	}
	EpfcPDFFontStrokeMode = {
			'type': 'Enumerated Types',
			'value': {
				'PDFFontStrokeMode_nil': 2,
				'PDF_STROKE_ALL_FONTS': 0,
				'PDF_USE_TRUE_TYPE_FONTS': 1,
			},
			'com': 'pfcls.pfcPDFFontStrokeMode.1',
	}
	EpfcPDFColorDepth = {
			'type': 'Enumerated Types',
			'value': {
				'PDFColorDepth_nil': 3,
				'PDF_CD_COLOR': 0,
				'PDF_CD_GRAY': 1,
				'PDF_CD_MONO': 2,
			},
			'com': 'pfcls.pfcPDFColorDepth.1',
	}
	EpfcPDFHiddenLineMode = {
			'type': 'Enumerated Types',
			'value': {
				'PDFHiddenLineMode_nil': 2,
				'PDF_HLM_DASHED': 1,
				'PDF_HLM_SOLID': 0,
			},
			'com': 'pfcls.pfcPDFHiddenLineMode.1',
	}
	EpfcPDFLayerMode = {
			'type': 'Enumerated Types',
			'value': {
				'PDFLayerMode_nil': 3,
				'PDF_LAYERS_ALL': 0,
				'PDF_LAYERS_NONE': 2,
				'PDF_LAYERS_VISIBLE': 1,
			},
			'com': 'pfcls.pfcPDFLayerMode.1',
	}
	EpfcPDFParameterMode = {
			'type': 'Enumerated Types',
			'value': {
				'PDFParameterMode_nil': 3,
				'PDF_PARAMS_ALL': 0,
				'PDF_PARAMS_DESIGNATED': 1,
				'PDF_PARAMS_NONE': 2,
			},
			'com': 'pfcls.pfcPDFParameterMode.1',
	}
	EpfcPDFPrintingMode = {
			'type': 'Enumerated Types',
			'value': {
				'PDFPrintingMode_nil': 2,
				'PDF_PRINTING_HIGH_RES': 1,
				'PDF_PRINTING_LOW_RES': 0,
			},
			'com': 'pfcls.pfcPDFPrintingMode.1',
	}
	EpfcPDFRestrictOperationsMode = {
			'type': 'Enumerated Types',
			'value': {
				'PDFRestrictOperationsMode_nil': 5,
				'PDF_RESTRICT_COMMENT_FORM_SIGNING': 3,
				'PDF_RESTRICT_EXTRACTING': 4,
				'PDF_RESTRICT_FORMS_SIGNING': 1,
				'PDF_RESTRICT_INSERT_DELETE_ROTATE': 2,
				'PDF_RESTRICT_NONE': 0,
			},
			'com': 'pfcls.pfcPDFRestrictOperationsMode.1',
	}
	EpfcPDFLinecap = {
			'type': 'Enumerated Types',
			'value': {
				'PDFLinecap_nil': 3,
				'PDF_LINECAP_BUTT': 0,
				'PDF_LINECAP_PROJECTING_SQUARE': 2,
				'PDF_LINECAP_ROUND': 1,
			},
			'com': 'pfcls.pfcPDFLinecap.1',
	}
	EpfcPDFLinejoin = {
			'type': 'Enumerated Types',
			'value': {
				'PDFLinejoin_nil': 3,
				'PDF_LINEJOIN_BEVEL': 2,
				'PDF_LINEJOIN_MITER': 0,
				'PDF_LINEJOIN_ROUND': 1,
			},
			'com': 'pfcls.pfcPDFLinejoin.1',
	}
	EpfcPDFU3DLightingMode = {
			'type': 'Enumerated Types',
			'value': {
				'PDFU3DLightingMode_nil': 11,
				'PDF_U3D_LIGHT_BLUE': 6,
				'PDF_U3D_LIGHT_BRIGHT': 3,
				'PDF_U3D_LIGHT_CAD': 9,
				'PDF_U3D_LIGHT_CUBE': 8,
				'PDF_U3D_LIGHT_DAY': 2,
				'PDF_U3D_LIGHT_HEADLAMP': 10,
				'PDF_U3D_LIGHT_NIGHT': 5,
				'PDF_U3D_LIGHT_NONE': 0,
				'PDF_U3D_LIGHT_PRIMARY': 4,
				'PDF_U3D_LIGHT_RED': 7,
				'PDF_U3D_LIGHT_WHITE': 1,
			},
			'com': 'pfcls.pfcPDFU3DLightingMode.1',
	}
	EpfcPDFU3DRenderMode = {
			'type': 'Enumerated Types',
			'value': {
				'PDFU3DRenderMode_nil': 15,
				'PDF_U3D_RENDER_BOUNDING_BOX': 0,
				'PDF_U3D_RENDER_HIDDEN_WIREFRAME': 14,
				'PDF_U3D_RENDER_ILLUSTRATION': 11,
				'PDF_U3D_RENDER_SHADED_ILLUSTRATION': 13,
				'PDF_U3D_RENDER_SHADED_VERTICES': 4,
				'PDF_U3D_RENDER_SHADED_WIREFRAME': 6,
				'PDF_U3D_RENDER_SOLID': 7,
				'PDF_U3D_RENDER_SOLID_OUTLINE': 12,
				'PDF_U3D_RENDER_SOLID_WIREFRAME': 9,
				'PDF_U3D_RENDER_TRANSPARENT': 8,
				'PDF_U3D_RENDER_TRANSPARENT_BOUNDING_BOX': 1,
				'PDF_U3D_RENDER_TRANSPARENT_BOUNDING_BOX_OUTLINE': 2,
				'PDF_U3D_RENDER_TRANSPARENT_WIREFRAME': 10,
				'PDF_U3D_RENDER_VERTICES': 3,
				'PDF_U3D_RENDER_WIREFRAME': 5,
			},
			'com': 'pfcls.pfcPDFU3DRenderMode.1',
	}
	EpfcPDFSelectedViewMode = {
			'type': 'Enumerated Types',
			'value': {
				'PDFSelectedViewMode_nil': 3,
				'PDF_VIEW_SELECT_ALL': 1,
				'PDF_VIEW_SELECT_BY_NAME': 2,
				'PDF_VIEW_SELECT_CURRENT': 0,
			},
			'com': 'pfcls.pfcPDFSelectedViewMode.1',
	}
	EpfcPDFExportMode = {
			'type': 'Enumerated Types',
			'value': {
				'PDFExportMode_nil': 4,
				'PDF_2D_DRAWING': 0,
				'PDF_3D_AS_NAMED_VIEWS': 1,
				'PDF_3D_AS_U3D': 3,
				'PDF_3D_AS_U3D_PDF': 2,
			},
			'com': 'pfcls.pfcPDFExportMode.1',
	}
	EpfcPrintSheets = {
			'type': 'Enumerated Types',
			'value': {
				'PRINT_ALL_SHEETS': 1,
				'PRINT_CURRENT_SHEET': 0,
				'PRINT_SELECTED_SHEETS': 2,
				'PrintSheets_nil': 3,
			},
			'com': 'pfcls.pfcPrintSheets.1',
	}
	EpfcPDFAnnotMode = {
			'type': 'Enumerated Types',
			'value': {
				'PDFAnnotMode_nil': 2,
				'PDF_EXCLUDE_ANNOTATION': 0,
				'PDF_INCLUDE_ANNOTATION': 1,
			},
			'com': 'pfcls.pfcPDFAnnotMode.1',
	}
	EpfcPDFOptionType = {
			'type': 'Enumerated Types',
			'value': {
				'PDFOPT_ADD_VIEWS': 42,
				'PDFOPT_ALLOW_ACCESSIBILITY': 24,
				'PDFOPT_ALLOW_COPYING': 23,
				'PDFOPT_ALLOW_MODE': 22,
				'PDFOPT_ALLOW_PRINTING': 20,
				'PDFOPT_ALLOW_PRINTING_MODE': 21,
				'PDFOPT_AUTHOR': 14,
				'PDFOPT_BACKGROUND_COLOR_BLUE': 41,
				'PDFOPT_BACKGROUND_COLOR_GREEN': 40,
				'PDFOPT_BACKGROUND_COLOR_RED': 39,
				'PDFOPT_BOOKMARK_FLAG_NOTES': 12,
				'PDFOPT_BOOKMARK_SHEETS': 11,
				'PDFOPT_BOOKMARK_VIEWS': 10,
				'PDFOPT_BOOKMARK_ZONES': 9,
				'PDFOPT_COLOR_DEPTH': 1,
				'PDFOPT_EXPORT_MODE': 30,
				'PDFOPT_FONT_STROKE': 0,
				'PDFOPT_HEIGHT': 34,
				'PDFOPT_HIDDENLINE_MODE': 2,
				'PDFOPT_HYPERLINKS': 8,
				'PDFOPT_INCL_ANNOT': 46,
				'PDFOPT_KEYWORDS': 16,
				'PDFOPT_LAUNCH_VIEWER': 5,
				'PDFOPT_LAYER_MODE': 6,
				'PDFOPT_LEFT_MARGIN': 38,
				'PDFOPT_LIGHT_DEFAULT': 31,
				'PDFOPT_LINECAP': 26,
				'PDFOPT_LINEJOIN': 27,
				'PDFOPT_MASTER_PASSWORD': 18,
				'PDFOPT_ORIENTATION': 36,
				'PDFOPT_PARAM_MODE': 7,
				'PDFOPT_PASSWORD_TO_OPEN': 17,
				'PDFOPT_PDF_SAVE': 45,
				'PDFOPT_PENTABLE': 25,
				'PDFOPT_RASTER_DPI': 4,
				'PDFOPT_RENDER_STYLE_DEFAULT': 32,
				'PDFOPT_RESTRICT_OPERATIONS': 19,
				'PDFOPT_SEARCHABLE_TEXT': 3,
				'PDFOPT_SELECTED_VIEW': 44,
				'PDFOPT_SHEETS': 28,
				'PDFOPT_SHEET_RANGE': 29,
				'PDFOPT_SIZE': 33,
				'PDFOPT_SUBJECT': 15,
				'PDFOPT_TITLE': 13,
				'PDFOPT_TOP_MARGIN': 37,
				'PDFOPT_VIEW_TO_EXPORT': 43,
				'PDFOPT_WIDTH': 35,
				'PDFOptionType_nil': 47,
			},
			'com': 'pfcls.pfcPDFOptionType.1',
	}
	EpfcPrintSaveMethod = {
			'type': 'Enumerated Types',
			'value': {
				'PRINT_SAVE_APPEND_TO_FILE': 3,
				'PRINT_SAVE_MULTIPLE_FILE': 2,
				'PRINT_SAVE_NULL': 0,
				'PRINT_SAVE_SINGLE_FILE': 1,
				'PrintSaveMethod_nil': 4,
			},
			'com': 'pfcls.pfcPrintSaveMethod.1',
	}
	EpfcProductViewFormat = {
			'type': 'Enumerated Types',
			'value': {
				'PV_FORMAT_ED': 1,
				'PV_FORMAT_EDZ': 2,
				'PV_FORMAT_PVS': 0,
				'PV_FORMAT_PVZ': 3,
				'ProductViewFormat_nil': 4,
			},
			'com': 'pfcls.pfcProductViewFormat.1',
	}
	EpfcNewModelImportType = {
			'type': 'Enumerated Types',
			'value': {
				'IMPORT_NEW_3MF': 28,
				'IMPORT_NEW_ACIS': 11,
				'IMPORT_NEW_CADDS': 3,
				'IMPORT_NEW_CATIA_CGR': 18,
				'IMPORT_NEW_CATIA_MODEL': 9,
				'IMPORT_NEW_CATIA_PART': 14,
				'IMPORT_NEW_CATIA_PRODUCT': 15,
				'IMPORT_NEW_CATIA_SESSION': 8,
				'IMPORT_NEW_CC': 24,
				'IMPORT_NEW_DXF': 10,
				'IMPORT_NEW_ICEM': 13,
				'IMPORT_NEW_IGES': 0,
				'IMPORT_NEW_INVENTOR_ASSEM': 23,
				'IMPORT_NEW_INVENTOR_PART': 22,
				'IMPORT_NEW_JT': 19,
				'IMPORT_NEW_NEUTRAL': 2,
				'IMPORT_NEW_PARASOLID': 12,
				'IMPORT_NEW_POLTXT': 7,
				'IMPORT_NEW_PRODUCTVIEW': 17,
				'IMPORT_NEW_SEDGE_ASSEMBLY': 26,
				'IMPORT_NEW_SEDGE_PART': 25,
				'IMPORT_NEW_SEDGE_SHEETMETAL_PART': 27,
				'IMPORT_NEW_STEP': 4,
				'IMPORT_NEW_STL': 5,
				'IMPORT_NEW_SW_ASSEM': 21,
				'IMPORT_NEW_SW_PART': 20,
				'IMPORT_NEW_UG': 16,
				'IMPORT_NEW_VDA': 1,
				'IMPORT_NEW_VRML': 6,
				'NewModelImportType_nil': 29,
			},
			'com': 'pfcls.pfcNewModelImportType.1',
	}
	EpfcImportAction = {
			'type': 'Enumerated Types',
			'value': {
				'IMPORT_LAYER_BLANK': 2,
				'IMPORT_LAYER_DISPLAY': 0,
				'IMPORT_LAYER_HIDDEN': 5,
				'IMPORT_LAYER_IGNORE': 3,
				'IMPORT_LAYER_NORMAL': 4,
				'IMPORT_LAYER_SKIP': 1,
				'ImportAction_nil': 6,
			},
			'com': 'pfcls.pfcImportAction.1',
	}
	EpfcMessageDialogType = {
			'type': 'Enumerated Types',
			'value': {
				'MESSAGE_ERROR': 0,
				'MESSAGE_INFO': 1,
				'MESSAGE_QUESTION': 3,
				'MESSAGE_WARNING': 2,
				'MessageDialogType_nil': 4,
			},
			'com': 'pfcls.pfcMessageDialogType.1',
	}
	EpfcMessageButton = {
			'type': 'Enumerated Types',
			'value': {
				'MESSAGE_BUTTON_ABORT': 4,
				'MESSAGE_BUTTON_CANCEL': 1,
				'MESSAGE_BUTTON_CONFIRM': 7,
				'MESSAGE_BUTTON_IGNORE': 6,
				'MESSAGE_BUTTON_NO': 3,
				'MESSAGE_BUTTON_OK': 0,
				'MESSAGE_BUTTON_RETRY': 5,
				'MESSAGE_BUTTON_YES': 2,
				'MessageButton_nil': 8,
			},
			'com': 'pfcls.pfcMessageButton.1',
	}
	EpfcServerDependency = {
			'type': 'Enumerated Types',
			'value': {
				'SERVER_DEPENDENCY_ALL': 0,
				'SERVER_DEPENDENCY_NONE': 2,
				'SERVER_DEPENDENCY_REQUIRED': 1,
				'ServerDependency_nil': 3,
			},
			'com': 'pfcls.pfcServerDependency.1',
	}
	EpfcServerIncludeInstances = {
			'type': 'Enumerated Types',
			'value': {
				'SERVER_INCLUDE_ALL': 0,
				'SERVER_INCLUDE_NONE': 2,
				'SERVER_INCLUDE_SELECTED': 1,
				'ServerIncludeInstances_nil': 3,
			},
			'com': 'pfcls.pfcServerIncludeInstances.1',
	}
	EpfcServerAutoresolveOption = {
			'type': 'Enumerated Types',
			'value': {
				'SERVER_AUTORESOLVE_IGNORE': 1,
				'SERVER_AUTORESOLVE_UPDATE_IGNORE': 2,
				'SERVER_DONT_AUTORESOLVE': 0,
				'ServerAutoresolveOption_nil': 3,
			},
			'com': 'pfcls.pfcServerAutoresolveOption.1',
	}
	EpfcModelCheckMode = {
			'type': 'Enumerated Types',
			'value': {
				'MODELCHECK_GRAPHICS': 0,
				'MODELCHECK_NO_GRAPHICS': 1,
				'ModelCheckMode_nil': 2,
			},
			'com': 'pfcls.pfcModelCheckMode.1',
	}
	EpfcWSImportExportMessageType = {
			'type': 'Enumerated Types',
			'value': {
				'WSIMPEX_MSG_CONFLICT': 2,
				'WSIMPEX_MSG_ERROR': 3,
				'WSIMPEX_MSG_INFO': 0,
				'WSIMPEX_MSG_WARNING': 1,
				'WSImportExportMessageType_nil': 4,
			},
			'com': 'pfcls.pfcWSImportExportMessageType.1',
	}
	EpfcParamType = {
			'type': 'Enumerated Types',
			'value': {
				'ALL_PARAMS': 5,
				'DIMTOL_PARAM': 3,
				'DIM_PARAM': 1,
				'GTOL_PARAM': 6,
				'PATTERN_PARAM': 2,
				'ParamType_nil': 8,
				'REFDIM_PARAM': 4,
				'SURFFIN_PARAM': 7,
				'USER_PARAM': 0,
			},
			'com': 'pfcls.pfcParamType.1',
	}
	EpfcFileListOpt = {
			'type': 'Enumerated Types',
			'value': {
				'FILE_LIST_ALL': 0,
				'FILE_LIST_ALL_INST': 2,
				'FILE_LIST_LATEST': 1,
				'FILE_LIST_LATEST_INST': 3,
				'FileListOpt_nil': 4,
			},
			'com': 'pfcls.pfcFileListOpt.1',
	}
	EpfcRelCriterion = {
			'type': 'Enumerated Types',
			'value': {
				'FILE_INCLUDE_ALL': 0,
				'FILE_INCLUDE_NONE': 2,
				'FILE_INCLUDE_REQUIRED': 1,
				'RelCriterion_nil': 3,
			},
			'com': 'pfcls.pfcRelCriterion.1',
	}
	EpfcMouseButton = {
			'type': 'Enumerated Types',
			'value': {
				'MOUSE_BTN_ANY': 4,
				'MOUSE_BTN_LEFT': 0,
				'MOUSE_BTN_LEFT_DOUBLECLICK': 3,
				'MOUSE_BTN_MIDDLE': 1,
				'MOUSE_BTN_MOVE': 5,
				'MOUSE_BTN_RIGHT': 2,
				'MouseButton_nil': 6,
			},
			'com': 'pfcls.pfcMouseButton.1',
	}
	EpfcCreoCompatibility = {
			'type': 'Enumerated Types',
			'value': {
				'C3Compatible': 3,
				'C4Compatible': 4,
				'CompatibilityUndefined': -1,
				'CreoCompatibility_nil': 5,
			},
			'com': 'pfcls.pfcCreoCompatibility.1',
	}
	EpfcTerminationStatus = {
			'type': 'Enumerated Types',
			'value': {
				'TERM_ABNORMAL': 1,
				'TERM_EXIT': 0,
				'TERM_SIGNAL': 2,
				'TerminationStatus_nil': 3,
			},
			'com': 'pfcls.pfcTerminationStatus.1',
	}
class Classes(Enum):
	IpfcACIS3DExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExport3DInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcACIS3DExportInstructions.Create': {
					'type': 'Function',
					'args': 'inConfiguration as IpfcAssemblyConfiguration, inGeometry as IpfcGeometryFlags',
					'return': 'IpfcACIS3DExportInstructions',
					'info': 'Creates a new instructions object used to export a model to ACIS format.',
					'com': 'pfcls.pfcACIS3DExportInstructions.1',
				},
			},
	}
	IpfcActionListener = {
			'type': 'Classes',
			'parent': {
				'IpfcApplicationCallback': '',
			},
			'child': {
				'IpfcFeatureActionListener': '',
				'IpfcModelEventActionListener': '',
				'IpfcModelActionListener': '',
				'IpfcSolidActionListener': '',
				'IpfcDisplayListener': '',
				'IpfcUICommandActionListener': '',
				'IpfcUICommandAccessListener': '',
				'IpfcUICommandBracketListener': '',
				'IpfcLayerImportFilter': '',
				'IpfcPopupmenuListener': '',
				'IpfcFileSaveRegisterListener': '',
				'IpfcFileOpenRegisterListener': '',
				'IpfcRelationFunctionListener': '',
				'IpfcModelCheckCustomCheckListener': '',
				'IpfcSessionActionListener': '',
				'IpfcAsyncActionListener': '',
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcActionSource = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcParameterOwner': '',
				'IpfcModelItem': '',
				'IpfcModel': '',
				'IpfcDisplayList2D': '',
				'IpfcDisplayList3D': '',
				'IpfcUICommand': '',
				'IpfcBaseSession': '',
				'IpfcAsyncConnection': '',
			},
			'properties': {
			},
			'method': {
				'AddActionListener': {
					'type': 'Sub',
					'args': 'Listener as IpfcActionListener',
					'return': '',
					'info': 'Adds an action listener to notify you of certain events.',
				},
				'AddActionListenerWithType': {
					'type': 'Sub',
					'args': 'Listener as IpfcActionListener, ATypes as IpfcActionTypes',
					'return': '',
					'info': 'Adds an action listener to notify chosen events.',
				},
				'RemoveActionListener': {
					'type': 'Sub',
					'args': 'Listener as IpfcActionListener',
					'return': '',
					'info': 'Deletes the specified action listener so you are no longer notified of those events.',
				},
			},
	}
	IpfcAngleDimensionSense = {
			'type': 'Classes',
			'parent': {
				'IpfcDimensionSense': '',
			},
			'child': {
			},
			'properties': {
				'AngleOptions': {
					'return': 'IpfcDimensionAngleOptions',
					'info': 'The flags determining the location of the angle dimension.',
				},
			},
			'method': {
				'CCpfcAngleDimensionSense.Create': {
					'type': 'Function',
					'args': 'AngleOptions as IpfcDimensionAngleOptions [optional]',
					'return': 'IpfcAngleDimensionSense',
					'info': 'Creates a new angle dimension sense object for use in creating a drawing dimension.',
					'com': 'pfcls.pfcAngleDimensionSense.1',
				},
			},
	}
	IpfcAngularDimOptions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'IsFirst': {
					'return': 'as Boolean [optional]',
					'info': '  ',
				},
				'ShouldFlip': {
					'return': 'as Boolean [optional]',
					'info': '  ',
				},
			},
			'method': {
				'CCpfcAngularDimOptions.Create': {
					'type': 'Function',
					'args': 'IsFirst as Boolean [optional], ShouldFlip as Boolean [optional]',
					'return': 'IpfcAngularDimOptions',
					'info': '  ',
					'com': 'pfcls.pfcAngularDimOptions.1',
				},
			},
	}
	IpfcAngularDimSense = {
			'type': 'Classes',
			'parent': {
				'IpfcDimSense': '',
			},
			'child': {
			},
			'properties': {
				'Options': {
					'return': 'IpfcAngularDimOptions',
					'info': '  ',
				},
			},
			'method': {
				'CCpfcAngularDimSense.Create': {
					'type': 'Function',
					'args': 'Options as IpfcAngularDimOptions [optional]',
					'return': 'IpfcAngularDimSense',
					'info': '  ',
					'com': 'pfcls.pfcAngularDimSense.1',
				},
			},
	}
	IpfcAnnotationTextStyle = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcAnnotationTextStyle.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcAnnotationTextStyle',
					'info': '  ',
					'com': 'pfcls.pfcAnnotationTextStyle.1',
				},
				'GetAngle': {
					'type': 'Function',
					'args': '',
					'return': ' as Double [optional]',
					'info': '  ',
				},
				'GetColor': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcColorRGB',
					'info': '  ',
				},
				'GetFont': {
					'type': 'Function',
					'args': '',
					'return': ' as String',
					'info': '  ',
				},
				'GetHeight': {
					'type': 'Function',
					'args': '',
					'return': ' as Double [optional]',
					'info': '  ',
				},
				'GetHorizontalJustification': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcHorizontalJustification',
					'info': '  ',
				},
				'GetSlantAngle': {
					'type': 'Function',
					'args': '',
					'return': ' as Double [optional]',
					'info': '  ',
				},
				'GetThickness': {
					'type': 'Function',
					'args': '',
					'return': ' as Double [optional]',
					'info': '  ',
				},
				'GetVerticalJustification': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcVerticalJustification',
					'info': '  ',
				},
				'GetWidth': {
					'type': 'Function',
					'args': '',
					'return': ' as Double [optional]',
					'info': '  ',
				},
				'IsHeightInModelUnits': {
					'type': 'Function',
					'args': '',
					'return': ' as Boolean',
					'info': '  ',
				},
				'IsTextMirrored': {
					'type': 'Function',
					'args': '',
					'return': ' as Boolean [optional]',
					'info': '  ',
				},
				'IsTextUnderlined': {
					'type': 'Function',
					'args': '',
					'return': ' as Boolean [optional]',
					'info': '  ',
				},
				'MirrorText': {
					'type': 'Sub',
					'args': 'Mirror as Boolean',
					'return': '',
					'info': '  ',
				},
				'SetAngle': {
					'type': 'Sub',
					'args': 'Angle as Double',
					'return': '',
					'info': '  ',
				},
				'SetColor': {
					'type': 'Sub',
					'args': 'Color as IpfcColorRGB',
					'return': '',
					'info': '  ',
				},
				'SetFont': {
					'type': 'Sub',
					'args': 'Font as String',
					'return': '',
					'info': '  ',
				},
				'SetHeight': {
					'type': 'Sub',
					'args': 'Height as Double',
					'return': '',
					'info': '  ',
				},
				'SetHeightInModelUnits': {
					'type': 'Sub',
					'args': 'ModelUnits as Boolean',
					'return': '',
					'info': '  ',
				},
				'SetHorizontalJustification': {
					'type': 'Sub',
					'args': 'Justification as IpfcHorizontalJustification',
					'return': '',
					'info': '  ',
				},
				'SetSlantAngle': {
					'type': 'Sub',
					'args': 'SlantAngle as Double',
					'return': '',
					'info': '  ',
				},
				'SetThickness': {
					'type': 'Sub',
					'args': 'Thickness as Double',
					'return': '',
					'info': '  ',
				},
				'SetVerticalJustification': {
					'type': 'Sub',
					'args': 'Justification as IpfcVerticalJustification',
					'return': '',
					'info': '  ',
				},
				'SetWidth': {
					'type': 'Sub',
					'args': 'Width as Double',
					'return': '',
					'info': '  ',
				},
				'UnderlineText': {
					'type': 'Sub',
					'args': 'Underline as Boolean',
					'return': '',
					'info': '  ',
				},
			},
	}
	IpfcAppInfo = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcAppInfo.Create': {
					'type': 'Function',
					'args': 'Compatibility as IpfcCreoCompatibility [optional]',
					'return': 'IpfcAppInfo',
					'info': '  ',
					'com': 'pfcls.pfcAppInfo.1',
				},
				'GetCompatibility': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcCreoCompatibility',
					'info': '  ',
				},
				'SetCompatibility': {
					'type': 'Sub',
					'args': 'Compatibility as IpfcCreoCompatibility',
					'return': '',
					'info': '  ',
				},
			},
	}
	IpfcApplicationCallback = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcActionListener': '',
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcArc = {
			'type': 'Classes',
			'parent': {
				'IpfcCurve': '',
			},
			'child': {
			},
			'properties': {
				'Center': {
					'return': 'IpfcPoint3D',
					'info': 'The center of the arc',
				},
				'EndAngle': {
					'return': 'as Double',
					'info': 'The angular parameter of the ending point',
				},
				'Radius': {
					'return': 'as Double',
					'info': 'The radius of the arc',
				},
				'StartAngle': {
					'return': 'as Double',
					'info': 'The angular parameter of the starting point',
				},
				'Vector1': {
					'return': 'IpfcVector3D',
					'info': 'The first vector that defines the plane of the arc',
				},
				'Vector2': {
					'return': 'IpfcVector3D',
					'info': 'The second vector that defines the plane of the arc',
				},
			},
			'method': {
			},
	}
	IpfcArcDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcCurveDescriptor': '',
			},
			'child': {
			},
			'properties': {
				'Center': {
					'return': 'IpfcPoint3D',
					'info': 'The center of the arc',
				},
				'EndAngle': {
					'return': 'as Double',
					'info': 'The angular parameter of the ending point',
				},
				'Radius': {
					'return': 'as Double',
					'info': 'The radius of the arc',
				},
				'StartAngle': {
					'return': 'as Double',
					'info': 'The angular parameter of the starting point',
				},
				'Vector1': {
					'return': 'IpfcVector3D',
					'info': 'The first vector that defines the plane of the arc',
				},
				'Vector2': {
					'return': 'IpfcVector3D',
					'info': 'The second vector that defines the plane of the arc',
				},
			},
			'method': {
				'CCpfcArcDescriptor.Create': {
					'type': 'Function',
					'args': 'Vector1 as IpfcVector3D, Vector2 as IpfcVector3D, Center as IpfcPoint3D, StartAngle as Double, EndAngle as Double, Radius as Double',
					'return': 'IpfcArcDescriptor',
					'info': 'This method creates a new ArcDescriptor object.',
					'com': 'pfcls.pfcArcDescriptor.1',
				},
			},
	}
	IpfcArgument = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Label': {
					'return': 'as String',
					'info': 'The argument name.',
				},
				'Value': {
					'return': 'IpfcArgValue',
					'info': 'The argument value.',
				},
			},
			'method': {
				'CCpfcArgument.Create': {
					'type': 'Function',
					'args': 'inLabel as String, inValue as IpfcArgValue',
					'return': 'IpfcArgument',
					'info': "Creates a new argument object, for use in passing value to other Creo Parametric auxiliary applications like Creo Parametric TOOLKIT DLL's.",
					'com': 'pfcls.pfcArgument.1',
				},
			},
	}
	IpfcArgValue = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'discr': {
					'return': 'IpfcArgValueType',
					'info': 'None',
				},
				'ASCIIStringValue': {
					'return': 'as String',
					'info': 'Used if the argument value contains a character string.',
				},
				'BoolValue': {
					'return': 'as Boolean',
					'info': 'Used if the argument value contains a boolean.',
				},
				'DoubleValue': {
					'return': 'as Double',
					'info': 'Used if the argument value contains a double.',
				},
				'IntValue': {
					'return': 'as Long',
					'info': 'Used if the argument value contains an integer.',
				},
				'SelectionValue': {
					'return': 'IpfcSelection',
					'info': 'Used if the argument value contains a Creo Parametric selection.',
				},
				'StringValue': {
					'return': 'as String',
					'info': 'Used if the argument value contains a string.',
				},
				'TransformValue': {
					'return': 'IpfcTransform3D',
					'info': 'Used if the argument value contains a coordinate transformation.',
				},
			},
			'method': {
			},
	}
	IpfcArrow = {
			'type': 'Classes',
			'parent': {
				'IpfcCurve': '',
			},
			'child': {
			},
			'properties': {
				'End1': {
					'return': 'IpfcPoint3D',
					'info': 'The first end of the arrow',
				},
				'End2': {
					'return': 'IpfcPoint3D',
					'info': 'The second end of the arrow',
				},
			},
			'method': {
			},
	}
	IpfcArrowDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcCurveDescriptor': '',
			},
			'child': {
			},
			'properties': {
				'End1': {
					'return': 'IpfcPoint3D',
					'info': 'The first end of the arrow',
				},
				'End2': {
					'return': 'IpfcPoint3D',
					'info': 'The second end of the arrow',
				},
			},
			'method': {
				'CCpfcArrowDescriptor.Create': {
					'type': 'Function',
					'args': 'End1 as IpfcPoint3D, End2 as IpfcPoint3D',
					'return': 'IpfcArrowDescriptor',
					'info': 'This method creates a new ArrowDescriptor object.',
					'com': 'pfcls.pfcArrowDescriptor.1',
				},
			},
	}
	IpfcAssembly = {
			'type': 'Classes',
			'parent': {
				'IpfcSolid': '',
			},
			'child': {
			},
			'properties': {
				'DynamicPositioning': {
					'return': 'as Boolean',
					'info': 'If the assembly uses dynamic positioning, this attribute is true; otherwise, it is false',
				},
				'IsExploded': {
					'return': 'as Boolean',
					'info': 'If the assembly is exploded, this attribute is true; otherwise, it isfalse',
				},
			},
			'method': {
				'AssembleByCopy': {
					'type': 'Function',
					'args': 'NewModelName as String, ModelToCopy as IpfcSolid, LeaveUnplaced as Boolean',
					'return': 'IpfcFeature',
					'info': 'Assembles a copy of a model into the assembly.',
				},
				'AssembleComponent': {
					'type': 'Function',
					'args': 'Model as IpfcSolid, Position as IpfcTransform3D [optional]',
					'return': 'IpfcFeature',
					'info': 'Assembles the model into the assembly.',
				},
				'AssembleSkeleton': {
					'type': 'Sub',
					'args': 'SkeletonModel as IpfcSolid',
					'return': '',
					'info': 'Adds a skeleton model in the assembly.',
				},
				'AssembleSkeletonByCopy': {
					'type': 'Sub',
					'args': 'NewSkeletonName as String, SkeletonToCopy as IpfcSolid',
					'return': '',
					'info': 'Copies the specified template model into the assembly as a skeleton model.',
				},
				'DeleteSkeleton': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Deletes the skeleton model from the assembly.',
				},
				'Explode': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Explodes the specified assembly.  The function invalidatesthe display list, but the application needs to repaint thewindow.',
				},
				'GetActiveExplodedState': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcExplodedState',
					'info': 'Returns the current active exploded state in the assembly.',
				},
				'GetDefaultExplodedState': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcExplodedState',
					'info': 'Returns the default exploded state in the assembly.',
				},
				'GetSkeleton': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSolid',
					'info': 'Returns the skeleton model in the assembly.',
				},
				'UnExplode': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Unexplodes the assembly.  The function invalidatesthe display list, but the application needs to repaint thewindow.',
				},
			},
	}
	IpfcASSEMTreeCFGImportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcImportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcASSEMTreeCFGImportInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcASSEMTreeCFGImportInstructions',
					'info': 'Creates a new instructions object used to import (read) from the ASSEM_TREE_CFG format file.',
					'com': 'pfcls.pfcASSEMTreeCFGImportInstructions.1',
				},
			},
	}
	IpfcAsyncActionListener = {
			'type': 'Classes',
			'parent': {
				'IpfcActionListener': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'OnTerminate': {
					'type': 'Sub',
					'args': 'Status as IpfcTerminationStatus',
					'return': '',
					'info': 'This listener method, available only in asynchronous mode, is called when Creo Parametric terminates normally or abnormally.',
				},
			},
	}
	IpfcAsyncConnection = {
			'type': 'Classes',
			'parent': {
				'IpfcActionSource': '',
			},
			'child': {
			},
			'properties': {
				'Session': {
					'return': 'IpfcSession',
					'info': 'The handle to the session of Creo Parametric for the current active connection.',
				},
			},
			'method': {
				'CCpfcAsyncConnection.Connect': {
					'type': 'Function',
					'args': 'Display as String [optional], UserID as String [optional], TextPath as String [optional], TimeoutSec as Long [optional]',
					'return': 'IpfcAsyncConnection',
					'info': 'Causes the application to connect to an existing Creo Parametric process running on a host using a specified display. It is intended for use in simple and full asynchronous modes.',
					'com': 'pfcls.pfcAsyncConnection.1',
				},
				'CCpfcAsyncConnection.ConnectById': {
					'type': 'Function',
					'args': 'Id as IpfcConnectionId, TextPath as String [optional], TimeoutSec as Long [optional]',
					'return': 'IpfcAsyncConnection',
					'info': 'Connects to a specific Creo Parametric session by passing the identification string. ',
					'com': 'pfcls.pfcAsyncConnection.1',
				},
				'CCpfcAsyncConnection.ConnectWS': {
					'type': 'Function',
					'args': 'Display as String [optional], UserID as String [optional], WSName as String [optional], TextPath as String [optional], TimeoutSec as Long [optional]',
					'return': 'IpfcAsyncConnection',
					'info': 'Causes the application to connect to an existing Creo Parametric process and to an existing Pro/INTRALINK worksapce running on a host using a specified display. For use in simple and full asynchronous modes.',
					'com': 'pfcls.pfcAsyncConnection.1',
				},
				'Disconnect': {
					'type': 'Sub',
					'args': 'TimeoutSec as Long [optional]',
					'return': '',
					'info': 'Disconnects the application from the current Creo Parametric process.  Creo Parametric will still be running after this call. ',
				},
				'End': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Terminates and disconnects from the current Creo Parametric process. ',
				},
				'EventProcess': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Enables your process to respond to listener calls from Creo Parametric. You must call this function within your control loop in order that your application will be able to respond to listener calls. Creo Parametric will be blocked until the application responds to a registered listener.',
				},
				'ExecuteAsyncJavaMethod': {
					'type': 'Function',
					'args': 'Class as String, Method as String, InputArguments as IpfcArguments, ClassForUnlock as String',
					'return': ' as Boolean',
					'info': 'This is the "JOTK sandbox" to execute methods with full OTK Asynchronous Java access. Can be used only in Java language binding.',
				},
				'CCpfcAsyncConnection.GetActiveConnection': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcAsyncConnection',
					'info': 'Connection established between your application and Creo Parametric. If your application is connected with Creo Parametric, call this function to get the connection object.',
					'com': 'pfcls.pfcAsyncConnection.1',
				},
				'GetConnectionId': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcConnectionId',
					'info': 'Extracts the identification handle of the current Creo Parametric process. ',
				},
				'InterruptEventProcessing': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Call this function from within a Creo Parametric callback function to halt a loop currently running in IpfcAsyncConnection.WaitForEvents().',
				},
				'IsRunning': {
					'type': 'Function',
					'args': '',
					'return': ' as Boolean',
					'info': 'Determines whether you are currently connected to an actively responding Creo Parametric session.',
				},
				'CCpfcAsyncConnection.Start': {
					'type': 'Function',
					'args': 'CmdLine as String, TextPath as String [optional]',
					'return': 'IpfcAsyncConnection',
					'info': 'Causes the application to spawn and connect to a new Creo Parametric session. For use in simple and full asynchronous modes.',
					'com': 'pfcls.pfcAsyncConnection.1',
				},
				'WaitForEvents': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Initiates an event loop which will call event processing periodically to handle events returned from Creo Parametric. Applicable only for full asynchronous applications which are not supposed to carry out any tasks while waiting for Creo Parametric events. ',
				},
			},
	}
	IpfcAttachment = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcFreeAttachment': '',
				'IpfcOffsetAttachment': '',
				'IpfcParametricAttachment': '',
				'IpfcUnsupportedAttachment': '',
			},
			'properties': {
			},
			'method': {
				'GetType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcAttachmentType',
					'info': 'Returns the type of detail attachment that this data object represents.',
				},
			},
	}
	IpfcAxis = {
			'type': 'Classes',
			'parent': {
				'IpfcModelItem': '',
			},
			'child': {
			},
			'properties': {
				'IsVisible': {
					'return': 'as Boolean',
					'info': 'true if the geometry is visible and active, false if it is invisible and inactive.  Inactive geometry may not have all geometric properties defined.',
				},
				'Surf': {
					'return': 'IpfcSurface',
					'info': 'The cylindrical or revolved surface through the center of which the axis runs.',
				},
			},
			'method': {
				'GetFeature': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcFeature',
					'info': 'Returns the feature which contains the geometry.',
				},
			},
	}
	IpfcBaseDimension = {
			'type': 'Classes',
			'parent': {
				'IpfcBaseParameter': '',
				'IpfcModelItem': '',
			},
			'child': {
				'IpfcDimension': '',
				'IpfcRefDimension': '',
				'IpfcUDFDimension': '',
				'IpfcDimension2D': '',
			},
			'properties': {
				'DimType': {
					'return': 'IpfcDimensionType',
					'info': 'The dimension type',
				},
				'DimValue': {
					'return': 'as Double',
					'info': 'The value of the dimension. ',
				},
				'ExtendsInNegativeDirection': {
					'return': 'as Boolean',
					'info': 'Identifies if the dimension is currently considered to have a negative "sign" in generating its direction. If the dimension has a negative sign then the dimension is oriented in the opposite manner to the default for the feature.',
				},
				'Symbol': {
					'return': 'as String',
					'info': 'The dimension name or symbol ',
				},
				'Texts': {
					'return': 'Istringseq',
					'info': 'The dimension text',
				},
			},
			'method': {
				'Erase': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Erases the dimension from a model or drawing.',
				},
				'Show': {
					'type': 'Sub',
					'args': 'Instructions as IpfcDimensionShowInstructions [optional]',
					'return': '',
					'info': 'Forces the display of the dimension in a model.  ',
				},
			},
	}
	IpfcBaseParameter = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
				'IpfcParameter': '',
				'IpfcBaseDimension': '',
			},
			'properties': {
				'IsDesignated': {
					'return': 'as Boolean',
					'info': 'If this is true, the parameter is designated for use with Windchill.',
				},
				'IsModified': {
					'return': 'as Boolean',
					'info': '''If this is true, the parameter is modified. A parameter is said to be "modified" when it has been changed, but the parameter's owner has not yet been regenerated. ''',
				},
				'IsRelationDriven': {
					'return': 'as Boolean',
					'info': 'Identifies if the dimension or parameter is relation driven.',
				},
				'Value': {
					'return': 'IpfcParamValue',
					'info': 'The value of the parameter.',
				},
			},
			'method': {
				'ResetFromBackup': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Resets the parameter to the value it had before it was last set in the current Creo Parametric.',
				},
			},
	}
	IpfcBaseSession = {
			'type': 'Classes',
			'parent': {
				'IpfcActionSource': '',
				'IpfcDisplay': '',
				'IpfcParent': '',
			},
			'child': {
				'IpfcSession': '',
			},
			'properties': {
				'ConnectionId': {
					'return': 'as String',
					'info': 'Returns the connection id for the Creo Parametric session that this application is connected to. The identification string can be passed to external asynchronous applications to allow them to connect to this Creo Parametric session.',
				},
				'CurrentModel': {
					'return': 'IpfcModel',
					'info': 'The current Creo Parametric object, or null, if there is no current object',
				},
				'CurrentWindow': {
					'return': 'IpfcWindow',
					'info': 'The current Creo Parametric window',
				},
				'DimensionDisplayMode': {
					'return': 'IpfcDimDisplayMode',
					'info': 'The current dimension display setting.',
				},
			},
			'method': {
				'AllowDuplicateModelItems': {
					'type': 'Sub',
					'args': 'val as Boolean',
					'return': '',
					'info': 'Turns on/off the control of creating duplicate model items.',
				},
				'AuthenticateBrowser': {
					'type': 'Sub',
					'args': 'Username as String, Password as String',
					'return': '',
					'info': 'Preset username and password to allow automatic login into HTTP servers that require authentication.',
				},
				'ChangeDirectory': {
					'type': 'Sub',
					'args': 'Path as String',
					'return': '',
					'info': 'Changes Creo Parametric to another working directory.',
				},
				'CheckoutToWS': {
					'type': 'Sub',
					'args': 'Objects as IpfcModelDescriptors, WSName as String, AsLink as Boolean, RelCriterion as IpfcRelCriterion',
					'return': '',
					'info': 'Checks out a model from Pro/INTRALINK into the workspace.  ',
				},
				'CopyFileFromWS': {
					'type': 'Sub',
					'args': 'SourceFile as String, TargetDirectory as String',
					'return': '',
					'info': 'Copies a file from a Windchill workspace            to a disk location.',
				},
				'CopyFileToWS': {
					'type': 'Sub',
					'args': 'SourceFile as String, TargetWorkspace as String, PrimaryContent as String [optional]',
					'return': '',
					'info': 'Copies a file from local disk to a Windchill	    workspace; either as a primary document or as secondary	    content of an existing document in the workspace.',
				},
				'CreateAssembly': {
					'type': 'Function',
					'args': 'Name as String',
					'return': 'IpfcAssembly',
					'info': 'Creates an empty assembly.',
				},
				'CreateDrawingFromTemplate': {
					'type': 'Function',
					'args': 'Name as String, Template as String, DrawingModel as IpfcModelDescriptor, Options as IpfcDrawingCreateOptions',
					'return': 'IpfcDrawing',
					'info': 'Creates a drawing from a template.',
				},
				'CreateModelWindow': {
					'type': 'Function',
					'args': 'Mdl as IpfcModel',
					'return': 'IpfcWindow',
					'info': 'Creates or retrieves a window for a specified model.',
				},
				'CreatePart': {
					'type': 'Function',
					'args': 'Name as String',
					'return': 'IpfcPart',
					'info': 'Creates a new empty part.',
				},
				'EraseUndisplayedModels': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Erases models which are not used in any Creo Parametric windows.',
				},
				'ExecuteModelCheck': {
					'type': 'Function',
					'args': 'Model as IpfcModel, Instructions as IpfcModelCheckInstructions [optional]',
					'return': 'IpfcModelCheckResults',
					'info': 'Execute ModelCheck from an external application.',
				},
				'ExportCurrentRasterImage': {
					'type': 'Sub',
					'args': 'ImageFileName as String, Instructions as IpfcRasterImageExportInstructions',
					'return': '',
					'info': 'Outputs a standard Creo Parametric raster output file (for current Window).',
				},
				'ExportDirectVRML': {
					'type': 'Sub',
					'args': 'ExportData as IpfcVRMLDirectExportInstructions',
					'return': '',
					'info': 'Exports a Creo Parametric object from disk into VRML format.',
				},
				'ExportFromCurrentWS': {
					'type': 'Function',
					'args': 'FileNames as Istringseq, TargetLocation as String, Dependency as IpfcRelCriterion',
					'return': 'IpfcWSImportExportMessages',
					'info': 'Export specified objects from the current workspace to a disk in            a linked session of Creo Parametric. Can be called only when there             are no objects in session .             The specified objects may be exported along with all or required by            Creo Parametric dependents (recursively), according to a specified            dependency criterion. All problems (warnings, conflicts or errors)            of processing individual objects are logged in proimpex.errors             file created in the Creo Parametric run directory.',
				},
				'FlushCurrentWindow': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Forces the flush of pending display commands   on the current window.   ',
				},
				'GetActiveModel': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcModel',
					'info': 'Retrieves the handle to the active model.',
				},
				'GetActiveServer': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcServer',
					'info': 'Gets the active server handle .',
				},
				'GetAliasFromAliasedUrl': {
					'type': 'Function',
					'args': 'AliasedUrl as String',
					'return': ' as String',
					'info': 'Returns server alias from aliased URL.',
				},
				'GetByRelationId': {
					'type': 'Function',
					'args': 'Id as Long',
					'return': 'IpfcModel',
					'info': 'Returns the model, given its relation identifier.',
				},
				'GetConfigOption': {
					'type': 'Function',
					'args': 'Name as String',
					'return': ' as String [optional]',
					'info': 'Retrieves the current value for the specifiedconfiguration file option.',
				},
				'GetConfigOptionValues': {
					'type': 'Function',
					'args': 'Name as String',
					'return': 'Istringseq',
					'info': 'Retrieves the current value(s) for the specified configuration file option.',
				},
				'GetCurrentDirectory': {
					'type': 'Function',
					'args': '',
					'return': ' as String',
					'info': 'Gets the absolute path name of the current working directory for Creo Parametric.',
				},
				'GetCurrentWS': {
					'type': 'Function',
					'args': '',
					'return': ' as String [optional]',
					'info': 'Returns the name of the Workspace currently registered in Creo Parametric.',
				},
				'GetEnvironmentVariable': {
					'type': 'Function',
					'args': 'Name as String',
					'return': ' as String [optional]',
					'info': 'Returns the value a system environment variable.',
				},
				'GetImportSourceType': {
					'type': 'Function',
					'args': 'FileToImport as String, NewModelType as IpfcNewModelImportType',
					'return': 'IpfcModelType',
					'info': 'Returns the type of model that can be created from a 3D format file.  Note: does not support all types of 3D formats.',
				},
				'GetLocalizedMessageContents': {
					'type': 'Function',
					'args': 'MsgFile as String, Format as String, MessageTexts as Istringseq [optional]',
					'return': ' as String',
					'info': 'Writes a text message to a buffer string.',
				},
				'GetMessageContents': {
					'type': 'Function',
					'args': 'MsgFile as String, Format as String, MessageTexts as Istringseq [optional]',
					'return': ' as String',
					'info': 'Writes a text message to a buffer string.',
				},
				'GetModel': {
					'type': 'Function',
					'args': 'Name as String, Type as IpfcModelType',
					'return': 'IpfcModel',
					'info': 'Finds a model in the Creo Parametric session, given the model name and type.',
				},
				'GetModelFromDescr': {
					'type': 'Function',
					'args': 'MdlDescr as IpfcModelDescriptor',
					'return': 'IpfcModel',
					'info': 'Finds a model in the Creo Parametric session, given the model descriptor.',
				},
				'GetModelFromFileName': {
					'type': 'Function',
					'args': 'FileName as String',
					'return': 'IpfcModel',
					'info': 'Locates and returns a model handle given its file name.',
				},
				'GetModelNameFromAliasedUrl': {
					'type': 'Function',
					'args': 'AliasedUrl as String',
					'return': ' as String',
					'info': 'Returns model name from aliased URL.',
				},
				'GetModelWindow': {
					'type': 'Function',
					'args': 'Mdl as IpfcModel',
					'return': 'IpfcWindow',
					'info': 'Returns the window where a Creo Parametric model is displayed.',
				},
				'GetPrintMdlOptions': {
					'type': 'Function',
					'args': 'model as IpfcModel',
					'return': 'IpfcPrintMdlOption',
					'info': 'Get model options for a specified model.',
				},
				'GetPrintPCFOptions': {
					'type': 'Function',
					'args': 'filename as String, model as IpfcModel',
					'return': 'IpfcPrinterPCFOptions',
					'info': 'Get the print options from a specified PCF file.',
				},
				'GetPrintPlacementOptions': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcPrintPlacementOption',
					'info': 'Get the current print placement options.',
				},
				'GetPrintPrinterOptions': {
					'type': 'Function',
					'args': 'printer_type as String',
					'return': 'IpfcPrintPrinterOption',
					'info': 'Get printer options for a specified printer.',
				},
				'GetProToolkitDll': {
					'type': 'Function',
					'args': 'ApplicationId as String',
					'return': 'IpfcDll',
					'info': 'Finds a Creo Parametric TOOLKIT DLL, given its identifer string.',
				},
				'GetRGBFromStdColor': {
					'type': 'Function',
					'args': 'StdClr as IpfcStdColor',
					'return': 'IpfcColorRGB',
					'info': 'Retrieves the RGB values for the specified color. ',
				},
				'GetServerByAlias': {
					'type': 'Function',
					'args': 'Alias as String',
					'return': 'IpfcServer',
					'info': 'Gets the server handle for a given server alias.',
				},
				'GetServerByUrl': {
					'type': 'Function',
					'args': 'Url as String, WorkspaceName as String [optional]',
					'return': 'IpfcServer',
					'info': 'Gets the server handle for a given server url and workspace name.',
				},
				'GetServerLocation': {
					'type': 'Function',
					'args': 'Url as String',
					'return': 'IpfcServerLocation',
					'info': 'Gets the server location handle for a given server url.',
				},
				'GetUrlFromAliasedUrl': {
					'type': 'Function',
					'args': 'AliasedUrl as String',
					'return': ' as String',
					'info': 'Converts an an aliased url (wtpub://alias/etc/p.prt) suitable for use in functions    like ProMdlLoad to an unaliased URL (http://windchillserver.ptc.com/etc).',
				},
				'GetWindow': {
					'type': 'Function',
					'args': 'Id as Long',
					'return': 'IpfcWindow',
					'info': 'Retrieves the window object by its identifier.',
				},
				'Import2DModel': {
					'type': 'Function',
					'args': 'NewModelName as String, Type as IpfcModelType, FilePath as String, Instructions as IpfcImport2DInstructions',
					'return': 'IpfcModel',
					'info': 'Creates a two-dimensional model from a geometry file.',
				},
				'ImportNewModel': {
					'type': 'Function',
					'args': 'FileToImport as String, NewModelType as IpfcNewModelImportType, Type as IpfcModelType, NewModelName as String, Filter as IpfcLayerImportFilter [optional]',
					'return': 'IpfcModel',
					'info': ' Imports a 3D format file as a new model.',
				},
				'ImportToCurrentWS': {
					'type': 'Function',
					'args': 'Filepaths as Istringseq, Dependency as IpfcRelCriterion',
					'return': 'IpfcWSImportExportMessages',
					'info': 'Import specified objects from a disk to the current workspace in            a linked session of Creo Parametric. Can be called only when there             are no objects in session .             The specified objects may be imported along with all or required by            Creo Parametric dependents (recursively), according to a specified            dependency criterion. All problems (warnings, conflicts or errors)            of processing individual objects are logged in proimpex.errors             file created in the Creo Parametric run directory.',
				},
				'IsConfigurationSupported': {
					'type': 'Function',
					'args': 'Type as IpfcExportType, Configuration as IpfcAssemblyConfiguration',
					'return': ' as Boolean',
					'info': 'Identifies if an assembly configuration is supported for a particular export type.',
				},
				'IsGeometryRepSupported': {
					'type': 'Function',
					'args': 'Type as IpfcExportType, Flags as IpfcGeometryFlags',
					'return': ' as Boolean',
					'info': 'Identifies if a combination of geometry flags are supported for export to a certain format. ',
				},
				'ListFiles': {
					'type': 'Function',
					'args': 'Filter as String, Version as IpfcFileListOpt, Path as String [optional]',
					'return': 'Istringseq',
					'info': 'Returns a list of the files in the designated directory.',
				},
				'ListModels': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcModels',
					'info': 'Retrieves the list of objects in the Creo Parametric session.',
				},
				'ListModelsByType': {
					'type': 'Function',
					'args': 'Type as IpfcModelType',
					'return': 'IpfcModels',
					'info': 'Returns all of the models of a particular type in the Creo Parametric session.',
				},
				'ListServers': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcServers',
					'info': 'Sequence of servers , or null if server was not found',
				},
				'ListSubdirectories': {
					'type': 'Function',
					'args': 'Path as String [optional]',
					'return': 'Istringseq',
					'info': 'Lists the subdirectories in a given directory location.',
				},
				'ListWindows': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcWindows',
					'info': 'Lists all the Creo Parametric windows.',
				},
				'LoadConfigFile': {
					'type': 'Sub',
					'args': 'ConfigFileName as String',
					'return': '',
					'info': 'Reads a configuration file into Creo Parametric.',
				},
				'LoadProToolkitDll': {
					'type': 'Function',
					'args': 'ApplicationName as String, DllPath as String, TextPath as String, UserDisplay as Boolean',
					'return': 'IpfcDll',
					'info': 'Causes Creo Parametric to load a Creo Parametric TOOLKIT DLL application.    ',
				},
				'LoadProToolkitLegacyDll': {
					'type': 'Function',
					'args': 'ApplicationName as String, DllPath as String, TextPath as String, UserDisplay as Boolean',
					'return': 'IpfcDll',
					'info': 'Causes Creo Parametric to load a Creo Parametric TOOLKIT DLL non-Unicode application.    ',
				},
				'OpenFile': {
					'type': 'Function',
					'args': 'MdlDescr as IpfcModelDescriptor',
					'return': 'IpfcWindow',
					'info': 'Retrieves the specified model and displays it in a window.',
				},
				'PLMSInitialize': {
					'type': 'Function',
					'args': 'request as String',
					'return': ' as String',
					'info': '  ',
				},
				'QueryFeatureEdit': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSelections',
					'info': "Find the list of features being displayed for modification, as is done in 'Edit' on a feature.",
				},
				'RegisterCustomModelCheck': {
					'type': 'Sub',
					'args': 'Instructions as IpfcCustomCheckInstructions',
					'return': '',
					'info': 'Registers an custom check that can be included in any ModelCheck execution.',
				},
				'RegisterRelationFunction': {
					'type': 'Sub',
					'args': 'Name as String, Listener as IpfcRelationFunctionListener, Options as IpfcRelationFunctionOptions [optional]',
					'return': '',
					'info': 'Register an external function which can be included in relations.',
				},
				'RegisterServer': {
					'type': 'Function',
					'args': 'Alias as String, Location as String, WorkspaceName as String',
					'return': 'IpfcServer',
					'info': 'Registers a server in the session given its root location and an alias. ',
				},
				'RetrieveAssemSimpRep': {
					'type': 'Function',
					'args': 'AssemName as String, Instructions as IpfcSimpRepInstructions [optional]',
					'return': 'IpfcAssembly',
					'info': 'Retrieves the specified simplified representation.   ',
				},
				'RetrieveGeometryPartRep': {
					'type': 'Function',
					'args': 'PartName as String',
					'return': 'IpfcPart',
					'info': "Retrieves the specified part's geometric simplified     representation.",
				},
				'RetrieveGeomSimpRep': {
					'type': 'Function',
					'args': 'AssemName as String',
					'return': 'IpfcAssembly',
					'info': 'Retrieves the geometry simplified representation of an assembly.   ',
				},
				'RetrieveGraphicsPartRep': {
					'type': 'Function',
					'args': 'PartName as String',
					'return': 'IpfcPart',
					'info': "Retrieves the specified part's graphic simplified     representation.",
				},
				'RetrieveGraphicsSimpRep': {
					'type': 'Function',
					'args': 'AssemName as String',
					'return': 'IpfcAssembly',
					'info': 'Retrieves the graphics simplified representation of an assembly.   ',
				},
				'RetrieveModel': {
					'type': 'Function',
					'args': 'MdlDescr as IpfcModelDescriptor',
					'return': 'IpfcModel',
					'info': 'Retrieves the specified model into the Creo Parametric session from standard directory.',
				},
				'RetrieveModelWithOpts': {
					'type': 'Function',
					'args': 'MdlDescr as IpfcModelDescriptor, Opts as IpfcRetrieveModelOptions',
					'return': 'IpfcModel',
					'info': 'Retrieves the specified model into the Creo Parametric session based on the path    specified in the model descriptor.',
				},
				'RetrievePartSimpRep': {
					'type': 'Function',
					'args': 'PartName as String, RepName as String',
					'return': 'IpfcPart',
					'info': "Retrieves the specified part's user-defined simplified    representation.",
				},
				'RetrieveSymbolicPartRep': {
					'type': 'Function',
					'args': 'PartName as String',
					'return': 'IpfcPart',
					'info': "Retrieves the specified part's symbolic simplified     representation.",
				},
				'RetrieveSymbolicSimpRep': {
					'type': 'Function',
					'args': 'AssemName as String',
					'return': 'IpfcAssembly',
					'info': 'Retrieves the symbolic simplified representation of an assembly.   ',
				},
				'RunMacro': {
					'type': 'Sub',
					'args': 'Macro as String',
					'return': '',
					'info': 'Runs the specified macro. ',
				},
				'Select': {
					'type': 'Function',
					'args': 'Options as IpfcSelectionOptions, InitialSels as IpfcSelections [optional]',
					'return': 'IpfcSelections',
					'info': 'Allows interactive selection of objects in Creo Parametric.',
				},
				'SetConfigOption': {
					'type': 'Sub',
					'args': 'Name as String, Value as String',
					'return': '',
					'info': 'Sets the value of the specified configuration file option.',
				},
				'SetCurrentWS': {
					'type': 'Sub',
					'args': 'NewWSName as String',
					'return': '',
					'info': 'Registers a Workspace in Creo Parametric. Only one is allowed at a time, so if one is already registered, the new one will substitute the current one, but only if thereare no objects in session.',
				},
				'SetLineStyle': {
					'type': 'Function',
					'args': 'NewStyle as IpfcStdLineStyle',
					'return': 'IpfcStdLineStyle',
					'info': 'Sets the line style.',
				},
				'SetStdColorFromRGB': {
					'type': 'Sub',
					'args': 'StdClr as IpfcStdColor, Color as IpfcColorRGB',
					'return': '',
					'info': 'Enables you to set the values of the three color componentsfor a standard color in Creo Parametric.',
				},
				'SetTextColor': {
					'type': 'Function',
					'args': 'NewColor as IpfcStdColor',
					'return': 'IpfcStdColor',
					'info': 'Enables you to change the text color so any subsequentgraphics window text is drawn in the specified color.',
				},
				'SetWSExportOptions': {
					'type': 'Sub',
					'args': 'Options as IpfcWSExportOptions',
					'return': '',
					'info': 'Sets the option controlling whether or not to export secondary            contents during a call to  IpfcBaseSession.ExportFromCurrentWS()',
				},
				'StartJLinkApplication': {
					'type': 'Function',
					'args': 'ApplicationName as String, ClassName as String, StartMethod as String, StopMethod as String, AdditionalClassPath as String [optional], TextPath as String [optional], UserDisplay as Boolean',
					'return': 'IpfcJLinkApplication',
					'info': 'Register and start a synchronous J-Link application.',
				},
				'UIRegisterFileOpen': {
					'type': 'Sub',
					'args': 'Options as IpfcFileOpenRegisterOptions, Listener as IpfcFileOpenRegisterListener',
					'return': '',
					'info': 'Register a file type in File Open Dialog.',
				},
				'UIRegisterFileSave': {
					'type': 'Sub',
					'args': 'Options as IpfcFileSaveRegisterOptions, Listener as IpfcFileSaveRegisterListener',
					'return': '',
					'info': "Register a file type in File 'Save a Copy' Dialog.",
				},
				'WLBInitialize': {
					'type': 'Sub',
					'args': 'val as Boolean',
					'return': '',
					'info': '  ',
				},
			},
	}
	IpfcBitmapImageExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcRasterImageExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcBitmapImageExportInstructions.Create': {
					'type': 'Function',
					'args': 'ImageWidth as Double, ImageHeight as Double',
					'return': 'IpfcBitmapImageExportInstructions',
					'info': 'Creates a new instructions object used to export BMP--format (type) image.',
					'com': 'pfcls.pfcBitmapImageExportInstructions.1',
				},
			},
	}
	IpfcBOMExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcBOMExportInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcBOMExportInstructions',
					'info': 'This method creates a new instructions object used to export a BOM for an assembly.',
					'com': 'pfcls.pfcBOMExportInstructions.1',
				},
			},
	}
	IpfcBSpline = {
			'type': 'Classes',
			'parent': {
				'IpfcCurve': '',
			},
			'child': {
			},
			'properties': {
				'Degree': {
					'return': 'as Long',
					'info': "The basis function's degree",
				},
				'Knots': {
					'return': 'Irealseq',
					'info': 'The array of knots on the parameter line',
				},
				'Points': {
					'return': 'IpfcBSplinePoints',
					'info': 'The array of control points',
				},
			},
			'method': {
			},
	}
	IpfcBSplineDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcCurveDescriptor': '',
			},
			'child': {
			},
			'properties': {
				'Degree': {
					'return': 'as Long',
					'info': "The basis function's degree",
				},
				'Knots': {
					'return': 'Irealseq',
					'info': 'The array of knots on the parameter line',
				},
				'Points': {
					'return': 'IpfcBSplinePoints',
					'info': 'The array of control points',
				},
			},
			'method': {
				'CCpfcBSplineDescriptor.Create': {
					'type': 'Function',
					'args': 'Degree as Long, Points as IpfcBSplinePoints, Knots as Irealseq',
					'return': 'IpfcBSplineDescriptor',
					'info': 'This method creates a new BSplineDescriptor object.',
					'com': 'pfcls.pfcBSplineDescriptor.1',
				},
			},
	}
	IpfcBSplinePoint = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Point': {
					'return': 'IpfcPoint3D',
					'info': 'The control point',
				},
				'Weight': {
					'return': 'as Double [optional]',
					'info': 'The weight',
				},
			},
			'method': {
				'CCpfcBSplinePoint.Create': {
					'type': 'Function',
					'args': 'Point as IpfcPoint3D, Weight as Double [optional]',
					'return': 'IpfcBSplinePoint',
					'info': 'This method creates a new BSpline point object.',
					'com': 'pfcls.pfcBSplinePoint.1',
				},
			},
	}
	IpfcCableParamsFileInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExportInstructions': '',
			},
			'child': {
			},
			'properties': {
				'CableName': {
					'return': 'as String',
					'info': 'The name of the cable in the harness whose parameters will be exported.',
				},
				'Mdl': {
					'return': 'IpfcModel',
					'info': 'A harness model referenced by the assembly.',
				},
			},
			'method': {
				'CCpfcCableParamsFileInstructions.Create': {
					'type': 'Function',
					'args': 'Mdl as IpfcModel, CableName as String',
					'return': 'IpfcCableParamsFileInstructions',
					'info': 'Creates a new instructions object used to export cable parameters from an assembly.',
					'com': 'pfcls.pfcCableParamsFileInstructions.1',
				},
			},
	}
	IpfcCableParamsImportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcImportInstructions': '',
			},
			'child': {
			},
			'properties': {
				'CableName': {
					'return': 'as String',
					'info': 'The name of the imported cable file.',
				},
				'Harness': {
					'return': 'IpfcModel',
					'info': 'A harness model referenced by the assembly.',
				},
			},
			'method': {
				'CCpfcCableParamsImportInstructions.Create': {
					'type': 'Function',
					'args': 'Harness as IpfcModel, CableName as String',
					'return': 'IpfcCableParamsImportInstructions',
					'info': 'Creates a new instructions object used to import from cable parameters (CABLE_PARAMS) type file.',
					'com': 'pfcls.pfcCableParamsImportInstructions.1',
				},
			},
	}
	IpfcCADDSExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcCADDSExportInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcCADDSExportInstructions',
					'info': 'Creates an instructions object used to export a model to CADDS format.',
					'com': 'pfcls.pfcCADDSExportInstructions.1',
				},
			},
	}
	IpfcCatiaCGR3DExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExport3DInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcCatiaCGR3DExportInstructions.Create': {
					'type': 'Function',
					'args': 'inConfiguration as IpfcAssemblyConfiguration, inGeometry as IpfcGeometryFlags',
					'return': 'IpfcCatiaCGR3DExportInstructions',
					'info': 'Creates a new instructions object used to export a solid model to Catia CGR format.',
					'com': 'pfcls.pfcCatiaCGR3DExportInstructions.1',
				},
			},
	}
	IpfcCATIAFacetsExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcCoordSysExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcCATIAFacetsExportInstructions.Create': {
					'type': 'Function',
					'args': 'CsysName as String [optional]',
					'return': 'IpfcCATIAFacetsExportInstructions',
					'info': 'Creates a new instructions object used to export a part or assembly in CATIA format (as a faceted model).',
					'com': 'pfcls.pfcCATIAFacetsExportInstructions.1',
				},
			},
	}
	IpfcCATIAModel3DExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExport3DInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcCATIAModel3DExportInstructions.Create': {
					'type': 'Function',
					'args': 'inConfiguration as IpfcAssemblyConfiguration, inGeometry as IpfcGeometryFlags',
					'return': 'IpfcCATIAModel3DExportInstructions',
					'info': 'Creates a new instructions object used to export a model to CATIA direct *.model format.',
					'com': 'pfcls.pfcCATIAModel3DExportInstructions.1',
				},
			},
	}
	IpfcCatiaPart3DExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExport3DInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcCatiaPart3DExportInstructions.Create': {
					'type': 'Function',
					'args': 'inConfiguration as IpfcAssemblyConfiguration, inGeometry as IpfcGeometryFlags',
					'return': 'IpfcCatiaPart3DExportInstructions',
					'info': 'Creates a new instructions object used to export a solid model to Catia part format.',
					'com': 'pfcls.pfcCatiaPart3DExportInstructions.1',
				},
			},
	}
	IpfcCatiaProduct3DExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExport3DInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcCatiaProduct3DExportInstructions.Create': {
					'type': 'Function',
					'args': 'inConfiguration as IpfcAssemblyConfiguration, inGeometry as IpfcGeometryFlags',
					'return': 'IpfcCatiaProduct3DExportInstructions',
					'info': 'Creates a new instructions object used to export a solid model to Catia product format.',
					'com': 'pfcls.pfcCatiaProduct3DExportInstructions.1',
				},
			},
	}
	IpfcCATIASession3DExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExport3DInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcCATIASession3DExportInstructions.Create': {
					'type': 'Function',
					'args': 'inConfiguration as IpfcAssemblyConfiguration, inGeometry as IpfcGeometryFlags',
					'return': 'IpfcCATIASession3DExportInstructions',
					'info': 'Creates a new instructions object used to export a solid model to a CATIA sesion.',
					'com': 'pfcls.pfcCATIASession3DExportInstructions.1',
				},
			},
	}
	IpfcCGMFILEExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExportInstructions': '',
			},
			'child': {
			},
			'properties': {
				'ExportType': {
					'return': 'IpfcCGMExportType',
					'info': 'A IpfcCGMExportType object that specifies ASCII or binary',
				},
				'WhichScale': {
					'return': 'IpfcCGMScaleType',
					'info': 'A IpfcCGMScaleType object that specifies abstract or metric scale',
				},
			},
			'method': {
				'CCpfcCGMFILEExportInstructions.Create': {
					'type': 'Function',
					'args': 'ExportType as IpfcCGMExportType, WhichScale as IpfcCGMScaleType',
					'return': 'IpfcCGMFILEExportInstructions',
					'info': 'Creates a new instructions object used to export a drawing in CGM format.',
					'com': 'pfcls.pfcCGMFILEExportInstructions.1',
				},
			},
	}
	IpfcCheckinOptions = {
			'type': 'Classes',
			'parent': {
				'IpfcUploadBaseOptions': '',
			},
			'child': {
			},
			'properties': {
				'BaselineLifecycle': {
					'return': 'as String [optional]',
					'info': 'Specifies the name of the lifecycle. ',
				},
				'BaselineLocation': {
					'return': 'as String [optional]',
					'info': 'Specifies the location of the baseline. ',
				},
				'BaselineName': {
					'return': 'as String [optional]',
					'info': 'Specifies the name of the baseline.null means not to create any baseline ',
				},
				'BaselineNumber': {
					'return': 'as String [optional]',
					'info': 'Specifies the number of the baseline.  ',
				},
				'KeepCheckedout': {
					'return': 'as Boolean [optional]',
					'info': 'Specifies the checked out status of the object .If the value specified is true, then the    contents of the selected object are checked in to the Windchill server and automatically checked out    again for further modification. ',
				},
			},
			'method': {
				'CCpfcCheckinOptions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcCheckinOptions',
					'info': 'Creates a new IpfcCheckinOptions object.',
					'com': 'pfcls.pfcCheckinOptions.1',
				},
			},
	}
	IpfcCheckoutOptions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Dependency': {
					'return': 'IpfcServerDependency',
					'info': 'Specifies the dependency rule while checking out dependents of the object that is selected for    checkout.',
				},
				'Download': {
					'return': 'as Boolean [optional]',
					'info': 'Specifies the checkout type as download or link. Download specifies that the object content is    downloaded and checked out while link specifies that only the metadata is downloaded and checked out. ',
				},
				'IncludeInstances': {
					'return': 'IpfcServerIncludeInstances',
					'info': 'Specifies the rule while including instances from the family table during checkout. ',
				},
				'Readonly': {
					'return': 'as Boolean [optional]',
					'info': 'Specifies the checkout type as a read-only checkout. This option is applicable only if the checkout     type is link. ',
				},
				'SelectedIncludes': {
					'return': 'Istringseq',
					'info': "Sequence of URL's to the selected includes, if include option = SERVER_INCLUDE_SELECTED.    Can be null",
				},
				'Version': {
					'return': 'as String [optional]',
					'info': 'Specifies the version of the object that is checked out. ',
				},
			},
			'method': {
				'CCpfcCheckoutOptions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcCheckoutOptions',
					'info': 'Creates a new IpfcCheckoutOptions object.',
					'com': 'pfcls.pfcCheckoutOptions.1',
				},
			},
	}
	IpfcChild = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
				'IpfcUnit': '',
				'IpfcUnitSystem': '',
				'IpfcNamedModelItem': '',
				'IpfcModelItem': '',
				'IpfcView': '',
				'IpfcExternalDataAccess': '',
				'IpfcModel': '',
				'IpfcXSection': '',
				'IpfcMaterial': '',
				'IpfcDisplayList2D': '',
				'IpfcDisplayList3D': '',
				'IpfcView2D': '',
				'IpfcSelectionBuffer': '',
				'IpfcWindow': '',
				'IpfcDll': '',
				'IpfcPopupmenu': '',
				'IpfcServerLocation': '',
				'IpfcNonRegisteredServer': '',
				'IpfcServer': '',
			},
			'properties': {
				'DBParent': {
					'return': 'IpfcParent',
					'info': 'The parent feature',
				},
				'OId': {
					'return': 'IpfcOId',
					'info': 'The object identifier',
				},
			},
			'method': {
			},
	}
	IpfcCircle = {
			'type': 'Classes',
			'parent': {
				'IpfcCurve': '',
			},
			'child': {
			},
			'properties': {
				'Center': {
					'return': 'IpfcPoint3D',
					'info': 'The center of the circle',
				},
				'Radius': {
					'return': 'as Double',
					'info': 'The radius',
				},
				'UnitNormal': {
					'return': 'IpfcVector3D',
					'info': 'The normal axis unit vector',
				},
			},
			'method': {
			},
	}
	IpfcCircleDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcCurveDescriptor': '',
			},
			'child': {
			},
			'properties': {
				'Center': {
					'return': 'IpfcPoint3D',
					'info': 'The center of the circle',
				},
				'Radius': {
					'return': 'as Double',
					'info': 'The radius',
				},
				'UnitNormal': {
					'return': 'IpfcVector3D',
					'info': 'The normal axis unit vector',
				},
			},
			'method': {
				'CCpfcCircleDescriptor.Create': {
					'type': 'Function',
					'args': 'Center as IpfcPoint3D, Radius as Double, UnitNormal as IpfcVector3D',
					'return': 'IpfcCircleDescriptor',
					'info': 'This method creates a new CircleDescriptor object.',
					'com': 'pfcls.pfcCircleDescriptor.1',
				},
			},
	}
	IpfcClearanceData = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
			},
			'properties': {
				'Distance': {
					'return': 'as Double',
					'info': 'The value of the computed clearance.',
				},
				'IsInterfering': {
					'return': 'as Boolean',
					'info': 'The bool flag that indicates if the interference is detected (true) or not (false).',
				},
				'NearestPoints': {
					'return': 'IpfcPoint3Ds',
					'info': "The closest points (in respective part's coord sys).",
				},
			},
			'method': {
			},
	}
	IpfcColorRGB = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Blue': {
					'return': 'as Double',
					'info': 'The blue intensity',
				},
				'Green': {
					'return': 'as Double',
					'info': 'The green intensity',
				},
				'Red': {
					'return': 'as Double',
					'info': 'The red intensity',
				},
			},
			'method': {
				'CCpfcColorRGB.Create': {
					'type': 'Function',
					'args': 'inRed as Double, inGreen as Double, inBlue as Double',
					'return': 'IpfcColorRGB',
					'info': 'Creates the values for the color map.',
					'com': 'pfcls.pfcColorRGB.1',
				},
			},
	}
	IpfcColumnCreateOption = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'ColumnWidth': {
					'return': 'as Double',
					'info': 'The column width.  This is either in length units or in the number of characters, depending on the value of the SizeType attribute in IpfcTableCreateInstructions.',
				},
				'Justification': {
					'return': 'IpfcColumnJustification',
					'info': 'The column justification.',
				},
			},
			'method': {
				'CCpfcColumnCreateOption.Create': {
					'type': 'Function',
					'args': 'Justification as IpfcColumnJustification, ColumnWidth as Double',
					'return': 'IpfcColumnCreateOption',
					'info': 'Creates a data object used for creating a column in a drawing table.',
					'com': 'pfcls.pfcColumnCreateOption.1',
				},
			},
	}
	IpfcCompModelReplace = {
			'type': 'Classes',
			'parent': {
				'IpfcFeatureOperation': '',
			},
			'child': {
			},
			'properties': {
				'NewModel': {
					'return': 'IpfcModel',
					'info': "The model that will replace the component's current model when the CompModelReplace operation is executed.",
				},
			},
			'method': {
			},
	}
	IpfcComponentConstraint = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'AssemblyDatumSide': {
					'return': 'IpfcDatumSide',
					'info': 'The datum plane side, if the AssemblyReference is a datum plane.',
				},
				'AssemblyReference': {
					'return': 'IpfcSelection',
					'info': 'The geometric reference from the assembly.  This reference must be created using an assembly component path. Use null for constraint types not requiring a component reference.',
				},
				'Attributes': {
					'return': 'IpfcConstraintAttributes',
					'info': 'Constraint related attributed. Use null for the default settings.',
				},
				'ComponentDatumSide': {
					'return': 'IpfcDatumSide',
					'info': 'The datum plane side, if the ComponentReference is a datum plane.',
				},
				'ComponentReference': {
					'return': 'IpfcSelection',
					'info': 'The geometric reference on the component.  Use null for constraint types not requiring a component reference.',
				},
				'Offset': {
					'return': 'as Double [optional]',
					'info': 'The offset value, if appropriate to the constraint type.  Use null to represent a 0.0 value, or for constraint types not requiring an offset.',
				},
				'Type': {
					'return': 'IpfcComponentConstraintType',
					'info': 'The type of constraint.',
				},
				'UserDefinedData': {
					'return': 'as String [optional]',
					'info': 'A user created string stored with the constraint.  Typically null.',
				},
			},
			'method': {
				'CCpfcComponentConstraint.Create': {
					'type': 'Function',
					'args': 'Type as IpfcComponentConstraintType',
					'return': 'IpfcComponentConstraint',
					'info': 'Creates a new component constraint object.',
					'com': 'pfcls.pfcComponentConstraint.1',
				},
			},
	}
	IpfcComponentDimensionShowInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcDimensionShowInstructions': '',
			},
			'child': {
				'IpfcDrawingDimensionShowInstructions': '',
			},
			'properties': {
				'Path': {
					'return': 'IpfcComponentPath',
					'info': 'The component path from the assembly to the component containing the dimension.',
				},
			},
			'method': {
				'CCpfcComponentDimensionShowInstructions.Create': {
					'type': 'Function',
					'args': 'Path as IpfcComponentPath [optional]',
					'return': 'IpfcComponentDimensionShowInstructions',
					'info': 'Returns a new instance of IpfcComponentDimensionShowInstructions.',
					'com': 'pfcls.pfcComponentDimensionShowInstructions.1',
				},
			},
	}
	IpfcComponentFeat = {
			'type': 'Classes',
			'parent': {
				'IpfcFeature': '',
			},
			'child': {
			},
			'properties': {
				'CompType': {
					'return': 'IpfcComponentType',
					'info': 'The component type, which identifies the purpose for which the component is used in a production assembly.',
				},
				'IsBulkitem': {
					'return': 'as Boolean',
					'info': 'true if the component is a bulkitem, false otherwise.',
				},
				'IsFrozen': {
					'return': 'as Boolean',
					'info': 'true if the component is frozen due to missing references, false otherwise.',
				},
				'IsPackaged': {
					'return': 'as Boolean',
					'info': 'true if the component is completely packaged, false if it is partially or fully constrained.',
				},
				'IsPlaced': {
					'return': 'as Boolean',
					'info': 'true if the component is placed in the assembly, false if it is unplaced.',
				},
				'IsSubstitute': {
					'return': 'as Boolean',
					'info': 'true if the component is a substitution in a simplified rep, false otherwise.',
				},
				'IsUnderconstrained': {
					'return': 'as Boolean',
					'info': 'true if the component is partially constrained, false if it is fully packaged or fully constrained.',
				},
				'ModelDescr': {
					'return': 'IpfcModelDescriptor',
					'info': 'The model descriptor of the component part or subassembly',
				},
				'Position': {
					'return': 'IpfcTransform3D',
					'info': 'The initial position and orientation of the component in the assembly.  If the component is fully packaged, this is the actual position.  If the component is underconstrained, the constraints will override part of this matrix. If the component is fully constrained, the entire matrix will be overridden.',
				},
			},
			'method': {
				'CopyTemplateContents': {
					'type': 'Sub',
					'args': 'TemplateModel as IpfcModel',
					'return': '',
					'info': 'Copies the specified template model in to the component.',
				},
				'CreateReplaceOp': {
					'type': 'Function',
					'args': 'NewModel as IpfcModel',
					'return': 'IpfcCompModelReplace',
					'info': 'Creates a replacement operation.',
				},
				'GetConstraints': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcComponentConstraints',
					'info': 'Returns a sequence of objects representing the component constraints.',
				},
				'MoveThroughUI': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Invokes the Creo Parametric UI to Package/Move the component.',
				},
				'RedefineThroughUI': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Invokes the Creo Parametric dialog box to redefine the component constraints.',
				},
				'Regenerate': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Regenerates the assembly component.',
				},
				'SetConstraints': {
					'type': 'Sub',
					'args': 'Constraints as IpfcComponentConstraints [optional], ReferenceAssembly as IpfcComponentPath [optional]',
					'return': '',
					'info': 'Sets the component constraints for the component feature.',
				},
			},
	}
	IpfcComponentPath = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
			},
			'properties': {
				'ComponentIds': {
					'return': 'Iintseq',
					'info': 'The path from the root assembly to the component through various subassembliescontaining this component. Each element of this sequence is an identifierof a component feature in its parent assembly.',
				},
				'Leaf': {
					'return': 'IpfcSolid',
					'info': 'The model associated with the component the path points to',
				},
				'Root': {
					'return': 'IpfcAssembly',
					'info': 'The root assembly',
				},
			},
			'method': {
				'GetIsVisible': {
					'type': 'Function',
					'args': 'InRep as IpfcSimpRep [optional]',
					'return': ' as Boolean',
					'info': 'Returns a flag indicating whether a given component is visible in a simplified representation.',
				},
				'GetTransform': {
					'type': 'Function',
					'args': 'BottomUp as Boolean',
					'return': 'IpfcTransform3D',
					'info': 'Retrieves the transformation matrix from the coordinate system of the root assemby to the coordinate system of the model pointed to by the component path, or from the coordinate system of the model tothe coordinate system of the root assembly.',
				},
				'SetTransform': {
					'type': 'Sub',
					'args': 'BottomUp as Boolean, position as IpfcTransform3D',
					'return': '',
					'info': 'Sets the position of the component described by the component path.',
				},
			},
	}
	IpfcCompositeCurve = {
			'type': 'Classes',
			'parent': {
				'IpfcCurve': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'ListElements': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcCurves',
					'info': 'Lists the component curves of the composite curve.',
				},
			},
	}
	IpfcCompositeCurveDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcCurveDescriptor': '',
			},
			'child': {
			},
			'properties': {
				'Elements': {
					'return': 'IpfcCurveDescriptors',
					'info': 'The geometry of the component curves.',
				},
			},
			'method': {
				'CCpfcCompositeCurveDescriptor.Create': {
					'type': 'Function',
					'args': 'Elements as IpfcCurveDescriptors',
					'return': 'IpfcCompositeCurveDescriptor',
					'info': 'This method creates a new CompositeCurveDescriptor object.',
					'com': 'pfcls.pfcCompositeCurveDescriptor.1',
				},
			},
	}
	IpfcCone = {
			'type': 'Classes',
			'parent': {
				'IpfcTransformedSurface': '',
			},
			'child': {
			},
			'properties': {
				'Alpha': {
					'return': 'as Double',
					'info': 'The angle between the axis of the cone and the generating line ',
				},
			},
			'method': {
			},
	}
	IpfcConeDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcTransformedSurfaceDescriptor': '',
			},
			'child': {
			},
			'properties': {
				'Alpha': {
					'return': 'as Double',
					'info': 'The angle between the axis of the cone and the generating line ',
				},
			},
			'method': {
				'CCpfcConeDescriptor.Create': {
					'type': 'Function',
					'args': 'Extents as IpfcSurfaceExtents, Orientation as IpfcSurfaceOrientation, Origin as IpfcTransform3D, Alpha as Double',
					'return': 'IpfcConeDescriptor',
					'info': 'This method creates a new ConeDescriptor object.',
					'com': 'pfcls.pfcConeDescriptor.1',
				},
			},
	}
	IpfcConfigImportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcImportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcConfigImportInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcConfigImportInstructions',
					'info': 'Creates a new instructions object used to import (read) from configuration data (CONFIG) type file. Users are recommended to use IpfcBaseSession.LoadConfigFile() rather than the return value of this method.',
					'com': 'pfcls.pfcConfigImportInstructions.1',
				},
			},
	}
	IpfcConnectionId = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'ExternalRep': {
					'return': 'as String',
					'info': 'The string representation of the connection id, suitable for use in other applications connecting to this same session of Creo Parametric.',
				},
			},
			'method': {
				'CCpfcConnectionId.Create': {
					'type': 'Function',
					'args': 'ExternalRep as String',
					'return': 'IpfcConnectionId',
					'info': 'Creates a new connection id object to use in IpfcAsyncConnection.ConnectById().',
					'com': 'pfcls.pfcConnectionId.1',
				},
			},
	}
	IpfcConnectorParamExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExportInstructions': '',
			},
			'child': {
			},
			'properties': {
				'MembIdTab': {
					'return': 'Iintseq',
					'info': 'A member identifier table, a sequence of integers that identify the components that form the path from the root assembly down to the component part or assembly being referred to.',
				},
			},
			'method': {
				'CCpfcConnectorParamExportInstructions.Create': {
					'type': 'Function',
					'args': 'MembIdTab as Iintseq',
					'return': 'IpfcConnectorParamExportInstructions',
					'info': 'Creates a new instructions object used to write the parameters of a connector to a file.',
					'com': 'pfcls.pfcConnectorParamExportInstructions.1',
				},
			},
	}
	IpfcConnectorParamsImportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcImportInstructions': '',
			},
			'child': {
			},
			'properties': {
				'MembIdTab': {
					'return': 'Iintseq',
					'info': 'A member identifier table, a sequence of integers that identify the components that form the path from the root assembly down to the component part or assembly being referred to.',
				},
			},
			'method': {
				'CCpfcConnectorParamsImportInstructions.Create': {
					'type': 'Function',
					'args': 'MembIdTab as Iintseq',
					'return': 'IpfcConnectorParamsImportInstructions',
					'info': 'Creates a new instructions object used to import the parameters of a connector from a file.',
					'com': 'pfcls.pfcConnectorParamsImportInstructions.1',
				},
			},
	}
	IpfcConstraintAttributes = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Force': {
					'return': 'as Boolean',
					'info': 'Force execution of this constraint (required for ASM_CONSTRAINT_PNT_ON_LINE).',
				},
				'Ignore': {
					'return': 'as Boolean',
					'info': 'Ignore this constraint during regeneration.',
				},
			},
			'method': {
				'CCpfcConstraintAttributes.Create': {
					'type': 'Function',
					'args': 'Force as Boolean, Ignore as Boolean',
					'return': 'IpfcConstraintAttributes',
					'info': 'Creates a new constraint attributes object.',
					'com': 'pfcls.pfcConstraintAttributes.1',
				},
			},
	}
	IpfcContour = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
			},
			'properties': {
				'InternalTraversal': {
					'return': 'IpfcContourTraversal',
					'info': "Determines if a countour's traversal is internal or external.",
				},
			},
			'method': {
				'EvalArea': {
					'type': 'Function',
					'args': '',
					'return': ' as Double',
					'info': 'Finds the surface area inside the given outer contour,accounting for internal voids.',
				},
				'EvalOutline': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcOutline2D',
					'info': 'Finds the two-dimensional bounding box of the surface inside the outercontour.',
				},
				'FindContainingContour': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcContour',
					'info': 'Finds the innermost contour that encloses the specifiedcontour.  If the specified contour is internal, the returnedcontour will be external, and vice versa.',
				},
				'ListElements': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcEdges',
					'info': 'Gets an array of edges for the contour. ',
				},
				'VerifyUV': {
					'type': 'Function',
					'args': 'Params as IpfcUVParams',
					'return': 'IpfcPlacement',
					'info': 'Verifies whether the specified UV point lies within thegiven contour.',
				},
			},
	}
	IpfcCoonsPatch = {
			'type': 'Classes',
			'parent': {
				'IpfcSurface': '',
			},
			'child': {
			},
			'properties': {
				'CornerPoints': {
					'return': 'IpfcCoonsCornerPoints',
					'info': 'The corner points',
				},
				'U0Profile': {
					'return': 'IpfcCurveDescriptor',
					'info': 'The curve geometry along u=0',
				},
				'U1Profile': {
					'return': 'IpfcCurveDescriptor',
					'info': 'The curve geometry along u=1',
				},
				'UVDerivatives': {
					'return': 'IpfcCoonsUVDerivatives',
					'info': 'The corner UV mixed derivatives',
				},
				'V0Profile': {
					'return': 'IpfcCurveDescriptor',
					'info': 'The curve geometry along v=0',
				},
				'V1Profile': {
					'return': 'IpfcCurveDescriptor',
					'info': 'The curve geometry along v=1',
				},
			},
			'method': {
			},
	}
	IpfcCoonsPatchDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcSurfaceDescriptor': '',
			},
			'child': {
			},
			'properties': {
				'CornerPoints': {
					'return': 'IpfcCoonsCornerPoints',
					'info': 'The corner points',
				},
				'U0Profile': {
					'return': 'IpfcCurveDescriptor',
					'info': 'The curve geometry along u=0',
				},
				'U1Profile': {
					'return': 'IpfcCurveDescriptor',
					'info': 'The curve geometry along u=1',
				},
				'UVDerivatives': {
					'return': 'IpfcCoonsUVDerivatives',
					'info': 'The corner UV mixed derivatives',
				},
				'V0Profile': {
					'return': 'IpfcCurveDescriptor',
					'info': 'The curve geometry along v=0',
				},
				'V1Profile': {
					'return': 'IpfcCurveDescriptor',
					'info': 'The curve geometry along v=1',
				},
			},
			'method': {
				'CCpfcCoonsPatchDescriptor.Create': {
					'type': 'Function',
					'args': 'Extents as IpfcSurfaceExtents, Orientation as IpfcSurfaceOrientation, U0Profile as IpfcCurveDescriptor, U1Profile as IpfcCurveDescriptor, V0Profile as IpfcCurveDescriptor, V1Profile as IpfcCurveDescriptor, CornerPoints as IpfcCoonsCornerPoints, UVDerivatives as IpfcCoonsUVDerivatives',
					'return': 'IpfcCoonsPatchDescriptor',
					'info': 'This method returns a new CoonsPatchDescriptor object.',
					'com': 'pfcls.pfcCoonsPatchDescriptor.1',
				},
			},
	}
	IpfcCoordSysExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExportInstructions': '',
			},
			'child': {
				'IpfcCATIAFacetsExportInstructions': '',
				'IpfcRenderExportInstructions': '',
				'IpfcSTLASCIIExportInstructions': '',
				'IpfcSTLBinaryExportInstructions': '',
				'IpfcInventorExportInstructions': '',
			},
			'properties': {
				'AngleControl': {
					'return': 'as Double [optional]',
					'info': 'The Angle Control setting. (Pass null if you specify Quality.).   Default value is 0.1.   ',
				},
				'CsysName': {
					'return': 'as String [optional]',
					'info': 'Stores the name of a coordinate-system feature in the model being    exported. A coordinate system that places the part or assembly in its    upper-right quadrant is recommended, so that all position/distance values    are positive. Allowed to be null if exporting as Slice',
				},
				'FacetControlOptions': {
					'return': 'IpfcFacetControlFlags',
					'info': 'Flags (FACET_STEP_SIZE_ADJUST and others) to control Facet export. (Pass null if not exporting slice formats).   ',
				},
				'MaxChordHeight': {
					'return': 'as Double [optional]',
					'info': 'The Maximun Chord Height setting. (Pass null if you specify Quality.).   Default value is 0.1.   ',
				},
				'Quality': {
					'return': 'as Long [optional]',
					'info': 'Can be used in place of MaxChordHeight and AngleControl. A value between 1 and 10. The higher the value you pass, the lower the MaxChordHeight setting and the higher the AngleControl setting that the method will use. Pass null if you use MaxChordHeight and AngleControl values. If Quality, MaxChordHeight, and AngleControl are all null, a Quality value of 3 is used. Default value is 1.0',
				},
				'StepSize': {
					'return': 'as Double [optional]',
					'info': 'The Step Size Control setting. (Pass null if you specify Quality.).   Default value is 0.0.   ',
				},
			},
			'method': {
				'GetSliceExportData': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSliceExportData',
					'info': "Returns an IpfcSliceExportData object or NULL if the IpfcSliceExportData isn't set.",
				},
				'SetSliceExportData': {
					'type': 'Sub',
					'args': 'SliceExpData as IpfcSliceExportData [optional]',
					'return': '',
					'info': 'Sets the IpfcSliceExportData .',
				},
			},
	}
	IpfcCoordSysFeat = {
			'type': 'Classes',
			'parent': {
				'IpfcFeature': '',
			},
			'child': {
			},
			'properties': {
				'DimensionConstraints': {
					'return': 'IpfcDatumCsysDimensionConstraints',
					'info': 'The dimension constraints of the coordinate system.',
				},
				'IsNormalToScreen': {
					'return': 'as Boolean',
					'info': 'Specifies if coordinate system is normal to screen.',
				},
				'OffsetType': {
					'return': 'IpfcDatumCsysOffsetType',
					'info': 'Specifies the offset mode of the coordinate system.',
				},
				'OnSurfaceType': {
					'return': 'IpfcDatumCsysOnSurfaceType',
					'info': 'Specifies the on-surface mode of the coordinate system.',
				},
				'OrientationConstraints': {
					'return': 'IpfcDatumCsysOrientMoveConstraints',
					'info': 'The orientation constraints of the coordinate system.',
				},
				'OrientByMethod': {
					'return': 'IpfcDatumCsysOrientByMethod',
					'info': 'Specifies the orientation method.',
				},
				'OriginConstraints': {
					'return': 'IpfcDatumCsysOriginConstraints',
					'info': 'The origin constraints of the coordinate system.',
				},
			},
			'method': {
			},
	}
	IpfcCoordSystem = {
			'type': 'Classes',
			'parent': {
				'IpfcModelItem': '',
			},
			'child': {
			},
			'properties': {
				'CoordSys': {
					'return': 'IpfcTransform3D',
					'info': 'The coordinate system data, including the transformation matrix, origin, and axes information',
				},
				'IsVisible': {
					'return': 'as Boolean',
					'info': 'true if the geometry is visible and active, false if it is invisible and inactive.  Inactive geometry may not have all geometric properties defined.',
				},
			},
			'method': {
				'GetFeature': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcFeature',
					'info': 'Returns the feature which contains the geometry.',
				},
			},
	}
	IpfcCopyInstructions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcCreateNewSimpRepInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcSimpRepInstructions': '',
			},
			'child': {
			},
			'properties': {
				'NewSimpName': {
					'return': 'as String',
					'info': 'Name of the creating representation.',
				},
			},
			'method': {
				'CCpfcCreateNewSimpRepInstructions.Create': {
					'type': 'Function',
					'args': 'NewSimpName as String',
					'return': 'IpfcCreateNewSimpRepInstructions',
					'info': 'reates a new object used to create a new representation.',
					'com': 'pfcls.pfcCreateNewSimpRepInstructions.1',
				},
			},
	}
	IpfcCriticalDistanceData = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
			},
			'properties': {
				'CriticalPoint1': {
					'return': 'IpfcPoint3D',
					'info': 'The first 3d point where the critical distance is calculated.',
				},
				'CriticalPoint2': {
					'return': 'IpfcPoint3D',
					'info': 'The second 3d point where the critical distance is calculated.',
				},
				'Distance': {
					'return': 'as Double',
					'info': 'The local minimum distance between two selected objects.',
				},
				'SurfParam1': {
					'return': 'IpfcUVParams',
					'info': 'The first surface UV-parameter where the distance is calculated.',
				},
				'SurfParam2': {
					'return': 'IpfcUVParams',
					'info': 'The second surface UV-parameter where the distance is calculated.',
				},
			},
			'method': {
			},
	}
	IpfcCurvatureData = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'MaxCurvature': {
					'return': 'as Double',
					'info': 'The maximum curvature',
				},
				'MaxDir': {
					'return': 'IpfcVector3D',
					'info': 'The vector that specifies the maximum direction',
				},
				'MinCurvature': {
					'return': 'as Double',
					'info': 'The minimum curvature',
				},
				'MinDir': {
					'return': 'IpfcVector3D',
					'info': 'The vector that specifies the minimum direction',
				},
			},
			'method': {
			},
	}
	IpfcCurve = {
			'type': 'Classes',
			'parent': {
				'IpfcGeomCurve': '',
				'IpfcModelItem': '',
			},
			'child': {
				'IpfcCompositeCurve': '',
				'IpfcPoint': '',
				'IpfcLine': '',
				'IpfcArrow': '',
				'IpfcArc': '',
				'IpfcSpline': '',
				'IpfcBSpline': '',
				'IpfcCircle': '',
				'IpfcEllipse': '',
				'IpfcPolygon': '',
				'IpfcText': '',
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcCurveDescriptor = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcCompositeCurveDescriptor': '',
				'IpfcPointDescriptor': '',
				'IpfcLineDescriptor': '',
				'IpfcArrowDescriptor': '',
				'IpfcArcDescriptor': '',
				'IpfcSplineDescriptor': '',
				'IpfcBSplineDescriptor': '',
				'IpfcCircleDescriptor': '',
				'IpfcEllipseDescriptor': '',
				'IpfcPolygonDescriptor': '',
				'IpfcTextDescriptor': '',
			},
			'properties': {
			},
			'method': {
				'GetCurveType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcCurveType',
					'info': 'This method returns the geometric type of the curve descriptor.',
				},
			},
	}
	IpfcCurveFeat = {
			'type': 'Classes',
			'parent': {
				'IpfcFeature': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcCurveXYZData = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Derivative1': {
					'return': 'IpfcVector3D',
					'info': 'The first partial derivaties of X, Y, and Z, with respect to t',
				},
				'Derivative2': {
					'return': 'IpfcVector3D',
					'info': 'The second partial derivaties of X, Y, and Z, with respect to t',
				},
				'Param': {
					'return': 'as Double',
					'info': 'The independent parameter, t',
				},
				'Point': {
					'return': 'IpfcPoint3D',
					'info': 'The X, Y, and Z coordinates of the point, with respect to the model coordinates',
				},
			},
			'method': {
			},
	}
	IpfcCustomCheckInstructions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'ActionButtonLabel': {
					'return': 'as String [optional]',
					'info': 'Specifies label of the action button. Can be null,   which means no action button will be shown.',
				},
				'CheckLabel': {
					'return': 'as String',
					'info': 'Specifies the label of the check.',
				},
				'CheckName': {
					'return': 'as String',
					'info': 'Specifies the name of the check.',
				},
				'Listener': {
					'return': 'IpfcModelCheckCustomCheckListener',
					'info': 'The object containing the implementation of the custom check   methods.',
				},
				'UpdateButtonLabel': {
					'return': 'as String [optional]',
					'info': 'Specifies label of the update button. Can be null,   which means no update button will be shown.',
				},
			},
			'method': {
				'CCpfcCustomCheckInstructions.Create': {
					'type': 'Function',
					'args': 'CheckName as String, CheckLabel as String, Listener as IpfcModelCheckCustomCheckListener',
					'return': 'IpfcCustomCheckInstructions',
					'info': 'Creates a new IpfcModelCheckInstructions   object.',
					'com': 'pfcls.pfcCustomCheckInstructions.1',
				},
			},
	}
	IpfcCustomCheckResults = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'ResultsCount': {
					'return': 'as Long',
					'info': 'Specifies the numerical value to be shown in the ModelCheck   report for this check.',
				},
				'ResultsTable': {
					'return': 'Istringseq',
					'info': 'A list of strings containing details of each found item.  Can   be null.  ',
				},
				'ResultsUrl': {
					'return': 'as String [optional]',
					'info': 'Specifies a URL to a page that provides details on the results   of this check.   If null then the check will be listed with no extra   information besides the count.  ',
				},
			},
			'method': {
				'CCpfcCustomCheckResults.Create': {
					'type': 'Function',
					'args': 'ResultsCount as Long',
					'return': 'IpfcCustomCheckResults',
					'info': 'Creates a new IpfcCustomCheckResults object.',
					'com': 'pfcls.pfcCustomCheckResults.1',
				},
			},
	}
	IpfcCylinder = {
			'type': 'Classes',
			'parent': {
				'IpfcTransformedSurface': '',
			},
			'child': {
			},
			'properties': {
				'Radius': {
					'return': 'as Double',
					'info': 'The radius of the cylinder',
				},
			},
			'method': {
			},
	}
	IpfcCylinderDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcTransformedSurfaceDescriptor': '',
			},
			'child': {
			},
			'properties': {
				'Radius': {
					'return': 'as Double',
					'info': 'The radius of the cylinder',
				},
			},
			'method': {
				'CCpfcCylinderDescriptor.Create': {
					'type': 'Function',
					'args': 'Extents as IpfcSurfaceExtents, Orientation as IpfcSurfaceOrientation, Origin as IpfcTransform3D, Radius as Double',
					'return': 'IpfcCylinderDescriptor',
					'info': 'This method creates a new CylinderDescriptor object.',
					'com': 'pfcls.pfcCylinderDescriptor.1',
				},
			},
	}
	IpfcCylindricalSplineSurface = {
			'type': 'Classes',
			'parent': {
				'IpfcTransformedSurface': '',
			},
			'child': {
			},
			'properties': {
				'SplineSurfaceData': {
					'return': 'IpfcSplineSurfaceDescriptor',
					'info': 'The spline surface geometry which defines the surface',
				},
			},
			'method': {
			},
	}
	IpfcCylindricalSplineSurfaceDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcTransformedSurfaceDescriptor': '',
			},
			'child': {
			},
			'properties': {
				'SplineSurfaceData': {
					'return': 'IpfcSplineSurfaceDescriptor',
					'info': 'The spline surface geometry which defines the surface',
				},
			},
			'method': {
				'CCpfcCylindricalSplineSurfaceDescriptor.Create': {
					'type': 'Function',
					'args': 'Extents as IpfcSurfaceExtents, Orientation as IpfcSurfaceOrientation, Origin as IpfcTransform3D, SplineSurfaceData as IpfcSplineSurfaceDescriptor',
					'return': 'IpfcCylindricalSplineSurfaceDescriptor',
					'info': 'This method creates a new CylindricalSplineSurfaceDescriptor object.',
					'com': 'pfcls.pfcCylindricalSplineSurfaceDescriptor.1',
				},
			},
	}
	IpfcDatumAxisConstraint = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'ConstraintRef': {
					'return': 'IpfcSelection',
					'info': 'This specifies selection handle for the constraint reference.',
				},
				'ConstraintType': {
					'return': 'IpfcDatumAxisConstraintType',
					'info': 'This specifies the type of datum axis constraint.',
				},
			},
			'method': {
				'CCpfcDatumAxisConstraint.Create': {
					'type': 'Function',
					'args': 'ConstraintType as IpfcDatumAxisConstraintType, ConstraintRef as IpfcSelection',
					'return': 'IpfcDatumAxisConstraint',
					'info': 'Creates a new DatumAxisConstraint object.',
					'com': 'pfcls.pfcDatumAxisConstraint.1',
				},
			},
	}
	IpfcDatumAxisDimensionConstraint = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'DimOffset': {
					'return': 'as Double',
					'info': 'The offset value specified by the dimension.',
				},
				'DimRef': {
					'return': 'IpfcSelection',
					'info': 'This specifies selection handle for the dimension reference.',
				},
			},
			'method': {
				'CCpfcDatumAxisDimensionConstraint.Create': {
					'type': 'Function',
					'args': 'DimRef as IpfcSelection, DimOffset as Double',
					'return': 'IpfcDatumAxisDimensionConstraint',
					'info': 'Creates a new DatumAxisDimensionConstraint object.',
					'com': 'pfcls.pfcDatumAxisDimensionConstraint.1',
				},
			},
	}
	IpfcDatumAxisFeat = {
			'type': 'Classes',
			'parent': {
				'IpfcFeature': '',
			},
			'child': {
			},
			'properties': {
				'Constraints': {
					'return': 'IpfcDatumAxisConstraints',
					'info': 'The constraints applying to the datum axis feature.',
				},
				'DimConstraints': {
					'return': 'IpfcDatumAxisDimensionConstraints',
					'info': 'The dimension constraints applying to the datum axis feature.',
				},
			},
			'method': {
			},
	}
	IpfcDatumCsysDimensionConstraint = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'DimConstraintType': {
					'return': 'IpfcDatumCsysDimConstraintType',
					'info': 'This specifies type of dimension constraint.',
				},
				'DimRef': {
					'return': 'IpfcSelection',
					'info': 'This specifies selection handle for the dimension reference.',
				},
				'DimValue': {
					'return': 'as Double',
					'info': 'This specifies constraint reference value.',
				},
			},
			'method': {
				'CCpfcDatumCsysDimensionConstraint.Create': {
					'type': 'Function',
					'args': 'DimRef as IpfcSelection, DimConstraintType as IpfcDatumCsysDimConstraintType, DimValue as Double',
					'return': 'IpfcDatumCsysDimensionConstraint',
					'info': 'Creates a new DatumCsysDimensionConstraint object.',
					'com': 'pfcls.pfcDatumCsysDimensionConstraint.1',
				},
			},
	}
	IpfcDatumCsysOrientMoveConstraint = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'OrientMoveConstraintType': {
					'return': 'IpfcDatumCsysOrientMoveConstraintType',
					'info': 'This specifies type of orientation constraint.',
				},
				'OrientMoveValue': {
					'return': 'as Double',
					'info': 'This specifies constraint reference value.',
				},
			},
			'method': {
				'CCpfcDatumCsysOrientMoveConstraint.Create': {
					'type': 'Function',
					'args': 'OrientMoveConstraintType as IpfcDatumCsysOrientMoveConstraintType, OrientMoveValue as Double',
					'return': 'IpfcDatumCsysOrientMoveConstraint',
					'info': 'Creates a new DatumCsysOrientMoveConstraint object.',
					'com': 'pfcls.pfcDatumCsysOrientMoveConstraint.1',
				},
			},
	}
	IpfcDatumCsysOriginConstraint = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'OriginRef': {
					'return': 'IpfcSelection',
					'info': 'This specifies selection handle for the origin reference.',
				},
			},
			'method': {
				'CCpfcDatumCsysOriginConstraint.Create': {
					'type': 'Function',
					'args': 'OriginRef as IpfcSelection',
					'return': 'IpfcDatumCsysOriginConstraint',
					'info': 'Creates a new DatumCsysOriginConstraint object.',
					'com': 'pfcls.pfcDatumCsysOriginConstraint.1',
				},
			},
	}
	IpfcDatumPlaneAngleConstraint = {
			'type': 'Classes',
			'parent': {
				'IpfcDatumPlaneConstraint': '',
			},
			'child': {
			},
			'properties': {
				'AngleRef': {
					'return': 'IpfcSelection',
					'info': "This specifies selection handle for the 'Angle' reference.",
				},
				'AngleValue': {
					'return': 'as Double',
					'info': 'The reference angle value.',
				},
			},
			'method': {
				'CCpfcDatumPlaneAngleConstraint.Create': {
					'type': 'Function',
					'args': 'AngleRef as IpfcSelection, AngleValue as Double',
					'return': 'IpfcDatumPlaneAngleConstraint',
					'info': 'Creates a new DatumPlaneAngleConstraint object.',
					'com': 'pfcls.pfcDatumPlaneAngleConstraint.1',
				},
			},
	}
	IpfcDatumPlaneConstraint = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcDatumPlaneThroughConstraint': '',
				'IpfcDatumPlaneNormalConstraint': '',
				'IpfcDatumPlaneParallelConstraint': '',
				'IpfcDatumPlaneOffsetConstraint': '',
				'IpfcDatumPlaneAngleConstraint': '',
				'IpfcDatumPlaneTangentConstraint': '',
				'IpfcDatumPlaneSectionConstraint': '',
				'IpfcDatumPlaneDefaultXConstraint': '',
				'IpfcDatumPlaneDefaultYConstraint': '',
				'IpfcDatumPlaneDefaultZConstraint': '',
			},
			'properties': {
				'ConstraintType': {
					'return': 'IpfcDatumPlaneConstraintType',
					'info': 'This specifies the type of datum plane constraint.',
				},
			},
			'method': {
			},
	}
	IpfcDatumPlaneDefaultXConstraint = {
			'type': 'Classes',
			'parent': {
				'IpfcDatumPlaneConstraint': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcDatumPlaneDefaultXConstraint.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDatumPlaneDefaultXConstraint',
					'info': 'Creates a new DatumPlaneDefaultXConstraint object.',
					'com': 'pfcls.pfcDatumPlaneDefaultXConstraint.1',
				},
			},
	}
	IpfcDatumPlaneDefaultYConstraint = {
			'type': 'Classes',
			'parent': {
				'IpfcDatumPlaneConstraint': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcDatumPlaneDefaultYConstraint.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDatumPlaneDefaultYConstraint',
					'info': 'Creates a new DatumPlaneDefaultYConstraint object.',
					'com': 'pfcls.pfcDatumPlaneDefaultYConstraint.1',
				},
			},
	}
	IpfcDatumPlaneDefaultZConstraint = {
			'type': 'Classes',
			'parent': {
				'IpfcDatumPlaneConstraint': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcDatumPlaneDefaultZConstraint.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDatumPlaneDefaultZConstraint',
					'info': 'Creates a new DatumPlaneDefaultZConstraint object.',
					'com': 'pfcls.pfcDatumPlaneDefaultZConstraint.1',
				},
			},
	}
	IpfcDatumPlaneFeat = {
			'type': 'Classes',
			'parent': {
				'IpfcFeature': '',
			},
			'child': {
			},
			'properties': {
				'Constraints': {
					'return': 'IpfcDatumPlaneConstraints',
					'info': 'The constraints applying to the datum plane feature.',
				},
				'Flip': {
					'return': 'as Boolean [readonly, optional]',
					'info': 'Specifies whether datum plane was flipped during creation.',
				},
			},
			'method': {
			},
	}
	IpfcDatumPlaneNormalConstraint = {
			'type': 'Classes',
			'parent': {
				'IpfcDatumPlaneConstraint': '',
			},
			'child': {
			},
			'properties': {
				'NormalRef': {
					'return': 'IpfcSelection',
					'info': "This specifies selection handle for the 'Normal' reference.",
				},
			},
			'method': {
				'CCpfcDatumPlaneNormalConstraint.Create': {
					'type': 'Function',
					'args': 'NormalRef as IpfcSelection',
					'return': 'IpfcDatumPlaneNormalConstraint',
					'info': 'Creates a new DatumPlaneNormalConstraint object.',
					'com': 'pfcls.pfcDatumPlaneNormalConstraint.1',
				},
			},
	}
	IpfcDatumPlaneOffsetConstraint = {
			'type': 'Classes',
			'parent': {
				'IpfcDatumPlaneConstraint': '',
			},
			'child': {
				'IpfcDatumPlaneOffsetCoordSysConstraint': '',
			},
			'properties': {
				'OffsetRef': {
					'return': 'IpfcSelection',
					'info': "This specifies selection handle for the 'Offset' reference.",
				},
				'OffsetValue': {
					'return': 'as Double',
					'info': 'The reference offset value.',
				},
			},
			'method': {
				'CCpfcDatumPlaneOffsetConstraint.Create': {
					'type': 'Function',
					'args': 'OffsetRef as IpfcSelection, OffsetValue as Double',
					'return': 'IpfcDatumPlaneOffsetConstraint',
					'info': 'Creates a new DatumPlaneOffsetConstraint object.',
					'com': 'pfcls.pfcDatumPlaneOffsetConstraint.1',
				},
			},
	}
	IpfcDatumPlaneOffsetCoordSysConstraint = {
			'type': 'Classes',
			'parent': {
				'IpfcDatumPlaneOffsetConstraint': '',
			},
			'child': {
			},
			'properties': {
				'CsysAxis': {
					'return': 'IpfcCoordAxis',
					'info': 'This specifies the reference Coordinate Axis.',
				},
			},
			'method': {
				'CCpfcDatumPlaneOffsetCoordSysConstraint.Create': {
					'type': 'Function',
					'args': 'CsysAxis as IpfcCoordAxis, OffsetRef as IpfcSelection, OffsetValue as Double',
					'return': 'IpfcDatumPlaneOffsetCoordSysConstraint',
					'info': 'Creates a new DatumPlaneOffsetCoordSysConstraint object.',
					'com': 'pfcls.pfcDatumPlaneOffsetCoordSysConstraint.1',
				},
			},
	}
	IpfcDatumPlaneParallelConstraint = {
			'type': 'Classes',
			'parent': {
				'IpfcDatumPlaneConstraint': '',
			},
			'child': {
			},
			'properties': {
				'ParallelRef': {
					'return': 'IpfcSelection',
					'info': "This specifies selection handle for the 'Parallel' reference.",
				},
			},
			'method': {
				'CCpfcDatumPlaneParallelConstraint.Create': {
					'type': 'Function',
					'args': 'ParallelRef as IpfcSelection',
					'return': 'IpfcDatumPlaneParallelConstraint',
					'info': 'Creates a new DatumPlaneParallelConstraint object.',
					'com': 'pfcls.pfcDatumPlaneParallelConstraint.1',
				},
			},
	}
	IpfcDatumPlaneSectionConstraint = {
			'type': 'Classes',
			'parent': {
				'IpfcDatumPlaneConstraint': '',
			},
			'child': {
			},
			'properties': {
				'SectionIndex': {
					'return': 'as Long',
					'info': 'The section index.',
				},
				'SectionRef': {
					'return': 'IpfcSelection',
					'info': "This specifies selection handle for the 'Section' reference.",
				},
			},
			'method': {
				'CCpfcDatumPlaneSectionConstraint.Create': {
					'type': 'Function',
					'args': 'SectionRef as IpfcSelection, SectionIndex as Long',
					'return': 'IpfcDatumPlaneSectionConstraint',
					'info': 'Creates a new DatumPlaneSectionConstraint object.',
					'com': 'pfcls.pfcDatumPlaneSectionConstraint.1',
				},
			},
	}
	IpfcDatumPlaneTangentConstraint = {
			'type': 'Classes',
			'parent': {
				'IpfcDatumPlaneConstraint': '',
			},
			'child': {
			},
			'properties': {
				'TangentRef': {
					'return': 'IpfcSelection',
					'info': "This specifies selection handle for the 'Tangent' reference.",
				},
			},
			'method': {
				'CCpfcDatumPlaneTangentConstraint.Create': {
					'type': 'Function',
					'args': 'TangentRef as IpfcSelection',
					'return': 'IpfcDatumPlaneTangentConstraint',
					'info': 'Creates a new DatumPlaneTangentConstraint object.',
					'com': 'pfcls.pfcDatumPlaneTangentConstraint.1',
				},
			},
	}
	IpfcDatumPlaneThroughConstraint = {
			'type': 'Classes',
			'parent': {
				'IpfcDatumPlaneConstraint': '',
			},
			'child': {
			},
			'properties': {
				'ThroughRef': {
					'return': 'IpfcSelection',
					'info': "This specifies selection handle for the 'Through' reference.",
				},
			},
			'method': {
				'CCpfcDatumPlaneThroughConstraint.Create': {
					'type': 'Function',
					'args': 'ThroughRef as IpfcSelection',
					'return': 'IpfcDatumPlaneThroughConstraint',
					'info': 'Creates a new DatumPlaneThroughConstraint object.',
					'com': 'pfcls.pfcDatumPlaneThroughConstraint.1',
				},
			},
	}
	IpfcDatumPointConstraint = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcDatumPointPlacementConstraint': '',
				'IpfcDatumPointDimensionConstraint': '',
			},
			'properties': {
				'ConstraintRef': {
					'return': 'IpfcSelection',
					'info': 'This specifies selection handle for the constraint reference.',
				},
				'ConstraintType': {
					'return': 'IpfcDatumPointConstraintType',
					'info': 'This specifies the type of datum point constraint.',
				},
				'Value': {
					'return': 'as Double',
					'info': 'This specifies the constraint reference value with respect to the datum point.',
				},
			},
			'method': {
			},
	}
	IpfcDatumPointDimensionConstraint = {
			'type': 'Classes',
			'parent': {
				'IpfcDatumPointConstraint': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcDatumPointDimensionConstraint.Create': {
					'type': 'Function',
					'args': 'ConstraintRef as IpfcSelection, Type as IpfcDatumPointConstraintType, Value as Double',
					'return': 'IpfcDatumPointDimensionConstraint',
					'info': 'Creates a new DatumPointDimensionConstraint object.',
					'com': 'pfcls.pfcDatumPointDimensionConstraint.1',
				},
			},
	}
	IpfcDatumPointFeat = {
			'type': 'Classes',
			'parent': {
				'IpfcFeature': '',
			},
			'child': {
			},
			'properties': {
				'FeatName': {
					'return': 'as String',
					'info': 'This specifies the name of the general datum point feature.',
				},
			},
			'method': {
				'GetPoints': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcGeneralDatumPoints',
					'info': 'Returns the constituent general datum point(s) in the feature.',
				},
			},
	}
	IpfcDatumPointPlacementConstraint = {
			'type': 'Classes',
			'parent': {
				'IpfcDatumPointConstraint': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcDatumPointPlacementConstraint.Create': {
					'type': 'Function',
					'args': 'ConstraintRef as IpfcSelection, Type as IpfcDatumPointConstraintType, Value as Double',
					'return': 'IpfcDatumPointPlacementConstraint',
					'info': 'Creates a new DatumPointPlacementConstraint object.',
					'com': 'pfcls.pfcDatumPointPlacementConstraint.1',
				},
			},
	}
	IpfcDeleteOperation = {
			'type': 'Classes',
			'parent': {
				'IpfcFeatureOperation': '',
			},
			'child': {
			},
			'properties': {
				'AllowChildGroupMembers': {
					'return': 'as Boolean',
					'info': "If true the children of feature, if members of a group,    will be individually deleted from their group. If false, then the     group containing the feature's children will be also deleted.    This attribute can be set to true if and only if both    IpfcDeleteOperation.Clip and     IpfcDeleteOperation.AllowGroupMembers    are set to true.    Default value is false.",
				},
				'AllowGroupMembers': {
					'return': 'as Boolean',
					'info': 'If true the feature, if member of a group, will be    individually deleted from the group. If false, the group containing   the feature will be also deleted. This attribute can be set to   true if and only if    IpfcDeleteOperation.Clip    is also set to true.   Default value is false.',
				},
				'Clip': {
					'return': 'as Boolean',
					'info': 'A Boolean flag that determines whether to delete all features     that follow the feature.Default value is false.',
				},
				'KeepEmbeddedDatums': {
					'return': 'as Boolean',
					'info': 'If true then the embedded datum(s) inside the feature    will be retained even after the feature is deleted.  If false   embedded datum(s) will be deleted along with parent fature.  ',
				},
			},
			'method': {
			},
	}
	IpfcDependency = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'DepModel': {
					'return': 'IpfcModelDescriptor',
					'info': 'The model descriptor that describes the dependency ',
				},
			},
			'method': {
				'IsUDFGroup': {
					'type': 'Function',
					'args': '',
					'return': ' as Boolean',
					'info': 'Identifies if a model dependency is to a UDF group.',
				},
			},
	}
	IpfcDescriptorContainer = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Descr': {
					'return': 'IpfcModelDescriptor',
					'info': '  ',
				},
			},
			'method': {
			},
	}
	IpfcDescriptorContainer2 = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Descr1': {
					'return': 'IpfcModelDescriptor',
					'info': '  ',
				},
				'Descr2': {
					'return': 'IpfcModelDescriptor',
					'info': '  ',
				},
			},
			'method': {
			},
	}
	IpfcDetailAttachment = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcDetailAttachment.Create': {
					'type': 'Function',
					'args': 'NoteAttach as IpfcAttachment, LeaderAttach as IpfcDetailLeaderAttachments [optional]',
					'return': 'IpfcDetailAttachment',
					'info': 'Creates attachment data for detail item. Currently supports only notes. ',
					'com': 'pfcls.pfcDetailAttachment.1',
				},
				'GetLeaderAttachments': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDetailLeaderAttachments',
					'info': 'Get attachment of note leader. ',
				},
				'GetNoteAttachment': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcAttachment',
					'info': 'Get attachment of note text. ',
				},
				'SetLeaderAttachments': {
					'type': 'Sub',
					'args': 'LeaderAttach as IpfcDetailLeaderAttachments [optional]',
					'return': '',
					'info': 'Set attachment of note leader. ',
				},
				'SetNoteAttachment': {
					'type': 'Sub',
					'args': 'NoteAttach as IpfcAttachment',
					'return': '',
					'info': 'Set attachment of note text. ',
				},
			},
	}
	IpfcDetailCreateInstructions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcDetailEntityInstructions': '',
				'IpfcDetailNoteInstructions': '',
				'IpfcDetailSymbolDefInstructions': '',
				'IpfcDetailSymbolInstInstructions': '',
				'IpfcDetailGroupInstructions': '',
			},
			'properties': {
				'Id': {
					'return': 'as Long',
					'info': 'This readonly attribute represents the identifier of an existing detail item.  This interface is not used for creating or modifying detail items. ',
				},
			},
			'method': {
				'GetType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDetailType',
					'info': 'Returns the type of detail item that this instructions object will create.',
				},
			},
	}
	IpfcDetailEntityInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcDetailCreateInstructions': '',
			},
			'child': {
			},
			'properties': {
				'Color': {
					'return': 'IpfcColorRGB',
					'info': 'The color of the detail item.  If null, the default drawing color is used.',
				},
				'FontName': {
					'return': 'as String [optional]',
					'info': 'The name of the linestyle.  If null, the default linestyle is used.',
				},
				'Geometry': {
					'return': 'IpfcCurveDescriptor',
					'info': 'The geometry of the detail entity.',
				},
				'IsConstruction': {
					'return': 'as Boolean [optional]',
					'info': 'Whether or not the entity is construction.  If null, the entity is not a construction entity.',
				},
				'View': {
					'return': 'IpfcView2D',
					'info': 'The drawing view associated with the entity.  This can be a view of a drawing model, or a drawing sheet background view.',
				},
				'Width': {
					'return': 'as Double [optional]',
					'info': 'The width of the entity line.  If null, the default width is used.',
				},
			},
			'method': {
				'CCpfcDetailEntityInstructions.Create': {
					'type': 'Function',
					'args': 'inGeometry as IpfcCurveDescriptor, inView as IpfcView2D [optional]',
					'return': 'IpfcDetailEntityInstructions',
					'info': 'Creates an instructions object describing how to construct a detail entity, for use in the methods IpfcDetailItemOwner.CreateDetailItem(), IpfcDetailSymbolDefItem.CreateDetailItem() and IpfcDetailEntityItem.Modify().',
					'com': 'pfcls.pfcDetailEntityInstructions.1',
				},
			},
	}
	IpfcDetailEntityItem = {
			'type': 'Classes',
			'parent': {
				'IpfcDetailItem': '',
			},
			'child': {
			},
			'properties': {
				'SymbolDef': {
					'return': 'IpfcDetailSymbolDefItem',
					'info': 'The symbol definition the entity belongs to, or null, if the entity is not a part of a symbol definition.',
				},
			},
			'method': {
				'Draw': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Draws a detail entity item. ',
				},
				'Erase': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Erases a detail entity item. ',
				},
				'GetInstructions': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDetailEntityInstructions',
					'info': 'Returns the data used to construct the detail entity item.',
				},
				'Modify': {
					'type': 'Sub',
					'args': 'Instructions as IpfcDetailEntityInstructions',
					'return': '',
					'info': 'Modifies the definition of a an entity item.',
				},
			},
	}
	IpfcDetailGroupInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcDetailCreateInstructions': '',
			},
			'child': {
			},
			'properties': {
				'Elements': {
					'return': 'IpfcDetailItems',
					'info': 'Sequence of detail items contained in the group.',
				},
				'IsDisplayed': {
					'return': 'as Boolean [optional]',
					'info': 'true if the group is displayed in the drawing, otherwise false.',
				},
				'Name': {
					'return': 'as String',
					'info': 'The name of the detail group.',
				},
			},
			'method': {
				'CCpfcDetailGroupInstructions.Create': {
					'type': 'Function',
					'args': 'inName as String, inElements as IpfcDetailItems',
					'return': 'IpfcDetailGroupInstructions',
					'info': 'Creates an instructions data object used to describe the construction of a detail group, for use in the methods IpfcDetailItemOwner.CreateDetailItem() and IpfcDetailGroupItem.Modify().',
					'com': 'pfcls.pfcDetailGroupInstructions.1',
				},
			},
	}
	IpfcDetailGroupItem = {
			'type': 'Classes',
			'parent': {
				'IpfcDetailItem': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'Draw': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Draws a group detail item.',
				},
				'Erase': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Erases a detail group item.',
				},
				'GetInstructions': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDetailGroupInstructions',
					'info': 'Gets a data object describing how a detail group item is constructed.',
				},
				'Modify': {
					'type': 'Sub',
					'args': 'Instructions as IpfcDetailGroupInstructions',
					'return': '',
					'info': 'Changes the definition of a detail group item.',
				},
			},
	}
	IpfcDetailItem = {
			'type': 'Classes',
			'parent': {
				'IpfcModelItem': '',
			},
			'child': {
				'IpfcDetailEntityItem': '',
				'IpfcDetailNoteItem': '',
				'IpfcDetailSymbolDefItem': '',
				'IpfcDetailSymbolInstItem': '',
				'IpfcDetailGroupItem': '',
				'IpfcDetailOLEObject': '',
			},
			'properties': {
			},
			'method': {
				'Delete': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Deletes a detail item.',
				},
				'GetDetailType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDetailType',
					'info': 'Gets the type of a detail item.',
				},
			},
	}
	IpfcDetailItemOwner = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcModel2D': '',
			},
			'properties': {
			},
			'method': {
				'AllowReadonlyNoteSelection': {
					'type': 'Sub',
					'args': 'Allow as Boolean',
					'return': '',
					'info': 'Controls whether or not the Creo Parametric user can select read only detail notes.',
				},
				'CreateDetailItem': {
					'type': 'Function',
					'args': 'Instructions as IpfcDetailCreateInstructions',
					'return': 'IpfcDetailItem',
					'info': 'Creates a new detail item in the model.',
				},
				'CreateFreeNote': {
					'type': 'Function',
					'args': 'TextLines as IpfcDetailTextLines, Attach as IpfcFreeAttachment',
					'return': 'IpfcDetailNoteItem',
					'info': 'Creates Free note in specifed 2D model. ',
				},
				'CreateLeaderNote': {
					'type': 'Function',
					'args': 'TextLines as IpfcDetailTextLines, NoteAttach as IpfcAttachment, LeaderAttach as IpfcLeaderAttachments, ElbowLength as Double [optional]',
					'return': 'IpfcDetailNoteItem',
					'info': 'Creates leader note in specifed 2D model. ',
				},
				'CreateOffsetNote': {
					'type': 'Function',
					'args': 'TextLines as IpfcDetailTextLines, Attach as IpfcOffsetAttachment',
					'return': 'IpfcDetailNoteItem',
					'info': 'Creates offset note in specifed 2D model. ',
				},
				'CreateOnItemNote': {
					'type': 'Function',
					'args': 'TextLines as IpfcDetailTextLines, Attach as IpfcParametricAttachment',
					'return': 'IpfcDetailNoteItem',
					'info': 'Creates on item note in specifed 2D model. ',
				},
				'GetDetailItem': {
					'type': 'Function',
					'args': 'Type as IpfcDetailType, Id as Long',
					'return': 'IpfcDetailItem',
					'info': 'Finds a specified detail item, given the identifier and type.',
				},
				'ListDetailItems': {
					'type': 'Function',
					'args': 'Type as IpfcDetailType [optional], SheetNumber as Long [optional]',
					'return': 'IpfcDetailItems',
					'info': 'Lists the detail items in the model.',
				},
				'RetrieveSymbolDefinition': {
					'type': 'Function',
					'args': 'FileName as String, FilePath as String [optional], Version as Long [optional], UpdateUnconditionally as Boolean [optional]',
					'return': 'IpfcDetailSymbolDefItem',
					'info': 'Retrieves a symbol definition from disk.',
				},
				'RetrieveSymbolDefItem': {
					'type': 'Function',
					'args': 'FileName as String, Source as IpfcDetailSymbolDefItemSource, FilePath as String [optional], Version as Long [optional], UpdateUnconditionally as Boolean [optional]',
					'return': 'IpfcDetailSymbolDefItem',
					'info': '  ',
				},
			},
	}
	IpfcDetailLeaderAttachment = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcLeaderAttachment': '',
				'IpfcNormalLeaderAttachment': '',
				'IpfcTangentLeaderAttachment': '',
			},
			'properties': {
			},
			'method': {
				'GetLeaderAttachment': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcAttachment',
					'info': 'Get leader attachment data. ',
				},
				'GetType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDetailLeaderAttachmentType',
					'info': 'Get type of leader used. ',
				},
				'SetLeaderAttachment': {
					'type': 'Sub',
					'args': 'LeaderAttach as IpfcAttachment',
					'return': '',
					'info': 'Set leader attachment data using Free or Parametric Attachment. ',
				},
			},
	}
	IpfcDetailLeaders = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'ElbowLength': {
					'return': 'as Double [optional]',
					'info': 'The elbow length for the leaders.  If null, the default elbow lenghth is used if there are leaders present.',
				},
				'ItemAttachment': {
					'return': 'IpfcAttachment',
					'info': "The location where the detail item is attached. If this is null when retrieving the information from an existing symbol, the item's leader type is not supported by this product.",
				},
				'Leaders': {
					'return': 'IpfcAttachments',
					'info': 'Sequence of leaders.  Indicates the locations where the leaders should be attached.If null, the detail item will not have leaders.',
				},
			},
			'method': {
				'CCpfcDetailLeaders.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDetailLeaders',
					'info': 'Creates a data object describing a set of detail item leaders.',
					'com': 'pfcls.pfcDetailLeaders.1',
				},
			},
	}
	IpfcDetailNoteInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcDetailCreateInstructions': '',
			},
			'child': {
			},
			'properties': {
				'Color': {
					'return': 'IpfcColorRGB',
					'info': 'The color of the detail item.  If null, the default drawing color is used.',
				},
				'Horizontal': {
					'return': 'IpfcHorizontalJustification',
					'info': 'The horizontal justification.  ',
				},
				'IsDisplayed': {
					'return': 'as Boolean [optional]',
					'info': 'true if the note is currently displayed.',
				},
				'IsMirrored': {
					'return': 'as Boolean [optional]',
					'info': 'true if the note is mirrored.  null or false if it has normal orientation.',
				},
				'IsReadOnly': {
					'return': 'as Boolean [optional]',
					'info': 'true if the note is readonly. null or false if it is user editable.',
				},
				'Leader': {
					'return': 'IpfcDetailLeaders',
					'info': 'The note attachment information, including placement and leaders.',
				},
				'TextAngle': {
					'return': 'as Double [optional]',
					'info': 'The text angle used for the note.  null indicates 0.0.',
				},
				'TextLines': {
					'return': 'IpfcDetailTextLines',
					'info': 'Sequence of lines contained in the note.',
				},
				'Vertical': {
					'return': 'IpfcVerticalJustification',
					'info': 'The vertical justification.',
				},
			},
			'method': {
				'CCpfcDetailNoteInstructions.Create': {
					'type': 'Function',
					'args': 'inTextLines as IpfcDetailTextLines',
					'return': 'IpfcDetailNoteInstructions',
					'info': 'Creates a data object used to describe how a detail note item should be construct, when passed to IpfcDetailItemOwner.CreateDetailItem(), IpfcDetailSymbolDefItem.CreateDetailItem(), or IpfcDetailNoteItem.Modify().',
					'com': 'pfcls.pfcDetailNoteInstructions.1',
				},
			},
	}
	IpfcDetailNoteItem = {
			'type': 'Classes',
			'parent': {
				'IpfcDetailItem': '',
			},
			'child': {
			},
			'properties': {
				'SymbolDef': {
					'return': 'IpfcDetailSymbolDefItem',
					'info': 'The symbol definition that the note belongs to, or null if the note does not belong to a symbol definition.',
				},
			},
			'method': {
				'Draw': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Draws a detail note item.  Drawn notes will be removed upon the next regeneration.',
				},
				'Erase': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Erases a detail note item.  Erased notes will be shown again after the next regeneration.',
				},
				'GetAttachment': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDetailAttachment',
					'info': 'Gets attachment information of note. ',
				},
				'GetElbowLength': {
					'type': 'Function',
					'args': '',
					'return': ' as Double [optional]',
					'info': 'Gets length of note leader elbow. ',
				},
				'GetInstructions': {
					'type': 'Function',
					'args': 'GiveParametersAsNames as Boolean',
					'return': 'IpfcDetailNoteInstructions',
					'info': 'Returns a data object describing how a detail note item is constructed.',
				},
				'GetLineEnvelope': {
					'type': 'Function',
					'args': 'LineNumber as Long',
					'return': 'IpfcEnvelope2D',
					'info': 'Returns the envelope of a line in the note, in screen coordinates.',
				},
				'GetModelReference': {
					'type': 'Function',
					'args': 'LineNumber as Long, Index as Long',
					'return': 'IpfcModel',
					'info': 'Returns the model referenced by parameterized text in a note.',
				},
				'GetNoteTextStyle': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcAnnotationTextStyle',
					'info': 'Gets textstyle of note.',
				},
				'GetOwner': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcModelItem',
					'info': '  ',
				},
				'GetTextLines': {
					'type': 'Function',
					'args': 'TxtDisplayOption as IpfcDetailTextDisplayOption',
					'return': 'IpfcDetailTextLines',
					'info': '  ',
				},
				'GetURL': {
					'type': 'Function',
					'args': '',
					'return': ' as String [optional]',
					'info': 'Get URL as part of note text. Availble only for 3D notes ',
				},
				'IsDisplayed': {
					'type': 'Function',
					'args': '',
					'return': ' as Boolean',
					'info': 'Checks if note data is displayed. This is useful for notes whose owner is not displayed in session ',
				},
				'IsReadOnly': {
					'type': 'Function',
					'args': '',
					'return': ' as Boolean',
					'info': 'Checks if note is read-only. ',
				},
				'Modify': {
					'type': 'Sub',
					'args': 'Instructions as IpfcDetailNoteInstructions',
					'return': '',
					'info': 'Modifies the definition of an existing detail note item.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Removes a detail note item. Removed notes will continue to be removed after the next regeneration. ',
				},
				'SetDisplayed': {
					'type': 'Sub',
					'args': 'Display as Boolean',
					'return': '',
					'info': 'Sets note data displayed. This is useful for notes whose owner is not displayed in session. ',
				},
				'SetElbow': {
					'type': 'Sub',
					'args': 'Length as Double [optional], Direction as IpfcVector3D [optional]',
					'return': '',
					'info': 'Set elbow to leader note. ',
				},
				'SetFreeAttachment': {
					'type': 'Sub',
					'args': 'Attachment as IpfcFreeAttachment',
					'return': '',
					'info': 'Sets free attachment information of note. ',
				},
				'SetLeaders': {
					'type': 'Sub',
					'args': 'LeaderAttachs as IpfcLeaderAttachments',
					'return': '',
					'info': 'Sets leader attachment information of note. ',
				},
				'SetNoteTextStyle': {
					'type': 'Sub',
					'args': 'TextStyle as IpfcAnnotationTextStyle',
					'return': '',
					'info': 'Sets textstyle of note.',
				},
				'SetOffsetAttachment': {
					'type': 'Sub',
					'args': 'Attachment as IpfcOffsetAttachment',
					'return': '',
					'info': 'Sets offset attachment information of note. ',
				},
				'SetOnItemAttachment': {
					'type': 'Sub',
					'args': 'Attachment as IpfcParametricAttachment',
					'return': '',
					'info': 'Sets on item attachment information of note. ',
				},
				'SetReadOnly': {
					'type': 'Sub',
					'args': 'ReadOnly as Boolean',
					'return': '',
					'info': 'Sets note read-only. ',
				},
				'SetTextLines': {
					'type': 'Sub',
					'args': 'TextLines as IpfcDetailTextLines',
					'return': '',
					'info': '  ',
				},
				'SetURL': {
					'type': 'Sub',
					'args': 'URLText as String',
					'return': '',
					'info': 'Set URL as part of note text. Availble only for 3D notes',
				},
				'Show': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Shows a note.  Shown notes will continue to be shown after the next regeneration.',
				},
			},
	}
	IpfcDetailOLEObject = {
			'type': 'Classes',
			'parent': {
				'IpfcDetailItem': '',
			},
			'child': {
			},
			'properties': {
				'ApplicationType': {
					'return': 'as String',
					'info': 'Returns the type of the object embedded in the model.',
				},
				'Outline': {
					'return': 'IpfcOutline2D',
					'info': 'Obtains the extents of the OLE object in the drawing.',
				},
				'Path': {
					'return': 'as String [readonly, optional]',
					'info': 'Returns the path to the file which is referenced by the OLE object.',
				},
				'Sheet': {
					'return': 'as Long',
					'info': 'Returns the sheet index on which the OLE object is embedded.',
				},
			},
			'method': {
			},
	}
	IpfcDetailSymbolDefInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcDetailCreateInstructions': '',
			},
			'child': {
			},
			'properties': {
				'Attachments': {
					'return': 'IpfcSymbolDefAttachments',
					'info': 'Sequence of possible instance attachment points for the symbol definition.',
				},
				'FullPath': {
					'return': 'as String',
					'info': 'The full path to the symbol definition file.',
				},
				'HasElbow': {
					'return': 'as Boolean [optional]',
					'info': 'null or true if the symbol definition includes an elbow, false if not.',
				},
				'Height': {
					'return': 'as Double [optional]',
					'info': 'The height value of the symbol definition.',
				},
				'IsTextAngleFixed': {
					'return': 'as Boolean [optional]',
					'info': 'null or true if the text angle is fixed, false if not.',
				},
				'Name': {
					'return': 'as String',
					'info': 'The name of the symbol defintion.',
				},
				'Reference': {
					'return': 'IpfcTextReference',
					'info': 'The text reference information, or null if unused.',
				},
				'ScaledHeight': {
					'return': 'as Double',
					'info': 'The symbol definition height in inches. ',
				},
				'SymbolHeight': {
					'return': 'IpfcSymbolDefHeight',
					'info': 'The height type for the symbol definition.',
				},
			},
			'method': {
				'CCpfcDetailSymbolDefInstructions.Create': {
					'type': 'Function',
					'args': 'inFullPath as String',
					'return': 'IpfcDetailSymbolDefInstructions',
					'info': 'Creates an instructions object used to create a symbol definition.  Pass this object to IpfcDetailItemOwner.CreateDetailItem() and IpfcDetailSymbolDefItem.Modify().',
					'com': 'pfcls.pfcDetailSymbolDefInstructions.1',
				},
			},
	}
	IpfcDetailSymbolDefItem = {
			'type': 'Classes',
			'parent': {
				'IpfcDetailItem': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CreateDetailItem': {
					'type': 'Function',
					'args': 'Instructions as IpfcDetailCreateInstructions',
					'return': 'IpfcDetailItem',
					'info': 'Creates a detail item in the symbol definition.',
				},
				'CreateFreeNote': {
					'type': 'Function',
					'args': 'TextLines as IpfcDetailTextLines, Attach as IpfcFreeAttachment',
					'return': 'IpfcDetailNoteItem',
					'info': '  ',
				},
				'CreateSubgroup': {
					'type': 'Function',
					'args': 'Instructions as IpfcDetailSymbolGroupInstructions, ParentGroup as IpfcDetailSymbolGroup [optional]',
					'return': 'IpfcDetailSymbolGroup',
					'info': 'Create a subgroup in the specified symbol group. ',
				},
				'GetDetailItem': {
					'type': 'Function',
					'args': 'Type as IpfcDetailType, Id as Long',
					'return': 'IpfcDetailItem',
					'info': 'Returns a detail item in a symbol definition, given its id and type.',
				},
				'GetInstructions': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDetailSymbolDefInstructions',
					'info': 'Returns an instructions data object describing how a symbol definition is constructed.',
				},
				'IsSubgroupLevelExclusive': {
					'type': 'Function',
					'args': 'ParentGroup as IpfcDetailSymbolGroup [optional]',
					'return': ' as Boolean',
					'info': 'Determine if the subgroups stored in the symbol definition on   the indicated level are exclusive.',
				},
				'ListDetailItems': {
					'type': 'Function',
					'args': 'Type as IpfcDetailType [optional]',
					'return': 'IpfcDetailItems',
					'info': 'Lists the detail items in the symbol definition.',
				},
				'ListSubgroups': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDetailSymbolGroups',
					'info': 'List the subgroups in the symbol definition.',
				},
				'Modify': {
					'type': 'Sub',
					'args': 'Instructions as IpfcDetailSymbolDefInstructions',
					'return': '',
					'info': 'Modifies a symbol definition.',
				},
				'SetSubgroupLevelExclusive': {
					'type': 'Sub',
					'args': 'ParentGroup as IpfcDetailSymbolGroup [optional]',
					'return': '',
					'info': 'Set the symbol groups exclusive at specified level.',
				},
				'SetSubgroupLevelIndependent': {
					'type': 'Sub',
					'args': 'ParentGroup as IpfcDetailSymbolGroup [optional]',
					'return': '',
					'info': 'Set the symbol groups independent at specified level.',
				},
			},
	}
	IpfcDetailSymbolGroup = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
			},
			'properties': {
				'ParentDefinition': {
					'return': 'IpfcDetailSymbolDefItem',
					'info': 'The symbol definition of the given group.  ',
				},
				'ParentGroup': {
					'return': 'IpfcDetailSymbolGroup',
					'info': 'The parent symbol group of the level to which the given group belongs.  ',
				},
			},
			'method': {
				'Delete': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'To delete the given symbol group.',
				},
				'GetInstructions': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDetailSymbolGroupInstructions',
					'info': 'To get the instructions object describing how the symbol group is constructed. ',
				},
				'ListChildren': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDetailSymbolGroups',
					'info': 'To list the subgroups of the given symbol group.',
				},
				'Modify': {
					'type': 'Sub',
					'args': 'Instructions as IpfcDetailSymbolGroupInstructions',
					'return': '',
					'info': 'To modify the given symbol group. ',
				},
			},
	}
	IpfcDetailSymbolGroupInstructions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Items': {
					'return': 'IpfcDetailItems',
					'info': 'The detail items contained in the symbol group.  ',
				},
				'Name': {
					'return': 'as String',
					'info': 'The name of the symbol group.',
				},
			},
			'method': {
				'CCpfcDetailSymbolGroupInstructions.Create': {
					'type': 'Function',
					'args': 'Name as String, Items as IpfcDetailItems',
					'return': 'IpfcDetailSymbolGroupInstructions',
					'info': 'Creates a data object having a specific name and list of detail items.  ',
					'com': 'pfcls.pfcDetailSymbolGroupInstructions.1',
				},
			},
	}
	IpfcDetailSymbolInstInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcDetailCreateInstructions': '',
			},
			'child': {
			},
			'properties': {
				'Angle': {
					'return': 'as Double [optional]',
					'info': 'The angle at which the instance was placed.  null represents a 0.0 degree angle.',
				},
				'AttachOnDefType': {
					'return': 'IpfcSymbolDefAttachmentType',
					'info': 'The instance attachment type.  null represents a free attachment.',
				},
				'Color': {
					'return': 'IpfcColorRGB',
					'info': 'The color of the detail item.  If null, the default drawing color is used.',
				},
				'CurrentTransform': {
					'return': 'IpfcTransform3D',
					'info': 'The coordinate transformation matrix for the symbol instance placement.',
				},
				'DefAttachment': {
					'return': 'IpfcSymbolDefAttachment',
					'info': 'The attachment on the symbol definition.',
				},
				'Height': {
					'return': 'as Double [optional]',
					'info': 'The symbol instance height.',
				},
				'InstAttachment': {
					'return': 'IpfcDetailLeaders',
					'info': 'The attachment of the instance, including leader information.',
				},
				'IsDisplayed': {
					'return': 'as Boolean [optional]',
					'info': 'Whether or not the symbol instance is displayed.',
				},
				'ScaledHeight': {
					'return': 'as Double [optional]',
					'info': 'The symbol instance height in the units of the drawing or model,   consistent with the values shown in the Properties dialog for the symbol   instance. ',
				},
				'SymbolDef': {
					'return': 'IpfcDetailSymbolDefItem',
					'info': 'The symbol definition used for the instance.',
				},
				'TextValues': {
					'return': 'IpfcDetailVariantTexts',
					'info': 'Sequence of variant text values used while placing the symbol instance.',
				},
			},
			'method': {
				'CCpfcDetailSymbolInstInstructions.Create': {
					'type': 'Function',
					'args': 'inSymbolDef as IpfcDetailSymbolDefItem',
					'return': 'IpfcDetailSymbolInstInstructions',
					'info': 'Creates a data object containing information about a symbol instance placement.     Pass this object to IpfcDetailItemOwner.CreateDetailItem()    and IpfcDetailSymbolInstItem.Modify().',
					'com': 'pfcls.pfcDetailSymbolInstInstructions.1',
				},
				'SetGroups': {
					'type': 'Sub',
					'args': 'option as IpfcDetailSymbolGroupOption, groups as IpfcDetailSymbolGroups [optional]',
					'return': '',
					'info': 'Sets the option for displaying groups in the symbol instance.',
				},
			},
	}
	IpfcDetailSymbolInstItem = {
			'type': 'Classes',
			'parent': {
				'IpfcDetailItem': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'Draw': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Draws a symbol instance.',
				},
				'Erase': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Erases a symbol instance.',
				},
				'GetInstructions': {
					'type': 'Function',
					'args': 'GiveParametersAsNames as Boolean',
					'return': 'IpfcDetailSymbolInstInstructions',
					'info': 'Returns a data object detailing how a symbol instance is constructed.',
				},
				'ListGroups': {
					'type': 'Function',
					'args': 'Filter as IpfcSymbolGroupFilter',
					'return': 'IpfcDetailSymbolGroups',
					'info': 'List the symbol groups included in a symbol instance.  ',
				},
				'Modify': {
					'type': 'Sub',
					'args': 'Instructions as IpfcDetailSymbolInstInstructions',
					'return': '',
					'info': 'Modifies the definition of a symbol instance.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Removes a symbol instance.',
				},
				'Show': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Shows a symbol instance.',
				},
			},
	}
	IpfcDetailText = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'FontName': {
					'return': 'as String [optional]',
					'info': 'The name of the text font.  If null, the default Creo Parametric font is used.',
				},
				'IsUnderlined': {
					'return': 'as Boolean [optional]',
					'info': 'Whether or not the text is underlined.  If null, the text is not underlined.',
				},
				'Text': {
					'return': 'as String',
					'info': 'The text string. ',
				},
				'TextHeight': {
					'return': 'as Double [optional]',
					'info': 'The text height.  If null, the default note text height is used.',
				},
				'TextSlantAngle': {
					'return': 'as Double [optional]',
					'info': 'The text slant angle. If null, no slant angle is applied.',
				},
				'TextThickness': {
					'return': 'as Double [optional]',
					'info': 'The text thickness.  If null, a thickness of 0.0 is applied.',
				},
				'TextWidthFactor': {
					'return': 'as Double [optional]',
					'info': 'The text width factor.  If null, the width factor of 1.0 is applied.',
				},
			},
			'method': {
				'CCpfcDetailText.Create': {
					'type': 'Function',
					'args': 'inText as String',
					'return': 'IpfcDetailText',
					'info': 'Creates a data object containing a text item in a note.',
					'com': 'pfcls.pfcDetailText.1',
				},
				'GetTextStyle': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcAnnotationTextStyle',
					'info': '  ',
				},
				'SetTextStyle': {
					'type': 'Sub',
					'args': 'TextStyle as IpfcAnnotationTextStyle',
					'return': '',
					'info': '  ',
				},
			},
	}
	IpfcDetailTextLine = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Texts': {
					'return': 'IpfcDetailTexts',
					'info': 'The text elements that make up the text line.',
				},
			},
			'method': {
				'CCpfcDetailTextLine.Create': {
					'type': 'Function',
					'args': 'inTexts as IpfcDetailTexts',
					'return': 'IpfcDetailTextLine',
					'info': 'Creates a data object containing a line of text in a note.',
					'com': 'pfcls.pfcDetailTextLine.1',
				},
			},
	}
	IpfcDetailVariantText = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Prompt': {
					'return': 'as String',
					'info': 'The variable text in the symbol definition (without the boundary "\" characters).',
				},
				'Value': {
					'return': 'as String',
					'info': 'The text to use in place of the variable text in a symbol instance placement.',
				},
			},
			'method': {
				'CCpfcDetailVariantText.Create': {
					'type': 'Function',
					'args': 'inPrompt as String, inValue as String',
					'return': 'IpfcDetailVariantText',
					'info': 'Creates a data object describing the replacement of variable text in a symbol definition.',
					'com': 'pfcls.pfcDetailVariantText.1',
				},
			},
	}
	IpfcDiagram = {
			'type': 'Classes',
			'parent': {
				'IpfcModel': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcDimension = {
			'type': 'Classes',
			'parent': {
				'IpfcBaseDimension': '',
			},
			'child': {
			},
			'properties': {
				'Tolerance': {
					'return': 'IpfcDimTolerance',
					'info': 'The dimension tolerance',
				},
			},
			'method': {
				'IsReference': {
					'type': 'Function',
					'args': '',
					'return': ' as Boolean',
					'info': '  ',
				},
			},
	}
	IpfcDimension2D = {
			'type': 'Classes',
			'parent': {
				'IpfcBaseDimension': '',
			},
			'child': {
			},
			'properties': {
				'IsAssociative': {
					'return': 'as Boolean',
					'info': 'Whether or not the dimension is associative.',
				},
				'IsDisplayed': {
					'return': 'as Boolean',
					'info': 'Whether or not the dimension is current displayed in the drawing.',
				},
				'IsToleranceDisplayed': {
					'return': 'as Boolean',
					'info': "Whether or not the dimension's tolerance is displayed in the drawing.",
				},
				'Location': {
					'return': 'IpfcPoint3D',
					'info': 'The placement location of the dimension.',
				},
			},
			'method': {
				'ConvertToBaseline': {
					'type': 'Function',
					'args': 'Location as IpfcVector2D',
					'return': 'IpfcDimension2D',
					'info': 'Converts a location on a linear drawing dimension to be used as an ordinate baseline dimension.  The linear dimension will be converted to ordinate.',
				},
				'ConvertToLinear': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Converts an ordinate drawing dimension to a linear dimension.',
				},
				'ConvertToOrdinate': {
					'type': 'Sub',
					'args': 'BaselineDimension as IpfcDimension2D',
					'return': '',
					'info': 'Converts a linear drawing dimension to ordinate.',
				},
				'EraseFromModel2D': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Erases the dimension from drawing permanently.',
				},
				'GetAttachmentPoints': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSelections',
					'info': 'Returns the attachment locations for a drawing dimension.',
				},
				'GetBaselineDimension': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDimension2D',
					'info': 'Returns the baseline dimension for an ordinate drawing dimension.',
				},
				'GetDimensionSenses': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDimensionSenses',
					'info': 'Returns the dimension senses for the drawing dimension.',
				},
				'GetIsReference': {
					'type': 'Function',
					'args': '',
					'return': ' as Boolean',
					'info': 'Determines if a drawing dimension is a reference dimension.',
				},
				'GetOrientationHint': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcOrientationHint',
					'info': 'Returns the orientation hint used for placing the drawing dimension.',
				},
				'GetTolerance': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDimTolerance',
					'info': 'Retrieves parameters of a specified dimension.',
				},
				'GetView': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcView2D',
					'info': 'Returns the view associated with a drawing dimension.',
				},
				'SetTolerance': {
					'type': 'Sub',
					'args': 'Limits as IpfcDimTolerance [optional]',
					'return': '',
					'info': 'Sets the tolerance of the specified dimension of the object.',
				},
				'SwitchView': {
					'type': 'Sub',
					'args': 'View as IpfcView2D',
					'return': '',
					'info': 'Switches a drawing dimension to a new view.',
				},
			},
	}
	IpfcDimensionAngleOptions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'IsFirst': {
					'return': 'as Boolean [optional]',
					'info': 'true if the angle starts from this entity in a    counterclockwise sense; false or null if the    dimension ends at this entity.  One entity in an angular    dimension must be true, and one must be false.   ',
				},
				'ShouldFlip': {
					'return': 'as Boolean [optional]',
					'info': '''If "should_flip" is false or null, and    the entity's inherent direction is away from the angle vertex, then    the dimension attaches directly to the entity. If the entity's direction    is towards the angle vertex, the dimension is attached to a witness    line which is in line with the entity but on the opposite side of the    angle vertex. If "should_flip" is true, then these cases are    interchanged.''',
				},
			},
			'method': {
				'CCpfcDimensionAngleOptions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDimensionAngleOptions',
					'info': 'Creates an empty DimensionAngleOptions object.',
					'com': 'pfcls.pfcDimensionAngleOptions.1',
				},
			},
	}
	IpfcDimensionSense = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcEmptyDimensionSense': '',
				'IpfcPointDimensionSense': '',
				'IpfcSplinePointDimensionSense': '',
				'IpfcTangentIndexDimensionSense': '',
				'IpfcLinAOCTangentDimensionSense': '',
				'IpfcAngleDimensionSense': '',
				'IpfcPointToAngleDimensionSense': '',
			},
			'properties': {
			},
			'method': {
				'GetType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDimensionSenseType',
					'info': 'Returns the type of option used for a dimension to attach to entities in a drawing. ',
				},
			},
	}
	IpfcDimensionShowInstructions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcComponentDimensionShowInstructions': '',
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcDimSense = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcEmptyDimSense': '',
				'IpfcPointDimSense': '',
				'IpfcSplinePointDimSense': '',
				'IpfcTangentIndexDimSense': '',
				'IpfcLineAOCTangentDimSense': '',
				'IpfcAngularDimSense': '',
				'IpfcPointToAngleDimSense': '',
			},
			'properties': {
			},
			'method': {
				'GetType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDimSenseType',
					'info': '  ',
				},
			},
	}
	IpfcDimTolerance = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcDimTolPlusMinus': '',
				'IpfcDimTolSymmetric': '',
				'IpfcDimTolSymSuperscript': '',
				'IpfcDimTolLimits': '',
				'IpfcDimTolISODIN': '',
			},
			'properties': {
				'Type': {
					'return': 'IpfcDimToleranceType',
					'info': 'The type of tolerance.',
				},
			},
			'method': {
			},
	}
	IpfcDimTolISODIN = {
			'type': 'Classes',
			'parent': {
				'IpfcDimTolerance': '',
			},
			'child': {
			},
			'properties': {
				'TableColumn': {
					'return': 'as Long',
					'info': 'The table column assigned to the dimension if the tolerance table type is hole/shaft.',
				},
				'TableName': {
					'return': 'as String',
					'info': 'The table name assigned to the dimension if the tolerance table type is hole/shaft.',
				},
				'TolTableType': {
					'return': 'IpfcToleranceTableType',
					'info': 'The tolerance type assigned to the dimension (general, broken edge, hole or shaft)',
				},
			},
			'method': {
				'CCpfcDimTolISODIN.Create': {
					'type': 'Function',
					'args': 'inTolTableType as IpfcToleranceTableType, inTableName as String, inTableColumn as Long',
					'return': 'IpfcDimTolISODIN',
					'info': 'Creates a DimTolISODIN object.',
					'com': 'pfcls.pfcDimTolISODIN.1',
				},
			},
	}
	IpfcDimTolLimits = {
			'type': 'Classes',
			'parent': {
				'IpfcDimTolerance': '',
			},
			'child': {
			},
			'properties': {
				'LowerLimit': {
					'return': 'as Double',
					'info': 'The lower limit of the tolerance',
				},
				'UpperLimit': {
					'return': 'as Double',
					'info': 'The upper limit of the tolerance',
				},
			},
			'method': {
				'CCpfcDimTolLimits.Create': {
					'type': 'Function',
					'args': 'inUpperLimit as Double [optional], inLowerLimit as Double [optional]',
					'return': 'IpfcDimTolLimits',
					'info': 'Returns an object containing a limits-type tolerance for a dimension.',
					'com': 'pfcls.pfcDimTolLimits.1',
				},
			},
	}
	IpfcDimTolPlusMinus = {
			'type': 'Classes',
			'parent': {
				'IpfcDimTolerance': '',
			},
			'child': {
			},
			'properties': {
				'Minus': {
					'return': 'as Double',
					'info': 'The tolerance amount below the nominal value',
				},
				'Plus': {
					'return': 'as Double',
					'info': 'The tolerance amount above the nominal value',
				},
			},
			'method': {
				'CCpfcDimTolPlusMinus.Create': {
					'type': 'Function',
					'args': 'inPlus as Double [optional], inMinus as Double [optional]',
					'return': 'IpfcDimTolPlusMinus',
					'info': 'Creates a new DimTolPlusMinus object.',
					'com': 'pfcls.pfcDimTolPlusMinus.1',
				},
			},
	}
	IpfcDimTolSymmetric = {
			'type': 'Classes',
			'parent': {
				'IpfcDimTolerance': '',
			},
			'child': {
			},
			'properties': {
				'Value': {
					'return': 'as Double',
					'info': 'The tolerance value',
				},
			},
			'method': {
				'CCpfcDimTolSymmetric.Create': {
					'type': 'Function',
					'args': 'inValue as Double [optional]',
					'return': 'IpfcDimTolSymmetric',
					'info': 'Creates a symmetric tolerance.',
					'com': 'pfcls.pfcDimTolSymmetric.1',
				},
			},
	}
	IpfcDimTolSymSuperscript = {
			'type': 'Classes',
			'parent': {
				'IpfcDimTolerance': '',
			},
			'child': {
			},
			'properties': {
				'Value': {
					'return': 'as Double',
					'info': 'The tolerance value',
				},
			},
			'method': {
				'CCpfcDimTolSymSuperscript.Create': {
					'type': 'Function',
					'args': 'inValue as Double [optional]',
					'return': 'IpfcDimTolSymSuperscript',
					'info': 'Creates a symmetric tolerance in superscript format.',
					'com': 'pfcls.pfcDimTolSymSuperscript.1',
				},
			},
	}
	IpfcDirectorySelectionOptions = {
			'type': 'Classes',
			'parent': {
				'IpfcFileUIOptions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcDirectorySelectionOptions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDirectorySelectionOptions',
					'info': 'Creates a class object for directory selection dialog options.   ',
					'com': 'pfcls.pfcDirectorySelectionOptions.1',
				},
			},
	}
	IpfcDisplay = {
			'type': 'Classes',
			'parent': {
				'IpfcParent': '',
			},
			'child': {
				'IpfcBaseSession': '',
			},
			'properties': {
				'CurrentFont': {
					'return': 'IpfcFont',
					'info': 'The current font used for text graphics in Creo Parametric.',
				},
				'CurrentGraphicsColor': {
					'return': 'IpfcStdColor',
					'info': 'The standard color used to display new graphics.',
				},
				'CurrentGraphicsMode': {
					'return': 'IpfcGraphicsMode',
					'info': 'The mode used to display graphics (i.e. normal or complement).',
				},
				'DefaultFont': {
					'return': 'IpfcFont',
					'info': 'The default font for this session of Creo Parametric.',
				},
				'RotationAngle': {
					'return': 'as Double',
					'info': 'The rotation angle of created text graphics (default is 0).',
				},
				'SlantAngle': {
					'return': 'as Double',
					'info': 'The slant angle of created text graphics (default is 0).',
				},
				'TextHeight': {
					'return': 'as Double',
					'info': 'The text height of created text graphics.',
				},
				'WidthFactor': {
					'return': 'as Double',
					'info': 'The width-to-height ratio for created text graphics.',
				},
			},
			'method': {
				'CreateDisplayList2D': {
					'type': 'Function',
					'args': 'Id as Long, Transform as IpfcScreenTransform, Action as IpfcDisplayListener',
					'return': 'IpfcDisplayList2D',
					'info': 'Creates a two-dimensional display list',
				},
				'CreateDisplayList3D': {
					'type': 'Function',
					'args': 'Id as Long, Action as IpfcDisplayListener',
					'return': 'IpfcDisplayList3D',
					'info': 'Creates a three-dimensional display list entity',
				},
				'DrawArc2D': {
					'type': 'Sub',
					'args': 'Center as IpfcPoint3D, Radius as Double, StartDirection as IpfcVector3D, EndDirection as IpfcVector3D',
					'return': '',
					'info': 'Draws an arc on the screen',
				},
				'DrawCircle': {
					'type': 'Sub',
					'args': 'Center as IpfcPoint3D, Radius as Double',
					'return': '',
					'info': 'Draws a circle on the screen',
				},
				'DrawLine': {
					'type': 'Sub',
					'args': 'Endpoint as IpfcPoint3D',
					'return': '',
					'info': 'Draws a line on the screen from the last pen position specified to the position specified by the Endpoint argument.The last pen position is specified using this same function previously times or by IpfcDisplay.SetPenPosition()',
				},
				'DrawPolygon2D': {
					'type': 'Sub',
					'args': 'Vertices as IpfcPoint2Ds, FillColor as IpfcStdColor [optional]',
					'return': '',
					'info': 'Draws two-dimensional polygon on the screen.',
				},
				'DrawPolyline': {
					'type': 'Sub',
					'args': 'Points as IpfcPoint3Ds',
					'return': '',
					'info': 'Draws a series of connected line segments on the screen.',
				},
				'DrawText2D': {
					'type': 'Sub',
					'args': 'StartPoint as IpfcPoint3D, TextLine as String',
					'return': '',
					'info': 'Draws text on the screen',
				},
				'GetFontById': {
					'type': 'Function',
					'args': 'Id as Long',
					'return': 'IpfcFont',
					'info': 'Creates a font object given a Creo Parametric font integer id.',
				},
				'GetFontByName': {
					'type': 'Function',
					'args': 'Name as String',
					'return': 'IpfcFont',
					'info': 'Returns a font object given a Creo Parametric font name.',
				},
				'Invalidate': {
					'type': 'Sub',
					'args': 'Model as IpfcModel',
					'return': '',
					'info': 'Invalidates the display list of the model singalling that a repaint is needed.',
				},
				'ResetTextAttributes': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Resets the Creo Parametric text attributes to use the default settings',
				},
				'SetPenPosition': {
					'type': 'Sub',
					'args': 'NewPosition as IpfcPoint3D',
					'return': '',
					'info': 'This method enables you to move to a point without producing any graphical output.  Thismethod is used when drawing lines using IpfcDisplay.DrawLine()',
				},
			},
	}
	IpfcDisplayList2D = {
			'type': 'Classes',
			'parent': {
				'IpfcActionSource': '',
				'IpfcChild': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'Delete': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Deletes the DisplayList2D from the session, causing not to update with further repaints.',
				},
				'Display': {
					'type': 'Sub',
					'args': 'Transform as IpfcScreenTransform',
					'return': '',
					'info': 'Forces the display of the DisplayList2D using the specified transformation matrix',
				},
			},
	}
	IpfcDisplayList3D = {
			'type': 'Classes',
			'parent': {
				'IpfcActionSource': '',
				'IpfcChild': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'Delete': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Deletes the DisplayList3D from the session, so it will no longer update during repaints.',
				},
				'Display': {
					'type': 'Sub',
					'args': 'Transform as IpfcTransform3D',
					'return': '',
					'info': 'Displays the DisplayList3D using the specified transformation matrix',
				},
			},
	}
	IpfcDisplayListener = {
			'type': 'Classes',
			'parent': {
				'IpfcActionListener': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'OnDisplay': {
					'type': 'Sub',
					'args': 'Display as IpfcDisplay',
					'return': '',
					'info': 'This method is called when a IpfcDisplayList2Dand IpfcDisplayList3D needs to be repainted by Creo Parametric',
				},
			},
	}
	IpfcDll = {
			'type': 'Classes',
			'parent': {
				'IpfcChild': '',
			},
			'child': {
			},
			'properties': {
				'Id': {
					'return': 'as String',
					'info': 'The DLL identifier string.  Use IpfcBaseSession.GetProToolkitDll() to obtain a DLL handle using this string.',
				},
			},
			'method': {
				'CallFunction': {
					'type': 'Function',
					'args': 'FunctionName as String, InputArguments as IpfcArguments, UsedToolkit as IpfcToolkitType',
					'return': 'IpfcFunctionReturn',
					'info': '  ',
				},
				'ExecuteFunction': {
					'type': 'Function',
					'args': 'FunctionName as String, InputArguments as IpfcArguments',
					'return': 'IpfcFunctionReturn',
					'info': 'Causes Creo Parametric to call a function in a Creo Parametric TOOLKIT DLL application.',
				},
				'IsActive': {
					'type': 'Function',
					'args': '',
					'return': ' as Boolean',
					'info': 'Helper method for determining if application that this IpfcDll object has been unloaded.',
				},
				'Unload': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Unloads a previously loaded DLL application.',
				},
			},
	}
	IpfcDrawing = {
			'type': 'Classes',
			'parent': {
				'IpfcModel2D': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'ConvertLinearDimensionToBaseline': {
					'type': 'Function',
					'args': 'Dim as IpfcDimension, Location as IpfcVector2D',
					'return': 'IpfcDimension',
					'info': 'Converts an existing linear dimension to ordinate baseline. To use this method drawing must be displayed. ',
				},
				'ConvertLinearDimensionToOrdinate': {
					'type': 'Sub',
					'args': 'Dim as IpfcDimension, BaselineDimension as IpfcDimension',
					'return': '',
					'info': 'Converts an existing linear dimension to ordinate. To use this method drawing must be displayed. ',
				},
				'ConvertOrdinateDimensionToLinear': {
					'type': 'Sub',
					'args': 'Dim as IpfcDimension',
					'return': '',
					'info': 'Converts an existing ordinate dimension to linear. To use this method drawing must be displayed. ',
				},
				'EraseDimension': {
					'type': 'Sub',
					'args': 'Dim as IpfcDimension',
					'return': '',
					'info': 'Erase dimension from specified drawing. ',
				},
				'GetBaselineDimension': {
					'type': 'Function',
					'args': 'Dim as IpfcDimension',
					'return': 'IpfcDimension',
					'info': 'Checks if dimension is an ordinate dimension and if it is ordinate then returns baseline dimension. ',
				},
				'GetDimensionAttachPoints': {
					'type': 'Function',
					'args': 'Dim as IpfcDimension',
					'return': 'IpfcDimensionAttachments',
					'info': 'Gets attachment information of dimension. ',
				},
				'GetDimensionLocation': {
					'type': 'Function',
					'args': 'Dim as IpfcDimension',
					'return': 'IpfcPoint3D',
					'info': 'Get location of dimension text in specified drawing. ',
				},
				'GetDimensionOrientHint': {
					'type': 'Function',
					'args': 'Dim as IpfcDimension',
					'return': 'IpfcDimOrientationHint',
					'info': 'Gets orientation of dimension ',
				},
				'GetDimensionSenses': {
					'type': 'Function',
					'args': 'Dim as IpfcDimension',
					'return': 'IpfcDimSenses',
					'info': 'Gets sense information of dimension. ',
				},
				'GetDimensionView': {
					'type': 'Function',
					'args': 'Dim as IpfcDimension',
					'return': 'IpfcView2D',
					'info': 'Get drawing view of dimension.  ',
				},
				'IsDimensionAssociative': {
					'type': 'Function',
					'args': 'Dim as IpfcDimension',
					'return': ' as Boolean',
					'info': 'Checks if solid dimension shown in drawing is associative.  ',
				},
				'IsDimensionShown': {
					'type': 'Function',
					'args': 'Dim as IpfcDimension',
					'return': ' as Boolean',
					'info': 'Checks if dimension is shown in a specified drawing. ',
				},
				'IsDimensionToleranceDisplayed': {
					'type': 'Function',
					'args': 'Dim as IpfcDimension',
					'return': ' as Boolean',
					'info': "Indicates whether a drawing dimension's tolerance is visible or not. ",
				},
				'SetDimensionLocation': {
					'type': 'Sub',
					'args': 'Dim as IpfcDimension, ToLocation as IpfcPoint3D',
					'return': '',
					'info': 'Changes location of dimension text. ',
				},
				'SwitchDimensionView': {
					'type': 'Sub',
					'args': 'Dim as IpfcDimension, View as IpfcView2D',
					'return': '',
					'info': 'Change view of the dimension in specified drawing. ',
				},
			},
	}
	IpfcDrawingCreateError = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'ObjectName': {
					'return': 'as String [optional]',
					'info': 'The object name related to the error.  If null, there is no object name associated with this error. ',
				},
				'SheetNumber': {
					'return': 'as Long',
					'info': 'The sheet number where the error occurred.',
				},
				'Type': {
					'return': 'IpfcDrawingCreateErrorType',
					'info': 'The type of error encountered.',
				},
				'View': {
					'return': 'IpfcView2D',
					'info': 'The drawing view where the error occurred.  If null, there is no drawing view associated with this error. ',
				},
				'ViewName': {
					'return': 'as String',
					'info': 'The name of the drawing view where the error occurred. ',
				},
			},
			'method': {
			},
	}
	IpfcDrawingDimCreateInstructions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Attachments': {
					'return': 'IpfcSelections',
					'info': 'The entities that the dimension references.  The selections should include the drawing model view.',
				},
				'Display': {
					'return': 'as Boolean',
					'info': 'Whether or not to display the new dimension upon creation.  The default is true.',
				},
				'IsRefDimension': {
					'return': 'as Boolean',
					'info': 'true if the dimension should be a reference dimension, null or false otherwise.',
				},
				'OrientationHint': {
					'return': 'IpfcOrientationHint',
					'info': 'The orientation hint for proper dimension placement.',
				},
				'Senses': {
					'return': 'IpfcDimensionSenses',
					'info': 'The dimension senses associated with the dimension attachments.',
				},
				'TextLocation': {
					'return': 'IpfcVector2D',
					'info': 'The location of the dimension text, in screen coordinates.',
				},
			},
			'method': {
				'CCpfcDrawingDimCreateInstructions.Create': {
					'type': 'Function',
					'args': 'Attachments as IpfcSelections, Senses as IpfcDimensionSenses, TextLocation as IpfcVector2D, Hint as IpfcOrientationHint',
					'return': 'IpfcDrawingDimCreateInstructions',
					'info': 'Create an instructions object used to create a new dimension in a drawing.',
					'com': 'pfcls.pfcDrawingDimCreateInstructions.1',
				},
			},
	}
	IpfcDrawingDimensionShowInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcComponentDimensionShowInstructions': '',
			},
			'child': {
			},
			'properties': {
				'View': {
					'return': 'IpfcView2D',
					'info': 'The drawing view that shows the dimension.',
				},
			},
			'method': {
				'CCpfcDrawingDimensionShowInstructions.Create': {
					'type': 'Function',
					'args': 'View as IpfcView2D, Path as IpfcComponentPath [optional]',
					'return': 'IpfcDrawingDimensionShowInstructions',
					'info': 'Returns a new instance of IpfcDrawingDimensionShowInstructions.',
					'com': 'pfcls.pfcDrawingDimensionShowInstructions.1',
				},
			},
	}
	IpfcDrawingFormat = {
			'type': 'Classes',
			'parent': {
				'IpfcModel': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcDWG3DExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExport3DInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcDWG3DExportInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDWG3DExportInstructions',
					'info': '        Creates a new instructions object used to export a model to DWG format.      ',
					'com': 'pfcls.pfcDWG3DExportInstructions.1',
				},
			},
	}
	IpfcDWGImport2DInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcImport2DInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcDWGImport2DInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDWGImport2DInstructions',
					'info': 'Creates a new data object used for importing a DWG file into Creo Parametric.',
					'com': 'pfcls.pfcDWGImport2DInstructions.1',
				},
			},
	}
	IpfcDWGSetupExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcDWGSetupExportInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDWGSetupExportInstructions',
					'info': 'Creates a new instructions object used to export a drawing setup file.',
					'com': 'pfcls.pfcDWGSetupExportInstructions.1',
				},
			},
	}
	IpfcDWGSetupImportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcImportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcDWGSetupImportInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDWGSetupImportInstructions',
					'info': 'Creates a new instructions object used to import (read) from a drawing setup (DWG_SETUP) type file.',
					'com': 'pfcls.pfcDWGSetupImportInstructions.1',
				},
			},
	}
	IpfcDXF3DExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExport3DInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcDXF3DExportInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDXF3DExportInstructions',
					'info': '        Creates a new instructions object used to export a model to DXF format.      ',
					'com': 'pfcls.pfcDXF3DExportInstructions.1',
				},
			},
	}
	IpfcDXFExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExportInstructions': '',
			},
			'child': {
			},
			'properties': {
				'OptionValue': {
					'return': 'IpfcExport2DOption',
					'info': 'Used to set export sheet options. It is supported in Object TOOLKIT only',
				},
			},
			'method': {
				'CCpfcDXFExportInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDXFExportInstructions',
					'info': 'Creates a new instructions object used to export a drawing in DXF format.',
					'com': 'pfcls.pfcDXFExportInstructions.1',
				},
			},
	}
	IpfcDXFImport2DInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcImport2DInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcDXFImport2DInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDXFImport2DInstructions',
					'info': 'Creates a new data object used for importing a 2-D DXF file into Creo Parametric.',
					'com': 'pfcls.pfcDXFImport2DInstructions.1',
				},
			},
	}
	IpfcEdge = {
			'type': 'Classes',
			'parent': {
				'IpfcGeomCurve': '',
				'IpfcModelItem': '',
			},
			'child': {
			},
			'properties': {
				'Edge1': {
					'return': 'IpfcEdge',
					'info': 'The next edge in the contour',
				},
				'Edge2': {
					'return': 'IpfcEdge',
					'info': 'The next edge in the other contour',
				},
				'Surface1': {
					'return': 'IpfcSurface',
					'info': 'The first adjacent surface ',
				},
				'Surface2': {
					'return': 'IpfcSurface',
					'info': 'The second adjacent surface',
				},
			},
			'method': {
				'EvalUV': {
					'type': 'Function',
					'args': 'Parameter as Double',
					'return': 'IpfcEdgeEvalData',
					'info': 'Evaluates the edge at a particular t-parameter location in terms of the UV coordinates of the edge. ',
				},
				'GetDirection': {
					'type': 'Function',
					'args': 'Surf as IpfcSurface',
					'return': ' as Long',
					'info': 'Gets the edge direction in the specified surface.',
				},
			},
	}
	IpfcEdgeEvalData = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Derivative1A': {
					'return': 'IpfcUVVector',
					'info': 'The first derivative for PointA ',
				},
				'Derivative1B': {
					'return': 'IpfcUVVector',
					'info': 'The first derivative for PointB',
				},
				'Derivative2A': {
					'return': 'IpfcUVVector',
					'info': 'The second derivative for PointA',
				},
				'Derivative2B': {
					'return': 'IpfcUVVector',
					'info': 'The second derivative for PointB',
				},
				'Parameter': {
					'return': 'as Double',
					'info': 'The normalized parameter on the edge',
				},
				'PointA': {
					'return': 'IpfcUVParams',
					'info': 'The first UV point',
				},
				'PointB': {
					'return': 'IpfcUVParams',
					'info': 'The second UV point',
				},
			},
			'method': {
			},
	}
	IpfcEllipse = {
			'type': 'Classes',
			'parent': {
				'IpfcCurve': '',
			},
			'child': {
			},
			'properties': {
				'Center': {
					'return': 'IpfcPoint3D',
					'info': 'The center of the ellipse',
				},
				'EndAngle': {
					'return': 'as Double',
					'info': 'The ending angle',
				},
				'MajorLength': {
					'return': 'as Double',
					'info': 'The major length',
				},
				'MinorLength': {
					'return': 'as Double',
					'info': 'The minor length',
				},
				'StartAngle': {
					'return': 'as Double',
					'info': 'The starting angle',
				},
				'UnitMajorAxis': {
					'return': 'IpfcVector3D',
					'info': 'The unit major axis',
				},
				'UnitNormal': {
					'return': 'IpfcVector3D',
					'info': 'The unit normal',
				},
			},
			'method': {
			},
	}
	IpfcEllipseDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcCurveDescriptor': '',
			},
			'child': {
			},
			'properties': {
				'Center': {
					'return': 'IpfcPoint3D',
					'info': 'The center of the ellipse',
				},
				'EndAngle': {
					'return': 'as Double',
					'info': 'The ending angle',
				},
				'MajorLength': {
					'return': 'as Double',
					'info': 'The major length',
				},
				'MinorLength': {
					'return': 'as Double',
					'info': 'The minor length',
				},
				'StartAngle': {
					'return': 'as Double',
					'info': 'The starting angle',
				},
				'UnitMajorAxis': {
					'return': 'IpfcVector3D',
					'info': 'The unit major axis',
				},
				'UnitNormal': {
					'return': 'IpfcVector3D',
					'info': 'The unit normal',
				},
			},
			'method': {
				'CCpfcEllipseDescriptor.Create': {
					'type': 'Function',
					'args': 'Center as IpfcPoint3D, UnitMajorAxis as IpfcVector3D, UnitNormal as IpfcVector3D, MajorLength as Double, MinorLength as Double, StartAngle as Double, EndAngle as Double',
					'return': 'IpfcEllipseDescriptor',
					'info': 'This method returns a new EllipseDescriptor object.',
					'com': 'pfcls.pfcEllipseDescriptor.1',
				},
			},
	}
	IpfcEmptyDimensionSense = {
			'type': 'Classes',
			'parent': {
				'IpfcDimensionSense': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcEmptyDimensionSense.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcEmptyDimensionSense',
					'info': 'Creates an empty dimension sense object, for use in creating a new drawing dimension.',
					'com': 'pfcls.pfcEmptyDimensionSense.1',
				},
			},
	}
	IpfcEmptyDimSense = {
			'type': 'Classes',
			'parent': {
				'IpfcDimSense': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcEmptyDimSense.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcEmptyDimSense',
					'info': '  ',
					'com': 'pfcls.pfcEmptyDimSense.1',
				},
			},
	}
	IpfcEPSImageExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcRasterImageExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcEPSImageExportInstructions.Create': {
					'type': 'Function',
					'args': 'ImageWidth as Double, ImageHeight as Double',
					'return': 'IpfcEPSImageExportInstructions',
					'info': 'Creates a new instructions object used to export EPSI-format (type) image.',
					'com': 'pfcls.pfcEPSImageExportInstructions.1',
				},
			},
	}
	IpfcExplodedState = {
			'type': 'Classes',
			'parent': {
				'IpfcModelItem': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'Activate': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Activates the exploded state in the assembly.',
				},
			},
	}
	IpfcExport2DOption = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'ExportSheetOption': {
					'return': 'IpfcExport2DSheetOption',
					'info': '  ',
				},
				'ModelSpaceSheet': {
					'return': 'as Long [optional]',
					'info': '  ',
				},
				'Sheets': {
					'return': 'Iintseq',
					'info': '  ',
				},
			},
			'method': {
				'CCpfcExport2DOption.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcExport2DOption',
					'info': '        Creates a new Export2DOption object used to export to        STEP or Medusa OR DXF format.      ',
					'com': 'pfcls.pfcExport2DOption.1',
				},
			},
	}
	IpfcExport3DInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExportInstructions': '',
			},
			'child': {
				'IpfcSTEP3DExportInstructions': '',
				'IpfcVDA3DExportInstructions': '',
				'IpfcIGES3DNewExportInstructions': '',
				'IpfcCATIAModel3DExportInstructions': '',
				'IpfcACIS3DExportInstructions': '',
				'IpfcDXF3DExportInstructions': '',
				'IpfcDWG3DExportInstructions': '',
				'IpfcCATIASession3DExportInstructions': '',
				'IpfcJT3DExportInstructions': '',
				'IpfcUG3DExportInstructions': '',
				'IpfcParaSolid3DExportInstructions': '',
				'IpfcCatiaPart3DExportInstructions': '',
				'IpfcCatiaProduct3DExportInstructions': '',
				'IpfcCatiaCGR3DExportInstructions': '',
				'IpfcSWPart3DExportInstructions': '',
				'IpfcSWAsm3DExportInstructions': '',
			},
			'properties': {
				'Configuration': {
					'return': 'IpfcAssemblyConfiguration',
					'info': 'The configuration to use when exporting the model.',
				},
				'Geometry': {
					'return': 'IpfcGeometryFlags',
					'info': 'The geometry type(s) to export.',
				},
				'IncludedEntities': {
					'return': 'IpfcInclusionFlags',
					'info': 'Flags indicating whether or not to include certain entities. If null, do not include these entities.',
				},
				'LayerOptions': {
					'return': 'IpfcLayerExportOptions',
					'info': 'Object indicating the special instructions to follow when exporting layers.  If null, layers will be exported without changes.',
				},
				'ReferenceSystem': {
					'return': 'IpfcSelection',
					'info': 'The coordinate system used for the export.  If null, the default model coordinate system is used.',
				},
				'TriangulationOptions': {
					'return': 'IpfcTriangulationInstructions',
					'info': '       Object indicating the special instructions to follow when exporting to a faceted format.         If null, export will be without triangulation.     ',
				},
			},
			'method': {
			},
	}
	IpfcExportInstructions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcRelationExportInstructions': '',
				'IpfcIGESFileExportInstructions': '',
				'IpfcModelInfoExportInstructions': '',
				'IpfcProgramExportInstructions': '',
				'IpfcDXFExportInstructions': '',
				'IpfcDWGSetupExportInstructions': '',
				'IpfcFeatIdExportInstructions': '',
				'IpfcCoordSysExportInstructions': '',
				'IpfcGeomExportInstructions': '',
				'IpfcBOMExportInstructions': '',
				'IpfcMaterialExportInstructions': '',
				'IpfcConnectorParamExportInstructions': '',
				'IpfcCGMFILEExportInstructions': '',
				'IpfcVRMLExportInstructions': '',
				'IpfcPlotInstructions': '',
				'IpfcSTEP2DExportInstructions': '',
				'IpfcMedusaExportInstructions': '',
				'IpfcCableParamsFileInstructions': '',
				'IpfcExport3DInstructions': '',
				'IpfcCADDSExportInstructions': '',
				'IpfcNEUTRALFileExportInstructions': '',
				'IpfcProductViewExportInstructions': '',
				'IpfcPDFExportInstructions': '',
				'IpfcPrinterInstructions': '',
			},
			'properties': {
			},
			'method': {
				'GetFileType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcExportType',
					'info': 'Returns a IpfcExportType instance that indicates the type of data the object exports when passed as an argument to IpfcModel.Export() .',
				},
			},
	}
	IpfcExternalData = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'discr': {
					'return': 'IpfcExternalDataType',
					'info': 'None',
				},
				'DoubleValue': {
					'return': 'as Double',
					'info': 'The double value.',
				},
				'IntegerValue': {
					'return': 'as Long',
					'info': 'The integer value.',
				},
				'StringValue': {
					'return': 'as String',
					'info': 'The string value.',
				},
			},
			'method': {
			},
	}
	IpfcExternalDataAccess = {
			'type': 'Classes',
			'parent': {
				'IpfcChild': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CreateClass': {
					'type': 'Function',
					'args': 'Name as String',
					'return': 'IpfcExternalDataClass',
					'info': "Creates a class in the model's external data.",
				},
				'IsValid': {
					'type': 'Function',
					'args': '',
					'return': ' as Boolean',
					'info': 'Returns true if this object is still valid for this model, false if access has been terminated',
				},
				'ListClasses': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcExternalDataClasses',
					'info': 'Lists all of the external data classes for the model',
				},
				'LoadAll': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': "Loads all of the model's external data into memory.",
				},
				'ReleaseDataBuffer': {
					'type': 'Sub',
					'args': 'data as IpfcExternalData',
					'return': '',
					'info': 'Frees the memory allocated for data buffer.',
				},
			},
	}
	IpfcExternalDataClass = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
			},
			'properties': {
				'Name': {
					'return': 'as String',
					'info': 'Unique name of the class for this model',
				},
				'Parent': {
					'return': 'IpfcExternalDataAccess',
					'info': 'ExternalDataAccess object to which the class belongs',
				},
			},
			'method': {
				'CreateSlot': {
					'type': 'Function',
					'args': 'Name as String',
					'return': 'IpfcExternalDataSlot',
					'info': 'Creates a new slot for the external data class',
				},
				'Delete': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Deletes the external data class from the model   ',
				},
				'IsValid': {
					'type': 'Function',
					'args': '',
					'return': ' as Boolean',
					'info': 'Returns true if the class is valid and can be accessed, false if it has been deleted or it cannot be accessed.',
				},
				'ListSlots': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcExternalDataSlots',
					'info': 'Lists all the slots available for the class',
				},
			},
	}
	IpfcExternalDataSlot = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
			},
			'properties': {
				'Class': {
					'return': 'IpfcExternalDataClass',
					'info': 'Class to which slot belongs',
				},
				'Id': {
					'return': 'as Long',
					'info': 'Unique identifier assigned by Creo Parametric',
				},
				'Name': {
					'return': 'as String [readonly, optional]',
					'info': 'Slot name.  Slots created using Creo Parametric TOOLKIT may not have a separate name.',
				},
				'Value': {
					'return': 'IpfcExternalData',
					'info': 'The value (integer, double, or string) stored in the slot.  Stream-type slots created byCreo Parametric TOOLKIT cannot be accessed by this attribute and an IpfcXBadExternalDataexception will result.',
				},
			},
			'method': {
				'Delete': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Deletes the external data slot',
				},
				'IsValid': {
					'type': 'Function',
					'args': '',
					'return': ' as Boolean',
					'info': 'Returns true if the slot exists and can be accessed, false if it has been deleted.',
				},
			},
	}
	IpfcFamColComp = {
			'type': 'Classes',
			'parent': {
				'IpfcFamColModelItem': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcFamColCompModel = {
			'type': 'Classes',
			'parent': {
				'IpfcFamColModelItem': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcFamColDimension = {
			'type': 'Classes',
			'parent': {
				'IpfcFamColModelItem': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcFamColExternalRef = {
			'type': 'Classes',
			'parent': {
				'IpfcFamilyTableColumn': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcFamColFeature = {
			'type': 'Classes',
			'parent': {
				'IpfcFamColModelItem': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcFamColGroup = {
			'type': 'Classes',
			'parent': {
				'IpfcFamColModelItem': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcFamColGTol = {
			'type': 'Classes',
			'parent': {
				'IpfcFamilyTableColumn': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcFamColInhFeatPart = {
			'type': 'Classes',
			'parent': {
				'IpfcFamColModelItem': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcFamColIParNote = {
			'type': 'Classes',
			'parent': {
				'IpfcFamilyTableColumn': '',
			},
			'child': {
			},
			'properties': {
				'RefParam': {
					'return': 'IpfcParameter',
					'info': 'The reference parameter.',
				},
			},
			'method': {
			},
	}
	IpfcFamColMergePart = {
			'type': 'Classes',
			'parent': {
				'IpfcFamColModelItem': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcFamColModelItem = {
			'type': 'Classes',
			'parent': {
				'IpfcFamilyTableColumn': '',
			},
			'child': {
				'IpfcFamColDimension': '',
				'IpfcFamColFeature': '',
				'IpfcFamColComp': '',
				'IpfcFamColCompModel': '',
				'IpfcFamColGroup': '',
				'IpfcFamColMergePart': '',
				'IpfcFamColInhFeatPart': '',
				'IpfcFamColTolPlus': '',
				'IpfcFamColTolMinus': '',
				'IpfcFamColTolPlusMinus': '',
			},
			'properties': {
				'RefItem': {
					'return': 'IpfcModelItem',
					'info': 'The reference item.',
				},
			},
			'method': {
			},
	}
	IpfcFamColParam = {
			'type': 'Classes',
			'parent': {
				'IpfcFamilyTableColumn': '',
			},
			'child': {
			},
			'properties': {
				'RefParam': {
					'return': 'IpfcParameter',
					'info': 'The reference parameter',
				},
			},
			'method': {
			},
	}
	IpfcFamColSim = {
			'type': 'Classes',
			'parent': {
				'IpfcFamilyTableColumn': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcFamColSystemParam = {
			'type': 'Classes',
			'parent': {
				'IpfcFamilyTableColumn': '',
			},
			'child': {
			},
			'properties': {
				'RefParam': {
					'return': 'IpfcParameter',
					'info': 'The reference parameter',
				},
			},
			'method': {
			},
	}
	IpfcFamColTolMinus = {
			'type': 'Classes',
			'parent': {
				'IpfcFamColModelItem': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcFamColTolPlus = {
			'type': 'Classes',
			'parent': {
				'IpfcFamColModelItem': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcFamColTolPlusMinus = {
			'type': 'Classes',
			'parent': {
				'IpfcFamColModelItem': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcFamColUDF = {
			'type': 'Classes',
			'parent': {
				'IpfcFamilyTableColumn': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcFamilyMember = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
				'IpfcSolid': '',
			},
			'properties': {
				'Parent': {
					'return': 'IpfcFamilyMember',
					'info': 'The parent of the member object (the immediate generic model).',
				},
			},
			'method': {
				'AddColumn': {
					'type': 'Function',
					'args': 'Column as IpfcFamilyTableColumn, Values as IpfcParamValues [optional]',
					'return': 'IpfcFamilyTableColumn',
					'info': 'Adds a new column to the family table. ',
				},
				'AddRow': {
					'type': 'Function',
					'args': 'InstanceName as String, Values as IpfcParamValues [optional]',
					'return': 'IpfcFamilyTableRow',
					'info': 'Adds a new row to the family table.',
				},
				'CreateColumn': {
					'type': 'Function',
					'args': 'Type as IpfcFamilyColumnType, Symbol as String',
					'return': 'IpfcFamilyTableColumn',
					'info': 'Creates a new family table column object given a string symbol.',
				},
				'CreateCompModelColumn': {
					'type': 'Function',
					'args': 'Comp as IpfcFeature',
					'return': 'IpfcFamColCompModel',
					'info': 'Creates a component model column that will be added to a family table.',
				},
				'CreateComponentColumn': {
					'type': 'Function',
					'args': 'Comp as IpfcFeature',
					'return': 'IpfcFamColComp',
					'info': 'Creates a single component column in the family table.',
				},
				'CreateDimensionColumn': {
					'type': 'Function',
					'args': 'Dim as IpfcDimension',
					'return': 'IpfcFamColDimension',
					'info': 'Creates a dimension column in the family table.',
				},
				'CreateFeatureColumn': {
					'type': 'Function',
					'args': 'Feat as IpfcFeature',
					'return': 'IpfcFamColFeature',
					'info': 'Creates a feature column in the family table.',
				},
				'CreateGroupColumn': {
					'type': 'Function',
					'args': 'Group as IpfcFeatureGroup',
					'return': 'IpfcFamColGroup',
					'info': 'Creates a group column in the family table.',
				},
				'CreateMergePartColumn': {
					'type': 'Function',
					'args': 'Feat as IpfcFeature',
					'return': 'IpfcFamColMergePart',
					'info': 'Creates a merged-part column in the family table.',
				},
				'CreateParamColumn': {
					'type': 'Function',
					'args': 'Param as IpfcParameter',
					'return': 'IpfcFamColParam',
					'info': 'Creates a parameter column in the family table.',
				},
				'CreateTolMinusColumn': {
					'type': 'Function',
					'args': 'Dim as IpfcDimension',
					'return': 'IpfcFamColTolMinus',
					'info': 'Creates a new family table column representing a dimension tolerance minus value.',
				},
				'CreateTolPlusColumn': {
					'type': 'Function',
					'args': 'Dim as IpfcDimension',
					'return': 'IpfcFamColTolPlus',
					'info': 'Creates a new family table column representing a dimension tolerance plus value.',
				},
				'CreateTolPlusMinusColumn': {
					'type': 'Function',
					'args': 'Dim as IpfcDimension',
					'return': 'IpfcFamColTolPlusMinus',
					'info': 'Creates a new family table column representing a dimension tolerance plus/minus symmetric value.',
				},
				'GetCell': {
					'type': 'Function',
					'args': 'Column as IpfcFamilyTableColumn, Row as IpfcFamilyTableRow',
					'return': 'IpfcParamValue',
					'info': 'Retrieves the value in the specified cell of the family table.',
				},
				'GetCellIsDefault': {
					'type': 'Function',
					'args': 'Column as IpfcFamilyTableColumn, Row as IpfcFamilyTableRow',
					'return': ' as Boolean',
					'info': 'Determines if the value of the item in the specified     cell is the default value.   ',
				},
				'GetColumn': {
					'type': 'Function',
					'args': 'Symbol as String',
					'return': 'IpfcFamilyTableColumn',
					'info': 'Returns a column in the family table, given its string name.',
				},
				'GetImmediateGenericInfo': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcModelDescriptor',
					'info': 'Gets the model descriptor of the immediate generic model.',
				},
				'GetRow': {
					'type': 'Function',
					'args': 'InstanceName as String',
					'return': 'IpfcFamilyTableRow',
					'info': 'Returns the specified row of the family table.',
				},
				'GetTopGenericInfo': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcModelDescriptor',
					'info': 'Gets the model descriptor of the top generic model.',
				},
				'ListColumns': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcFamilyTableColumns',
					'info': 'Lists the columns of the family table.',
				},
				'ListRows': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcFamilyTableRows',
					'info': 'Lists the rows of the family table.',
				},
				'RemoveColumn': {
					'type': 'Sub',
					'args': 'Column as IpfcFamilyTableColumn',
					'return': '',
					'info': 'Removes a specified column from the family table.',
				},
				'RemoveRow': {
					'type': 'Sub',
					'args': 'Row as IpfcFamilyTableRow',
					'return': '',
					'info': 'Removes a specified row from the family table.',
				},
				'SetCell': {
					'type': 'Sub',
					'args': 'Column as IpfcFamilyTableColumn, Row as IpfcFamilyTableRow, Value as IpfcParamValue',
					'return': '',
					'info': 'Sets the value of the specified cell in the family table.',
				},
			},
	}
	IpfcFamilyTableColumn = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
				'IpfcFamColModelItem': '',
				'IpfcFamColParam': '',
				'IpfcFamColIParNote': '',
				'IpfcFamColUDF': '',
				'IpfcFamColGTol': '',
				'IpfcFamColSim': '',
				'IpfcFamColSystemParam': '',
				'IpfcFamColExternalRef': '',
			},
			'properties': {
				'Symbol': {
					'return': 'as String',
					'info': 'The symbol ',
				},
				'Type': {
					'return': 'IpfcFamilyColumnType',
					'info': 'The type of column',
				},
			},
			'method': {
			},
	}
	IpfcFamilyTableRow = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
			},
			'properties': {
				'InstanceName': {
					'return': 'as String',
					'info': 'The name of the instance.',
				},
				'IsExtLocked': {
					'return': 'as Boolean',
					'info': 'If this is true, the instance is locked     by external application.',
				},
				'IsLocked': {
					'return': 'as Boolean',
					'info': 'If this is true, the model is locked for modification. If this is false, the model is unlocked.',
				},
				'IsVerified': {
					'return': 'IpfcFaminstanceVerifyStatus',
					'info': 'Specifies the verification status of the instance.',
				},
			},
			'method': {
				'CreateInstance': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcModel',
					'info': 'Regenerates and adds the instance model to memory.',
				},
				'Erase': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Erases the specified instance model from memory.',
				},
			},
	}
	IpfcFeatIdExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExportInstructions': '',
			},
			'child': {
				'IpfcFeatInfoExportInstructions': '',
				'IpfcMFGCLExportInstructions': '',
			},
			'properties': {
				'FeatId': {
					'return': 'as Long',
					'info': 'The identifier of the feature whose data is being exported.',
				},
			},
			'method': {
			},
	}
	IpfcFeatInfoExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcFeatIdExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcFeatInfoExportInstructions.Create': {
					'type': 'Function',
					'args': 'FeatId as Long',
					'return': 'IpfcFeatInfoExportInstructions',
					'info': 'Creates a new instructions object to export data about a specific feature.',
					'com': 'pfcls.pfcFeatInfoExportInstructions.1',
				},
			},
	}
	IpfcFeature = {
			'type': 'Classes',
			'parent': {
				'IpfcModelItem': '',
			},
			'child': {
				'IpfcComponentFeat': '',
				'IpfcDatumAxisFeat': '',
				'IpfcCurveFeat': '',
				'IpfcCoordSysFeat': '',
				'IpfcDatumPlaneFeat': '',
				'IpfcRoundFeat': '',
				'IpfcDatumPointFeat': '',
			},
			'properties': {
				'FeatSubType': {
					'return': 'as String',
					'info': 'The feature subtype, i.e. "Extrude" or "Revolve" for a protrusion.',
				},
				'FeatType': {
					'return': 'IpfcFeatureType',
					'info': 'The feature type.',
				},
				'FeatTypeName': {
					'return': 'as String',
					'info': 'The feature type expressed as a string.',
				},
				'Group': {
					'return': 'IpfcFeatureGroup',
					'info': 'The group, if the feature is a member of a group.  Otherwise this will be null.',
				},
				'GroupPattern': {
					'return': 'IpfcGroupPattern',
					'info': 'The group pattern, if the feature is a member of a group pattern.  Otherwise this will be null.',
				},
				'IsEmbedded': {
					'return': 'as Boolean',
					'info': 'A Boolean flag that specifies whether the feature    is an embedded datum or not.',
				},
				'IsGroupMember': {
					'return': 'as Boolean',
					'info': 'Retrieves the group status of the specified feature.',
				},
				'IsReadonly': {
					'return': 'as Boolean',
					'info': 'A Boolean flag that specifies whether the feature is read only',
				},
				'IsVisible': {
					'return': 'as Boolean',
					'info': 'A Boolean flag that specifies whether the feature is visible.',
				},
				'Number': {
					'return': 'as Long [readonly, optional]',
					'info': 'The regeneration number of the feature.  If the feature is suppressed or unregenerated, this will be null.',
				},
				'Pattern': {
					'return': 'IpfcFeaturePattern',
					'info': 'The pattern, if the feature is a member of a pattern.  Otherwise this will be null.',
				},
				'Status': {
					'return': 'IpfcFeatureStatus',
					'info': 'The feature status.',
				},
				'VersionStamp': {
					'return': 'as String',
					'info': 'The version stamp',
				},
			},
			'method': {
				'CreateDeleteOp': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDeleteOperation',
					'info': 'Creates a feature delete operation.',
				},
				'CreateReorderAfterOp': {
					'type': 'Function',
					'args': 'AfterFeat as IpfcFeature',
					'return': 'IpfcReorderAfterOperation',
					'info': 'Creates an operation that will reorder the features after the specified feature.',
				},
				'CreateReorderBeforeOp': {
					'type': 'Function',
					'args': 'BeforeFeat as IpfcFeature',
					'return': 'IpfcReorderBeforeOperation',
					'info': 'Creates an operation that will reorder the features before the specified feature.',
				},
				'CreateResumeOp': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcResumeOperation',
					'info': 'Creates a new resume operation object.',
				},
				'CreateSuppressOp': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSuppressOperation',
					'info': 'Creates a new suppress operation object.',
				},
				'ListChildren': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcFeatures',
					'info': 'Lists the children of the feature.',
				},
				'ListParents': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcFeatures',
					'info': 'Lists the parents of the feature.',
				},
				'ListSubItems': {
					'type': 'Function',
					'args': 'Type as IpfcModelItemType [optional]',
					'return': 'IpfcModelItems',
					'info': 'Lists the component model items that make up the feature.',
				},
			},
	}
	IpfcFeatureActionListener = {
			'type': 'Classes',
			'parent': {
				'IpfcActionListener': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'OnAfterCopy': {
					'type': 'Sub',
					'args': 'FromFeat as IpfcFeature, ToFeat as IpfcFeature, Type as IpfcFeatureCopyType',
					'return': '',
					'info': 'This is the listener method called after a successful feature copy.',
				},
				'OnAfterRegen': {
					'type': 'Sub',
					'args': 'Feat as IpfcFeature',
					'return': '',
					'info': 'This is the notification function called after a feature has beenregenerated.',
				},
				'OnAfterSuppress': {
					'type': 'Sub',
					'args': 'Feat as IpfcFeature',
					'return': '',
					'info': 'This is the notification function called after a feature has been suppressed from a solid.',
				},
				'OnBeforeDelete': {
					'type': 'Sub',
					'args': 'Feat as IpfcFeature',
					'return': '',
					'info': 'This is the action listener called before deleting a feature.',
				},
				'OnBeforeParameterCreate': {
					'type': 'Sub',
					'args': 'Owner as IpfcFeature, Name as String, Value as IpfcParamValue',
					'return': '',
					'info': 'This is the notification function called before a parameter is created.    To abort parameter creation throw exception IpfcXCancelProEAction.',
				},
				'OnBeforeParameterDelete': {
					'type': 'Sub',
					'args': 'Param as IpfcParameter',
					'return': '',
					'info': 'This is the listener method called before deleting a parameter.   To abort parameter delete operation throw exception IpfcXCancelProEAction.',
				},
				'OnBeforeParameterModify': {
					'type': 'Sub',
					'args': 'Param as IpfcParameter, Value as IpfcParamValue',
					'return': '',
					'info': 'This is the notification function called before a parameter is modified.   To abort parameter modification throw exception IpfcXCancelProEAction.',
				},
				'OnBeforeRedefine': {
					'type': 'Sub',
					'args': 'Feat as IpfcFeature',
					'return': '',
					'info': 'This is the notification function called before a feature is redefined.',
				},
				'OnBeforeRegen': {
					'type': 'Sub',
					'args': 'Feat as IpfcFeature',
					'return': '',
					'info': 'This is the notification function called before a feature is regenerated.',
				},
				'OnBeforeSuppress': {
					'type': 'Sub',
					'args': 'Feat as IpfcFeature',
					'return': '',
					'info': 'This is the notification function called before a feature is suppressed.',
				},
				'OnRegenFailure': {
					'type': 'Sub',
					'args': 'Feat as IpfcFeature',
					'return': '',
					'info': 'This is the notification function called when a regeneration fails.',
				},
			},
	}
	IpfcFeatureCreateInstructions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'GetType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcFeatureType',
					'info': 'Gets the type of feature to be created by the instructions.',
				},
			},
	}
	IpfcFeatureGroup = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
			},
			'properties': {
				'GroupLeader': {
					'return': 'IpfcFeature',
					'info': 'The group leader Feature',
				},
				'GroupName': {
					'return': 'as String [readonly, optional]',
					'info': 'The name of the group.',
				},
				'IsTableDriven': {
					'return': 'as Boolean',
					'info': 'Reports whether the UDF is table-driven.',
				},
				'Pattern': {
					'return': 'IpfcGroupPattern',
					'info': 'The group pattern, if the group is a member of a pattern.',
				},
				'UDFInstanceName': {
					'return': 'as String [readonly, optional]',
					'info': 'If the group is an instance of a UDF, this is the UDF instance name. Otherwise this will be NULL.',
				},
				'UDFName': {
					'return': 'as String [readonly, optional]',
					'info': 'If the group represents a UDF, this is the UDF name. If the group is not a UDF, this will be NULL. ',
				},
			},
			'method': {
				'CollectUDFDimensions': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDimensions',
					'info': '  ',
				},
				'GetUDFDimensionName': {
					'type': 'Function',
					'args': 'Dim as IpfcDimension',
					'return': ' as String',
					'info': '  ',
				},
				'ListMembers': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcFeatures',
					'info': 'Lists the members of a feature group.',
				},
				'ListUDFDimensions': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcUDFDimensions',
					'info': 'Lists the dimensions of a user defined feature group.',
				},
			},
	}
	IpfcFeatureOperation = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
				'IpfcSuppressOperation': '',
				'IpfcResumeOperation': '',
				'IpfcDeleteOperation': '',
				'IpfcReorderAfterOperation': '',
				'IpfcReorderBeforeOperation': '',
				'IpfcCompModelReplace': '',
			},
			'properties': {
				'OpFeature': {
					'return': 'IpfcFeature',
					'info': 'The feature on which to operate.',
				},
			},
			'method': {
			},
	}
	IpfcFeaturePattern = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
			},
			'properties': {
				'PatternLeader': {
					'return': 'IpfcFeature',
					'info': 'The pattern leader',
				},
			},
			'method': {
				'Delete': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Deletes a feature pattern. ',
				},
				'ListMembers': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcFeatures',
					'info': 'Lists the members of the feature pattern.',
				},
			},
	}
	IpfcFeaturePlacement = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcFIATExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcGeomExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcFIATExportInstructions.Create': {
					'type': 'Function',
					'args': 'Flags as IpfcGeomExportFlags',
					'return': 'IpfcFIATExportInstructions',
					'info': 'Creates a new instructions object used to export a part or assembly in FIAT format. ',
					'com': 'pfcls.pfcFIATExportInstructions.1',
				},
			},
	}
	IpfcFileOpenOptions = {
			'type': 'Classes',
			'parent': {
				'IpfcFileUIOptions': '',
			},
			'child': {
			},
			'properties': {
				'FilterString': {
					'return': 'as String',
					'info': 'The filter string used for file extensions. The extensions must be specified with wildcards and seperated by commas; for example, "*.prt,*.txt".  ',
				},
				'PreselectedItem': {
					'return': 'as String [optional]',
					'info': 'A preselected file or item.  ',
				},
			},
			'method': {
				'CCpfcFileOpenOptions.Create': {
					'type': 'Function',
					'args': 'FilterString as String',
					'return': 'IpfcFileOpenOptions',
					'info': 'Creates a class object for file open dialog options.',
					'com': 'pfcls.pfcFileOpenOptions.1',
				},
			},
	}
	IpfcFileOpenRegisterListener = {
			'type': 'Classes',
			'parent': {
				'IpfcActionListener': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'FileOpenAccess': {
					'type': 'Function',
					'args': 'FileType as String',
					'return': ' as Boolean',
					'info': 'The callback function to decide if the file type should be   selectable in File Open dialog or not.',
				},
				'OnFileOpenRegister': {
					'type': 'Sub',
					'args': 'FileType as String, FilePath as String',
					'return': '',
					'info': 'This callback function is called upon pressing Open button   for the new registered type.',
				},
			},
	}
	IpfcFileOpenRegisterOptions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'FileDescription': {
					'return': 'as String',
					'info': 'Short description of the file type to be opened.',
				},
				'FileType': {
					'return': 'as String',
					'info': 'The file type to be opened.',
				},
			},
			'method': {
				'CCpfcFileOpenRegisterOptions.Create': {
					'type': 'Function',
					'args': 'FileType as String, FileDescription as String',
					'return': 'IpfcFileOpenRegisterOptions',
					'info': 'Creates an object of class FileOpenRegisterOptions',
					'com': 'pfcls.pfcFileOpenRegisterOptions.1',
				},
			},
	}
	IpfcFileOpenShortcut = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'ShortcutName': {
					'return': 'as String',
					'info': 'String for shortcut name.  ',
				},
				'ShortcutPath': {
					'return': 'as String',
					'info': 'String for shortcut path.   ',
				},
			},
			'method': {
				'CCpfcFileOpenShortcut.Create': {
					'type': 'Function',
					'args': 'ShorcutPath as String, ShortcutName as String',
					'return': 'IpfcFileOpenShortcut',
					'info': 'Creates a class object for file open shortcuts dialog.   ',
					'com': 'pfcls.pfcFileOpenShortcut.1',
				},
			},
	}
	IpfcFileSaveOptions = {
			'type': 'Classes',
			'parent': {
				'IpfcFileUIOptions': '',
			},
			'child': {
			},
			'properties': {
				'FilterString': {
					'return': 'as String',
					'info': 'The filter string used for file extensions.  ',
				},
				'PreselectedItem': {
					'return': 'as String [optional]',
					'info': 'A preselected file or item.  ',
				},
			},
			'method': {
				'CCpfcFileSaveOptions.Create': {
					'type': 'Function',
					'args': 'FilterString as String',
					'return': 'IpfcFileSaveOptions',
					'info': 'Creates a class object for file save dialog options.',
					'com': 'pfcls.pfcFileSaveOptions.1',
				},
			},
	}
	IpfcFileSaveRegisterListener = {
			'type': 'Classes',
			'parent': {
				'IpfcActionListener': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'FileSaveAccess': {
					'type': 'Function',
					'args': 'FileType as String, Model as IpfcModel',
					'return': ' as Boolean',
					'info': "The callback function to decide if the file type should be   selectable in File 'Save a Copy' dialog or not.",
				},
				'OnFileSaveRegister': {
					'type': 'Sub',
					'args': 'FileType as String, Model as IpfcModel, FilePath as String',
					'return': '',
					'info': "This callback function is called upon pressing OK button on  the File 'Save a Copy' dialog for the new registered type.",
				},
			},
	}
	IpfcFileSaveRegisterOptions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'FileDescription': {
					'return': 'as String',
					'info': 'Short description of the file type to be saved.',
				},
				'FileType': {
					'return': 'as String',
					'info': 'The file type to be saved.',
				},
			},
			'method': {
				'CCpfcFileSaveRegisterOptions.Create': {
					'type': 'Function',
					'args': 'FileType as String, FileDescription as String',
					'return': 'IpfcFileSaveRegisterOptions',
					'info': 'Creates an object of class FileSaveRegisterOptions',
					'com': 'pfcls.pfcFileSaveRegisterOptions.1',
				},
			},
	}
	IpfcFileUIOptions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcFileOpenOptions': '',
				'IpfcFileSaveOptions': '',
				'IpfcDirectorySelectionOptions': '',
			},
			'properties': {
				'DefaultPath': {
					'return': 'as String [optional]',
					'info': 'The default path used to start browsing.  ',
				},
				'DialogLabel': {
					'return': 'as String [optional]',
					'info': 'The label used for the dialog box.  ',
				},
				'Shortcuts': {
					'return': 'IpfcFileOpenShortcuts',
					'info': 'An array to store the different file shortcuts.  ',
				},
			},
			'method': {
			},
	}
	IpfcFilletSurface = {
			'type': 'Classes',
			'parent': {
				'IpfcSurface': '',
			},
			'child': {
			},
			'properties': {
				'CenterProfile': {
					'return': 'IpfcSplineDescriptor',
					'info': 'The geometry of the curve running along the centers of the fillet arcs',
				},
				'TangentProfile': {
					'return': 'IpfcSplineDescriptor',
					'info': 'The geometry of the unit tangents to the axis of the filler arcs',
				},
				'U0Profile': {
					'return': 'IpfcSplineDescriptor',
					'info': 'The geometry of the curve running along the u=0 boundary',
				},
			},
			'method': {
			},
	}
	IpfcFilletSurfaceDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcSurfaceDescriptor': '',
			},
			'child': {
			},
			'properties': {
				'CenterProfile': {
					'return': 'IpfcSplineDescriptor',
					'info': 'The geometry of the curve running along the centers of the fillet arcs',
				},
				'TangentProfile': {
					'return': 'IpfcSplineDescriptor',
					'info': 'The geometry of the unit tangents to the axis of the filler arcs',
				},
				'U0Profile': {
					'return': 'IpfcSplineDescriptor',
					'info': 'The geometry of the curve running along the u=0 boundary',
				},
			},
			'method': {
				'CCpfcFilletSurfaceDescriptor.Create': {
					'type': 'Function',
					'args': 'Extents as IpfcSurfaceExtents, Orientation as IpfcSurfaceOrientation, U0Profile as IpfcSplineDescriptor, CenterProfile as IpfcSplineDescriptor, TangentProfile as IpfcSplineDescriptor',
					'return': 'IpfcFilletSurfaceDescriptor',
					'info': 'This method creates a new FilletSurfaceDescriptor object.',
					'com': 'pfcls.pfcFilletSurfaceDescriptor.1',
				},
			},
	}
	IpfcFolderAssignment = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Folder': {
					'return': 'as String',
					'info': 'Specifies the folder location on the server in which a certain object will be checked in ',
				},
				'ModelName': {
					'return': 'as String',
					'info': 'Specifies the name of the  model to checkin. ',
				},
			},
			'method': {
				'CCpfcFolderAssignment.Create': {
					'type': 'Function',
					'args': 'Folder as String, ModelName as String',
					'return': 'IpfcFolderAssignment',
					'info': 'Creates a new IpfcFolderAssignment object.',
					'com': 'pfcls.pfcFolderAssignment.1',
				},
			},
	}
	IpfcFont = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Id': {
					'return': 'as Long',
					'info': 'The Creo Parametric TOOLKIT font Id',
				},
				'Name': {
					'return': 'as String',
					'info': 'The Creo Parametric font name',
				},
			},
			'method': {
			},
	}
	IpfcForeignSurface = {
			'type': 'Classes',
			'parent': {
				'IpfcTransformedSurface': '',
			},
			'child': {
			},
			'properties': {
				'ForeignID': {
					'return': 'as Long',
					'info': 'The integer identifier of the foreign surface',
				},
			},
			'method': {
			},
	}
	IpfcForeignSurfaceDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcTransformedSurfaceDescriptor': '',
			},
			'child': {
			},
			'properties': {
				'ForeignID': {
					'return': 'as Long',
					'info': 'The integer identifier of the foreign surface',
				},
			},
			'method': {
				'CCpfcForeignSurfaceDescriptor.Create': {
					'type': 'Function',
					'args': 'Extents as IpfcSurfaceExtents, Orientation as IpfcSurfaceOrientation, Origin as IpfcTransform3D, ForeignID as Long',
					'return': 'IpfcForeignSurfaceDescriptor',
					'info': 'This method creates a new ForeignSurfaceDescriptor object.',
					'com': 'pfcls.pfcForeignSurfaceDescriptor.1',
				},
			},
	}
	IpfcFreeAttachment = {
			'type': 'Classes',
			'parent': {
				'IpfcAttachment': '',
			},
			'child': {
			},
			'properties': {
				'AttachmentPoint': {
					'return': 'IpfcPoint3D',
					'info': 'The attachment point, in screen coordinates.',
				},
				'View': {
					'return': 'IpfcView2D',
					'info': 'The drawing model view (the attachment point will move relative to this view).  If null, the attachment will not be with respect to a view.',
				},
			},
			'method': {
				'CCpfcFreeAttachment.Create': {
					'type': 'Function',
					'args': 'inAttachmentPoint as IpfcPoint3D',
					'return': 'IpfcFreeAttachment',
					'info': 'Creates a data object used for representing a free attachment.  A free attachment is relative to a fixed location, or relative to a fixed location with respect to a drawing view.',
					'com': 'pfcls.pfcFreeAttachment.1',
				},
			},
	}
	IpfcFunctionReturn = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'FunctionReturn': {
					'return': 'as Long',
					'info': 'The function return value.',
				},
				'OutputArguments': {
					'return': 'IpfcArguments',
					'info': 'Sequence of output arguments from the function call.',
				},
			},
			'method': {
			},
	}
	IpfcGeneralDatumPoint = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'DimConstraints': {
					'return': 'IpfcDatumPointDimensionConstraints',
					'info': 'This specifies the dimension constraints of the general datum point.',
				},
				'Name': {
					'return': 'as String',
					'info': 'This specifies the name of the general datum point.',
				},
				'PlaceConstraints': {
					'return': 'IpfcDatumPointPlacementConstraints',
					'info': 'This specifies the placement constraints of the general datum point.',
				},
			},
			'method': {
			},
	}
	IpfcGeneralViewCreateInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcView2DCreateInstructions': '',
			},
			'child': {
			},
			'properties': {
				'Exploded': {
					'return': 'as Boolean [optional]',
					'info': 'true if the view should be an exploded view, null or false otherwise.',
				},
				'Location': {
					'return': 'IpfcPoint3D',
					'info': 'The location on the sheet where the view should be placed.',
				},
				'Orientation': {
					'return': 'IpfcTransform3D',
					'info': 'The orientation of the model in the view.',
				},
				'Scale': {
					'return': 'as Double [optional]',
					'info': 'The scale of the view (null to use the default scale).',
				},
				'SheetNumber': {
					'return': 'as Long',
					'info': 'The sheet number in which to create the view.',
				},
				'ViewModel': {
					'return': 'IpfcModel',
					'info': 'The solid model to display in the view.',
				},
				'ViewScale': {
					'return': 'as Double [optional]',
					'info': 'The scale of the view (null to use the default scale).',
				},
			},
			'method': {
				'CCpfcGeneralViewCreateInstructions.Create': {
					'type': 'Function',
					'args': 'ViewModel as IpfcModel, SheetNumber as Long, Location as IpfcPoint3D, Orientation as IpfcTransform3D',
					'return': 'IpfcGeneralViewCreateInstructions',
					'info': 'Creates an instructions data object used for creating general drawing views.',
					'com': 'pfcls.pfcGeneralViewCreateInstructions.1',
				},
			},
	}
	IpfcGeomCurve = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
				'IpfcEdge': '',
				'IpfcCurve': '',
			},
			'properties': {
				'IsVisible': {
					'return': 'as Boolean',
					'info': 'true if the geometry is visible and active, false if it is invisible and inactive.  Inactive geometry may not have all geometric properties defined.',
				},
			},
			'method': {
				'Eval3DData': {
					'type': 'Function',
					'args': 'Param as Double',
					'return': 'IpfcCurveXYZData',
					'info': 'Returns a IpfcCurveXYZData object with information on the point represented by input parameter t.',
				},
				'EvalFromLength': {
					'type': 'Function',
					'args': 'StartParameter as Double, Length as Double',
					'return': 'IpfcCurveXYZData',
					'info': 'Returns a CURVEXYZData object with information on the point that is a specified distance from the starting point as specified by input parameter t.',
				},
				'EvalLength': {
					'type': 'Function',
					'args': '',
					'return': ' as Double',
					'info': 'Finds the length of the specified edge.',
				},
				'EvalLengthBetween': {
					'type': 'Function',
					'args': 'Param1 as Double, Param2 as Double',
					'return': ' as Double',
					'info': 'Finds the length of the specified curve between two givenparameters.',
				},
				'EvalParameter': {
					'type': 'Function',
					'args': 'Point as IpfcPoint3D',
					'return': ' as Double',
					'info': 'Finds the corresponding normalized parameter on the curve, given the XYZpoint.',
				},
				'GetCurveDescriptor': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcCurveDescriptor',
					'info': 'This method returns a data object containing the geometry of the edge or curve.',
				},
				'GetFeature': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcFeature',
					'info': 'Returns the feature which contains the geometry.',
				},
				'GetNURBSRepresentation': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcBSplineDescriptor',
					'info': 'This method returns the geometry of the edge or curve as a non-uniform rational B-spline curve.',
				},
			},
	}
	IpfcGeometryFlags = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'AsQuilts': {
					'return': 'as Boolean',
					'info': 'Whether or not to export as a set of quilts.',
				},
				'AsSolids': {
					'return': 'as Boolean',
					'info': 'Whether or not to export as solid objects.',
				},
				'AsSurfaces': {
					'return': 'as Boolean',
					'info': 'Whether or not to export as a set of surfaces.',
				},
				'AsWireframe': {
					'return': 'as Boolean',
					'info': 'Whether or not export as wireframe entities.',
				},
			},
			'method': {
				'CCpfcGeometryFlags.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcGeometryFlags',
					'info': 'Creates a new geometry flags object used when exporting a model to a 3D representation.',
					'com': 'pfcls.pfcGeometryFlags.1',
				},
			},
	}
	IpfcGeomExportFlags = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Bezier': {
					'return': 'as Boolean',
					'info': 'Specifies how B-splines are exported. If true, exports all B-splines as Bezier surfaces. If false, spline surfaces are exported. (Bezier surfaces are created temporarily to increase order and decrease the number of patches in the resulting spline surface.)This value overrides the intf_out_as_ibezier option in the Creo Parametric configuration file, if it exists.',
				},
				'ExtendSRF': {
					'return': 'as Boolean',
					'info': 'Specifies whether or not surfaces are extended.If true, the surfaces are extended. If false, the surfaces are exported as they are, with no extending.This value overrides the intf3d_out_extend_surface option in the Creo Parametric configuration file, if it exists.',
				},
			},
			'method': {
				'CCpfcGeomExportFlags.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcGeomExportFlags',
					'info': 'Creates a IpfcGeomExportFlags object that stores extend-surface and Bezier options for use when exporting geometric information from a model.',
					'com': 'pfcls.pfcGeomExportFlags.1',
				},
			},
	}
	IpfcGeomExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExportInstructions': '',
			},
			'child': {
				'IpfcIGES3DExportInstructions': '',
				'IpfcSTEPExportInstructions': '',
				'IpfcVDAExportInstructions': '',
				'IpfcFIATExportInstructions': '',
			},
			'properties': {
				'Flags': {
					'return': 'IpfcGeomExportFlags',
					'info': 'A IpfcGeomExportFlags object that stores extend-surface and Bezier options for use when exporting geometric information from a model',
				},
			},
			'method': {
			},
	}
	IpfcGlobalEvaluator = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
			},
			'properties': {
				'Assem': {
					'return': 'IpfcAssembly',
					'info': 'The top-level assembly to be checked for interference.',
				},
			},
			'method': {
				'ComputeGlobalInterference': {
					'type': 'Function',
					'args': 'SolidOnly as Boolean',
					'return': 'IpfcGlobalInterferences',
					'info': 'Computes the global interference on the assembly.',
				},
			},
	}
	IpfcGlobalInterference = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
			},
			'properties': {
				'SelParts': {
					'return': 'IpfcSelectionPair',
					'info': 'Selection pair that represents the interfering parts.',
				},
				'Volume': {
					'return': 'IpfcInterferenceVolume',
					'info': 'The InterferenceVolume object containing the boundaries of the interference.',
				},
			},
			'method': {
			},
	}
	IpfcGroupPattern = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
			},
			'properties': {
				'GroupPatternLeader': {
					'return': 'IpfcFeatureGroup',
					'info': 'The feature group that is the group pattern leader.',
				},
			},
			'method': {
				'ListFeatMembers': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcFeatures',
					'info': 'Lists the features that are members of the group pattern.',
				},
				'ListGroupMembers': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcFeatureGroups',
					'info': 'Lists groups that are members of the group pattern.',
				},
			},
	}
	IpfcIGES3DExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcGeomExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcIGES3DExportInstructions.Create': {
					'type': 'Function',
					'args': 'Flags as IpfcGeomExportFlags',
					'return': 'IpfcIGES3DExportInstructions',
					'info': 'Creates a new instructions object used to export a part or assembly in IGES format.',
					'com': 'pfcls.pfcIGES3DExportInstructions.1',
				},
			},
	}
	IpfcIGES3DNewExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExport3DInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcIGES3DNewExportInstructions.Create': {
					'type': 'Function',
					'args': 'inConfiguration as IpfcAssemblyConfiguration, inGeometry as IpfcGeometryFlags',
					'return': 'IpfcIGES3DNewExportInstructions',
					'info': 'Creates a new instructions object used to export a 3D model to IGES format.',
					'com': 'pfcls.pfcIGES3DNewExportInstructions.1',
				},
			},
	}
	IpfcIGESFileExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcIGESFileExportInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcIGESFileExportInstructions',
					'info': 'Creates a new instructions object used to export a drawing in IGES format.',
					'com': 'pfcls.pfcIGESFileExportInstructions.1',
				},
			},
	}
	IpfcIGESImport2DInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcImport2DInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcIGESImport2DInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcIGESImport2DInstructions',
					'info': 'Creates a new data object used for importing a 2-D IGES file into Creo Parametric.',
					'com': 'pfcls.pfcIGESImport2DInstructions.1',
				},
			},
	}
	IpfcIGESSectionImportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcImportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcIGESSectionImportInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcIGESSectionImportInstructions',
					'info': 'Creates a new instructions object used to import (read) into a section model from an IGES format file.',
					'com': 'pfcls.pfcIGESSectionImportInstructions.1',
				},
			},
	}
	IpfcImport2DInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcImportInstructions': '',
			},
			'child': {
				'IpfcSTEPImport2DInstructions': '',
				'IpfcIGESImport2DInstructions': '',
				'IpfcDXFImport2DInstructions': '',
				'IpfcDWGImport2DInstructions': '',
			},
			'properties': {
				'FitToLeftCorner': {
					'return': 'as Boolean [optional]',
					'info': '  ',
				},
				'Import2DViews': {
					'return': 'as Boolean [optional]',
					'info': '  ',
				},
				'ScaleToFit': {
					'return': 'as Boolean [optional]',
					'info': '  ',
				},
			},
			'method': {
			},
	}
	IpfcImportedLayer = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
			},
			'properties': {
				'CurveCount': {
					'return': 'as Long',
					'info': 'The number of curves found in the layer.',
				},
				'Name': {
					'return': 'as String',
					'info': 'The name of the layer as stored in the external geometry file.',
				},
				'SurfaceCount': {
					'return': 'as Long',
					'info': 'The number of surfaces found in the layer.',
				},
				'TrimmedSurfaceCount': {
					'return': 'as Long',
					'info': 'The number of trimmed surfaces found in the layer.',
				},
			},
			'method': {
				'SetAction': {
					'type': 'Sub',
					'args': 'Action as IpfcImportAction',
					'return': '',
					'info': 'Sets the behavior for Creo Parametric to use when importing this layer.',
				},
				'SetNewName': {
					'type': 'Sub',
					'args': 'NewName as String',
					'return': '',
					'info': 'Sets the new name for the imported layer.',
				},
			},
	}
	IpfcImportFeatAttr = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'JoinSurfs': {
					'return': 'as Boolean',
					'info': 'This bool attribute indicates if the surfaces whih do not quite meet will be joined or not.',
				},
				'MakeSolid': {
					'return': 'as Boolean',
					'info': 'This bool attribute indicates if solids will be made from every closed quilt.',
				},
				'Operation': {
					'return': 'IpfcOperationType',
					'info': 'Operation to apply when making solid.',
				},
			},
			'method': {
				'CCpfcImportFeatAttr.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcImportFeatAttr',
					'info': 'Creates a new import feature attributes object - the attributes for creation of the new import feature.',
					'com': 'pfcls.pfcImportFeatAttr.1',
				},
			},
	}
	IpfcImportInstructions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcImport2DInstructions': '',
				'IpfcRelationImportInstructions': '',
				'IpfcIGESSectionImportInstructions': '',
				'IpfcProgramImportInstructions': '',
				'IpfcConfigImportInstructions': '',
				'IpfcDWGSetupImportInstructions': '',
				'IpfcSpoolImportInstructions': '',
				'IpfcConnectorParamsImportInstructions': '',
				'IpfcCableParamsImportInstructions': '',
				'IpfcASSEMTreeCFGImportInstructions': '',
				'IpfcWireListImportInstructions': '',
			},
			'properties': {
			},
			'method': {
				'GetFileType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcImportType',
					'info': 'Returns a IpfcImportType instance that indicates the type of data the object imports when passed as an argument to the IpfcModel.Import() method.',
				},
			},
	}
	IpfcInclusionFlags = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'IncludeBlanked': {
					'return': 'as Boolean',
					'info': 'true to export geometry on blanked layers; false to skip export of this geometry.',
				},
				'IncludeDatums': {
					'return': 'as Boolean',
					'info': 'true to export datums; false to skip export of datums.',
				},
				'IncludeFaceted': {
					'return': 'as Boolean',
					'info': '  ',
				},
			},
			'method': {
				'CCpfcInclusionFlags.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcInclusionFlags',
					'info': 'Creates a new  object used to indicate whether geometry should be included during export of a model.',
					'com': 'pfcls.pfcInclusionFlags.1',
				},
			},
	}
	IpfcIntegerOId = {
			'type': 'Classes',
			'parent': {
				'IpfcOId': '',
			},
			'child': {
				'IpfcModelItemOId': '',
				'IpfcWindowOId': '',
			},
			'properties': {
				'Id': {
					'return': 'as Long',
					'info': 'The integer identifier',
				},
			},
			'method': {
			},
	}
	IpfcInterferenceVolume = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
			},
			'properties': {
				'Boundaries': {
					'return': 'IpfcSurfaceDescriptors',
					'info': 'The surface geometry surrounding the interference volume.',
				},
			},
			'method': {
				'ComputeVolume': {
					'type': 'Function',
					'args': '',
					'return': ' as Double',
					'info': 'Enables the user to compute the total volume of the specified array of surfaces which makes up the solid interference.',
				},
				'Highlight': {
					'type': 'Sub',
					'args': 'Color as IpfcStdColor',
					'return': '',
					'info': 'Enables the user to highlight the boundaries of the volume of interference.',
				},
			},
	}
	IpfcIntfACIS = {
			'type': 'Classes',
			'parent': {
				'IpfcIntfDataSource': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcIntfACIS.Create': {
					'type': 'Function',
					'args': 'FileName as String',
					'return': 'IpfcIntfACIS',
					'info': 'Creates a new object representing the ACIS file from which to create an import feature.',
					'com': 'pfcls.pfcIntfACIS.1',
				},
			},
	}
	IpfcIntfAI = {
			'type': 'Classes',
			'parent': {
				'IpfcIntfDataSource': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcIntfAI.Create': {
					'type': 'Function',
					'args': 'FileName as String',
					'return': 'IpfcIntfAI',
					'info': '  ',
					'com': 'pfcls.pfcIntfAI.1',
				},
			},
	}
	IpfcIntfCatiaCGR = {
			'type': 'Classes',
			'parent': {
				'IpfcIntfDataSource': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcIntfCatiaCGR.Create': {
					'type': 'Function',
					'args': 'FileName as String',
					'return': 'IpfcIntfCatiaCGR',
					'info': 'Creates a new object representing the CatiaCGR file from which to create an import feature.',
					'com': 'pfcls.pfcIntfCatiaCGR.1',
				},
			},
	}
	IpfcIntfCatiaPart = {
			'type': 'Classes',
			'parent': {
				'IpfcIntfDataSource': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcIntfCatiaPart.Create': {
					'type': 'Function',
					'args': 'FileName as String',
					'return': 'IpfcIntfCatiaPart',
					'info': 'Creates a new object representing the CatiaPart file from which to create an import feature.',
					'com': 'pfcls.pfcIntfCatiaPart.1',
				},
			},
	}
	IpfcIntfCatiaProduct = {
			'type': 'Classes',
			'parent': {
				'IpfcIntfDataSource': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcIntfCatiaProduct.Create': {
					'type': 'Function',
					'args': 'FileName as String',
					'return': 'IpfcIntfCatiaProduct',
					'info': '  ',
					'com': 'pfcls.pfcIntfCatiaProduct.1',
				},
			},
	}
	IpfcIntfCDRS = {
			'type': 'Classes',
			'parent': {
				'IpfcIntfDataSource': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcIntfCDRS.Create': {
					'type': 'Function',
					'args': 'FileName as String',
					'return': 'IpfcIntfCDRS',
					'info': 'Creates a new object representing the CDRS file from which to create an import feature.',
					'com': 'pfcls.pfcIntfCDRS.1',
				},
			},
	}
	IpfcIntfDataSource = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcIntfNeutral': '',
				'IpfcIntfNeutralFile': '',
				'IpfcIntfIges': '',
				'IpfcIntfStep': '',
				'IpfcIntfVDA': '',
				'IpfcIntfICEM': '',
				'IpfcIntfACIS': '',
				'IpfcIntfDXF': '',
				'IpfcIntfCDRS': '',
				'IpfcIntfSTL': '',
				'IpfcIntfVRML': '',
				'IpfcIntfParaSolid': '',
				'IpfcIntfAI': '',
				'IpfcIntfCatiaPart': '',
				'IpfcIntfUG': '',
				'IpfcIntfProductView': '',
				'IpfcIntfCatiaProduct': '',
				'IpfcIntfCatiaCGR': '',
				'IpfcIntfJT': '',
			},
			'properties': {
				'FileName': {
					'return': 'as String',
					'info': 'The name of the imported feature file.',
				},
			},
			'method': {
				'GetType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcIntfType',
					'info': 'Returns a IpfcIntfType instance that indicates the type of the source of data from which the import feature will be created.',
				},
			},
	}
	IpfcIntfDXF = {
			'type': 'Classes',
			'parent': {
				'IpfcIntfDataSource': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcIntfDXF.Create': {
					'type': 'Function',
					'args': 'FileName as String',
					'return': 'IpfcIntfDXF',
					'info': 'Creates a new object representing the DXF file from which to create an import feature.',
					'com': 'pfcls.pfcIntfDXF.1',
				},
			},
	}
	IpfcIntfICEM = {
			'type': 'Classes',
			'parent': {
				'IpfcIntfDataSource': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcIntfICEM.Create': {
					'type': 'Function',
					'args': 'FileName as String',
					'return': 'IpfcIntfICEM',
					'info': 'Creates a new object representing the ICEM file from which to create an import feature.',
					'com': 'pfcls.pfcIntfICEM.1',
				},
			},
	}
	IpfcIntfIges = {
			'type': 'Classes',
			'parent': {
				'IpfcIntfDataSource': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcIntfIges.Create': {
					'type': 'Function',
					'args': 'FileName as String',
					'return': 'IpfcIntfIges',
					'info': 'Creates a new object representing the IGES files from which to create an import feature.',
					'com': 'pfcls.pfcIntfIges.1',
				},
			},
	}
	IpfcIntfJT = {
			'type': 'Classes',
			'parent': {
				'IpfcIntfDataSource': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcIntfJT.Create': {
					'type': 'Function',
					'args': 'FileName as String',
					'return': 'IpfcIntfJT',
					'info': 'Creates a new object representing the JT file from which to create an import feature.',
					'com': 'pfcls.pfcIntfJT.1',
				},
			},
	}
	IpfcIntfNeutral = {
			'type': 'Classes',
			'parent': {
				'IpfcIntfDataSource': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcIntfNeutral.Create': {
					'type': 'Function',
					'args': 'FileName as String',
					'return': 'IpfcIntfNeutral',
					'info': 'Creates a new object representing the source of data from which to create an import feature.',
					'com': 'pfcls.pfcIntfNeutral.1',
				},
			},
	}
	IpfcIntfNeutralFile = {
			'type': 'Classes',
			'parent': {
				'IpfcIntfDataSource': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcIntfNeutralFile.Create': {
					'type': 'Function',
					'args': 'FileName as String',
					'return': 'IpfcIntfNeutralFile',
					'info': 'Creates a new object representing a neutral file from which to create an import feature.',
					'com': 'pfcls.pfcIntfNeutralFile.1',
				},
			},
	}
	IpfcIntfParaSolid = {
			'type': 'Classes',
			'parent': {
				'IpfcIntfDataSource': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcIntfParaSolid.Create': {
					'type': 'Function',
					'args': 'FileName as String',
					'return': 'IpfcIntfParaSolid',
					'info': 'Creates a new object representing the ParaSolid file from which to create an import feature.',
					'com': 'pfcls.pfcIntfParaSolid.1',
				},
			},
	}
	IpfcIntfProductView = {
			'type': 'Classes',
			'parent': {
				'IpfcIntfDataSource': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcIntfProductView.Create': {
					'type': 'Function',
					'args': 'FileName as String',
					'return': 'IpfcIntfProductView',
					'info': '  ',
					'com': 'pfcls.pfcIntfProductView.1',
				},
			},
	}
	IpfcIntfStep = {
			'type': 'Classes',
			'parent': {
				'IpfcIntfDataSource': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcIntfStep.Create': {
					'type': 'Function',
					'args': 'FileName as String',
					'return': 'IpfcIntfStep',
					'info': 'Creates a new object representing the STEP file from which to create an import feature.',
					'com': 'pfcls.pfcIntfStep.1',
				},
			},
	}
	IpfcIntfSTL = {
			'type': 'Classes',
			'parent': {
				'IpfcIntfDataSource': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcIntfSTL.Create': {
					'type': 'Function',
					'args': 'FileName as String',
					'return': 'IpfcIntfSTL',
					'info': 'Creates a new object representing the STL file from which to create an import feature.',
					'com': 'pfcls.pfcIntfSTL.1',
				},
			},
	}
	IpfcIntfUG = {
			'type': 'Classes',
			'parent': {
				'IpfcIntfDataSource': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcIntfUG.Create': {
					'type': 'Function',
					'args': 'FileName as String',
					'return': 'IpfcIntfUG',
					'info': '  ',
					'com': 'pfcls.pfcIntfUG.1',
				},
			},
	}
	IpfcIntfVDA = {
			'type': 'Classes',
			'parent': {
				'IpfcIntfDataSource': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcIntfVDA.Create': {
					'type': 'Function',
					'args': 'FileName as String',
					'return': 'IpfcIntfVDA',
					'info': 'Creates a new object representing the VDA file from which to create an import feature.',
					'com': 'pfcls.pfcIntfVDA.1',
				},
			},
	}
	IpfcIntfVRML = {
			'type': 'Classes',
			'parent': {
				'IpfcIntfDataSource': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcIntfVRML.Create': {
					'type': 'Function',
					'args': 'FileName as String',
					'return': 'IpfcIntfVRML',
					'info': '  ',
					'com': 'pfcls.pfcIntfVRML.1',
				},
			},
	}
	IpfcInventorExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcCoordSysExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcInventorExportInstructions.Create': {
					'type': 'Function',
					'args': 'CsysName as String [optional]',
					'return': 'IpfcInventorExportInstructions',
					'info': 'Creates a new instructions object used to export a part or assembly in Inventor format.',
					'com': 'pfcls.pfcInventorExportInstructions.1',
				},
			},
	}
	IpfcJLinkApplication = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'ExecuteTask': {
					'type': 'Function',
					'args': 'TaskId as String, InputArguments as IpfcArguments',
					'return': 'IpfcArguments',
					'info': 'Call a method in a J-Link application, with user-specified arguments.',
				},
				'IsActive': {
					'type': 'Function',
					'args': '',
					'return': ' as Boolean',
					'info': 'Identifies if the application is currently running.',
				},
				'Stop': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': "Stop a previously loaded J-Link application. The application's stop method will be called.",
				},
			},
	}
	IpfcJPEGImageExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcRasterImageExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcJPEGImageExportInstructions.Create': {
					'type': 'Function',
					'args': 'ImageWidth as Double, ImageHeight as Double',
					'return': 'IpfcJPEGImageExportInstructions',
					'info': 'Creates a new instructions object used to export a JPEG image.',
					'com': 'pfcls.pfcJPEGImageExportInstructions.1',
				},
			},
	}
	IpfcJT3DExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExport3DInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcJT3DExportInstructions.Create': {
					'type': 'Function',
					'args': 'inConfiguration as IpfcAssemblyConfiguration, inGeometry as IpfcGeometryFlags',
					'return': 'IpfcJT3DExportInstructions',
					'info': 'Creates a new instructions object used to export a solid model to JT format.',
					'com': 'pfcls.pfcJT3DExportInstructions.1',
				},
			},
	}
	IpfcLayer = {
			'type': 'Classes',
			'parent': {
				'IpfcModelItem': '',
			},
			'child': {
			},
			'properties': {
				'Status': {
					'return': 'IpfcDisplayStatus',
					'info': 'The display status',
				},
			},
			'method': {
				'AddItem': {
					'type': 'Sub',
					'args': 'Item as IpfcModelItem',
					'return': '',
					'info': 'Adds the specified item to the layer.',
				},
				'CountUnsupportedItems': {
					'type': 'Function',
					'args': '',
					'return': ' as Long',
					'info': '  ',
				},
				'Delete': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Deletes the layer.',
				},
				'HasUnsupportedItems': {
					'type': 'Function',
					'args': '',
					'return': ' as Boolean',
					'info': 'Returns true if the layer contains one or more item types   not supported as IpfcModelItem objects in   PFC.  Returns false otherwise. ',
				},
				'ListItems': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcModelItems',
					'info': 'This method returns only items which are supported as    IpfcModelItem objects in PFC. To identify    if the layer has any items not supported in PFC, use    IpfcLayer.HasUnsupportedItems().',
				},
				'RemoveItem': {
					'type': 'Sub',
					'args': 'Item as IpfcModelItem',
					'return': '',
					'info': 'Removes the specified item from the layer.',
				},
			},
	}
	IpfcLayerExportOptions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'LayerSetupFile': {
					'return': 'as String [optional]',
					'info': 'Full path to the layer setup file, which contains mappings from the Creo Parametric layer names to layer names or ids that should be used in the exported file.',
				},
				'UseAutoId': {
					'return': 'as Boolean',
					'info': 'true to automatically assign unique integer ids to each layer, false to maintain the Creo Parametric layer names.',
				},
			},
			'method': {
				'CCpfcLayerExportOptions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcLayerExportOptions',
					'info': 'Creates a new  object used to indicate how layers should be treated during export of a model.',
					'com': 'pfcls.pfcLayerExportOptions.1',
				},
			},
	}
	IpfcLayerImportFilter = {
			'type': 'Classes',
			'parent': {
				'IpfcActionListener': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'OnLayerImport': {
					'type': 'Sub',
					'args': 'LayerObject as IpfcImportedLayer',
					'return': '',
					'info': 'This listener method will be called for each layer imported from the external geometry file.  If this method throws a IpfcXCancelProEAction exception then the filter will not be called for remaining layers.',
				},
			},
	}
	IpfcLayout = {
			'type': 'Classes',
			'parent': {
				'IpfcModel2D': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcLeaderAttachment = {
			'type': 'Classes',
			'parent': {
				'IpfcDetailLeaderAttachment': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcLeaderAttachment.Create': {
					'type': 'Function',
					'args': 'LeaderAttachs as IpfcAttachment',
					'return': 'IpfcLeaderAttachment',
					'info': 'Creates leader attachment. ',
					'com': 'pfcls.pfcLeaderAttachment.1',
				},
			},
	}
	IpfcLengthUnits = {
			'type': 'Classes',
			'parent': {
				'IpfcModelUnits': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'GetType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcLengthUnitType',
					'info': 'Returns the type of length units in the data object.',
				},
			},
	}
	IpfcLinAOCTangentDimensionSense = {
			'type': 'Classes',
			'parent': {
				'IpfcDimensionSense': '',
			},
			'child': {
			},
			'properties': {
				'TangentType': {
					'return': 'IpfcDimensionLinAOCTangentType',
					'info': 'The type of tangent attachment.',
				},
			},
			'method': {
				'CCpfcLinAOCTangentDimensionSense.Create': {
					'type': 'Function',
					'args': 'TangentType as IpfcDimensionLinAOCTangentType',
					'return': 'IpfcLinAOCTangentDimensionSense',
					'info': 'Creates a new linear to arc or circle tangent dimension sense object for use in creating a drawing dimension.',
					'com': 'pfcls.pfcLinAOCTangentDimensionSense.1',
				},
			},
	}
	IpfcLine = {
			'type': 'Classes',
			'parent': {
				'IpfcCurve': '',
			},
			'child': {
			},
			'properties': {
				'End1': {
					'return': 'IpfcPoint3D',
					'info': 'The beginning of the line',
				},
				'End2': {
					'return': 'IpfcPoint3D',
					'info': 'The end of the line',
				},
			},
			'method': {
			},
	}
	IpfcLineAOCTangentDimSense = {
			'type': 'Classes',
			'parent': {
				'IpfcDimSense': '',
			},
			'child': {
			},
			'properties': {
				'TangentType': {
					'return': 'IpfcDimLineAOCTangentType',
					'info': '  ',
				},
			},
			'method': {
				'CCpfcLineAOCTangentDimSense.Create': {
					'type': 'Function',
					'args': 'TangentType as IpfcDimLineAOCTangentType',
					'return': 'IpfcLineAOCTangentDimSense',
					'info': '  ',
					'com': 'pfcls.pfcLineAOCTangentDimSense.1',
				},
			},
	}
	IpfcLineDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcCurveDescriptor': '',
			},
			'child': {
			},
			'properties': {
				'End1': {
					'return': 'IpfcPoint3D',
					'info': 'The first endpoint',
				},
				'End2': {
					'return': 'IpfcPoint3D',
					'info': 'The second endpoint',
				},
			},
			'method': {
				'CCpfcLineDescriptor.Create': {
					'type': 'Function',
					'args': 'End1 as IpfcPoint3D, End2 as IpfcPoint3D',
					'return': 'IpfcLineDescriptor',
					'info': 'This method creates a new LineDescriptor object.',
					'com': 'pfcls.pfcLineDescriptor.1',
				},
			},
	}
	IpfcMarkup = {
			'type': 'Classes',
			'parent': {
				'IpfcModel': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcMassProperty = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'CenterGravityInertiaTensor': {
					'return': 'IpfcInertia',
					'info': 'The inertia tensor translated to center of gravity',
				},
				'CoordSysInertia': {
					'return': 'IpfcInertia',
					'info': 'The inertia matrix with respect to coordinate frame:(element ij is the integral of x_i x_j over the object)',
				},
				'CoordSysInertiaTensor': {
					'return': 'IpfcInertia',
					'info': 'The inertia tensor with respect to coordinate frame:CoordSysInertiaTensor = trace(CoordSysInertia) * identity - CoordSysInertia',
				},
				'Density': {
					'return': 'as Double',
					'info': 'The density of the model.',
				},
				'GravityCenter': {
					'return': 'IpfcPoint3D',
					'info': 'The center of gravity with respect to coordinate frame',
				},
				'Mass': {
					'return': 'as Double',
					'info': 'The model mass.',
				},
				'PrincipalAxes': {
					'return': 'IpfcPrincipalAxes',
					'info': 'The principal axes (the eigenvectors of CenterGravityInertiaTensor);The vectors are stored in columns.',
				},
				'PrincipalMoments': {
					'return': 'IpfcVector3D',
					'info': 'The principal moments of inertia (eigenvalues of CenterGravityInertiaTensor)',
				},
				'SurfaceArea': {
					'return': 'as Double',
					'info': "The model's surface area.",
				},
				'Volume': {
					'return': 'as Double',
					'info': "The model's volume.",
				},
			},
			'method': {
				'CCpfcMassProperty.Create': {
					'type': 'Function',
					'args': 'Volume as Double, SurfaceArea as Double, Density as Double, Mass as Double, GravityCenter as IpfcPoint3D, CoordSysInertia as IpfcInertia, CoordSysInertiaTensor as IpfcInertia, CenterGravityInertiaTensor as IpfcInertia, PrincipalMoments as IpfcVector3D, PrincipalAxes as IpfcPrincipalAxes',
					'return': 'IpfcMassProperty',
					'info': 'Creates a new object with mass property information.',
					'com': 'pfcls.pfcMassProperty.1',
				},
			},
	}
	IpfcMaterial = {
			'type': 'Classes',
			'parent': {
				'IpfcParameterOwner': '',
				'IpfcChild': '',
			},
			'child': {
			},
			'properties': {
				'BendTable': {
					'return': 'as String',
					'info': 'The bend table',
				},
				'Condition': {
					'return': 'as String',
					'info': 'The condition',
				},
				'CrossHatchFile': {
					'return': 'as String [optional]',
					'info': 'The name of the cross-hatching file used in detailing for this material.',
				},
				'Description': {
					'return': 'as String [optional]',
					'info': 'The material description string.',
				},
				'Emissivity': {
					'return': 'as Double',
					'info': 'The emissivity',
				},
				'FailureCriterion': {
					'return': 'as String [optional]',
					'info': 'The failure criterion.   ',
				},
				'FatigueMaterialFinish': {
					'return': 'as String [optional]',
					'info': 'The fatigue material finish.     ',
				},
				'FatigueMaterialType': {
					'return': 'as String [optional]',
					'info': 'The fatigue material type.     ',
				},
				'FatigueType': {
					'return': 'as String [optional]',
					'info': 'The fatigue type.     ',
				},
				'Hardness': {
					'return': 'as Double',
					'info': 'The hardness',
				},
				'HardnessType': {
					'return': 'as String [optional]',
					'info': 'The hardness type.',
				},
				'Id': {
					'return': 'as Long',
					'info': 'The id of the material',
				},
				'InitBendYFactor': {
					'return': 'as Double',
					'info': 'The initial bend Y-factor',
				},
				'MassDensity': {
					'return': 'as Double',
					'info': 'The mass density',
				},
				'MaterialModel': {
					'return': 'as String [optional]',
					'info': 'The type of hyperelastic material model.',
				},
				'ModelDefByTests': {
					'return': 'as Boolean [optional]',
					'info': 'Specifies whether the selected hyperelastic material model is   	defined using test data.',
				},
				'Name': {
					'return': 'as String',
					'info': 'The name of the material',
				},
				'PermittedFailureCriteria': {
					'return': 'Istringseq',
					'info': 'A list of the permitted string values for the material failure criterion.   ',
				},
				'PermittedFatigueMaterialFinishes': {
					'return': 'Istringseq',
					'info': 'A list of the permitted string values for the material fatigue material finish.   ',
				},
				'PermittedFatigueMaterialTypes': {
					'return': 'Istringseq',
					'info': 'A list of the permitted string values for the material fatigue material type.   ',
				},
				'PermittedFatigueTypes': {
					'return': 'Istringseq',
					'info': 'A list of the permitted string values for the material fatigue type.   ',
				},
				'PermittedMaterialModels': {
					'return': 'Istringseq',
					'info': 'A list of the permitted string values for the material model.',
				},
				'PermittedSubTypes': {
					'return': 'Istringseq',
					'info': 'A list of the permitted string values for the material sub type.',
				},
				'PoissonRatio': {
					'return': 'as Double',
					'info': "The isotropic Poisson's ratio",
				},
				'ShearModulus': {
					'return': 'as Double',
					'info': 'The isotropic shear modulus',
				},
				'SpecificHeat': {
					'return': 'as Double',
					'info': 'The specific heat',
				},
				'StressLimCompress': {
					'return': 'as Double',
					'info': 'The isotropic compression ultimate stress.',
				},
				'StressLimShear': {
					'return': 'as Double',
					'info': 'The shear ultimate stress.',
				},
				'StressLimTension': {
					'return': 'as Double',
					'info': 'The isotropic tensile ultimate stress.',
				},
				'StructDampCoef': {
					'return': 'as Double',
					'info': 'The structural damping coefficient',
				},
				'StructuralMaterialType': {
					'return': 'IpfcMaterialType',
					'info': 'The material type for the structural properties.',
				},
				'SubType': {
					'return': 'as String [optional]',
					'info': "The isotropic material type's sub type.",
				},
				'ThermalMaterialType': {
					'return': 'IpfcMaterialType',
					'info': 'The material type for the thermal properties.',
				},
				'ThermConductivity': {
					'return': 'as Double',
					'info': 'The isotropic thermal conductivity',
				},
				'ThermExpCoef': {
					'return': 'as Double',
					'info': 'The isotropic thermal expansion coefficient',
				},
				'ThermExpRefTemp': {
					'return': 'as Double',
					'info': 'The thermal expansion reference temperature',
				},
				'YoungModulus': {
					'return': 'as Double',
					'info': "The isotropic Young's modulus",
				},
			},
			'method': {
				'Delete': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': "Deletes the material from the part's database.",
				},
				'GetPropertyValue': {
					'type': 'Function',
					'args': 'Type as IpfcMaterialPropertyType',
					'return': 'IpfcMaterialProperty',
					'info': 'Returns the value and units for a material property.',
				},
				'RemoveProperty': {
					'type': 'Sub',
					'args': 'Type as IpfcMaterialPropertyType',
					'return': '',
					'info': 'Removes a material property.',
				},
				'Save': {
					'type': 'Sub',
					'args': 'FileName as String',
					'return': '',
					'info': 'Writes the specified part material to a file.',
				},
				'SetPropertyUnits': {
					'type': 'Sub',
					'args': 'Type as IpfcMaterialPropertyType, Units as String, Convert as Boolean [optional]',
					'return': '',
					'info': 'Sets the units for a material property.  ',
				},
				'SetPropertyValue': {
					'type': 'Sub',
					'args': 'Type as IpfcMaterialPropertyType, Value as IpfcMaterialProperty',
					'return': '',
					'info': 'Sets the value and units for a material property.  ',
				},
			},
	}
	IpfcMaterialExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcMaterialExportInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcMaterialExportInstructions',
					'info': 'Creates a new instructions object used to export a material from a part.',
					'com': 'pfcls.pfcMaterialExportInstructions.1',
				},
			},
	}
	IpfcMaterialOId = {
			'type': 'Classes',
			'parent': {
				'IpfcStringOId': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcMaterialProperty = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Units': {
					'return': 'as String [optional]',
					'info': "The units material property. null represents a property that doesn't use units, or a property that   is using the units of the owner part.",
				},
				'Value': {
					'return': 'as Double',
					'info': 'The value of the material property in the property units.',
				},
			},
			'method': {
				'CCpfcMaterialProperty.Create': {
					'type': 'Function',
					'args': 'Value as Double, Units as String [optional]',
					'return': 'IpfcMaterialProperty',
					'info': 'Returns a new instance of a material property object. ',
					'com': 'pfcls.pfcMaterialProperty.1',
				},
			},
	}
	IpfcMedusaExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExportInstructions': '',
			},
			'child': {
			},
			'properties': {
				'OptionValue': {
					'return': 'IpfcExport2DOption',
					'info': '  ',
				},
			},
			'method': {
				'CCpfcMedusaExportInstructions.Create': {
					'type': 'Function',
					'args': 'inOption as IpfcExport2DOption [optional]',
					'return': 'IpfcMedusaExportInstructions',
					'info': 'Creates a new instructions object used to export a Medusa format.',
					'com': 'pfcls.pfcMedusaExportInstructions.1',
				},
			},
	}
	IpfcMessageDialogOptions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Buttons': {
					'return': 'IpfcMessageButtons',
					'info': 'An array of possible button identifiers to show in the dialog. If null, the dialog will include only an OK button.   ',
				},
				'DefaultButton': {
					'return': 'IpfcMessageButton',
					'info': 'The identifier of the default button. If null, the default button will be the OK button.   ',
				},
				'DialogLabel': {
					'return': 'as String [optional]',
					'info': 'The text to display as the dialog title. If null, the dialog title will be "Info" regardless of the language used by Creo Parametric.   ',
				},
				'MessageDialogType': {
					'return': 'IpfcMessageDialogType',
					'info': ' The type of icon to show with the message dialog. If null, the dialog will show the Info icon.     ',
				},
			},
			'method': {
				'CCpfcMessageDialogOptions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcMessageDialogOptions',
					'info': 'Creates a class object for message dialog options.   ',
					'com': 'pfcls.pfcMessageDialogOptions.1',
				},
			},
	}
	IpfcMFG = {
			'type': 'Classes',
			'parent': {
				'IpfcModel': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'GetSolid': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSolid',
					'info': "Retrieves the solid in which the manufacturing model's  features are placed.",
				},
			},
	}
	IpfcMFGCLExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcFeatIdExportInstructions': '',
			},
			'child': {
				'IpfcMFGFeatCLExportInstructions': '',
				'IpfcMFGOperCLExportInstructions': '',
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcMFGFeatCLExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcMFGCLExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcMFGFeatCLExportInstructions.Create': {
					'type': 'Function',
					'args': 'FeatId as Long',
					'return': 'IpfcMFGFeatCLExportInstructions',
					'info': 'Creates a new instructions object used to export a cutter location (CL) file for one NC sequence in a manufacturing assembly. ',
					'com': 'pfcls.pfcMFGFeatCLExportInstructions.1',
				},
			},
	}
	IpfcMFGOperCLExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcMFGCLExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcMFGOperCLExportInstructions.Create': {
					'type': 'Function',
					'args': 'FeatId as Long',
					'return': 'IpfcMFGOperCLExportInstructions',
					'info': 'Creates a new instructions object used to export from a manufacturing assembly a cutter location (CL) file for all the NC sequences in an operation. ',
					'com': 'pfcls.pfcMFGOperCLExportInstructions.1',
				},
			},
	}
	IpfcModel = {
			'type': 'Classes',
			'parent': {
				'IpfcModelItemOwner': '',
				'IpfcViewOwner': '',
				'IpfcChild': '',
				'IpfcActionSource': '',
				'IpfcRelationOwner': '',
				'IpfcParameterOwner': '',
			},
			'child': {
				'IpfcSolid': '',
				'IpfcDrawingFormat': '',
				'IpfcModel2D': '',
				'IpfcMFG': '',
				'IpfcSection2D': '',
				'IpfcDiagram': '',
				'IpfcMarkup': '',
			},
			'properties': {
				'Branch': {
					'return': 'as String [readonly, optional]',
					'info': 'The branch. This attribute can be null.',
				},
				'CommonName': {
					'return': 'as String',
					'info': 'The common name for the model.  This is the name that will shown for the model in Windchill PDM.  It can be modified for models which are not owned by PDM.',
				},
				'Descr': {
					'return': 'IpfcModelDescriptor',
					'info': 'The model descriptor. ',
				},
				'FileName': {
					'return': 'as String',
					'info': 'The model file name in "name"."type" format.',
				},
				'FullName': {
					'return': 'as String',
					'info': 'The full name of the model in instance<generic> format.',
				},
				'GenericName': {
					'return': 'as String [readonly, optional]',
					'info': 'The name of the generic model. If the model is not an instance, this attribute must be null or an empty string.',
				},
				'InstanceName': {
					'return': 'as String',
					'info': 'The name of the model, or, if the model is an instance, the instance name.',
				},
				'IsModified': {
					'return': 'as Boolean',
					'info': 'A Boolean flag that specifies whether the model has been modified since the last save.',
				},
				'Origin': {
					'return': 'as String',
					'info': 'The location of the origin of the model.',
				},
				'PostRegenerationRelations': {
					'return': 'Istringseq',
					'info': 'The list of the post-regeneration relations assigned to the model.  This attribute can be null.',
				},
				'RelationId': {
					'return': 'as Long [readonly, optional]',
					'info': 'The relation identifier. This attribute can be null. ',
				},
				'ReleaseLevel': {
					'return': 'as String [readonly, optional]',
					'info': 'The release level. This attribute can be null.',
				},
				'Revision': {
					'return': 'as String [readonly, optional]',
					'info': 'The revision number. This attribute can be null.',
				},
				'Type': {
					'return': 'IpfcModelType',
					'info': 'The model type.',
				},
				'Version': {
					'return': 'as String [readonly, optional]',
					'info': 'The version. This attribute can be null.',
				},
				'VersionStamp': {
					'return': 'as String',
					'info': 'The version stamp.',
				},
			},
			'method': {
				'AccessExternalData': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcExternalDataAccess',
					'info': 'Initializes External Data access   ',
				},
				'Backup': {
					'type': 'Sub',
					'args': 'WhereTo as IpfcModelDescriptor',
					'return': '',
					'info': 'Backs up the model to a specified directory.    ',
				},
				'CheckIsModifiable': {
					'type': 'Function',
					'args': 'ShowUI as Boolean',
					'return': ' as Boolean',
					'info': 'Check if given model is modifiable, indicating that the model could be saved     (without checking for any subordinate models).',
				},
				'CheckIsSaveAllowed': {
					'type': 'Function',
					'args': 'ShowUI as Boolean',
					'return': ' as Boolean',
					'info': "Check if given model can be saved along with all its subordinate models that     are to be saved together with it according to their modification status and the configuration     option 'save_objects'.",
				},
				'CleanupDependencies': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': '       Forces cleaning up dependencies database of an object in the       Creo Parametric workspace. This function should not be called       during reference altering operations like feature create, edit       definition or restructure.     ',
				},
				'ClearIntf3DModelData': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': '       Clears model data set for export.       Clears model data which user set using following methods       IpfcModel.SetIntf3DLayerSetupFile()         <reference to unknown entity pfcModel::Model::SetIntf3DCsys>          ',
				},
				'Copy': {
					'type': 'Sub',
					'args': 'NewName as String, Instructions as IpfcCopyInstructions [optional]',
					'return': '',
					'info': 'Copies the disk file for the model to another named file. The model copy is not retrieved into the session.   ',
				},
				'CopyAndRetrieve': {
					'type': 'Function',
					'args': 'NewName as String, Instructions as IpfcCopyInstructions [optional]',
					'return': 'IpfcModel',
					'info': 'Copies the model to a new one, and retrieves the new model into memory.   ',
				},
				'CreateLayer': {
					'type': 'Function',
					'args': 'Name as String',
					'return': 'IpfcLayer',
					'info': 'Creates a new layer in the object.   ',
				},
				'Delete': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Deletes the model from memory and disk.   ',
				},
				'DeletePostRegenerationRelations': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Deletes the post-regeneration relations in the model.',
				},
				'Display': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Displays the model in its window.   ',
				},
				'DisplayInNewWindow': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': ' ',
				},
				'Erase': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Erases the model from the session. This method does not delete the model filefrom the disk.   ',
				},
				'EraseWithDependencies': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Erases the model and its dependencies from the session.   ',
				},
				'Export': {
					'type': 'Sub',
					'args': 'FileName as String, ExportData as IpfcExportInstructions',
					'return': '',
					'info': 'Exports model data to a file or creates a plot.   NOTE: Interface IpfcExport3DInstructions is deprecated in Creo5.     Use superseding method    <reference to unknown entity pfcModel::ExportIntf3D>   to export these type of objects.   ',
				},
				'ExportIntf3D': {
					'type': 'Sub',
					'args': 'NewFileName as String, ExpType as IpfcExportType, ProfileFile as String [optional]',
					'return': '',
					'info': 'Exports a Creo Parametric model using default export profile.',
				},
				'Import': {
					'type': 'Sub',
					'args': 'FilePath as String, ImportData as IpfcImportInstructions',
					'return': '',
					'info': 'Reads a file into Creo Parametric. The format must be the same as if these files were created by Creo Parametric.   ',
				},
				'IsCommonNameModifiable': {
					'type': 'Function',
					'args': '',
					'return': ' as Boolean',
					'info': 'Identifies if the model common name (IpfcModel.CommonName) can be modified. ',
				},
				'IsIntf3DCsysIgnored': {
					'type': 'Function',
					'args': '',
					'return': ' as Boolean',
					'info': '       Checks that reference CSYS for export is ignored or not.                This method returns CSYS ignored status for         last export using method ExportIntf3D.         Call this method after ExportIntf3D         otherwise results will not be reliable.       ',
				},
				'IsIntf3DLayerSetupFileIgnored': {
					'type': 'Function',
					'args': '',
					'return': ' as Boolean',
					'info': '       Checks that layer setup file for export is ignored or not.                This method returns layer setup file ignored status for         last export using method ExportIntf3D.         Call this method after ExportIntf3D          otherwise results will not be reliable.       ',
				},
				'IsNativeModel': {
					'type': 'Function',
					'args': '',
					'return': ' as Boolean',
					'info': 'Returns true if the origin of the model is Creo; false otherwise.',
				},
				'ListDeclaredModels': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcModelDescriptors',
					'info': 'Finds all the first-level objects declared for the model.   ',
				},
				'ListDependencies': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcDependencies',
					'info': 'Finds the first-level dependencies for the model inthe Creo Parametric workspace.   ',
				},
				'RegeneratePostRegenerationRelations': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Regenerates the post-regeneration relations in the model.',
				},
				'Rename': {
					'type': 'Sub',
					'args': 'NewName as String, RenameFilesToo as Boolean [optional]',
					'return': '',
					'info': 'Renames the model.   ',
				},
				'Save': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Writes the model to disk.   ',
				},
				'SetIntf3DLayerSetupFile': {
					'type': 'Sub',
					'args': 'setupFilePath as String [optional]',
					'return': '',
					'info': '       Set layer setup file for export     ',
				},
				'TerminateExternalData': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Cleans up the setup required for the generic application data operations for the specified model.   ',
				},
			},
	}
	IpfcModel2D = {
			'type': 'Classes',
			'parent': {
				'IpfcSheetOwner': '',
				'IpfcDetailItemOwner': '',
				'IpfcTableOwner': '',
				'IpfcModel': '',
			},
			'child': {
				'IpfcDrawing': '',
				'IpfcReport': '',
				'IpfcLayout': '',
			},
			'properties': {
				'TextHeight': {
					'return': 'as Double',
					'info': 'The text height of the model. The model must be in the current window inorderto access this property.',
				},
			},
			'method': {
				'AddModel': {
					'type': 'Sub',
					'args': 'NewModel as IpfcModel',
					'return': '',
					'info': 'Adds a solid model to the drawing.',
				},
				'AddSimplifiedRep': {
					'type': 'Sub',
					'args': 'SimplifiedRep as IpfcSimpRep',
					'return': '',
					'info': 'Adds a new simplified representation of a model to a drawing.',
				},
				'CollectShownDimensions': {
					'type': 'Function',
					'args': 'SolidMdl as IpfcSolid',
					'return': 'IpfcDimensions',
					'info': '  ',
				},
				'CollectShownRefDimensions': {
					'type': 'Function',
					'args': 'SolidMdl as IpfcSolid',
					'return': 'IpfcDimensions',
					'info': '  ',
				},
				'CreateDimension': {
					'type': 'Function',
					'args': 'Attachments as IpfcDimensionAttachments, Senses as IpfcDimSenses, hint as IpfcDimOrientationHint, Location as IpfcPoint2D',
					'return': 'IpfcDimension',
					'info': '  ',
				},
				'CreateDrawingDimension': {
					'type': 'Function',
					'args': 'Instructions as IpfcDrawingDimCreateInstructions',
					'return': 'IpfcDimension2D',
					'info': 'Creates a new drawing dimension.',
				},
				'CreateRefDimension': {
					'type': 'Function',
					'args': 'Attachments as IpfcDimensionAttachments, Senses as IpfcDimSenses, hint as IpfcDimOrientationHint, Location as IpfcPoint2D',
					'return': 'IpfcDimension',
					'info': '  ',
				},
				'CreateView': {
					'type': 'Function',
					'args': 'Instructions as IpfcView2DCreateInstructions',
					'return': 'IpfcView2D',
					'info': 'Creates a new view in the drawing.',
				},
				'DeleteModel': {
					'type': 'Sub',
					'args': 'Model as IpfcModel',
					'return': '',
					'info': 'Removes a model from the drawing.',
				},
				'DeleteSimplifiedRep': {
					'type': 'Sub',
					'args': 'SimplifiedRep as IpfcSimpRep',
					'return': '',
					'info': 'Deletes a simplified representation of a model from a drawing.',
				},
				'GetCurrentSolid': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcModel',
					'info': 'Gets the current solid model of the drawing',
				},
				'GetViewByName': {
					'type': 'Function',
					'args': 'ViewName as String',
					'return': 'IpfcView2D',
					'info': 'Returns a drawing view, given its name.',
				},
				'GetViewDisplaying': {
					'type': 'Function',
					'args': 'Dim as IpfcBaseDimension',
					'return': 'IpfcView2D',
					'info': 'Returns the drawing view that displays a dimension.',
				},
				'List2DViews': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcView2Ds',
					'info': 'Lists the drawing views.',
				},
				'ListModels': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcModels',
					'info': 'Lists the models used in the drawing.',
				},
				'ListShownDimensions': {
					'type': 'Function',
					'args': 'Model as IpfcModel, Type as IpfcModelItemType [optional]',
					'return': 'IpfcDimension2Ds',
					'info': 'Lists the solid-model owned dimensions and reference dimensions shown in the drawing.',
				},
				'ListSimplifiedReps': {
					'type': 'Function',
					'args': 'Solid as IpfcModel',
					'return': 'IpfcSimpReps',
					'info': 'Returns a list of the simplified representations referenced by the drawing.',
				},
				'Regenerate': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Regenerates the drawing draft entities.',
				},
				'ReplaceModel': {
					'type': 'Sub',
					'args': 'FromModel as IpfcModel, ToModel as IpfcModel, Unrepresent as Boolean',
					'return': '',
					'info': 'Replaces a drawing model solid with another solid. The old and new solids must be members of the same family table.',
				},
				'SetCurrentSolid': {
					'type': 'Sub',
					'args': 'NewCurrentSolid as IpfcModel',
					'return': '',
					'info': 'Sets the current solid model for the drawing.',
				},
				'SetViewDisplaying': {
					'type': 'Sub',
					'args': 'Dim as IpfcBaseDimension, NewView as IpfcView2D',
					'return': '',
					'info': 'Sets the view that shows the model dimension.',
				},
			},
	}
	IpfcModelActionListener = {
			'type': 'Classes',
			'parent': {
				'IpfcActionListener': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'OnAfterModelCopy': {
					'type': 'Sub',
					'args': 'FromMdl as IpfcModel, ToMdl as IpfcModel [optional]',
					'return': '',
					'info': 'The listener method called after successfulexecution of a copy command.',
				},
				'OnAfterModelCopyAll': {
					'type': 'Sub',
					'args': 'FromMdl as IpfcModel, ToMdl as IpfcModel [optional]',
					'return': '',
					'info': 'This is the listener method called after successfulcopying of any model (not just an explicit copy of a single model). ',
				},
				'OnAfterModelCreate': {
					'type': 'Sub',
					'args': 'Mdl as IpfcModel',
					'return': '',
					'info': 'This is the listener method called after successfulcreation of a model.',
				},
				'OnAfterModelDelete': {
					'type': 'Sub',
					'args': 'Mdl as IpfcModel',
					'return': '',
					'info': 'This is the listener method called after successfuldeletion of a model.',
				},
				'OnAfterModelDeleteAll': {
					'type': 'Sub',
					'args': 'Descr as IpfcModelDescriptor',
					'return': '',
					'info': 'The listener method called after successfuldeletion of any model, (not just an explicit deletion of a single model).',
				},
				'OnAfterModelErase': {
					'type': 'Sub',
					'args': 'Mdl as IpfcModel',
					'return': '',
					'info': 'This is the listener method called after successfulerasing of a model.',
				},
				'OnAfterModelEraseAll': {
					'type': 'Sub',
					'args': 'Descr as IpfcModelDescriptor',
					'return': '',
					'info': 'This is the listener method called after successfulerasure of any model, (not just an explicit erasure of a single model). ',
				},
				'OnAfterModelRename': {
					'type': 'Sub',
					'args': 'FromMdl as IpfcModel, ToMdl as IpfcModel [optional]',
					'return': '',
					'info': 'This is the listener method called after successful renaming of a model.',
				},
				'OnAfterModelRetrieve': {
					'type': 'Sub',
					'args': 'Mdl as IpfcModel',
					'return': '',
					'info': 'This is the listener method called after successful retrieval of a model.',
				},
				'OnAfterModelRetrieveAll': {
					'type': 'Sub',
					'args': 'Descr as IpfcModelDescriptor',
					'return': '',
					'info': 'This is the listener method called after successful retrieval ofany model, (not just an explicit retrieval of a single model).',
				},
				'OnAfterModelSave': {
					'type': 'Sub',
					'args': 'Descr as IpfcModelDescriptor',
					'return': '',
					'info': 'This is the listener mothod called after successfulsaving of a model.',
				},
				'OnAfterModelSaveAll': {
					'type': 'Sub',
					'args': 'Descr as IpfcModelDescriptor',
					'return': '',
					'info': 'This is the listener method called after successfulsaving of any model, (not just an explicit save of a single model).',
				},
				'OnBeforeModelDisplay': {
					'type': 'Sub',
					'args': 'Mdl as IpfcModel',
					'return': '',
					'info': 'The listener method called before a model is displayed.   ',
				},
				'OnBeforeParameterCreate': {
					'type': 'Sub',
					'args': 'Owner as IpfcModel, Name as String, Value as IpfcParamValue',
					'return': '',
					'info': '       This is the listener method called before creating a parameter.       To abort parameter creation throw exception IpfcXCancelProEAction.',
				},
				'OnBeforeParameterDelete': {
					'type': 'Sub',
					'args': 'Param as IpfcParameter',
					'return': '',
					'info': '       This is the listener method called before deleting a parameter.       To abort parameter delete operation throw exception IpfcXCancelProEAction.',
				},
				'OnBeforeParameterModify': {
					'type': 'Sub',
					'args': 'Param as IpfcParameter, Value as IpfcParamValue',
					'return': '',
					'info': '       This is the listener method called before modifying a parameter.       To abort parameter modification throw exception IpfcXCancelProEAction.',
				},
			},
	}
	IpfcModelCheckCustomCheckListener = {
			'type': 'Classes',
			'parent': {
				'IpfcActionListener': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'OnCustomCheck': {
					'type': 'Function',
					'args': 'CheckName as String, Mdl as IpfcModel',
					'return': 'IpfcCustomCheckResults',
					'info': 'Override this method to evaluate a ModelCheck externally   defined check.',
				},
				'OnCustomCheckAction': {
					'type': 'Sub',
					'args': 'CheckName as String, Mdl as IpfcModel, SelectedItem as String [optional]',
					'return': '',
					'info': 'Override this method to execute a repair action on an item   found by a custom check.',
				},
				'OnCustomCheckUpdate': {
					'type': 'Sub',
					'args': 'CheckName as String, Mdl as IpfcModel, SelectedItem as String [optional]',
					'return': '',
					'info': 'Override this method to update an item found by a custom check.',
				},
			},
	}
	IpfcModelCheckInstructions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'ConfigDir': {
					'return': 'as String [optional]',
					'info': "Specifies the directory which contains the ModelCheck configuration directory   (which must be named 'config'). If null then the default   ModelCheck configuration directory will be used.  ",
				},
				'Mode': {
					'return': 'IpfcModelCheckMode',
					'info': 'Specifies the mode to use when executing ModelCheck.   If null, the non-graphic mode will be used.   ',
				},
				'OutputDir': {
					'return': 'as String [optional]',
					'info': 'Specifies the directory where the report will be written.   If null then default ModelCheck output directory ,as per config_init.mc , will be used.  ',
				},
				'ShowInBrowser': {
					'return': 'as Boolean [optional]',
					'info': 'Specifies if the results report should be shown in the browser.   If null (the default) it will not be shown.   ',
				},
			},
			'method': {
				'CCpfcModelCheckInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcModelCheckInstructions',
					'info': 'Creates a new IpfcModelCheckInstructions object.',
					'com': 'pfcls.pfcModelCheckInstructions.1',
				},
			},
	}
	IpfcModelCheckResults = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'NumberOfErrors': {
					'return': 'as Long',
					'info': 'Specifies the number of errors detected.  ',
				},
				'NumberOfWarnings': {
					'return': 'as Long',
					'info': 'Specifies the number of warnings detected. ',
				},
				'WasModelSaved': {
					'return': 'as Boolean',
					'info': 'Specifies whether model was saved or not. ',
				},
			},
			'method': {
			},
	}
	IpfcModelDescriptor = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Device': {
					'return': 'as String',
					'info': 'The device. On UNIX systems, this field contains an empty string. If you change this value on a UNIX system, Creo Parametric may experience problems when reading or writing the file.',
				},
				'FileVersion': {
					'return': 'as Long [optional]',
					'info': 'The version of the file',
				},
				'GenericName': {
					'return': 'as String [optional]',
					'info': 'The name of the generic model. If the model is not an instance, this attribute must be null or an empty string.',
				},
				'Host': {
					'return': 'as String',
					'info': 'The host. If the model resides on the local Windows or UNIX host, this field contains an empty string. If you change this value, Creo Parametric may experience problems when reading or writing the file.',
				},
				'InstanceName': {
					'return': 'as String',
					'info': 'The name of the model, or, if the model is an instance, the instance name. This string can be neither null nor empty. ',
				},
				'Path': {
					'return': 'as String',
					'info': 'The path to the file. Note that this argument is ignored by the RetrieveModel method.',
				},
				'Type': {
					'return': 'IpfcModelType',
					'info': 'The model type.  If the model descriptor represents a Creo Parametric related file that is not a true model type, a IpfcXUnusedValue exception will result if you try to access this value.',
				},
			},
			'method': {
				'CCpfcModelDescriptor.Create': {
					'type': 'Function',
					'args': 'Type as IpfcModelType, InstanceName as String, GenericName as String [optional]',
					'return': 'IpfcModelDescriptor',
					'info': 'Creates a new model descriptor. ',
					'com': 'pfcls.pfcModelDescriptor.1',
				},
				'CCpfcModelDescriptor.CreateFromFileName': {
					'type': 'Function',
					'args': 'FileName as String',
					'return': 'IpfcModelDescriptor',
					'info': 'Creates a new model descriptor object, given a file name string.',
					'com': 'pfcls.pfcModelDescriptor.1',
				},
				'GetExtension': {
					'type': 'Function',
					'args': '',
					'return': ' as String',
					'info': 'Retrieves the extension of the model identified by the model descriptor.',
				},
				'GetFileName': {
					'type': 'Function',
					'args': '',
					'return': ' as String',
					'info': 'Returns the file name for the object represented by the model descriptor.',
				},
				'GetFullName': {
					'type': 'Function',
					'args': '',
					'return': ' as String',
					'info': 'Retrieves the full name of the model identified by the model descriptor.',
				},
			},
	}
	IpfcModelEventActionListener = {
			'type': 'Classes',
			'parent': {
				'IpfcActionListener': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'OnAfterModelCopy': {
					'type': 'Sub',
					'args': 'FromMdl as IpfcModelDescriptor, ToMdl as IpfcModelDescriptor',
					'return': '',
					'info': 'The listener method called after successfulexecution of a copy command.',
				},
				'OnAfterModelCopyAll': {
					'type': 'Sub',
					'args': 'FromMdl as IpfcModelDescriptor, ToMdl as IpfcModelDescriptor',
					'return': '',
					'info': 'This is the listener method called after successfulcopying of any model, (not just an explicit copy of a single model). ',
				},
				'OnAfterModelDelete': {
					'type': 'Sub',
					'args': 'Descr as IpfcModelDescriptor',
					'return': '',
					'info': 'This is the listener method called after successfuldeletion of a model.',
				},
				'OnAfterModelErase': {
					'type': 'Sub',
					'args': 'Descr as IpfcModelDescriptor',
					'return': '',
					'info': 'This is the listener method called after successfulerasing of a model.',
				},
				'OnAfterModelRename': {
					'type': 'Sub',
					'args': 'FromMdl as IpfcModelDescriptor, ToMdl as IpfcModelDescriptor',
					'return': '',
					'info': 'This is the listener method called after successfulrenaming of a model.',
				},
			},
	}
	IpfcModelInfoExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcModelInfoExportInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcModelInfoExportInstructions',
					'info': 'Creates a new instructions object used to export information about a model, including units information, features, and children.',
					'com': 'pfcls.pfcModelInfoExportInstructions.1',
				},
			},
	}
	IpfcModelItem = {
			'type': 'Classes',
			'parent': {
				'IpfcChild': '',
				'IpfcActionSource': '',
				'IpfcRelationOwner': '',
				'IpfcParameterOwner': '',
			},
			'child': {
				'IpfcLayer': '',
				'IpfcSolidGeometryLayerItem': '',
				'IpfcBaseDimension': '',
				'IpfcFeature': '',
				'IpfcEdge': '',
				'IpfcSurface': '',
				'IpfcCoordSystem': '',
				'IpfcAxis': '',
				'IpfcQuilt': '',
				'IpfcCurve': '',
				'IpfcSimpRep': '',
				'IpfcExplodedState': '',
				'IpfcTable': '',
				'IpfcNote': '',
				'IpfcDetailItem': '',
			},
			'properties': {
				'Id': {
					'return': 'as Long',
					'info': 'The identifier of the model item',
				},
				'Type': {
					'return': 'IpfcModelItemType',
					'info': 'The type of model item',
				},
			},
			'method': {
				'GetName': {
					'type': 'Function',
					'args': '',
					'return': ' as String [optional]',
					'info': 'Gets the name of a model item.',
				},
				'SetName': {
					'type': 'Sub',
					'args': 'Name as String',
					'return': '',
					'info': 'Sets the item name.',
				},
			},
	}
	IpfcModelItemOId = {
			'type': 'Classes',
			'parent': {
				'IpfcIntegerOId': '',
			},
			'child': {
			},
			'properties': {
				'Type': {
					'return': 'IpfcModelItemType',
					'info': 'The model item type (ITEM_FEATURE, ITEM_SURFACE, and so on) ',
				},
			},
			'method': {
				'CCpfcModelItemOId.Create': {
					'type': 'Function',
					'args': 'Type as IpfcModelItemType, Id as Long',
					'return': 'IpfcModelItemOId',
					'info': 'Creates a model-item identifier object.',
					'com': 'pfcls.pfcModelItemOId.1',
				},
			},
	}
	IpfcModelItemOwner = {
			'type': 'Classes',
			'parent': {
				'IpfcParent': '',
			},
			'child': {
				'IpfcModel': '',
			},
			'properties': {
			},
			'method': {
				'GetItemById': {
					'type': 'Function',
					'args': 'Type as IpfcModelItemType, Id as Long',
					'return': 'IpfcModelItem',
					'info': 'Returns the specified model item, given its identifier and type.',
				},
				'GetItemByName': {
					'type': 'Function',
					'args': 'Type as IpfcModelItemType, Name as String',
					'return': 'IpfcModelItem',
					'info': 'Returns a model item, given a string name and type, if it exists.',
				},
				'ListItems': {
					'type': 'Function',
					'args': 'Type as IpfcModelItemType [optional]',
					'return': 'IpfcModelItems',
					'info': 'Provides a list of model items of the specified type.',
				},
			},
	}
	IpfcModelOId = {
			'type': 'Classes',
			'parent': {
				'IpfcStringOId': '',
			},
			'child': {
			},
			'properties': {
				'Type': {
					'return': 'IpfcModelType',
					'info': 'The model type (MDL_PART, MDL_ASSEMBLY, and so on)',
				},
			},
			'method': {
				'CCpfcModelOId.Create': {
					'type': 'Function',
					'args': 'Type as IpfcModelType, Name as String',
					'return': 'IpfcModelOId',
					'info': 'Creates a model identifier object with the specified model type and model name. ',
					'com': 'pfcls.pfcModelOId.1',
				},
			},
	}
	IpfcModelUnits = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcLengthUnits': '',
			},
			'properties': {
				'UnitName': {
					'return': 'as String [optional]',
					'info': 'The name of the units.',
				},
			},
			'method': {
				'GetUnitType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcUnitType',
					'info': 'Returns the type of units described by the data object.',
				},
			},
	}
	IpfcMouseStatus = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Position': {
					'return': 'IpfcPoint3D',
					'info': 'The coordinates of the picked point.',
				},
				'SelectedButton': {
					'return': 'IpfcMouseButton',
					'info': 'The currently selected mouse button. Value is from the MouseButton enumerated list or null if none selected.',
				},
			},
			'method': {
				'CCpfcMouseStatus.Create': {
					'type': 'Function',
					'args': 'Position as IpfcPoint3D',
					'return': 'IpfcMouseStatus',
					'info': '  ',
					'com': 'pfcls.pfcMouseStatus.1',
				},
			},
	}
	IpfcNamedModelItem = {
			'type': 'Classes',
			'parent': {
				'IpfcChild': '',
			},
			'child': {
				'IpfcParameter': '',
			},
			'properties': {
				'Name': {
					'return': 'as String',
					'info': '  ',
				},
			},
			'method': {
			},
	}
	IpfcNEUTRALFileExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcNEUTRALFileExportInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcNEUTRALFileExportInstructions',
					'info': '  ',
					'com': 'pfcls.pfcNEUTRALFileExportInstructions.1',
				},
			},
	}
	IpfcNonRegisteredServer = {
			'type': 'Classes',
			'parent': {
				'IpfcChild': '',
				'IpfcServerLocation': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcNormalLeaderAttachment = {
			'type': 'Classes',
			'parent': {
				'IpfcDetailLeaderAttachment': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcNormalLeaderAttachment.Create': {
					'type': 'Function',
					'args': 'LeaderAttachs as IpfcAttachment',
					'return': 'IpfcNormalLeaderAttachment',
					'info': 'Creates normal leader attachment. ',
					'com': 'pfcls.pfcNormalLeaderAttachment.1',
				},
			},
	}
	IpfcNote = {
			'type': 'Classes',
			'parent': {
				'IpfcModelItem': '',
			},
			'child': {
			},
			'properties': {
				'Lines': {
					'return': 'Istringseq',
					'info': 'The text of the note.',
				},
				'URL': {
					'return': 'as String',
					'info': 'The URL pointed to by the note.',
				},
			},
			'method': {
				'Delete': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Deletes the solid model note.',
				},
				'Display': {
					'type': 'Sub',
					'args': 'Mode as IpfcGraphicsMode',
					'return': '',
					'info': 'Displays the solid model note.',
				},
				'GetOwner': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcModelItem',
					'info': 'Returns the model item that owns the note.',
				},
				'GetText': {
					'type': 'Function',
					'args': 'GiveParametersAsNames as Boolean',
					'return': 'Istringseq',
					'info': 'Returns the text of the solid model note.',
				},
			},
	}
	IpfcNURBSSurface = {
			'type': 'Classes',
			'parent': {
				'IpfcSurface': '',
			},
			'child': {
			},
			'properties': {
				'Points': {
					'return': 'IpfcBSplinePoints',
					'info': 'Array of control points and weights',
				},
				'UDegree': {
					'return': 'as Long',
					'info': 'Degree of the basis function in U',
				},
				'UKnots': {
					'return': 'Irealseq',
					'info': 'Array of knots on the parameter line U',
				},
				'VDegree': {
					'return': 'as Long',
					'info': 'Degree of the basis function in V',
				},
				'VKnots': {
					'return': 'Irealseq',
					'info': 'Array of knots on the parameter line V',
				},
			},
			'method': {
			},
	}
	IpfcNURBSSurfaceDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcSurfaceDescriptor': '',
			},
			'child': {
			},
			'properties': {
				'Points': {
					'return': 'IpfcBSplinePoints',
					'info': 'Array of control points and weights',
				},
				'UDegree': {
					'return': 'as Long',
					'info': 'Degree of the basis function in U',
				},
				'UKnots': {
					'return': 'Irealseq',
					'info': 'Array of knots on the parameter line U',
				},
				'VDegree': {
					'return': 'as Long',
					'info': 'Degree of the basis function in V',
				},
				'VKnots': {
					'return': 'Irealseq',
					'info': 'Array of knots on the parameter line V',
				},
			},
			'method': {
				'CCpfcNURBSSurfaceDescriptor.Create': {
					'type': 'Function',
					'args': 'Extents as IpfcSurfaceExtents, Orientation as IpfcSurfaceOrientation, UDegree as Long, VDegree as Long, UKnots as Irealseq, VKnots as Irealseq, Points as IpfcBSplinePoints',
					'return': 'IpfcNURBSSurfaceDescriptor',
					'info': 'This method creates a new NURBSSurfaceDescriptor object.',
					'com': 'pfcls.pfcNURBSSurfaceDescriptor.1',
				},
			},
	}
	IpfcObject = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcParent': '',
				'IpfcChild': '',
				'IpfcBaseParameter': '',
				'IpfcRelationOwner': '',
				'IpfcExternalDataClass': '',
				'IpfcExternalDataSlot': '',
				'IpfcFeaturePattern': '',
				'IpfcGroupPattern': '',
				'IpfcFeatureGroup': '',
				'IpfcFeatureOperation': '',
				'IpfcFeaturePlacement': '',
				'IpfcGeomCurve': '',
				'IpfcContour': '',
				'IpfcFamilyTableColumn': '',
				'IpfcFamilyTableRow': '',
				'IpfcFamilyMember': '',
				'IpfcComponentPath': '',
				'IpfcSelection': '',
				'IpfcDetailSymbolGroup': '',
				'IpfcUICommand': '',
				'IpfcInterferenceVolume': '',
				'IpfcClearanceData': '',
				'IpfcCriticalDistanceData': '',
				'IpfcSelectionEvaluator': '',
				'IpfcGlobalInterference': '',
				'IpfcGlobalEvaluator': '',
				'IpfcImportedLayer': '',
				'IpfcJLinkApplication': '',
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcOffsetAttachment = {
			'type': 'Classes',
			'parent': {
				'IpfcAttachment': '',
			},
			'child': {
			},
			'properties': {
				'AttachedGeometry': {
					'return': 'IpfcSelection',
					'info': 'Selection representing the item to attach to. This should include the drawing model view. The attachment will occur at the selected parameters',
				},
				'AttachmentPoint': {
					'return': 'IpfcPoint3D',
					'info': 'The attachment point, in screen coordinates.',
				},
			},
			'method': {
				'CCpfcOffsetAttachment.Create': {
					'type': 'Function',
					'args': 'inAttachedGeometry as IpfcSelection, inAttachmentPoint as IpfcPoint3D',
					'return': 'IpfcOffsetAttachment',
					'info': 'Creates a data object, used for attaching a detail item, offset to a reference point or axis.',
					'com': 'pfcls.pfcOffsetAttachment.1',
				},
			},
	}
	IpfcOId = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcIntegerOId': '',
				'IpfcStringOId': '',
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcParameter = {
			'type': 'Classes',
			'parent': {
				'IpfcBaseParameter': '',
				'IpfcNamedModelItem': '',
			},
			'child': {
			},
			'properties': {
				'Description': {
					'return': 'as String [optional]',
					'info': 'Parameter description string.',
				},
				'Units': {
					'return': 'IpfcUnit',
					'info': 'Units assigned to the parameter.',
				},
			},
			'method': {
				'Delete': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Deletes a parameter.',
				},
				'GetDriverType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcParameterDriverType',
					'info': 'Obtains the type of driver for a material parameter.',
				},
				'GetRestriction': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcParameterRestriction',
					'info': "Identifies if a parameter's value is restricted to a certain range or enumerated.",
				},
				'GetScaledValue': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcParamValue',
					'info': 'Gets the parameter value in the units of the parameter, instead of the units of the owner model.',
				},
				'Reorder': {
					'type': 'Sub',
					'args': 'ParamBefore as IpfcParameter [optional]',
					'return': '',
					'info': 'Reorders the given parameter to come just after the indicated parameter.   ',
				},
				'SetScaledValue': {
					'type': 'Sub',
					'args': 'value as IpfcParamValue, Units as IpfcUnit [optional]',
					'return': '',
					'info': 'Sets the parameter value in terms of the units provided, instead of using the units of the owner model.',
				},
			},
	}
	IpfcParameterEnumeration = {
			'type': 'Classes',
			'parent': {
				'IpfcParameterRestriction': '',
			},
			'child': {
			},
			'properties': {
				'PermittedValues': {
					'return': 'IpfcParamValues',
					'info': 'The permitted values.',
				},
			},
			'method': {
			},
	}
	IpfcParameterLimit = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Type': {
					'return': 'IpfcParameterLimitType',
					'info': 'The type of parameter limit.',
				},
				'Value': {
					'return': 'IpfcParamValue',
					'info': 'The value of the parameter.',
				},
			},
			'method': {
			},
	}
	IpfcParameterOwner = {
			'type': 'Classes',
			'parent': {
				'IpfcActionSource': '',
				'IpfcParent': '',
			},
			'child': {
				'IpfcModelItem': '',
				'IpfcModel': '',
				'IpfcMaterial': '',
			},
			'properties': {
			},
			'method': {
				'CreateParam': {
					'type': 'Function',
					'args': 'Name as String, Value as IpfcParamValue',
					'return': 'IpfcParameter',
					'info': 'Adds the specified parameter to the database and returns it.',
				},
				'CreateParamWithUnits': {
					'type': 'Function',
					'args': 'Name as String, Value as IpfcParamValue, Units as IpfcUnit',
					'return': 'IpfcParameter',
					'info': 'Adds the specified parameter to the database and initializes the handle. The parameter will be created with units.   ',
				},
				'GetParam': {
					'type': 'Function',
					'args': 'Name as String',
					'return': 'IpfcParameter',
					'info': 'Retrieves the parameter specified by name. ',
				},
				'ListParams': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcParameters',
					'info': 'Provides a list of parameters associated with the parameter owner.',
				},
				'SelectParam': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcParameter',
					'info': 'Enables a user to select parameters interactively from a list.',
				},
				'SelectParameters': {
					'type': 'Function',
					'args': 'Options as IpfcParameterSelectionOptions [optional]',
					'return': 'IpfcParameters',
					'info': 'Enables a user to select parameters interactively from a list displayed based on the parameter selection options specified.   ',
				},
			},
	}
	IpfcParameterRange = {
			'type': 'Classes',
			'parent': {
				'IpfcParameterRestriction': '',
			},
			'child': {
			},
			'properties': {
				'Maximum': {
					'return': 'IpfcParameterLimit',
					'info': 'The maximum parameter value limit.',
				},
				'Minimum': {
					'return': 'IpfcParameterLimit',
					'info': 'The minimum parameter value limit.',
				},
			},
			'method': {
			},
	}
	IpfcParameterRestriction = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcParameterEnumeration': '',
				'IpfcParameterRange': '',
			},
			'properties': {
				'Type': {
					'return': 'IpfcRestrictionType',
					'info': 'The type of parameter restriction.',
				},
			},
			'method': {
			},
	}
	IpfcParameterSelectionOptions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'AllowContextSelection': {
					'return': 'as Boolean [optional]',
					'info': 'Whether or not to allow parameter selection from multiple contexts, or only from the invoking parameter owner.The default is to allow selection only from the invoking parameter owner.',
				},
				'AllowMultipleSelections': {
					'return': 'as Boolean [optional]',
					'info': 'Whether or not to allow multiple parameters to be selected from the dialog box, or only a single parameter. By default, it is true, and allows selection of multiple parameters.',
				},
				'Contexts': {
					'return': 'IpfcParameterSelectionContexts',
					'info': 'The parameter selection contexts.The default indicates that parameters may be selected from any context.',
				},
				'SelectButtonLabel': {
					'return': 'as String [optional]',
					'info': 'Label for the select button.The default is null.',
				},
			},
			'method': {
				'CCpfcParameterSelectionOptions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcParameterSelectionOptions',
					'info': 'Creates a ParameterSelectionOptions object.',
					'com': 'pfcls.pfcParameterSelectionOptions.1',
				},
			},
	}
	IpfcParametricAttachment = {
			'type': 'Classes',
			'parent': {
				'IpfcAttachment': '',
			},
			'child': {
			},
			'properties': {
				'AttachedGeometry': {
					'return': 'IpfcSelection',
					'info': 'Selection representing the item to attach to. This should include the drawing model view. The attachment will occur at the selected parameters',
				},
			},
			'method': {
				'CCpfcParametricAttachment.Create': {
					'type': 'Function',
					'args': 'inAttachedGeometry as IpfcSelection',
					'return': 'IpfcParametricAttachment',
					'info': 'Creates a data object, used for attaching a detail item to an item.',
					'com': 'pfcls.pfcParametricAttachment.1',
				},
			},
	}
	IpfcParamOId = {
			'type': 'Classes',
			'parent': {
				'IpfcStringOId': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcParamOId.Create': {
					'type': 'Function',
					'args': 'Name as String',
					'return': 'IpfcParamOId',
					'info': 'Creates a parameter identifier object given the parameter name.',
					'com': 'pfcls.pfcParamOId.1',
				},
			},
	}
	IpfcParamValue = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'discr': {
					'return': 'IpfcParamValueType',
					'info': 'None',
				},
				'BoolValue': {
					'return': 'as Boolean',
					'info': 'If the parameter type is PARAM_BOOLEAN, this is a Boolean value. ',
				},
				'DoubleValue': {
					'return': 'as Double',
					'info': 'If the parameter type is PARAM_DOUBLE, this is a double value.',
				},
				'IntValue': {
					'return': 'as Long',
					'info': 'If the parameter type is PARAM_INTEGER, this is an integer value.',
				},
				'NoteId': {
					'return': 'as Long',
					'info': 'If the parameter type is PARAM_NOTE, this is a note identifier.',
				},
				'StringValue': {
					'return': 'as String',
					'info': 'If the parameter type is PARAM_STRING, this is a string value.',
				},
			},
			'method': {
			},
	}
	IpfcParaSolid3DExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExport3DInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcParaSolid3DExportInstructions.Create': {
					'type': 'Function',
					'args': 'inConfiguration as IpfcAssemblyConfiguration, inGeometry as IpfcGeometryFlags',
					'return': 'IpfcParaSolid3DExportInstructions',
					'info': 'Creates a new instructions object used to export a solid model to parasolid format.',
					'com': 'pfcls.pfcParaSolid3DExportInstructions.1',
				},
			},
	}
	IpfcParent = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
				'IpfcParameterOwner': '',
				'IpfcModelItemOwner': '',
				'IpfcViewOwner': '',
				'IpfcDisplay': '',
				'IpfcBaseSession': '',
			},
			'properties': {
			},
			'method': {
				'GetChild': {
					'type': 'Function',
					'args': 'Id as IpfcOId',
					'return': 'IpfcChild',
					'info': 'Retrieves the child of the specified owner as specified by the ID.',
				},
			},
	}
	IpfcPart = {
			'type': 'Classes',
			'parent': {
				'IpfcSolid': '',
			},
			'child': {
			},
			'properties': {
				'CurrentMaterial': {
					'return': 'IpfcMaterial',
					'info': 'The part material ',
				},
			},
			'method': {
				'CreateMaterial': {
					'type': 'Function',
					'args': 'Name as String',
					'return': 'IpfcMaterial',
					'info': 'Creates a material of the specified name (with default values)in the part. ',
				},
				'GetMaterial': {
					'type': 'Function',
					'args': 'Name as String',
					'return': 'IpfcMaterial',
					'info': 'Locates a material by name.',
				},
				'ListMaterials': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcMaterials',
					'info': 'Lists the materials that exist in the part.',
				},
				'RetrieveMaterial': {
					'type': 'Function',
					'args': 'FileName as String',
					'return': 'IpfcMaterial',
					'info': "Reads the named material properties from file andadds (or updates) the specified material name to the part'sdatabase.",
				},
			},
	}
	IpfcPDFExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExportInstructions': '',
			},
			'child': {
			},
			'properties': {
				'FilePath': {
					'return': 'as String',
					'info': 'The name of the output file.',
				},
				'Options': {
					'return': 'IpfcPDFOptions',
					'info': 'The PDF export options.  If null, the default values are used.',
				},
				'ProfilePath': {
					'return': 'as String [optional]',
					'info': '       The Profile path. Can be null Currently used only for drawings                If you specify non null Profile path the PDF export options will be ignored on call to IpfcModel.Export()',
				},
			},
			'method': {
				'CCpfcPDFExportInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcPDFExportInstructions',
					'info': 'Creates an instructions object used to export a solid model to a PDF file.   ',
					'com': 'pfcls.pfcPDFExportInstructions.1',
				},
			},
	}
	IpfcPDFOption = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'OptionType': {
					'return': 'IpfcPDFOptionType',
					'info': 'Type of option.',
				},
				'OptionValue': {
					'return': 'IpfcArgValue',
					'info': 'Value of option.',
				},
			},
			'method': {
				'CCpfcPDFOption.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcPDFOption',
					'info': 'Creates an object used to define the PDF file option.   ',
					'com': 'pfcls.pfcPDFOption.1',
				},
			},
	}
	IpfcPlane = {
			'type': 'Classes',
			'parent': {
				'IpfcTransformedSurface': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcPlaneDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcTransformedSurfaceDescriptor': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcPlaneDescriptor.Create': {
					'type': 'Function',
					'args': 'Extents as IpfcSurfaceExtents, Orientation as IpfcSurfaceOrientation, Origin as IpfcTransform3D',
					'return': 'IpfcPlaneDescriptor',
					'info': 'Method to create a set of information describing a planar surface.',
					'com': 'pfcls.pfcPlaneDescriptor.1',
				},
			},
	}
	IpfcPlotInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExportInstructions': '',
			},
			'child': {
			},
			'properties': {
				'FirstPage': {
					'return': 'as Long [optional]',
					'info': 'When PageRangeChoice is PlotPageRange.PLOT_RANGE_OF_PAGES, this specifies the first page to print. Otherwise, the value is null ',
				},
				'LabelPlot': {
					'return': 'as Boolean',
					'info': 'If set to true , generates the plot with a label. Default is false; no label is created.',
				},
				'LastPage': {
					'return': 'as Long [optional]',
					'info': 'When PageRangeChoice is PlotPageRange.PLOT_RANGE_OF_PAGES, this specifies the last page to print. Otherwise, the value is null ',
				},
				'OutputQuality': {
					'return': 'as Long',
					'info': 'A value of 0, 1, 2, or 3. Default is 1. Defines the amount of checking for overlapping lines in a plot or 2-D export file, such as IGES, before making a file. The values are interpreted as follows: 0-Does not check for overlapping lines or collect lines of the same pen color. 1-Does not check for overlapping lines, but collects lines of the same pen color for plotting. 2-Partially checks edges with two vertices, and collects lines of the same pen color for plotting. 3-Does a complete check of all edges against each other, regardless of the number of vertices, font, or color. Collects lines of the same pen color for plotting.',
				},
				'PageRangeChoice': {
					'return': 'IpfcPlotPageRange',
					'info': 'One of the IpfcPlotPageRange enumeration objects. Default is PlotPageRange.PLOT_RANGE_ALL.',
				},
				'PaperSize': {
					'return': 'IpfcPlotPaperSize',
					'info': 'One of the IpfcPlotPaperSize enumeration objects. Default is PlotPaperSize.ASIZEPLOT.',
				},
				'PaperSizeX': {
					'return': 'as Double [optional]',
					'info': 'When PaperSize is PlotPaperSize.VARIABLEPLOTSIZE, this specifies the size of the plotter paper in the X dimension. Otherwise, the value is null',
				},
				'PaperSizeY': {
					'return': 'as Double [optional]',
					'info': 'When PaperSize is PlotPaperSize.VARIABLEPLOTSIZE, this specifies the size of the plotter paper in the Y dimension. Otherwise, the value is null. ',
				},
				'PenSlew': {
					'return': 'as Boolean',
					'info': 'Set to true if you want to adjust pen velocity. Default is false. ',
				},
				'PenVelocityX': {
					'return': 'as Double',
					'info': 'When PenSlew is true, this value is a multiple of the default pen speed in the X dimension. Permitted range is 0.1 to 100. Ignored when PenSlew is false.',
				},
				'PenVelocityY': {
					'return': 'as Double',
					'info': 'When PenSlew is true, this value is a multiple of the default pen speed in the y dimension. Permitted range is 0.1 to 100. Ignored when PenSlew is false.',
				},
				'PlotterName': {
					'return': 'as String',
					'info': 'c.f. The print dialog for support names	    eg: POSTSCRIPT, COLORPOSTSC   ',
				},
				'SegmentedOutput': {
					'return': 'as Boolean',
					'info': 'Set to true to generate a segmented plot. Default is false.This may be true only if you are plotting a single page.',
				},
				'SeparatePlotFiles': {
					'return': 'as Boolean',
					'info': 'Defines the default in the Print to File dialog box.true-Sets the default to Create Separate Files. false -A single file is created by default.',
				},
				'UserScale': {
					'return': 'as Double',
					'info': 'Specifies a scale factor between 0.01 and 100 for scaling a model or drawing for plotting. Default is 0.01.',
				},
			},
			'method': {
				'CCpfcPlotInstructions.Create': {
					'type': 'Function',
					'args': 'PlotterName as String',
					'return': 'IpfcPlotInstructions',
					'info': 'Creates a new instructions object used to plot a part, drawing, or assembly.',
					'com': 'pfcls.pfcPlotInstructions.1',
				},
			},
	}
	IpfcPoint = {
			'type': 'Classes',
			'parent': {
				'IpfcCurve': '',
			},
			'child': {
			},
			'properties': {
				'Point': {
					'return': 'IpfcPoint3D',
					'info': 'The coordinates of the point',
				},
			},
			'method': {
			},
	}
	IpfcPointDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcCurveDescriptor': '',
			},
			'child': {
			},
			'properties': {
				'Point': {
					'return': 'IpfcPoint3D',
					'info': 'The point coordinates',
				},
			},
			'method': {
				'CCpfcPointDescriptor.Create': {
					'type': 'Function',
					'args': 'Point as IpfcPoint3D',
					'return': 'IpfcPointDescriptor',
					'info': 'This method creates a new PointDescriptor object.',
					'com': 'pfcls.pfcPointDescriptor.1',
				},
			},
	}
	IpfcPointDimensionSense = {
			'type': 'Classes',
			'parent': {
				'IpfcDimensionSense': '',
			},
			'child': {
			},
			'properties': {
				'PointType': {
					'return': 'IpfcDimensionPointType',
					'info': 'The type of point referred to.',
				},
			},
			'method': {
				'CCpfcPointDimensionSense.Create': {
					'type': 'Function',
					'args': 'PointType as IpfcDimensionPointType',
					'return': 'IpfcPointDimensionSense',
					'info': 'Creates a new point dimension sense object for use in creating a drawing dimension.',
					'com': 'pfcls.pfcPointDimensionSense.1',
				},
			},
	}
	IpfcPointDimSense = {
			'type': 'Classes',
			'parent': {
				'IpfcDimSense': '',
			},
			'child': {
			},
			'properties': {
				'PointType': {
					'return': 'IpfcDimPointType',
					'info': '  ',
				},
			},
			'method': {
				'CCpfcPointDimSense.Create': {
					'type': 'Function',
					'args': 'PointType as IpfcDimPointType',
					'return': 'IpfcPointDimSense',
					'info': '  ',
					'com': 'pfcls.pfcPointDimSense.1',
				},
			},
	}
	IpfcPointToAngleDimensionSense = {
			'type': 'Classes',
			'parent': {
				'IpfcDimensionSense': '',
			},
			'child': {
			},
			'properties': {
				'AngleOptions': {
					'return': 'IpfcDimensionAngleOptions',
					'info': 'The flags determining the location of the angle dimension.',
				},
				'PointType': {
					'return': 'IpfcDimensionPointType',
					'info': 'The type of point attachment.',
				},
			},
			'method': {
				'CCpfcPointToAngleDimensionSense.Create': {
					'type': 'Function',
					'args': 'PointType as IpfcDimensionPointType, AngleOptions as IpfcDimensionAngleOptions [optional]',
					'return': 'IpfcPointToAngleDimensionSense',
					'info': 'Creates a new point-to-angle dimension sense object for use in creating a drawing dimension.',
					'com': 'pfcls.pfcPointToAngleDimensionSense.1',
				},
			},
	}
	IpfcPointToAngleDimSense = {
			'type': 'Classes',
			'parent': {
				'IpfcDimSense': '',
			},
			'child': {
			},
			'properties': {
				'Options': {
					'return': 'IpfcAngularDimOptions',
					'info': '  ',
				},
				'PointType': {
					'return': 'IpfcDimPointType',
					'info': '  ',
				},
			},
			'method': {
				'CCpfcPointToAngleDimSense.Create': {
					'type': 'Function',
					'args': 'PointType as IpfcDimPointType, Options as IpfcAngularDimOptions [optional]',
					'return': 'IpfcPointToAngleDimSense',
					'info': '  ',
					'com': 'pfcls.pfcPointToAngleDimSense.1',
				},
			},
	}
	IpfcPolygon = {
			'type': 'Classes',
			'parent': {
				'IpfcCurve': '',
			},
			'child': {
			},
			'properties': {
				'Vertices': {
					'return': 'IpfcPoint3Ds',
					'info': 'The vertices of the polygon ',
				},
			},
			'method': {
			},
	}
	IpfcPolygonDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcCurveDescriptor': '',
			},
			'child': {
			},
			'properties': {
				'Vertices': {
					'return': 'IpfcPoint3Ds',
					'info': 'Array of the vertices of the polygon.',
				},
			},
			'method': {
				'CCpfcPolygonDescriptor.Create': {
					'type': 'Function',
					'args': 'Vertices as IpfcPoint3Ds',
					'return': 'IpfcPolygonDescriptor',
					'info': 'This method creates a new PolygonDescriptor object.',
					'com': 'pfcls.pfcPolygonDescriptor.1',
				},
			},
	}
	IpfcPopupmenu = {
			'type': 'Classes',
			'parent': {
				'IpfcChild': '',
			},
			'child': {
			},
			'properties': {
				'Name': {
					'return': 'as String',
					'info': 'Name of the popup menu.',
				},
			},
			'method': {
				'AddButton': {
					'type': 'Sub',
					'args': 'Command as IpfcUICommand, Options as IpfcPopupmenuOptions',
					'return': '',
					'info': 'Add a new item to a Creo Parametric popup menu.   ',
				},
				'AddMenu': {
					'type': 'Function',
					'args': 'Options as IpfcPopupmenuOptions',
					'return': 'IpfcPopupmenu',
					'info': 'Adds a cascade button to the popup menu.   ',
				},
			},
	}
	IpfcPopupmenuListener = {
			'type': 'Classes',
			'parent': {
				'IpfcActionListener': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'OnPopupmenuCreate': {
					'type': 'Sub',
					'args': 'Menu as IpfcPopupmenu',
					'return': '',
					'info': 'This notification function is called after a popup menu is created internally in Creo Parametric. Use this notification to assign application-specific buttons to the popup menu.   ',
				},
			},
	}
	IpfcPopupmenuOptions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Helptext': {
					'return': 'as String [optional]',
					'info': 'The button or the cascade menu helptext. If null, help text is assigned as the item label.   ',
				},
				'Label': {
					'return': 'as String [optional]',
					'info': 'The menu button or cascade menu label. If null, label is assigned as the internal name.   ',
				},
				'Name': {
					'return': 'as String',
					'info': 'Internal name of the new popup menu or cascade button.   ',
				},
				'PositionIndex': {
					'return': 'as Long [optional]',
					'info': 'Position at which to add the menu or cascade button. If position null, the position will be at the end of the menu. ',
				},
			},
			'method': {
				'CCpfcPopupmenuOptions.Create': {
					'type': 'Function',
					'args': 'Name as String',
					'return': 'IpfcPopupmenuOptions',
					'info': 'Creates a class object for popup menu options.   ',
					'com': 'pfcls.pfcPopupmenuOptions.1',
				},
			},
	}
	IpfcPrinterInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExportInstructions': '',
			},
			'child': {
			},
			'properties': {
				'ModelOption': {
					'return': 'IpfcPrintMdlOption',
					'info': 'To define the model options for printing purpose.',
				},
				'PlacementOption': {
					'return': 'IpfcPrintPlacementOption',
					'info': 'To define the placement options for printing purpose.',
				},
				'PrinterOption': {
					'return': 'IpfcPrintPrinterOption',
					'info': 'To define the printer settings for printing a file.',
				},
				'WindowId': {
					'return': 'as Long',
					'info': 'Current window identifier.',
				},
			},
			'method': {
				'CCpfcPrinterInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcPrinterInstructions',
					'info': 'Creates an object used to define the printer instructions for printing a file.   ',
					'com': 'pfcls.pfcPrinterInstructions.1',
				},
			},
	}
	IpfcPrinterPCFOptions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'ModelOption': {
					'return': 'IpfcPrintMdlOption',
					'info': 'To define the model options for printing purpose.',
				},
				'PlacementOption': {
					'return': 'IpfcPrintPlacementOption',
					'info': 'To define the placement options for printing purpose.',
				},
				'PrinterOption': {
					'return': 'IpfcPrintPrinterOption',
					'info': 'To define the printer settings for printing purpose.',
				},
			},
			'method': {
				'CCpfcPrinterPCFOptions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcPrinterPCFOptions',
					'info': 'Creates an object used to define the printer options for printing a PFC file.   ',
					'com': 'pfcls.pfcPrinterPCFOptions.1',
				},
			},
	}
	IpfcPrintMdlOption = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'DrawFormat': {
					'return': 'as Boolean',
					'info': 'true to display drawing format; false to skip drawing format.',
				},
				'FirstPage': {
					'return': 'as Long',
					'info': 'First Page number.',
				},
				'LastPage': {
					'return': 'as Long',
					'info': 'Last page number.',
				},
				'LayerName': {
					'return': 'as String',
					'info': 'Name of the layer.',
				},
				'LayerOnly': {
					'return': 'as Boolean',
					'info': 'true to display layer only; false to skip layer-only display.',
				},
				'Mdl': {
					'return': 'IpfcModel',
					'info': 'The object of the model to be printed.',
				},
				'Quality': {
					'return': 'as Long',
					'info': 'Quality value for print.',
				},
				'Segmented': {
					'return': 'as Boolean',
					'info': 'true to display segmented; false to skip segmentation.',
				},
				'Sheets': {
					'return': 'IpfcPrintSheets',
					'info': '  ',
				},
				'UseDrawingSize': {
					'return': 'as Boolean',
					'info': 'true to use drawing size; false to skip drawing size.',
				},
				'UseSolidScale': {
					'return': 'as Boolean',
					'info': 'true to use solid scale; false to skip solid scale.',
				},
			},
			'method': {
				'CCpfcPrintMdlOption.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcPrintMdlOption',
					'info': 'Creates an object used to define the model options for printing purpose.   ',
					'com': 'pfcls.pfcPrintMdlOption.1',
				},
			},
	}
	IpfcPrintPlacementOption = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'BottomOffset': {
					'return': 'as Double',
					'info': 'Offset from the bottom.',
				},
				'ClipPlot': {
					'return': 'as Boolean',
					'info': 'true to clip plot; false to skip clipping of plot.',
				},
				'KeepPanzoom': {
					'return': 'as Boolean',
					'info': 'true to keep pan zoom; false to skip pan zoom.',
				},
				'LabelHeight': {
					'return': 'as Double',
					'info': 'Height of label.',
				},
				'PlaceLabel': {
					'return': 'as Boolean',
					'info': 'true to place label; false to skip placing of label.',
				},
				'Scale': {
					'return': 'as Double',
					'info': 'Scale value of content.',
				},
				'ShiftAllCorner': {
					'return': 'as Boolean',
					'info': 'true to shift all corners; false to skip shifting of corners.',
				},
				'SideOffset': {
					'return': 'as Double',
					'info': 'Offset from the sides.',
				},
				'X1ClipPosition': {
					'return': 'as Double',
					'info': 'First X paramter for defining clip position.',
				},
				'X2ClipPosition': {
					'return': 'as Double',
					'info': 'Second X paramter for defining clip position.',
				},
				'Y1ClipPosition': {
					'return': 'as Double',
					'info': 'First Y paramter for defining clip position.',
				},
				'Y2ClipPosition': {
					'return': 'as Double',
					'info': 'Second Y paramter for defining clip position.',
				},
			},
			'method': {
				'CCpfcPrintPlacementOption.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcPrintPlacementOption',
					'info': 'Creates an object used to define the placement options for printing purpose.   ',
					'com': 'pfcls.pfcPrintPlacementOption.1',
				},
			},
	}
	IpfcPrintPrinterOption = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'DeleteAfter': {
					'return': 'as Boolean',
					'info': 'true to delete file after printing; false to not delete file.',
				},
				'FileName': {
					'return': 'as String',
					'info': 'Name of a file where the print should go.',
				},
				'PaperSize': {
					'return': 'IpfcPrintSize',
					'info': 'Size of paper to be printed.',
				},
				'PenTable': {
					'return': 'as String',
					'info': 'File containing pen table',
				},
				'PrintCommand': {
					'return': 'as String',
					'info': 'Print command.',
				},
				'PrinterType': {
					'return': 'as String',
					'info': 'Type of printer.',
				},
				'Quantity': {
					'return': 'as Long',
					'info': 'Number of papers to be printed.',
				},
				'RollMedia': {
					'return': 'as Boolean',
					'info': 'true to use roll media for printing; false to not use roll media.',
				},
				'RotatePlot': {
					'return': 'as Boolean',
					'info': 'true to rotate the plot; false to skip rotating plot.',
				},
				'SaveMethod': {
					'return': 'IpfcPrintSaveMethod',
					'info': 'Save method.',
				},
				'SaveToFile': {
					'return': 'as Boolean',
					'info': 'true to save file after print; false to skip file saving.',
				},
				'SendToPrinter': {
					'return': 'as Boolean',
					'info': 'true to send file to printer; false to not send file to printer.',
				},
				'Slew': {
					'return': 'as Double',
					'info': 'Slew value',
				},
				'SwHandshake': {
					'return': 'as Boolean',
					'info': 'true to use handshake method for printing; false to not use handshake method.',
				},
				'UseTtf': {
					'return': 'as Boolean',
					'info': 'true to use TTF support for printing; false to stroke all text.',
				},
			},
			'method': {
				'CCpfcPrintPrinterOption.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcPrintPrinterOption',
					'info': 'Creates an object used to define the printer settings for printing a file.   ',
					'com': 'pfcls.pfcPrintPrinterOption.1',
				},
			},
	}
	IpfcPrintSize = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Height': {
					'return': 'as Double',
					'info': 'Height of paper.',
				},
				'PaperSize': {
					'return': 'IpfcPlotPaperSize',
					'info': 'Size of paper.',
				},
				'Width': {
					'return': 'as Double',
					'info': 'Width of paper.',
				},
			},
			'method': {
				'CCpfcPrintSize.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcPrintSize',
					'info': 'Creates an object used to define the paper parameters for print  option.   ',
					'com': 'pfcls.pfcPrintSize.1',
				},
			},
	}
	IpfcProductViewExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExportInstructions': '',
			},
			'child': {
			},
			'properties': {
				'PVExportOptions': {
					'return': 'IpfcProductViewExportOptions',
					'info': '  ',
				},
			},
			'method': {
				'CCpfcProductViewExportInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcProductViewExportInstructions',
					'info': 'Creates a new instructions object for exporting a part, assembly    or drawing into ProductView format.',
					'com': 'pfcls.pfcProductViewExportInstructions.1',
				},
			},
	}
	IpfcProductViewExportOptions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'PVFormat': {
					'return': 'IpfcProductViewFormat',
					'info': '  ',
				},
			},
			'method': {
				'CCpfcProductViewExportOptions.Create': {
					'type': 'Function',
					'args': 'inPVFormat as IpfcProductViewFormat',
					'return': 'IpfcProductViewExportOptions',
					'info': ' Set options for ProductView export format',
					'com': 'pfcls.pfcProductViewExportOptions.1',
				},
			},
	}
	IpfcProgramExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcProgramExportInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcProgramExportInstructions',
					'info': 'Creates a new instructions object used to export a program file for a part or assembly, which can be edited to change the model.',
					'com': 'pfcls.pfcProgramExportInstructions.1',
				},
			},
	}
	IpfcProgramImportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcImportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcProgramImportInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcProgramImportInstructions',
					'info': 'Creates a new instructions object used to import from a program file.',
					'com': 'pfcls.pfcProgramImportInstructions.1',
				},
			},
	}
	IpfcProjectionViewCreateInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcView2DCreateInstructions': '',
			},
			'child': {
			},
			'properties': {
				'Exploded': {
					'return': 'as Boolean [optional]',
					'info': 'true if the view should be exploded, null or false otherwise.',
				},
				'Location': {
					'return': 'IpfcPoint3D',
					'info': 'The location for the projected view.  The view location determines how the view will be oriented.',
				},
				'ParentView': {
					'return': 'IpfcView2D',
					'info': 'The parent view for the projected view.',
				},
			},
			'method': {
				'CCpfcProjectionViewCreateInstructions.Create': {
					'type': 'Function',
					'args': 'ParentView as IpfcView2D, Location as IpfcPoint3D',
					'return': 'IpfcProjectionViewCreateInstructions',
					'info': 'Creates a data object used for creating projected drawing views.',
					'com': 'pfcls.pfcProjectionViewCreateInstructions.1',
				},
			},
	}
	IpfcQuilt = {
			'type': 'Classes',
			'parent': {
				'IpfcModelItem': '',
			},
			'child': {
			},
			'properties': {
				'IsVisible': {
					'return': 'as Boolean',
					'info': 'true if the geometry is visible and active, false if it is invisible and inactive.  Inactive geometry may not have all geometric properties defined.',
				},
			},
			'method': {
				'GetFeature': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcFeature',
					'info': 'Returns the feature which contains the geometry.',
				},
				'ListElements': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSurfaces',
					'info': 'Retrieves an array of surfaces in the quilt.',
				},
			},
	}
	IpfcRasterImageExportInstructions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcBitmapImageExportInstructions': '',
				'IpfcTIFFImageExportInstructions': '',
				'IpfcEPSImageExportInstructions': '',
				'IpfcJPEGImageExportInstructions': '',
			},
			'properties': {
				'DotsPerInch': {
					'return': 'IpfcDotsPerInch',
					'info': 'The dots per inch of the output image.',
				},
				'ImageDepth': {
					'return': 'IpfcRasterDepth',
					'info': 'The depth of the output image.',
				},
				'ImageHeight': {
					'return': 'as Double',
					'info': 'The height of the output image in inches.',
				},
				'ImageWidth': {
					'return': 'as Double',
					'info': 'The width of the output image in inches.',
				},
			},
			'method': {
				'GetRasterType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcRasterType',
					'info': 'Returns a RasterType instance that indicates the type of the raster image export instructions.',
				},
			},
	}
	IpfcRefDimension = {
			'type': 'Classes',
			'parent': {
				'IpfcBaseDimension': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcRegenInstructions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'AllowFixUI': {
					'return': 'as Boolean',
					'info': 'If this is true, the Fix Model interface is displayed if there is an error. ',
				},
				'ForceRegen': {
					'return': 'as Boolean',
					'info': 'Forces the solid to fully regenerate. This will regenerate every feature in the solid. If not set, Creo Parametric uses its internal algorithm to determine which features to regenerate.',
				},
				'FromFeat': {
					'return': 'IpfcFeature',
					'info': 'Not used. Reserved for the future.',
				},
				'RefreshModelTree': {
					'return': 'as Boolean',
					'info': 'If this is true, refresh the Creo Parametric Model Tree after the regeneration.  If this is false (the default) the tree is not refreshed.  A model must be active to use this attribute. ',
				},
				'ResumeExcludedComponents': {
					'return': 'as Boolean',
					'info': 'Enables Creo Parametric to resume available excluded components of the simplified representation during regeneration. This can result in a more accurate update of the simplified representation.',
				},
				'UpdateAssemblyOnly': {
					'return': 'as Boolean',
					'info': 'Updates assembly and sub-assembly placements and regenerates assembly features and intersected parts. If the affected assembly is retrieved as a simplified representation, this flag will update the locations of the components.If the flag is not set, the component locations are not updated by default when the simplified representation is retrieved.',
				},
				'UpdateInstances': {
					'return': 'as Boolean',
					'info': 'Updates instances of the solid in memory. This may slow down the regeneration process.',
				},
			},
			'method': {
				'CCpfcRegenInstructions.Create': {
					'type': 'Function',
					'args': 'AllowFixUI as Boolean [optional], ForceRegen as Boolean [optional], FromFeat as IpfcFeature [optional]',
					'return': 'IpfcRegenInstructions',
					'info': 'Creates a new RegenInstructions object. ',
					'com': 'pfcls.pfcRegenInstructions.1',
				},
			},
	}
	IpfcRelationExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcRelationExportInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcRelationExportInstructions',
					'info': 'Creates a new instructions object used to export a list of the relations and parameters in a part or assembly.',
					'com': 'pfcls.pfcRelationExportInstructions.1',
				},
			},
	}
	IpfcRelationFunctionArgument = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'IsOptional': {
					'return': 'as Boolean [optional]',
					'info': 'Specifies whether argument is optional,   indicating that the argument does not always need to be suppiled in the   relation.  Optional arguments must come at the end of   the argument list. Default is false.',
				},
				'Type': {
					'return': 'IpfcParamValueType',
					'info': 'Parameter value type of argument. ',
				},
			},
			'method': {
				'CCpfcRelationFunctionArgument.Create': {
					'type': 'Function',
					'args': 'Type as IpfcParamValueType',
					'return': 'IpfcRelationFunctionArgument',
					'info': 'Create a relation function argument object.',
					'com': 'pfcls.pfcRelationFunctionArgument.1',
				},
			},
	}
	IpfcRelationFunctionListener = {
			'type': 'Classes',
			'parent': {
				'IpfcActionListener': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'AssignValue': {
					'type': 'Sub',
					'args': 'Owner as IpfcRelationOwner, FunctionName as String, Arguments as IpfcParamValues, Assignment as IpfcParamValue',
					'return': '',
					'info': 'Function called to evaluate a custom relation function invoked    on the left hand side of a relation.',
				},
				'CheckArguments': {
					'type': 'Function',
					'args': 'Owner as IpfcRelationOwner, FunctionName as String, Arguments as IpfcParamValues',
					'return': ' as Boolean',
					'info': 'To check the validity of arguments of external function.',
				},
				'EvaluateFunction': {
					'type': 'Function',
					'args': 'Owner as IpfcRelationOwner, FunctionName as String, Arguments as IpfcParamValues',
					'return': 'IpfcParamValue',
					'info': 'Function called to evaluate a custom relation function invoked    on the right hand side of a relation.',
				},
			},
	}
	IpfcRelationFunctionOptions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'ArgumentTypes': {
					'return': 'IpfcRelationFunctionArguments',
					'info': 'The types of arguments permitted  by the relation function. Default is null, indicating that no  arguments are permitted.',
				},
				'EnableArgumentCheckMethod': {
					'return': 'as Boolean [optional]',
					'info': ' Turn the method (using  IpfcRelationFunctionListener.CheckArguments())  on/off. Default is false.',
				},
				'EnableExpressionEvaluationMethod': {
					'return': 'as Boolean [optional]',
					'info': ' Turn the method (using  IpfcRelationFunctionListener.EvaluateFunction()) on/off. Default is true.',
				},
				'EnableTypeChecking': {
					'return': 'as Boolean [optional]',
					'info': 'Turn argument type  checking (using  IpfcRelationFunctionOptions.ArgumentTypes)  on/off.  Default is false.',
				},
				'EnableValueAssignmentMethod': {
					'return': 'as Boolean [optional]',
					'info': ' Turn IpfcRelationFunctionListener.AssignValue() on/off. Default is false.',
				},
			},
			'method': {
				'CCpfcRelationFunctionOptions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcRelationFunctionOptions',
					'info': 'Create a  IpfcRelationFunctionOptions object. ',
					'com': 'pfcls.pfcRelationFunctionOptions.1',
				},
			},
	}
	IpfcRelationImportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcImportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcRelationImportInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcRelationImportInstructions',
					'info': 'Creates a new instructions object used to import (read) a list of the relations and parameters of a part or assembly from RELATION type file.',
					'com': 'pfcls.pfcRelationImportInstructions.1',
				},
			},
	}
	IpfcRelationOwner = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
				'IpfcModelItem': '',
				'IpfcModel': '',
			},
			'properties': {
				'Relations': {
					'return': 'Istringseq',
					'info': 'The list of the actual relations assigned to the item. ',
				},
			},
			'method': {
				'DeleteRelations': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Deletes all relations assigned to the item.',
				},
				'EvaluateExpression': {
					'type': 'Function',
					'args': 'Expression as String',
					'return': 'IpfcParamValue',
					'info': 'Use Creo Parametric to evaluate the given relations-basedexpression.',
				},
				'RegenerateRelations': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Regenerates the relations assigned to the item.',
				},
			},
	}
	IpfcRenderExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcCoordSysExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcRenderExportInstructions.Create': {
					'type': 'Function',
					'args': 'CsysName as String [optional]',
					'return': 'IpfcRenderExportInstructions',
					'info': 'Creates a new instructions object used to export a part or assembly in RENDER format.',
					'com': 'pfcls.pfcRenderExportInstructions.1',
				},
			},
	}
	IpfcReorderAfterOperation = {
			'type': 'Classes',
			'parent': {
				'IpfcFeatureOperation': '',
			},
			'child': {
			},
			'properties': {
				'AfterFeat': {
					'return': 'IpfcFeature',
					'info': 'The feature after which you want the features reordered',
				},
			},
			'method': {
			},
	}
	IpfcReorderBeforeOperation = {
			'type': 'Classes',
			'parent': {
				'IpfcFeatureOperation': '',
			},
			'child': {
			},
			'properties': {
				'BeforeFeat': {
					'return': 'IpfcFeature',
					'info': 'The feature before which you want to reorder the features',
				},
			},
			'method': {
			},
	}
	IpfcReport = {
			'type': 'Classes',
			'parent': {
				'IpfcModel2D': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcResumeOperation = {
			'type': 'Classes',
			'parent': {
				'IpfcFeatureOperation': '',
			},
			'child': {
			},
			'properties': {
				'WithParents': {
					'return': 'as Boolean',
					'info': 'A Boolean flag that specifies whether to resume the parents of the feature',
				},
			},
			'method': {
			},
	}
	IpfcRetrieveExistingSimpRepInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcSimpRepInstructions': '',
			},
			'child': {
			},
			'properties': {
				'ExistSimpName': {
					'return': 'as String',
					'info': 'Name of the existing representation.',
				},
			},
			'method': {
				'CCpfcRetrieveExistingSimpRepInstructions.Create': {
					'type': 'Function',
					'args': 'ExistSimpName as String',
					'return': 'IpfcRetrieveExistingSimpRepInstructions',
					'info': 'Creates a new object used to retrieve an existing simplified representation.',
					'com': 'pfcls.pfcRetrieveExistingSimpRepInstructions.1',
				},
			},
	}
	IpfcRetrieveModelOptions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'AskUserAboutReps': {
					'return': 'as Boolean',
					'info': '  ',
				},
			},
			'method': {
				'CCpfcRetrieveModelOptions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcRetrieveModelOptions',
					'info': 'Creates a class object to hold the retrieve model information.',
					'com': 'pfcls.pfcRetrieveModelOptions.1',
				},
			},
	}
	IpfcRevolvedSurface = {
			'type': 'Classes',
			'parent': {
				'IpfcTransformedSurface': '',
			},
			'child': {
			},
			'properties': {
				'Profile': {
					'return': 'IpfcCurveDescriptor',
					'info': 'The curve geometry',
				},
			},
			'method': {
			},
	}
	IpfcRevolvedSurfaceDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcTransformedSurfaceDescriptor': '',
			},
			'child': {
			},
			'properties': {
				'Profile': {
					'return': 'IpfcCurveDescriptor',
					'info': 'The curve geometry',
				},
			},
			'method': {
				'CCpfcRevolvedSurfaceDescriptor.Create': {
					'type': 'Function',
					'args': 'Extents as IpfcSurfaceExtents, Orientation as IpfcSurfaceOrientation, Origin as IpfcTransform3D, Profile as IpfcCurveDescriptor',
					'return': 'IpfcRevolvedSurfaceDescriptor',
					'info': 'This method creates a new RevolvedSurfaceDescriptor object.',
					'com': 'pfcls.pfcRevolvedSurfaceDescriptor.1',
				},
			},
	}
	IpfcRoundFeat = {
			'type': 'Classes',
			'parent': {
				'IpfcFeature': '',
			},
			'child': {
			},
			'properties': {
				'IsAutoRoundMember': {
					'return': 'as Boolean',
					'info': 'Determines whether the specified round feature is a member   of an Auto Round Feature.',
				},
			},
			'method': {
			},
	}
	IpfcRuledSurface = {
			'type': 'Classes',
			'parent': {
				'IpfcTransformedSurface': '',
			},
			'child': {
			},
			'properties': {
				'Profile1': {
					'return': 'IpfcCurveDescriptor',
					'info': "The first curve's geometry",
				},
				'Profile2': {
					'return': 'IpfcCurveDescriptor',
					'info': "The second curve's geometry",
				},
			},
			'method': {
			},
	}
	IpfcRuledSurfaceDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcTransformedSurfaceDescriptor': '',
			},
			'child': {
			},
			'properties': {
				'Profile1': {
					'return': 'IpfcCurveDescriptor',
					'info': "The first curve's geometry",
				},
				'Profile2': {
					'return': 'IpfcCurveDescriptor',
					'info': "The second curve's geometry",
				},
			},
			'method': {
				'CCpfcRuledSurfaceDescriptor.Create': {
					'type': 'Function',
					'args': 'Extents as IpfcSurfaceExtents, Orientation as IpfcSurfaceOrientation, Origin as IpfcTransform3D, Profile1 as IpfcCurveDescriptor, Profile2 as IpfcCurveDescriptor',
					'return': 'IpfcRuledSurfaceDescriptor',
					'info': 'This method creates a new RuledSurfaceDescriptor object.',
					'com': 'pfcls.pfcRuledSurfaceDescriptor.1',
				},
			},
	}
	IpfcScreenTransform = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'PanX': {
					'return': 'as Double [optional]',
					'info': 'A value between -1.0 and 1.0 that specifies the amount of horizontal panning. -1.0 pans one window width to the left; 1.0 pans one window width to the right. 0 specifies no horizontal panning. This attribute can be null, in which case 0 is used.',
				},
				'PanY': {
					'return': 'as Double [optional]',
					'info': 'A value between -1.0 and 1.0 that specifies the amount of vertical panning. -1.0 pans up by the height of the window; 1.0 pans down by the height of the window. 0 specifies no vertical panning. This attribute can be null, in which case 0 is used.',
				},
				'Zoom': {
					'return': 'as Double [optional]',
					'info': 'A value greater than 0 and the less than or equal to the value of the Creo Parametric variable MAX_ZOOM_SCALE that specifies the zoom factor. The value 0.1, for example, would zoom out by a factor of 10. This attribute can be null, in which case 1 (no zoom) is used.',
				},
			},
			'method': {
				'CCpfcScreenTransform.Create': {
					'type': 'Function',
					'args': 'PanX as Double [optional], PanY as Double [optional], Zoom as Double [optional]',
					'return': 'IpfcScreenTransform',
					'info': 'Creates a new ScreenTransform object.',
					'com': 'pfcls.pfcScreenTransform.1',
				},
			},
	}
	IpfcSection2D = {
			'type': 'Classes',
			'parent': {
				'IpfcModel': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcSelection = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
			},
			'properties': {
				'Depth': {
					'return': 'as Double',
					'info': 'The depth of the selection',
				},
				'Params': {
					'return': 'IpfcUVParams',
					'info': 'The UV parameters',
				},
				'Path': {
					'return': 'IpfcComponentPath',
					'info': 'The component path',
				},
				'Point': {
					'return': 'IpfcPoint3D',
					'info': 'The point',
				},
				'SelItem': {
					'return': 'IpfcModelItem',
					'info': 'The selected model item',
				},
				'SelModel': {
					'return': 'IpfcModel',
					'info': 'The selected model',
				},
				'SelTableCell': {
					'return': 'IpfcTableCell',
					'info': 'The table cell, or null, if the selection does not represent a drawing table cell selection.',
				},
				'SelTableSegment': {
					'return': 'as Long [readonly, optional]',
					'info': 'The drawing table segment, or null, if the selection does not contain a drawing table.',
				},
				'SelView2D': {
					'return': 'IpfcView2D',
					'info': 'The drawing view, or null, if the selection does not contain a drawing view.',
				},
				'TParam': {
					'return': 'as Double [optional]',
					'info': 'The T parameter of an edge or curve',
				},
			},
			'method': {
				'Display': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Displays the selection. Note that this is a one-time action(the next repaint will erase this display).',
				},
				'Highlight': {
					'type': 'Sub',
					'args': 'Color as IpfcStdColor',
					'return': '',
					'info': 'Highlights the selection in the current window.The selection remains highlighted until you call the Unhighlight() function.',
				},
				'SetIntf3DCsys': {
					'type': 'Sub',
					'args': 'ReferenceCsys as IpfcSelection [optional]',
					'return': '',
					'info': '       Set reference CSYS for export file.       User should conver IpfcModel to         IpfcSelection and then call this method on it.       Use method pfcCreateModelSelection() to get        IpfcSelection from IpfcModel',
				},
				'UnHighlight': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Removes the highlighting from the selection highlighted by a previous call to the Highlight() function.',
				},
			},
	}
	IpfcSelectionBuffer = {
			'type': 'Classes',
			'parent': {
				'IpfcChild': '',
			},
			'child': {
			},
			'properties': {
				'Contents': {
					'return': 'IpfcSelections',
					'info': 'Contents of the active selection buffer.',
				},
			},
			'method': {
				'AddSelection': {
					'type': 'Sub',
					'args': 'ToAdd as IpfcSelection',
					'return': '',
					'info': ' Add an item to the selection buffer.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Clear the selection buffer of all contents. ',
				},
				'RemoveSelection': {
					'type': 'Sub',
					'args': 'IndexToRemove as Long',
					'return': '',
					'info': 'Removes the selection at the indicated location from the selection tool. ',
				},
			},
	}
	IpfcSelectionEvaluator = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
			},
			'child': {
			},
			'properties': {
				'Selections': {
					'return': 'IpfcSelectionPair',
					'info': 'The SelectionPair: the pair of selected objects.',
				},
			},
			'method': {
				'ComputeClearance': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcClearanceData',
					'info': 'Enables the user to compute the clearance between two selected parts or surfaces.',
				},
				'ComputeInterference': {
					'type': 'Function',
					'args': 'SolidOnly as Boolean',
					'return': 'IpfcInterferenceVolume',
					'info': 'Enables the user to measure the interference between the two specified parts.',
				},
				'ComputeNearestCriticalDistance': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcCriticalDistanceData',
					'info': 'Enables the user to find the nearest local minimum (not the absolute minimum) of the distance function between two selected objects.',
				},
			},
	}
	IpfcSelectionOptions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'MaxNumSels': {
					'return': 'as Long [optional]',
					'info': 'The maximum number of selections allowed. If this is a negative number, there is an unlimited number of selections. ',
				},
				'OptionKeywords': {
					'return': 'as String',
					'info': 'The selection filter.',
				},
			},
			'method': {
				'CCpfcSelectionOptions.Create': {
					'type': 'Function',
					'args': 'inOptionKeywords as String',
					'return': 'IpfcSelectionOptions',
					'info': 'Specifies the options used in the selection. ',
					'com': 'pfcls.pfcSelectionOptions.1',
				},
			},
	}
	IpfcSelectionPair = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Sel1': {
					'return': 'IpfcSelection',
					'info': 'The first Selected object.',
				},
				'Sel2': {
					'return': 'IpfcSelection',
					'info': 'The second Selected object.',
				},
			},
			'method': {
				'CCpfcSelectionPair.Create': {
					'type': 'Function',
					'args': 'inSel1 as IpfcSelection, inSel2 as IpfcSelection',
					'return': 'IpfcSelectionPair',
					'info': "Creates a new SelectionPair object; it's a field of SelectionEvaluator and GlobalInterference classes and represents the data necessary to call several functions that deal with interference.",
					'com': 'pfcls.pfcSelectionPair.1',
				},
			},
	}
	IpfcServer = {
			'type': 'Classes',
			'parent': {
				'IpfcChild': '',
				'IpfcServerLocation': '',
			},
			'child': {
			},
			'properties': {
				'ActiveWorkspace': {
					'return': 'as String',
					'info': 'Specifies the name of active server. ',
				},
				'Alias': {
					'return': 'as String',
					'info': 'Specifies the alias of server. ',
				},
				'Context': {
					'return': 'as String',
					'info': 'Specifies context of the server',
				},
				'IsActive': {
					'return': 'as Boolean',
					'info': 'Specifies whether server active or not.',
				},
			},
			'method': {
				'Activate': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Sets the server to be active in session.',
				},
				'CheckinObjects': {
					'type': 'Sub',
					'args': 'Mdl as IpfcModel [optional], Options as IpfcCheckinOptions [optional]',
					'return': '',
					'info': 'Checks in an object to the database.',
				},
				'CheckoutMultipleObjects': {
					'type': 'Function',
					'args': 'Files as Istringseq, Checkout as Boolean, Options as IpfcCheckoutOptions [optional]',
					'return': 'Istringseq',
					'info': 'Checks out or downloads multiple objects to the workspace. Checkout rules are processed based on the Workspace config spec, unless the version is set using  IpfcCheckoutOptions.Version. If the config spec is LATEST, for example, the latest version of the indicated object(s) will be checked out. If an aliased URL path to a model is supplied, and more than one revision of the model is in the path, Creo Parametric will apply the workspace config spec rules against the objects which reside in that folder. ',
				},
				'CheckoutObjects': {
					'type': 'Function',
					'args': 'Mdl as IpfcModel [optional], File as String [optional], Checkout as Boolean, Options as IpfcCheckoutOptions [optional]',
					'return': ' as String',
					'info': 'Checks out or downloads an object to the workspace.  Checkout rules are processed based    on the Workspace config spec, unless the version is set using   IpfcCheckoutOptions.Version.   If the config spec is LATEST, for example, the latest version of the indicated object(s) will be    checked out. If an aliased URL path to the model is supplied, and more than one revision of the model    is in the path, Creo Parametric will apply the workspace config spec rules against the objects which     reside in that folder. ',
				},
				'CreateWorkspace': {
					'type': 'Sub',
					'args': 'Definition as IpfcWorkspaceDefinition',
					'return': '',
					'info': 'Creates and activates a new workspace.',
				},
				'GetAliasedUrl': {
					'type': 'Function',
					'args': 'ModelName as String',
					'return': ' as String',
					'info': 'Gets aliased url for a given model name.',
				},
				'IsObjectCheckedOut': {
					'type': 'Function',
					'args': 'WorkspaceName as String, ObjectName as String',
					'return': ' as Boolean',
					'info': 'Returns the checkout status for the given object in the workspace.',
				},
				'IsObjectModified': {
					'type': 'Function',
					'args': 'WorkspaceName as String, ObjectName as String',
					'return': ' as Boolean',
					'info': 'Returns the modification status for the given object in the workspace.',
				},
				'IsServerObjectModified': {
					'type': 'Function',
					'args': 'WorkspaceName as String, ObjectName as String',
					'return': 'IpfcServerObjectStatus',
					'info': 'Checks if the given object is modified in workspace or modified locally.',
				},
				'RemoveObjects': {
					'type': 'Sub',
					'args': 'ModelNames as Istringseq [optional]',
					'return': '',
					'info': 'Removes a list of models from a workspace.',
				},
				'UndoCheckout': {
					'type': 'Sub',
					'args': 'Mdl as IpfcModel',
					'return': '',
					'info': 'Undoes the checkout of the given object.',
				},
				'Unregister': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Unregisters a given server and cleans a local cache for the server. ',
				},
				'UploadObjects': {
					'type': 'Sub',
					'args': 'Mdl as IpfcModel',
					'return': '',
					'info': 'Uploads an object to the database.',
				},
				'UploadObjectsWithOptions': {
					'type': 'Sub',
					'args': 'TopModel as IpfcModel [optional], Options as IpfcUploadOptions',
					'return': '',
					'info': 'Uploads objects to the database with the specified options.',
				},
			},
	}
	IpfcServerLocation = {
			'type': 'Classes',
			'parent': {
				'IpfcChild': '',
			},
			'child': {
				'IpfcNonRegisteredServer': '',
				'IpfcServer': '',
			},
			'properties': {
				'Class': {
					'return': 'as String',
					'info': 'Specifies the class of the server . The values of the server class are "Windchill" and    "ProjectLink." "Windchill" denotes either a Windchill Classic PDM or a Windchill PDMLink server,    while "ProjectLink" denotes Windchill ProjectLink type of servers.',
				},
				'Location': {
					'return': 'as String',
					'info': 'Specifies the codebase URL . ',
				},
				'Version': {
					'return': 'as String',
					'info': 'Specifies the version of Windchill that is configured on the server, for example, "7.0" or "8.0."    ',
				},
			},
			'method': {
				'CollectWorkspaces': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcWorkspaceDefinitions',
					'info': 'Gets the list of the workspaces .',
				},
				'DeleteWorkspace': {
					'type': 'Sub',
					'args': 'WorkspaceName as String',
					'return': '',
					'info': 'Deletes a workspace.',
				},
				'ListContexts': {
					'type': 'Function',
					'args': '',
					'return': 'Istringseq',
					'info': 'Gets the list of the contexts',
				},
			},
	}
	IpfcServerObjectStatus = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'IsCheckedOut': {
					'return': 'as Boolean',
					'info': 'true if checked out and false if not.',
				},
				'IsModifiedInWorkspace': {
					'return': 'as Boolean',
					'info': 'true if modified in workspace and false if not.',
				},
				'IsModifiedLocally': {
					'return': 'as Boolean',
					'info': 'true if modified locally and false if not.',
				},
			},
			'method': {
			},
	}
	IpfcSession = {
			'type': 'Classes',
			'parent': {
				'IpfcBaseSession': '',
			},
			'child': {
			},
			'properties': {
				'CurrentSelectionBuffer': {
					'return': 'IpfcSelectionBuffer',
					'info': 'The current selection buffer object.',
				},
			},
			'method': {
				'GetAppInfo': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcAppInfo',
					'info': 'Returns the application information from the applicatoin session.',
				},
				'ModelDescrContainsOrigin': {
					'type': 'Function',
					'args': 'Descr as IpfcModelDescriptor',
					'return': ' as Boolean',
					'info': '  ',
				},
				'NavigatorPaneBrowserAdd': {
					'type': 'Sub',
					'args': 'PaneName as String, IconFileName as String [optional], URL as String',
					'return': '',
					'info': ' Adds a new Navigator Pane.',
				},
				'NavigatorPaneBrowserIconSet': {
					'type': 'Sub',
					'args': 'PaneName as String, IconFileName as String',
					'return': '',
					'info': ' Set the icon for a Navigator Pane.',
				},
				'NavigatorPaneBrowserURLSet': {
					'type': 'Sub',
					'args': 'PaneName as String, URL as String',
					'return': '',
					'info': ' Set the url for a Navigator Pane.',
				},
				'RibbonDefinitionfileLoad': {
					'type': 'Sub',
					'args': 'FileName as String',
					'return': '',
					'info': ' Loads ribbon definition file from a default path.',
				},
				'SetAppInfo': {
					'type': 'Sub',
					'args': 'Info as IpfcAppInfo',
					'return': '',
					'info': 'Sends the application information for the application session.',
				},
				'UIAddButton': {
					'type': 'Sub',
					'args': 'Command as IpfcUICommand, MenuName as String, NeighborButton as String [optional], ButtonName as String, Message as String, MsgFile as String',
					'return': '',
					'info': 'Adds a new button to an existing menu.',
				},
				'UIAddMenu': {
					'type': 'Sub',
					'args': 'MenuName as String, NeighborItem as String, FileName as String, ParentMenu as String [optional]',
					'return': '',
					'info': 'Creates a new menu.',
				},
				'UIClearMessage': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Scrolls the text in the message area up one line after a call to IpfcSession.UIDisplayMessage() . This command produces only one carriage return; if called multiple times, the command is ignored.  ',
				},
				'UICreateCommand': {
					'type': 'Function',
					'args': 'Name as String, Action as IpfcUICommandActionListener',
					'return': 'IpfcUICommand',
					'info': 'Creates a command with the specified name.',
				},
				'UICreateMaxPriorityCommand': {
					'type': 'Function',
					'args': 'Name as String, Action as IpfcUICommandActionListener',
					'return': 'IpfcUICommand',
					'info': 'Creates a command with the specified name. This command    differs from a command created by   IpfcSession.UICreateCommand() in that it    uses the maximum command priority available. Maximum command priority    should be used only in commands which open and activate a new model in    a window. All other commands should be created using    IpfcSession.UICreateCommand() because    commands created with maximum priority may dismiss context of the    current model in certain situations.',
				},
				'UIDisplayFeatureParams': {
					'type': 'Sub',
					'args': 'Selection as IpfcSelection, Type as IpfcParamType',
					'return': '',
					'info': 'Displays parameters of a selected feature.',
				},
				'UIDisplayLocalizedMessage': {
					'type': 'Sub',
					'args': 'MsgFile as String, Format as String, Messages as Istringseq [optional]',
					'return': '',
					'info': 'Prints a text message to the message area in Creo Parametric.',
				},
				'UIDisplayMessage': {
					'type': 'Sub',
					'args': 'MsgFile as String, Format as String, Messages as Istringseq [optional]',
					'return': '',
					'info': 'Prints a text message to the message area in Creo Parametric.',
				},
				'UIGetCommand': {
					'type': 'Function',
					'args': 'Name as String',
					'return': 'IpfcUICommand',
					'info': 'Finds the identifier of the specified action or option. ',
				},
				'UIGetCurrentMouseStatus': {
					'type': 'Function',
					'args': 'SnapToGrid as Boolean',
					'return': 'IpfcMouseStatus',
					'info': 'Returns the status of mouse at this particular moment.  This method returns whenever the mouse is moved or a button is pressed.',
				},
				'UIGetNextMousePick': {
					'type': 'Function',
					'args': 'ExpectedButton as IpfcMouseButton [optional]',
					'return': 'IpfcMouseStatus',
					'info': 'Returns the mouse status at the time that the user makes a mouse pick.',
				},
				'UIOpenFile': {
					'type': 'Function',
					'args': 'Options as IpfcFileOpenOptions',
					'return': ' as String',
					'info': 'Prompts the standard file browser interface of Creo Parametric.',
				},
				'UIPickMouseBox': {
					'type': 'Function',
					'args': 'FirstCorner as IpfcPoint3D [optional]',
					'return': 'IpfcOutline3D',
					'info': 'Prompt the user to select a rectangle using the mouse.',
				},
				'UIReadIntMessage': {
					'type': 'Function',
					'args': 'LowerLimit as Long, UpperLimit as Long',
					'return': ' as Long [optional]',
					'info': 'Reads an integer from the keyboard.           ',
				},
				'UIReadRealMessage': {
					'type': 'Function',
					'args': 'LowerLimit as Double, UpperLimit as Double',
					'return': ' as Double [optional]',
					'info': 'Reads a double-precision float from the keyboard. ',
				},
				'UIReadStringMessage': {
					'type': 'Function',
					'args': 'HideInput as Boolean [optional]',
					'return': ' as String [optional]',
					'info': 'Reads a line of keyboard input and returns the contents as a wide string.',
				},
				'UISaveFile': {
					'type': 'Function',
					'args': 'Options as IpfcFileSaveOptions',
					'return': ' as String',
					'info': ' Prompts the standard file browser interface of Creo Parametric, set up for the purpose of allowing the user to save a file.',
				},
				'UISelectDirectory': {
					'type': 'Function',
					'args': 'Options as IpfcDirectorySelectionOptions',
					'return': ' as String',
					'info': ' Prompts the standard file browser interface of Creo Parametric, set up for the purpose of allowing the user to select a directory.',
				},
				'UIShowMessageDialog': {
					'type': 'Function',
					'args': 'Message as String, Options as IpfcMessageDialogOptions [optional]',
					'return': 'IpfcMessageButton',
					'info': 'Displays the UI Message Dialog.',
				},
			},
	}
	IpfcSessionActionListener = {
			'type': 'Classes',
			'parent': {
				'IpfcActionListener': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'OnAfterDirectoryChange': {
					'type': 'Sub',
					'args': 'Path as String',
					'return': '',
					'info': 'Provides a notification function called after the current directory has been changed.',
				},
				'OnAfterModelDisplay': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Provides a notification function called after displaying a model. ',
				},
				'OnAfterModelPurge': {
					'type': 'Sub',
					'args': 'Desrc as IpfcModelDescriptor',
					'return': '',
					'info': 'This is the listener method called after purging a model.',
				},
				'OnAfterWindowChange': {
					'type': 'Sub',
					'args': 'NewWindow as IpfcWindow [optional]',
					'return': '',
					'info': 'Provides a listener method called after changing a window in Creo Parametric.',
				},
				'OnBeforeModelCopy': {
					'type': 'Sub',
					'args': 'Container as IpfcDescriptorContainer2',
					'return': '',
					'info': 'This is the listener method called before copying a model. ',
				},
				'OnBeforeModelDelete': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'This is the listener method  called before deleting a model.',
				},
				'OnBeforeModelErase': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'This is the  stener method called before erasing a model.',
				},
				'OnBeforeModelPurge': {
					'type': 'Sub',
					'args': 'Container as IpfcDescriptorContainer',
					'return': '',
					'info': 'This is the listener method  called before purging a model.',
				},
				'OnBeforeModelRename': {
					'type': 'Sub',
					'args': 'Container as IpfcDescriptorContainer2',
					'return': '',
					'info': 'This is the listener method called before renaming a model.',
				},
				'OnBeforeModelSave': {
					'type': 'Sub',
					'args': 'Container as IpfcDescriptorContainer',
					'return': '',
					'info': 'This is the listener method called before saving a model.',
				},
			},
	}
	IpfcSheetData = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Height': {
					'return': 'as Long',
					'info': 'The height of the drawing.',
				},
				'Orientation': {
					'return': 'IpfcSheetOrientation',
					'info': 'The orientation (portrait or landscape).',
				},
				'SheetSize': {
					'return': 'IpfcPlotPaperSize',
					'info': 'The size of the sheet.',
				},
				'Units': {
					'return': 'IpfcLengthUnits',
					'info': 'The units used in the drawing.',
				},
				'Width': {
					'return': 'as Long',
					'info': 'The width of the drawing.',
				},
			},
			'method': {
			},
	}
	IpfcSheetInfo = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Height': {
					'return': 'as Double',
					'info': 'The height of the drawing.',
				},
				'Orientation': {
					'return': 'IpfcSheetOrientation',
					'info': 'The orientation (portrait or landscape).',
				},
				'SheetSize': {
					'return': 'IpfcPlotPaperSize',
					'info': 'The size of the sheet.',
				},
				'Units': {
					'return': 'IpfcLengthUnits',
					'info': 'The units used in the drawing.',
				},
				'Width': {
					'return': 'as Double',
					'info': 'The width of the drawing.',
				},
			},
			'method': {
			},
	}
	IpfcSheetOwner = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcModel2D': '',
			},
			'properties': {
				'CurrentSheetNumber': {
					'return': 'as Long',
					'info': 'The current sheet number.',
				},
				'NumberOfSheets': {
					'return': 'as Long',
					'info': 'The number of sheets in the model.',
				},
			},
			'method': {
				'AddSheet': {
					'type': 'Function',
					'args': '',
					'return': ' as Long',
					'info': 'Adds a sheet to the model.  ',
				},
				'DeleteSheet': {
					'type': 'Sub',
					'args': 'SheetNumber as Long',
					'return': '',
					'info': 'Removes a sheet from the model.',
				},
				'GetSheetBackgroundView': {
					'type': 'Function',
					'args': 'SheetNumber as Long',
					'return': 'IpfcView2D',
					'info': 'Returns the background view for the given sheet.',
				},
				'GetSheetData': {
					'type': 'Function',
					'args': 'SheetNumber as Long',
					'return': 'IpfcSheetData',
					'info': 'Returns information about the specified sheet.',
				},
				'GetSheetFormat': {
					'type': 'Function',
					'args': 'SheetNumber as Long',
					'return': 'IpfcDrawingFormat',
					'info': 'Returns the drawing format used for the particular sheet.',
				},
				'GetSheetFormatDescr': {
					'type': 'Function',
					'args': 'SheetNumber as Long',
					'return': 'IpfcModelDescriptor',
					'info': 'Returns the model descriptor of the drawing format used for the particular sheet. ',
				},
				'GetSheetInfo': {
					'type': 'Function',
					'args': 'SheetNumber as Long',
					'return': 'IpfcSheetInfo',
					'info': 'Returns information about the specified sheet.',
				},
				'GetSheetScale': {
					'type': 'Function',
					'args': 'SheetNumber as Long, DrawingModel as IpfcModel [optional]',
					'return': ' as Double',
					'info': 'Returns the scale of the drawing on particular sheet.',
				},
				'GetSheetTransform': {
					'type': 'Function',
					'args': 'SheetNumber as Long',
					'return': 'IpfcTransform3D',
					'info': 'Returns the transformation matrix for the sheet.',
				},
				'GetSheetUnits': {
					'type': 'Function',
					'args': 'SheetNumber as Long',
					'return': 'IpfcUnit',
					'info': 'Returns the length units for the given sheet. ',
				},
				'RegenerateSheet': {
					'type': 'Sub',
					'args': 'SheetNumber as Long',
					'return': '',
					'info': 'Regenerates a specified sheet.',
				},
				'ReorderSheet': {
					'type': 'Sub',
					'args': 'FromSheetNumber as Long, To as Long',
					'return': '',
					'info': 'Reorders the given sheet to a new sheet number.   ',
				},
				'SetSheetFormat': {
					'type': 'Sub',
					'args': 'SheetNumber as Long, Format as IpfcDrawingFormat, FormatSheetNumber as Long [optional], DrawingModel as IpfcModel [optional]',
					'return': '',
					'info': 'Applies the specified format to a drawing sheet.',
				},
				'SetSheetScale': {
					'type': 'Sub',
					'args': 'SheetNumber as Long, Scale as Double, DrawingModel as IpfcModel [optional]',
					'return': '',
					'info': 'Sets the scale for a model on the sheet.',
				},
			},
	}
	IpfcShrinkwrapExportInstructions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcShrinkwrapModelExportInstructions': '',
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcShrinkwrapFacetedFormatInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcShrinkwrapModelExportInstructions': '',
			},
			'child': {
				'IpfcShrinkwrapFacetedPartInstructions': '',
				'IpfcShrinkwrapVRMLInstructions': '',
				'IpfcShrinkwrapSTLInstructions': '',
			},
			'properties': {
				'Format': {
					'return': 'IpfcShrinkwrapFacetedFormat',
					'info': 'The type of export to produce.',
				},
				'FramesFile': {
					'return': 'as String [optional]',
					'info': 'The frames file name and path used for the export of a motion envelope.',
				},
			},
			'method': {
			},
	}
	IpfcShrinkwrapFacetedPartInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcShrinkwrapFacetedFormatInstructions': '',
			},
			'child': {
			},
			'properties': {
				'Lightweight': {
					'return': 'as Boolean',
					'info': 'true if the output part should be lightweight, null or false otherwise.',
				},
				'OutputModel': {
					'return': 'IpfcModel',
					'info': 'The solid model handle where the geometry will be created. You can create an empty part through IpfcBaseSession.CreatePart() or copy a template model using IpfcModel.Copy().',
				},
			},
			'method': {
				'CCpfcShrinkwrapFacetedPartInstructions.Create': {
					'type': 'Function',
					'args': 'OutputModel as IpfcModel, Lightweight as Boolean',
					'return': 'IpfcShrinkwrapFacetedPartInstructions',
					'info': 'Creates a new instructions object used to export a faceted shrinkwrap model.',
					'com': 'pfcls.pfcShrinkwrapFacetedPartInstructions.1',
				},
			},
	}
	IpfcShrinkwrapMergedSolidInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcShrinkwrapModelExportInstructions': '',
			},
			'child': {
			},
			'properties': {
				'AdditionalComponents': {
					'return': 'IpfcSelections',
					'info': 'Sequence of additional components to include in the output model.',
				},
				'OutputModel': {
					'return': 'IpfcModel',
					'info': 'The output model where geometry will be created.  Use IpfcBaseSession.CreatePart() to obtain an empty part, or copy a template with IpfcModel.Copy().',
				},
			},
			'method': {
				'CCpfcShrinkwrapMergedSolidInstructions.Create': {
					'type': 'Function',
					'args': 'OutputModel as IpfcModel',
					'return': 'IpfcShrinkwrapMergedSolidInstructions',
					'info': 'Creates a new instructions object used to export a shrinkwrap model using merged solids.',
					'com': 'pfcls.pfcShrinkwrapMergedSolidInstructions.1',
				},
			},
	}
	IpfcShrinkwrapModelExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcShrinkwrapExportInstructions': '',
			},
			'child': {
				'IpfcShrinkwrapSurfaceSubsetInstructions': '',
				'IpfcShrinkwrapFacetedFormatInstructions': '',
				'IpfcShrinkwrapMergedSolidInstructions': '',
			},
			'properties': {
				'AssignMassProperties': {
					'return': 'as Boolean [optional]',
					'info': 'true to calculate mass properties for the shrinkwrap model of SmallSurfPercentage, false or null otherwise.',
				},
				'AutoHoleFilling': {
					'return': 'as Boolean [optional]',
					'info': 'true or null to fill voids located entirely inside the outer boundary of the shrinkwrap model, false otherwise.',
				},
				'DatumReferences': {
					'return': 'IpfcSelections',
					'info': 'Sequence of datum references used in the shrinkwrap export.',
				},
				'IgnoreQuilts': {
					'return': 'as Boolean [optional]',
					'info': 'true or null to ignore quilts, false otherwise.',
				},
				'IgnoreSkeleton': {
					'return': 'as Boolean [optional]',
					'info': 'true or null to ignore the contents of the skeleton model (if present), false otherwise.',
				},
				'IgnoreSmallSurfaces': {
					'return': 'as Boolean [optional]',
					'info': 'true to ignore surfaces smaller than the value of SmallSurfPercentage, false or null otherwise.',
				},
				'Method': {
					'return': 'IpfcShrinkwrapMethod',
					'info': 'Indicates the method used to perform the shrinkwrap export.',
				},
				'Quality': {
					'return': 'as Long [optional]',
					'info': 'The export quality.  1 produces the coarsest representation surfaces, while 10 produces the finest.  If null, a quality of 1 will be used.',
				},
				'SmallSurfPercentage': {
					'return': 'as Double [optional]',
					'info': 'Surfaces smaller than this percentage with respect to the model size will not be exported, if IgnoreSmallSurfaces is true.',
				},
			},
			'method': {
			},
	}
	IpfcShrinkwrapSTLInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcShrinkwrapFacetedFormatInstructions': '',
			},
			'child': {
			},
			'properties': {
				'OutputFile': {
					'return': 'as String',
					'info': 'The name of the STL file, without the extension.  The file will be produced in the Creo Parametric working directory.',
				},
			},
			'method': {
				'CCpfcShrinkwrapSTLInstructions.Create': {
					'type': 'Function',
					'args': 'OutputFile as String',
					'return': 'IpfcShrinkwrapSTLInstructions',
					'info': 'Creates a new instructions object used to export an STL format shrinkwrap model.',
					'com': 'pfcls.pfcShrinkwrapSTLInstructions.1',
				},
			},
	}
	IpfcShrinkwrapSurfaceSubsetInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcShrinkwrapModelExportInstructions': '',
			},
			'child': {
			},
			'properties': {
				'AdditionalSurfaces': {
					'return': 'IpfcSelections',
					'info': 'Additional surfaces to include in the export.',
				},
				'OutputModel': {
					'return': 'IpfcModel',
					'info': 'The output model where geometry will be created.  Use IpfcBaseSession.CreatePart() to obtain an empty part, or copy a template with IpfcModel.Copy().',
				},
			},
			'method': {
				'CCpfcShrinkwrapSurfaceSubsetInstructions.Create': {
					'type': 'Function',
					'args': 'OutputModel as IpfcModel',
					'return': 'IpfcShrinkwrapSurfaceSubsetInstructions',
					'info': 'Creates a new instructions object used to export a shrinkwrap model based on a set of selected surfaces.',
					'com': 'pfcls.pfcShrinkwrapSurfaceSubsetInstructions.1',
				},
			},
	}
	IpfcShrinkwrapVRMLInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcShrinkwrapFacetedFormatInstructions': '',
			},
			'child': {
			},
			'properties': {
				'OutputFile': {
					'return': 'as String',
					'info': 'The name of the VRML file to produce, without the extension.  The file will be produces in the Creo Parametric working directory.',
				},
			},
			'method': {
				'CCpfcShrinkwrapVRMLInstructions.Create': {
					'type': 'Function',
					'args': 'OutputFile as String',
					'return': 'IpfcShrinkwrapVRMLInstructions',
					'info': 'Creates a new instructions object used to export a VRML format shrinkwrap model.',
					'com': 'pfcls.pfcShrinkwrapVRMLInstructions.1',
				},
			},
	}
	IpfcSimpRep = {
			'type': 'Classes',
			'parent': {
				'IpfcModelItem': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'GetInstructions': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSimpRepInstructions',
					'info': 'Returns instructions that specify which internal data is required to define a simplified representation.',
				},
				'GetSimpRepType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSimpRepType',
					'info': 'Returns the type of a simplified representation.',
				},
				'SetInstructions': {
					'type': 'Sub',
					'args': 'Instructions as IpfcSimpRepInstructions [optional]',
					'return': '',
					'info': 'Enables you to set (if null) or change instructions that specify the internal data required to define a simplified representation.',
				},
			},
	}
	IpfcSimpRepAction = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcSimpRepReverse': '',
				'IpfcSimpRepInclude': '',
				'IpfcSimpRepExclude': '',
				'IpfcSimpRepSubstitute': '',
				'IpfcSimpRepNone': '',
				'IpfcSimpRepGeom': '',
				'IpfcSimpRepGraphics': '',
				'IpfcSimpRepSymb': '',
				'IpfcSimpRepDefaultEnvelope': '',
				'IpfcSimpRepBoundBox': '',
				'IpfcSimpRepLightWeightGraphics': '',
				'IpfcSimpRepAutomatic': '',
			},
			'properties': {
			},
			'method': {
				'GetType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSimpRepActionType',
					'info': 'Reterns a SimpRepActionType enum value that indicates the type of the action.',
				},
			},
	}
	IpfcSimpRepAutomatic = {
			'type': 'Classes',
			'parent': {
				'IpfcSimpRepAction': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcSimpRepAutomatic.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSimpRepAutomatic',
					'info': '       Creates a new object that represents the automatic       representation of the component.     ',
					'com': 'pfcls.pfcSimpRepAutomatic.1',
				},
			},
	}
	IpfcSimpRepBoundBox = {
			'type': 'Classes',
			'parent': {
				'IpfcSimpRepAction': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcSimpRepBoundBox.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSimpRepBoundBox',
					'info': ' ',
					'com': 'pfcls.pfcSimpRepBoundBox.1',
				},
			},
	}
	IpfcSimpRepCompItemPath = {
			'type': 'Classes',
			'parent': {
				'IpfcSimpRepItemPath': '',
			},
			'child': {
			},
			'properties': {
				'ItemPath': {
					'return': 'Iintseq',
					'info': 'Path to the item.',
				},
			},
			'method': {
				'CCpfcSimpRepCompItemPath.Create': {
					'type': 'Function',
					'args': 'ItemPath as Iintseq',
					'return': 'IpfcSimpRepCompItemPath',
					'info': 'Creates a new object that used to point to a component.',
					'com': 'pfcls.pfcSimpRepCompItemPath.1',
				},
			},
	}
	IpfcSimpRepDefaultEnvelope = {
			'type': 'Classes',
			'parent': {
				'IpfcSimpRepAction': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcSimpRepDefaultEnvelope.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSimpRepDefaultEnvelope',
					'info': ' ',
					'com': 'pfcls.pfcSimpRepDefaultEnvelope.1',
				},
			},
	}
	IpfcSimpRepExclude = {
			'type': 'Classes',
			'parent': {
				'IpfcSimpRepAction': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcSimpRepExclude.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSimpRepExclude',
					'info': 'Creates a new object for excluding this component from the simplified representation.',
					'com': 'pfcls.pfcSimpRepExclude.1',
				},
			},
	}
	IpfcSimpRepFeatItemPath = {
			'type': 'Classes',
			'parent': {
				'IpfcSimpRepItemPath': '',
			},
			'child': {
			},
			'properties': {
				'FeatId': {
					'return': 'as Long',
					'info': 'The Feat Id.',
				},
			},
			'method': {
				'CCpfcSimpRepFeatItemPath.Create': {
					'type': 'Function',
					'args': 'FeatId as Long',
					'return': 'IpfcSimpRepFeatItemPath',
					'info': 'Creates a new object used to point to a feature that is not a component.NOTE:  Not implemented in this J-Link release.',
					'com': 'pfcls.pfcSimpRepFeatItemPath.1',
				},
			},
	}
	IpfcSimpRepGeom = {
			'type': 'Classes',
			'parent': {
				'IpfcSimpRepAction': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcSimpRepGeom.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSimpRepGeom',
					'info': 'Creates a new object that represents the "geometry only" representation of the component.',
					'com': 'pfcls.pfcSimpRepGeom.1',
				},
			},
	}
	IpfcSimpRepGraphics = {
			'type': 'Classes',
			'parent': {
				'IpfcSimpRepAction': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcSimpRepGraphics.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSimpRepGraphics',
					'info': 'Creates a new object that represents the "graphics only" representation of the component.',
					'com': 'pfcls.pfcSimpRepGraphics.1',
				},
			},
	}
	IpfcSimpRepInclude = {
			'type': 'Classes',
			'parent': {
				'IpfcSimpRepAction': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcSimpRepInclude.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSimpRepInclude',
					'info': 'Creates a new object that includesthis component in the simplified representation.',
					'com': 'pfcls.pfcSimpRepInclude.1',
				},
			},
	}
	IpfcSimpRepInstructions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcRetrieveExistingSimpRepInstructions': '',
				'IpfcCreateNewSimpRepInstructions': '',
			},
			'properties': {
				'DefaultAction': {
					'return': 'IpfcSimpRepActionType',
					'info': 'Type of the action (SIMPREP_INCLUDE or SIMPREP_EXCLUDE).',
				},
				'IsTemporary': {
					'return': 'as Boolean',
					'info': 'Is the representation temporary true or not false.',
				},
				'Items': {
					'return': 'IpfcSimpRepItems',
					'info': 'An array of actions to apply to the components or features in this simplified representation.',
				},
			},
			'method': {
			},
	}
	IpfcSimpRepItem = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Action': {
					'return': 'IpfcSimpRepAction',
					'info': 'Specifies the action (exclude, include, substitute, etc.) to apply to the simplified representation items.   	    Set this to NULL to delete an existing item.   ',
				},
				'ItemPath': {
					'return': 'IpfcSimpRepItemPath',
					'info': 'Specifies the simplified representation item.',
				},
			},
			'method': {
				'CCpfcSimpRepItem.Create': {
					'type': 'Function',
					'args': 'ItemPath as IpfcSimpRepItemPath',
					'return': 'IpfcSimpRepItem',
					'info': 'Creates a new object that defines the status of the component or feature in a simplified representation.',
					'com': 'pfcls.pfcSimpRepItem.1',
				},
			},
	}
	IpfcSimpRepItemPath = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcSimpRepCompItemPath': '',
				'IpfcSimpRepFeatItemPath': '',
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcSimpRepLightWeightGraphics = {
			'type': 'Classes',
			'parent': {
				'IpfcSimpRepAction': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcSimpRepLightWeightGraphics.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSimpRepLightWeightGraphics',
					'info': ' ',
					'com': 'pfcls.pfcSimpRepLightWeightGraphics.1',
				},
			},
	}
	IpfcSimpRepNone = {
			'type': 'Classes',
			'parent': {
				'IpfcSimpRepAction': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcSimpRepNone.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSimpRepNone',
					'info': '  ',
					'com': 'pfcls.pfcSimpRepNone.1',
				},
			},
	}
	IpfcSimpRepReverse = {
			'type': 'Classes',
			'parent': {
				'IpfcSimpRepAction': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcSimpRepReverse.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSimpRepReverse',
					'info': 'Creates a new object that reverses the default rule for the component in the simplified rep.',
					'com': 'pfcls.pfcSimpRepReverse.1',
				},
			},
	}
	IpfcSimpRepSubstitute = {
			'type': 'Classes',
			'parent': {
				'IpfcSimpRepAction': '',
			},
			'child': {
			},
			'properties': {
				'SubstituteData': {
					'return': 'IpfcSubstitution',
					'info': 'Specifies the substitution type and substitute data.',
				},
			},
			'method': {
				'CCpfcSimpRepSubstitute.Create': {
					'type': 'Function',
					'args': 'SubsData as IpfcSubstitution',
					'return': 'IpfcSimpRepSubstitute',
					'info': 'Creates a new object used to substitute the component in the simplified representation.',
					'com': 'pfcls.pfcSimpRepSubstitute.1',
				},
			},
	}
	IpfcSimpRepSymb = {
			'type': 'Classes',
			'parent': {
				'IpfcSimpRepAction': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcSimpRepSymb.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSimpRepSymb',
					'info': 'Creates a new object that represents the symbolicrepresentation of the component.',
					'com': 'pfcls.pfcSimpRepSymb.1',
				},
			},
	}
	IpfcSliceExportData = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'CompIds': {
					'return': 'Iintseq',
					'info': 'A sequence of integers that identify the components that form the path from theroot assembly down to the component part or assembly being referred to.',
				},
			},
			'method': {
				'CCpfcSliceExportData.Create': {
					'type': 'Function',
					'args': 'CompIds as Iintseq [optional]',
					'return': 'IpfcSliceExportData',
					'info': 'Creates a new SliceExportData object used for slice export.',
					'com': 'pfcls.pfcSliceExportData.1',
				},
			},
	}
	IpfcSolid = {
			'type': 'Classes',
			'parent': {
				'IpfcFamilyMember': '',
				'IpfcModel': '',
			},
			'child': {
				'IpfcAssembly': '',
				'IpfcPart': '',
			},
			'properties': {
				'AbsoluteAccuracy': {
					'return': 'as Double [optional]',
					'info': 'The absolute accuracy of the solid or null, if relative accuracy is used',
				},
				'GeomOutline': {
					'return': 'IpfcOutline3D',
					'info': 'The outline of the solid with respect to the base coordinate system orientation',
				},
				'IsSkeleton': {
					'return': 'as Boolean',
					'info': 'true if the model is a skeleton model, false otherwise.',
				},
				'RelativeAccuracy': {
					'return': 'as Double [optional]',
					'info': 'The relative accuracy of the solid or null, if absolute accuracy is used',
				},
			},
			'method': {
				'ActivateSimpRep': {
					'type': 'Sub',
					'args': 'SimpRep as IpfcSimpRep',
					'return': '',
					'info': 'Activates the specified simplified representation.',
				},
				'CreateCustomUnit': {
					'type': 'Function',
					'args': 'Name as String, ConversionFactor as IpfcUnitConversionFactor, ReferenceUnit as IpfcUnit',
					'return': 'IpfcUnit',
					'info': 'Creates a custom unit based on name, conversion factor and  reference unit.',
				},
				'CreateFeature': {
					'type': 'Function',
					'args': 'Instructions as IpfcFeatureCreateInstructions',
					'return': 'IpfcFeature',
					'info': 'Not implemented in the current release. Creates a feature, based on the instructions provided.',
				},
				'CreateImportFeat': {
					'type': 'Function',
					'args': 'IntfData as IpfcIntfDataSource, CoordSys as IpfcCoordSystem [optional], FeatAttr as IpfcImportFeatAttr [optional]',
					'return': 'IpfcFeature',
					'info': '     Creates a new import feature in the solid (part). Assembly case is not supported.   ',
				},
				'CreateLocalGroup': {
					'type': 'Function',
					'args': 'Members as IpfcFeatures, Name as String',
					'return': 'IpfcFeatureGroup',
					'info': 'Creates a local group out of the specified set of features.',
				},
				'CreateNote': {
					'type': 'Function',
					'args': 'Lines as Istringseq, Owner as IpfcModelItem [optional]',
					'return': 'IpfcModelItem',
					'info': '  ',
				},
				'CreateSimpRep': {
					'type': 'Function',
					'args': 'Instructions as IpfcCreateNewSimpRepInstructions',
					'return': 'IpfcSimpRep',
					'info': 'Creates a simplified representation based on the supplied instructions.',
				},
				'CreateUDFGroup': {
					'type': 'Function',
					'args': 'Instructions as IpfcUDFGroupCreateInstructions',
					'return': 'IpfcFeatureGroup',
					'info': 'Creates a FeatureGroup, based on the instructions provided.',
				},
				'CreateUnitSystem': {
					'type': 'Function',
					'args': 'Name as String, Type as IpfcUnitSystemType, Units as IpfcUnits',
					'return': 'IpfcUnitSystem',
					'info': 'Creates a new unit system in the model. ',
				},
				'DeleteSimpRep': {
					'type': 'Sub',
					'args': 'SimpRep as IpfcSimpRep',
					'return': '',
					'info': 'Deletes the specified simplified representation from its owner model.',
				},
				'EvalOutline': {
					'type': 'Function',
					'args': 'Trf as IpfcTransform3D [optional], ExcludeTypes as IpfcModelItemTypes [optional]',
					'return': 'IpfcOutline3D',
					'info': 'Computes the outline of a solid.',
				},
				'ExecuteFeatureOps': {
					'type': 'Sub',
					'args': 'Ops as IpfcFeatureOperations, Instrs as IpfcRegenInstructions [optional]',
					'return': '',
					'info': 'Performs the specified feature operations. ',
				},
				'ExportShrinkwrap': {
					'type': 'Sub',
					'args': 'Instructions as IpfcShrinkwrapExportInstructions',
					'return': '',
					'info': 'Exports a solid model to shrinkwrap format.',
				},
				'GetActiveSimpRep': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSimpRep',
					'info': 'Returns the current active simplified representation.',
				},
				'GetCrossSection': {
					'type': 'Function',
					'args': 'Name as String',
					'return': 'IpfcXSection',
					'info': 'Returns a cross-section object, given its name.',
				},
				'GetFeatureById': {
					'type': 'Function',
					'args': 'Id as Long',
					'return': 'IpfcFeature',
					'info': 'Retrieves the specified feature, given its identifier.',
				},
				'GetFeatureByName': {
					'type': 'Function',
					'args': 'Name as String',
					'return': 'IpfcFeature',
					'info': 'Locates the feature object, given its name.',
				},
				'GetGeomRep': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSimpRep',
					'info': "Returns the object representing the solid model's Geometry Rep.",
				},
				'GetGraphicsRep': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSimpRep',
					'info': "Returns the object representing the solid model's Grpahics Rep.",
				},
				'GetMassProperty': {
					'type': 'Function',
					'args': 'CoordSysName as String [optional]',
					'return': 'IpfcMassProperty',
					'info': 'Gets the mass properties for the solid.',
				},
				'GetMasterRep': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSimpRep',
					'info': "Returns the object representing the solid model's Master Rep.",
				},
				'GetPrincipalUnits': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcUnitSystem',
					'info': 'Gets the system of units assigned to the solid model.',
				},
				'GetSimpRep': {
					'type': 'Function',
					'args': 'SimpRepName as String',
					'return': 'IpfcSimpRep',
					'info': 'Returns the handle to the specified simplified representation.',
				},
				'GetUnit': {
					'type': 'Function',
					'args': 'Name as String, ByExpression as Boolean [optional]',
					'return': 'IpfcUnit',
					'info': 'Get the handle to a particular unit based on name orexpression.',
				},
				'HasRetrievalErrors': {
					'type': 'Function',
					'args': '',
					'return': ' as Boolean',
					'info': 'Identifies if a previous call to retrieve the model resulted in errors.',
				},
				'ListCrossSections': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcXSections',
					'info': 'Lists the cross-sections in the solid model.',
				},
				'ListFailedFeatures': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcFeatures',
					'info': 'Retrieves the list of failed features in thepart or assembly.',
				},
				'ListFeaturesByType': {
					'type': 'Function',
					'args': 'VisibleOnly as Boolean [optional], Type as IpfcFeatureType [optional]',
					'return': 'IpfcFeatures',
					'info': 'Lists the features according to type. ',
				},
				'ListGroups': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcFeatureGroups',
					'info': 'Collect groups (including UDFs) in the solid.',
				},
				'ListUnits': {
					'type': 'Function',
					'args': 'Type as IpfcUnitType [optional]',
					'return': 'IpfcUnits',
					'info': 'Lists the units available in the model. ',
				},
				'ListUnitSystems': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcUnitSystems',
					'info': 'Lists the unit systems available in the model. ',
				},
				'Regenerate': {
					'type': 'Sub',
					'args': 'Instrs as IpfcRegenInstructions [optional]',
					'return': '',
					'info': 'Regenerates the solid.',
				},
				'SelectSimpRep': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSimpRep',
					'info': 'Enables the user to select a simplified representation.',
				},
				'SetPrincipalUnits': {
					'type': 'Sub',
					'args': 'Units as IpfcUnitSystem, Options as IpfcUnitConversionOptions',
					'return': '',
					'info': 'Sets the system of units for the solid.',
				},
			},
	}
	IpfcSolidActionListener = {
			'type': 'Classes',
			'parent': {
				'IpfcActionListener': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'OnAfterFeatureCreate': {
					'type': 'Sub',
					'args': 'Sld as IpfcSolid, Feat as IpfcFeature',
					'return': '',
					'info': 'Provides an action listener called after a feature is created.',
				},
				'OnAfterFeatureDelete': {
					'type': 'Sub',
					'args': 'Sld as IpfcSolid, FeatId as Long',
					'return': '',
					'info': 'Provides an action listener called after a feature is deleted.',
				},
				'OnAfterRegen': {
					'type': 'Sub',
					'args': 'Sld as IpfcSolid, StartFeature as IpfcFeature [optional], WasSuccessful as Boolean',
					'return': '',
					'info': 'Provides a notification function called after regenerating a solid.',
				},
				'OnAfterUnitConvert': {
					'type': 'Sub',
					'args': 'Sld as IpfcSolid, ConvertNumbers as Boolean',
					'return': '',
					'info': 'Provides an action listener called after the unit conversion of a solid.',
				},
				'OnBeforeFeatureCreate': {
					'type': 'Sub',
					'args': 'Sld as IpfcSolid, FeatId as Long',
					'return': '',
					'info': 'Provides a callback function called before creating a feature.',
				},
				'OnBeforeRegen': {
					'type': 'Sub',
					'args': 'Sld as IpfcSolid, StartFeature as IpfcFeature [optional]',
					'return': '',
					'info': 'Provides a notification function called before the solid is regenerated.',
				},
				'OnBeforeUnitConvert': {
					'type': 'Sub',
					'args': 'Sld as IpfcSolid, ConvertNumbers as Boolean',
					'return': '',
					'info': 'Provides an action listener called before the unit conversion of a solid.',
				},
			},
	}
	IpfcSolidGeometryLayerItem = {
			'type': 'Classes',
			'parent': {
				'IpfcModelItem': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcSphericalSplineSurface = {
			'type': 'Classes',
			'parent': {
				'IpfcTransformedSurface': '',
			},
			'child': {
			},
			'properties': {
				'SplineSurfaceData': {
					'return': 'IpfcSplineSurfaceDescriptor',
					'info': 'The spline surface geometry which defines the surface',
				},
			},
			'method': {
			},
	}
	IpfcSphericalSplineSurfaceDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcTransformedSurfaceDescriptor': '',
			},
			'child': {
			},
			'properties': {
				'SplineSurfaceData': {
					'return': 'IpfcSplineSurfaceDescriptor',
					'info': 'The spline surface geometry which defines the surface',
				},
			},
			'method': {
				'CCpfcSphericalSplineSurfaceDescriptor.Create': {
					'type': 'Function',
					'args': 'Extents as IpfcSurfaceExtents, Orientation as IpfcSurfaceOrientation, Origin as IpfcTransform3D, SplineSurfaceData as IpfcSplineSurfaceDescriptor',
					'return': 'IpfcSphericalSplineSurfaceDescriptor',
					'info': 'This method creates a new SphericalSplineSurfaceDescriptor object.',
					'com': 'pfcls.pfcSphericalSplineSurfaceDescriptor.1',
				},
			},
	}
	IpfcSpline = {
			'type': 'Classes',
			'parent': {
				'IpfcCurve': '',
			},
			'child': {
			},
			'properties': {
				'Points': {
					'return': 'IpfcSplinePoints',
					'info': 'An array of spline interpolant points, tangent vectors and parameters.',
				},
			},
			'method': {
			},
	}
	IpfcSplineDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcCurveDescriptor': '',
			},
			'child': {
			},
			'properties': {
				'Points': {
					'return': 'IpfcSplinePoints',
					'info': 'An array of spline interpolant points,tangent vectors and parameters',
				},
			},
			'method': {
				'CCpfcSplineDescriptor.Create': {
					'type': 'Function',
					'args': 'Points as IpfcSplinePoints',
					'return': 'IpfcSplineDescriptor',
					'info': 'This method creates a new SplineDescriptor object.',
					'com': 'pfcls.pfcSplineDescriptor.1',
				},
			},
	}
	IpfcSplinePoint = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Parameter': {
					'return': 'as Double',
					'info': 'The parameter',
				},
				'Point': {
					'return': 'IpfcPoint3D',
					'info': 'The three-dimensional point',
				},
				'Tangent': {
					'return': 'IpfcVector3D',
					'info': 'The tangent',
				},
			},
			'method': {
				'CCpfcSplinePoint.Create': {
					'type': 'Function',
					'args': 'Parameter as Double, Point as IpfcPoint3D, Tangent as IpfcVector3D',
					'return': 'IpfcSplinePoint',
					'info': 'This method creates a new spline point object.',
					'com': 'pfcls.pfcSplinePoint.1',
				},
			},
	}
	IpfcSplinePointDimensionSense = {
			'type': 'Classes',
			'parent': {
				'IpfcDimensionSense': '',
			},
			'child': {
			},
			'properties': {
				'SplinePointIndex': {
					'return': 'as Long',
					'info': '  ',
				},
			},
			'method': {
				'CCpfcSplinePointDimensionSense.Create': {
					'type': 'Function',
					'args': 'SplinePointIndex as Long',
					'return': 'IpfcSplinePointDimensionSense',
					'info': 'Creates a new spline point dimension sense object for use in creating a drawing dimension.',
					'com': 'pfcls.pfcSplinePointDimensionSense.1',
				},
			},
	}
	IpfcSplinePointDimSense = {
			'type': 'Classes',
			'parent': {
				'IpfcDimSense': '',
			},
			'child': {
			},
			'properties': {
				'SplinePointIndex': {
					'return': 'as Long',
					'info': '  ',
				},
			},
			'method': {
				'CCpfcSplinePointDimSense.Create': {
					'type': 'Function',
					'args': 'SplinePointIndex as Long',
					'return': 'IpfcSplinePointDimSense',
					'info': '  ',
					'com': 'pfcls.pfcSplinePointDimSense.1',
				},
			},
	}
	IpfcSplineSurface = {
			'type': 'Classes',
			'parent': {
				'IpfcSurface': '',
			},
			'child': {
			},
			'properties': {
				'Points': {
					'return': 'IpfcSplineSurfacePoints',
					'info': 'An array of spline interpolant points and derivatives',
				},
			},
			'method': {
			},
	}
	IpfcSplineSurfaceDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcSurfaceDescriptor': '',
			},
			'child': {
			},
			'properties': {
				'Points': {
					'return': 'IpfcSplineSurfacePoints',
					'info': 'An array of spline interpolant points and derivatives',
				},
			},
			'method': {
				'CCpfcSplineSurfaceDescriptor.Create': {
					'type': 'Function',
					'args': 'Extents as IpfcSurfaceExtents, Orientation as IpfcSurfaceOrientation, Points as IpfcSplineSurfacePoints',
					'return': 'IpfcSplineSurfaceDescriptor',
					'info': 'This method creates a new SplineSurfaceDescriptor object.',
					'com': 'pfcls.pfcSplineSurfaceDescriptor.1',
				},
			},
	}
	IpfcSplineSurfacePoint = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Parameter': {
					'return': 'IpfcUVParams',
					'info': 'The UV parameters of the point on the surface.',
				},
				'Point': {
					'return': 'IpfcPoint3D',
					'info': 'The XYZ point',
				},
				'UTangent': {
					'return': 'IpfcVector3D',
					'info': 'The tangent vector in the U direction at the point',
				},
				'UVDerivative': {
					'return': 'IpfcVector3D',
					'info': 'The mixed derivative at the point',
				},
				'VTangent': {
					'return': 'IpfcVector3D',
					'info': 'The tangent vector in the V direction at the point',
				},
			},
			'method': {
				'CCpfcSplineSurfacePoint.Create': {
					'type': 'Function',
					'args': 'Parameter as IpfcUVParams, Point as IpfcPoint3D, UTangent as IpfcVector3D, VTangent as IpfcVector3D, UVDerivative as IpfcVector3D',
					'return': 'IpfcSplineSurfacePoint',
					'info': 'This method creates a new SplineSurfacePoint object.',
					'com': 'pfcls.pfcSplineSurfacePoint.1',
				},
			},
	}
	IpfcSpoolImportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcImportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcSpoolImportInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSpoolImportInstructions',
					'info': 'Creates a new instructions object used to import (read) from the SPOOL type file.',
					'com': 'pfcls.pfcSpoolImportInstructions.1',
				},
			},
	}
	IpfcSTEP2DExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExportInstructions': '',
			},
			'child': {
			},
			'properties': {
				'OptionValue': {
					'return': 'IpfcExport2DOption',
					'info': '  ',
				},
			},
			'method': {
				'CCpfcSTEP2DExportInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSTEP2DExportInstructions',
					'info': 'Creates a new instructions object used to export a non-solid model to STEP format.',
					'com': 'pfcls.pfcSTEP2DExportInstructions.1',
				},
			},
	}
	IpfcSTEP3DExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExport3DInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcSTEP3DExportInstructions.Create': {
					'type': 'Function',
					'args': 'inConfiguration as IpfcAssemblyConfiguration, inGeometry as IpfcGeometryFlags',
					'return': 'IpfcSTEP3DExportInstructions',
					'info': 'Creates a new instructions object used to export a solid model to STEP format.',
					'com': 'pfcls.pfcSTEP3DExportInstructions.1',
				},
			},
	}
	IpfcSTEPExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcGeomExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcSTEPExportInstructions.Create': {
					'type': 'Function',
					'args': 'Flags as IpfcGeomExportFlags',
					'return': 'IpfcSTEPExportInstructions',
					'info': 'Creates a new instructions object used to export a part or assembly in STEP format.',
					'com': 'pfcls.pfcSTEPExportInstructions.1',
				},
			},
	}
	IpfcSTEPImport2DInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcImport2DInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcSTEPImport2DInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSTEPImport2DInstructions',
					'info': 'Creates a new data object used for importing a 2-D STEP file into Creo Parametric.',
					'com': 'pfcls.pfcSTEPImport2DInstructions.1',
				},
			},
	}
	IpfcSTLASCIIExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcCoordSysExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcSTLASCIIExportInstructions.Create': {
					'type': 'Function',
					'args': 'CsysName as String [optional]',
					'return': 'IpfcSTLASCIIExportInstructions',
					'info': 'Creates a new instructions object used to export a part or assembly to an ASCII STL file.',
					'com': 'pfcls.pfcSTLASCIIExportInstructions.1',
				},
			},
	}
	IpfcSTLBinaryExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcCoordSysExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcSTLBinaryExportInstructions.Create': {
					'type': 'Function',
					'args': 'CsysName as String [optional]',
					'return': 'IpfcSTLBinaryExportInstructions',
					'info': 'Creates a new instructions object used to export a part or assembly in a binary STL file.',
					'com': 'pfcls.pfcSTLBinaryExportInstructions.1',
				},
			},
	}
	IpfcStringOId = {
			'type': 'Classes',
			'parent': {
				'IpfcOId': '',
			},
			'child': {
				'IpfcParamOId': '',
				'IpfcViewOId': '',
				'IpfcModelOId': '',
				'IpfcMaterialOId': '',
			},
			'properties': {
				'Name': {
					'return': 'as String',
					'info': 'The name of the owner object',
				},
			},
			'method': {
			},
	}
	IpfcSubstAsmRep = {
			'type': 'Classes',
			'parent': {
				'IpfcSubstitution': '',
			},
			'child': {
			},
			'properties': {
				'AsmRep': {
					'return': 'IpfcSimpRep',
					'info': 'Simplified representation handle.',
				},
			},
			'method': {
				'CCpfcSubstAsmRep.Create': {
					'type': 'Function',
					'args': 'SubstPath as Iintseq, AsmRep as IpfcSimpRep',
					'return': 'IpfcSubstAsmRep',
					'info': 'Creates a new object used to substitute an assembly with a simplified representation.',
					'com': 'pfcls.pfcSubstAsmRep.1',
				},
			},
	}
	IpfcSubstEnvelope = {
			'type': 'Classes',
			'parent': {
				'IpfcSubstitution': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcSubstEnvelope.Create': {
					'type': 'Function',
					'args': 'SubstPath as Iintseq',
					'return': 'IpfcSubstEnvelope',
					'info': 'Creates a new object used to substitute with an envelope.',
					'com': 'pfcls.pfcSubstEnvelope.1',
				},
			},
	}
	IpfcSubstInterchg = {
			'type': 'Classes',
			'parent': {
				'IpfcSubstitution': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcSubstInterchg.Create': {
					'type': 'Function',
					'args': 'SubstPath as Iintseq',
					'return': 'IpfcSubstInterchg',
					'info': 'Creates a new object used to substitute using an interchange assembly or a family table.',
					'com': 'pfcls.pfcSubstInterchg.1',
				},
			},
	}
	IpfcSubstitution = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcSubstInterchg': '',
				'IpfcSubstPrtRep': '',
				'IpfcSubstAsmRep': '',
				'IpfcSubstEnvelope': '',
			},
			'properties': {
				'SubstPath': {
					'return': 'Iintseq',
					'info': 'Path to the substitute component.',
				},
			},
			'method': {
				'GetSubstType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSubstType',
					'info': 'Returns a SubstType instance indicating the type of substitution object that can used in the simplified representation.',
				},
			},
	}
	IpfcSubstPrtRep = {
			'type': 'Classes',
			'parent': {
				'IpfcSubstitution': '',
			},
			'child': {
			},
			'properties': {
				'PartRep': {
					'return': 'IpfcSimpRep',
					'info': 'Simplified representation handle (PRO_SUBST_PRT_REP or PRO_SUBST_ASM_REP).',
				},
			},
			'method': {
				'CCpfcSubstPrtRep.Create': {
					'type': 'Function',
					'args': 'SubstPath as Iintseq, PartRep as IpfcSimpRep',
					'return': 'IpfcSubstPrtRep',
					'info': 'Creates a new object used to substitute a part with a simplified representation.',
					'com': 'pfcls.pfcSubstPrtRep.1',
				},
			},
	}
	IpfcSuppressOperation = {
			'type': 'Classes',
			'parent': {
				'IpfcFeatureOperation': '',
			},
			'child': {
			},
			'properties': {
				'AllowChildGroupMembers': {
					'return': 'as Boolean',
					'info': "If true the children of feature, if members of a group,    will be individually suppressed from their group. If false,     then the group containing the feature's children will be also     suppressed. This attribute can be set to    true if and only if both   IpfcSuppressOperation.Clip and     IpfcSuppressOperation.AllowGroupMembers    are set to true. Default value is false.",
				},
				'AllowGroupMembers': {
					'return': 'as Boolean',
					'info': 'If true the feature, if member of a group, will be    individually suppressed from the group. If false, the group    containing the feature will be also suppressed.  This attribute    can be set to true if and only if    IpfcSuppressOperation.Clip    is also set to true. Default value is false.',
				},
				'Clip': {
					'return': 'as Boolean',
					'info': 'A Boolean flag that specifies whether to suppress the selected    feature and all the features that follow.    Default value is false. ',
				},
			},
			'method': {
			},
	}
	IpfcSurface = {
			'type': 'Classes',
			'parent': {
				'IpfcModelItem': '',
			},
			'child': {
				'IpfcTransformedSurface': '',
				'IpfcCoonsPatch': '',
				'IpfcFilletSurface': '',
				'IpfcSplineSurface': '',
				'IpfcNURBSSurface': '',
			},
			'properties': {
				'IsVisible': {
					'return': 'as Boolean',
					'info': 'true if the geometry is visible and active, false if it is invisible and inactive.  Inactive geometry may not have all geometric properties defined.',
				},
				'OwnerQuilt': {
					'return': 'IpfcQuilt',
					'info': 'The quilt that contains the surface.',
				},
			},
			'method': {
				'Eval3DData': {
					'type': 'Function',
					'args': 'Params as IpfcUVParams',
					'return': 'IpfcSurfXYZData',
					'info': 'Evaluates the surface in the specified UV point.',
				},
				'EvalArea': {
					'type': 'Function',
					'args': '',
					'return': ' as Double',
					'info': 'Finds the surface area.',
				},
				'EvalClosestPoint': {
					'type': 'Function',
					'args': 'ToPoint as IpfcPoint3D',
					'return': 'IpfcPoint3D',
					'info': 'Finds the point on the surface that is closest to the given point.  ',
				},
				'EvalClosestPointOnSurface': {
					'type': 'Function',
					'args': 'ToPoint as IpfcPoint3D',
					'return': 'IpfcPoint3D',
					'info': 'Determines whether the specified point is within a small epsilon value of the invoking surface.',
				},
				'EvalDiameter': {
					'type': 'Function',
					'args': 'Params as IpfcUVParams [optional]',
					'return': ' as Double',
					'info': 'Finds the surface diameter at the specified UV point. ',
				},
				'EvalMaximum': {
					'type': 'Function',
					'args': 'Proj as IpfcVector3D',
					'return': 'IpfcPoint3D',
					'info': 'Finds the coordinates of the surface edge at the maximum projection,in the specified direction. The accuracy of this resultis limited to the accuracy of the edge tessellation.',
				},
				'EvalMinimum': {
					'type': 'Function',
					'args': 'Proj as IpfcVector3D',
					'return': 'IpfcPoint3D',
					'info': 'Finds the coordinates of the surface edge at the minimum projection,in the specified direction. The accuracy of this resultis limited to the accuracy of the edge tessellation.',
				},
				'EvalParameters': {
					'type': 'Function',
					'args': 'Point as IpfcPoint3D',
					'return': 'IpfcUVParams',
					'info': 'Finds the corresponding UV point on the surface, given the XYZpoint.',
				},
				'EvalPrincipalCurv': {
					'type': 'Function',
					'args': 'Params as IpfcUVParams',
					'return': 'IpfcCurvatureData',
					'info': 'This method returns the principal curvatures and directions of a surfaceat a specified UV point.',
				},
				'GetFeature': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcFeature',
					'info': 'Returns the feature which contains the geometry.',
				},
				'GetNURBSRepresentation': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcNURBSSurfaceDescriptor',
					'info': 'This method calculates the surface geometry as a non-uniform rational B-spline surface.',
				},
				'GetOrientation': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSurfaceOrientation',
					'info': 'This method returns the orientation of the surface',
				},
				'GetSurfaceDescriptor': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSurfaceDescriptor',
					'info': 'This method returns a data object representing the geometry of the surface.',
				},
				'GetSurfaceType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSurfaceType',
					'info': 'This method returns the type of the surface.',
				},
				'GetUVExtents': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcUVOutline',
					'info': 'This method returns the UV parameters at the corners of the surface.',
				},
				'GetXYZExtents': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcOutline3D',
					'info': 'This method returns the XYZ points at the corners of the surface.',
				},
				'ListContours': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcContours',
					'info': 'Lists all the contours on the surface.  ',
				},
				'ListSameSurfaces': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSurfaces',
					'info': 'Finds and returns an array of surfaces that are the same as theinput surface. For example, in the case of a cylinder, Creo Parametric   creates two, half-cylindrical surfaces. If you input one half of the cylinder, this function returns the other half.',
				},
				'VerifyUV': {
					'type': 'Function',
					'args': 'Params as IpfcUVParams',
					'return': 'IpfcPlacement',
					'info': 'Verifies whether the specified UV point lies within the surfaceboundaries.',
				},
			},
	}
	IpfcSurfaceDescriptor = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcTransformedSurfaceDescriptor': '',
				'IpfcCoonsPatchDescriptor': '',
				'IpfcFilletSurfaceDescriptor': '',
				'IpfcSplineSurfaceDescriptor': '',
				'IpfcNURBSSurfaceDescriptor': '',
			},
			'properties': {
				'Extents': {
					'return': 'IpfcSurfaceExtents',
					'info': 'The minimum and maximum points on the surface',
				},
				'Orientation': {
					'return': 'IpfcSurfaceOrientation',
					'info': 'The orientation of the surface',
				},
			},
			'method': {
				'GetSurfaceType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSurfaceType',
					'info': 'This method returns the type of surface represented by a SurfaceDescriptor.',
				},
			},
	}
	IpfcSurfaceExtents = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'UVExtents': {
					'return': 'IpfcUVOutline',
					'info': 'The UV parameters of the two corner points of the surface.',
				},
				'XYZExtents': {
					'return': 'IpfcOutline3D',
					'info': 'The XYZ points corresponding to the UV corner points.  Used only for output purposes.',
				},
			},
			'method': {
				'CCpfcSurfaceExtents.Create': {
					'type': 'Function',
					'args': 'UVExtents as IpfcUVOutline',
					'return': 'IpfcSurfaceExtents',
					'info': 'This method creates a new SurfaceExtents object.',
					'com': 'pfcls.pfcSurfaceExtents.1',
				},
			},
	}
	IpfcSurfXYZData = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Derivative1': {
					'return': 'IpfcVector3D',
					'info': 'The first partial derivatives of X, Y, and Z, with respect to u and v ',
				},
				'Derivative2': {
					'return': 'IpfcVector3D',
					'info': 'The first partial derivatives of X, Y, and Z, with respect to u and v',
				},
				'Normal': {
					'return': 'IpfcVector3D',
					'info': 'A unit vector in the direction of the outward normal to the surface at that point',
				},
				'Params': {
					'return': 'IpfcUVParams',
					'info': 'The u and v parameters ',
				},
				'Point': {
					'return': 'IpfcPoint3D',
					'info': 'The X, Y, and Z coordinates of the point, with respect to the model coordinates',
				},
			},
			'method': {
			},
	}
	IpfcSWAsm3DExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExport3DInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcSWAsm3DExportInstructions.Create': {
					'type': 'Function',
					'args': 'inConfiguration as IpfcAssemblyConfiguration, inGeometry as IpfcGeometryFlags',
					'return': 'IpfcSWAsm3DExportInstructions',
					'info': '        Creates a new instructions object used to export a solid model to SolidWorks assembly format.      ',
					'com': 'pfcls.pfcSWAsm3DExportInstructions.1',
				},
			},
	}
	IpfcSWPart3DExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExport3DInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcSWPart3DExportInstructions.Create': {
					'type': 'Function',
					'args': 'inConfiguration as IpfcAssemblyConfiguration, inGeometry as IpfcGeometryFlags',
					'return': 'IpfcSWPart3DExportInstructions',
					'info': '        Creates a new instructions object used to export a solid model to SolidWorks part format.      ',
					'com': 'pfcls.pfcSWPart3DExportInstructions.1',
				},
			},
	}
	IpfcSymbolDefAttachment = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'AttachmentParam': {
					'return': 'as Double [optional]',
					'info': 'The parameter on the entity (typically a t parameter).  null, if the attachment type is SYMDEFATTACH_FREE or SYMDEFATTACH_RADIAL_LEADER. ',
				},
				'AttachmentPoint': {
					'return': 'IpfcPoint3D',
					'info': 'The attachment point, in the coordinate system of the symbol definition.',
				},
				'EntityId': {
					'return': 'as Long [optional]',
					'info': 'The identifier of the symbol definition entity.  null, if the attachment type is SYMDEFATTACH_FREE.',
				},
				'Type': {
					'return': 'IpfcSymbolDefAttachmentType',
					'info': 'The type of attachment.',
				},
			},
			'method': {
				'CCpfcSymbolDefAttachment.Create': {
					'type': 'Function',
					'args': 'inType as IpfcSymbolDefAttachmentType, inAttachmentPoint as IpfcPoint3D',
					'return': 'IpfcSymbolDefAttachment',
					'info': 'Creates a data object that represents the way that an instance may be attached to a symbol definition.',
					'com': 'pfcls.pfcSymbolDefAttachment.1',
				},
			},
	}
	IpfcTable = {
			'type': 'Classes',
			'parent': {
				'IpfcModelItem': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CheckIfIsFromFormat': {
					'type': 'Function',
					'args': 'SheetNumber as Long',
					'return': ' as Boolean',
					'info': 'Identifies if the drawing table was created by the format.',
				},
				'DeleteColumn': {
					'type': 'Sub',
					'args': 'Column as Long, Repaint as Boolean [optional]',
					'return': '',
					'info': 'Deletes a column in the table.',
				},
				'DeleteRow': {
					'type': 'Sub',
					'args': 'Row as Long, Repaint as Boolean [optional]',
					'return': '',
					'info': 'Deletes a row in the table.',
				},
				'Display': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Displays the table, if it has been erased.',
				},
				'Erase': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Erases the table.',
				},
				'GetCellComponentModel': {
					'type': 'Function',
					'args': 'Cell as IpfcTableCell',
					'return': 'IpfcComponentPath',
					'info': 'Returns the component model referred to by a cell in a repeat region of a table.  This function will not return a valid result if the cell has the attribute "NO DUPLICATE" or "NO DUPLICATE/LEVEL" as there is no unique path available. In that case use the functions IpfcTable.GetCellTopModel() (for Top level model) or IpfcTable.GetCellReferenceModel().',
				},
				'GetCellNote': {
					'type': 'Function',
					'args': 'Cell as IpfcTableCell',
					'return': 'IpfcModelItem',
					'info': 'Returns the detail note contained in the table cell.',
				},
				'GetCellReferenceModel': {
					'type': 'Function',
					'args': 'Cell as IpfcTableCell',
					'return': 'IpfcModel',
					'info': 'Returns the reference component referred to by thiscell in a repeat region drawing table. Differs from IpfcTable.GetCellComponentModel() in that this function will return the reference object if the cell attribute is set to "NO DUPLICATE" or "NO DUPLICATE/LEVEL". ',
				},
				'GetCellTopModel': {
					'type': 'Function',
					'args': 'Cell as IpfcTableCell',
					'return': 'IpfcAssembly',
					'info': 'Returns the top model referred to by thiscell in a repeat region drawing table. Differs from IpfcTable.GetCellComponentModel() in that this function will return the object if the cell attribute is set to "NO DUPLICATE" or "NO DUPLICATE/LEVEL". ',
				},
				'GetColumnCount': {
					'type': 'Function',
					'args': '',
					'return': ' as Long',
					'info': 'Returns the number of columns in the table.',
				},
				'GetColumnSize': {
					'type': 'Function',
					'args': 'SegmentId as Long, Column as Long',
					'return': ' as Double',
					'info': 'Returns the width of the drawing table column.',
				},
				'GetInfo': {
					'type': 'Function',
					'args': 'SegmentId as Long',
					'return': 'IpfcTableInfo',
					'info': 'Returns information about the drawing table.',
				},
				'GetRowCount': {
					'type': 'Function',
					'args': '',
					'return': ' as Long',
					'info': 'Returns the number of rows in the table.',
				},
				'GetRowSize': {
					'type': 'Function',
					'args': 'SegmentId as Long, Row as Long',
					'return': ' as Double',
					'info': 'Returns the height of the row.',
				},
				'GetSegmentCount': {
					'type': 'Function',
					'args': '',
					'return': ' as Long',
					'info': 'Returns the number of segments in the table.',
				},
				'GetSegmentSheet': {
					'type': 'Function',
					'args': 'SegmentNumber as Long',
					'return': ' as Long',
					'info': 'Returns the sheet where a particuar table segment lies.',
				},
				'GetText': {
					'type': 'Function',
					'args': 'Cell as IpfcTableCell, Mode as IpfcParamMode',
					'return': 'Istringseq',
					'info': 'Returns the text in a drawing table cell.',
				},
				'InsertColumn': {
					'type': 'Sub',
					'args': 'Width as Double, InsertAfterColumn as Long [optional], Repaint as Boolean [optional]',
					'return': '',
					'info': 'Inserts a column in the drawing table.',
				},
				'InsertRow': {
					'type': 'Sub',
					'args': 'Height as Double, InsertAfterRow as Long [optional], Repaint as Boolean [optional]',
					'return': '',
					'info': 'Inserts a new row into the table.',
				},
				'IsCommentCell': {
					'type': 'Function',
					'args': 'Cell as IpfcTableCell',
					'return': ' as Boolean',
					'info': 'Determines if a table cell is a comment cell in a repeat region.',
				},
				'MergeRegion': {
					'type': 'Sub',
					'args': 'UpperLeft as IpfcTableCell, LowerRight as IpfcTableCell, Repaint as Boolean [optional]',
					'return': '',
					'info': 'Merges a rectangular section of table cells.',
				},
				'MoveSegment': {
					'type': 'Sub',
					'args': 'SegmentNumber as Long, NewPosition as IpfcPoint3D, Repaint as Boolean [optional]',
					'return': '',
					'info': 'Moves a segment of the table.',
				},
				'RotateClockwise': {
					'type': 'Sub',
					'args': 'Rotation as IpfcRotationDegree, Repaint as Boolean [optional]',
					'return': '',
					'info': 'Rotates a table clockwise.',
				},
				'SetText': {
					'type': 'Sub',
					'args': 'Cell as IpfcTableCell, Lines as Istringseq',
					'return': '',
					'info': 'Sets the text in the table cell.',
				},
				'SubdivideRegion': {
					'type': 'Sub',
					'args': 'UpperLeft as IpfcTableCell, LowerRight as IpfcTableCell, Repaint as Boolean [optional]',
					'return': '',
					'info': 'Removes all merges in the specified region of previously merged cells.',
				},
			},
	}
	IpfcTableCell = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'ColumnNumber': {
					'return': 'as Long',
					'info': 'The column number.  Column numbers range from 1 to the number of columns.',
				},
				'RowNumber': {
					'return': 'as Long',
					'info': 'The row number.  Row numbers range from 1 to the number of rows.',
				},
			},
			'method': {
				'CCpfcTableCell.Create': {
					'type': 'Function',
					'args': 'RowNumber as Long, ColumnNumber as Long',
					'return': 'IpfcTableCell',
					'info': 'Creates a new data object representing a cell in drawing table.',
					'com': 'pfcls.pfcTableCell.1',
				},
			},
	}
	IpfcTableCreateInstructions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'ColumnData': {
					'return': 'IpfcColumnCreateOptions',
					'info': 'Sequence of column heights and justifications (the number of elements included indicates the number of columns to create). The maximum number of columns that can be created is 50.',
				},
				'Origin': {
					'return': 'IpfcPoint3D',
					'info': 'The origin for the table placement.',
				},
				'RowHeights': {
					'return': 'Irealseq',
					'info': 'Sequence of row heights (the number of heights included indicates the number of rows to create). The maximum number of rows that can be created is 100.',
				},
				'SizeType': {
					'return': 'IpfcTableSizeType',
					'info': 'Indicates the scale used when calculating the size of rows and columns.',
				},
			},
			'method': {
				'CCpfcTableCreateInstructions.Create': {
					'type': 'Function',
					'args': 'Origin as IpfcPoint3D',
					'return': 'IpfcTableCreateInstructions',
					'info': 'Creates a data object used to create a table, when passed to IpfcTableOwner.CreateTable().',
					'com': 'pfcls.pfcTableCreateInstructions.1',
				},
			},
	}
	IpfcTableInfo = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'CharHeight': {
					'return': 'as Double',
					'info': '  ',
				},
				'CharWidth': {
					'return': 'as Double',
					'info': '  ',
				},
				'NumberOfColumns': {
					'return': 'as Long',
					'info': 'The number of columns in the table.',
				},
				'NumberOfRows': {
					'return': 'as Long',
					'info': 'The number of rows in the table.',
				},
				'Origin': {
					'return': 'IpfcPoint3D',
					'info': 'The table origin.',
				},
				'Outline': {
					'return': 'IpfcOutline3D',
					'info': 'The coordinates of the table outline.',
				},
				'Rotation': {
					'return': 'as Double',
					'info': 'The table rotation angle, in degrees.',
				},
				'SegCharHeight': {
					'return': 'as Double',
					'info': 'The character height in the table segment.',
				},
			},
			'method': {
			},
	}
	IpfcTableOwner = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcModel2D': '',
			},
			'properties': {
			},
			'method': {
				'CreateTable': {
					'type': 'Function',
					'args': 'Instructions as IpfcTableCreateInstructions',
					'return': 'IpfcTable',
					'info': 'Creates a table in the model.',
				},
				'DeleteTable': {
					'type': 'Sub',
					'args': 'Table as IpfcTable, Repaint as Boolean [optional]',
					'return': '',
					'info': 'Deletes a table from the model.',
				},
				'GetTable': {
					'type': 'Function',
					'args': 'Id as Long',
					'return': 'IpfcTable',
					'info': 'Gets a table in the model, given its identifier.',
				},
				'ListTables': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcTables',
					'info': 'Lists the tables in the model.',
				},
				'RetrieveTable': {
					'type': 'Function',
					'args': 'Instructions as IpfcTableRetrieveInstructions',
					'return': 'IpfcTable',
					'info': "Retrieves a table from a file on disk.This method always place table's top left corner at a point sepcified by instruction.",
				},
				'RetrieveTableByOrigin': {
					'type': 'Function',
					'args': 'Instructions as IpfcTableRetrieveInstructions',
					'return': 'IpfcTable',
					'info': "Retrieves a table from a file on disk. This method place table's origin at a point sepcified by instructions.",
				},
				'UpdateTables': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Updates all drawing tables.',
				},
			},
	}
	IpfcTableRetrieveInstructions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'FileName': {
					'return': 'as String',
					'info': 'Name, excluding the extension.',
				},
				'Path': {
					'return': 'as String [optional]',
					'info': 'Path to the table file, or null, if the file is in the working directory.',
				},
				'Position': {
					'return': 'IpfcPoint3D',
					'info': 'The position for the retrieved table.',
				},
				'ReferenceRep': {
					'return': 'IpfcSimpRep',
					'info': 'The simplified representation to use when copying data in the table.  If null, the master representation of the reference solid will be used.',
				},
				'ReferenceSolid': {
					'return': 'IpfcSolid',
					'info': 'The solid model from which to copy data into the table.  If null, the table will not use any solid model data.',
				},
				'Version': {
					'return': 'as Long [optional]',
					'info': 'The file version, or null, for the latest file.',
				},
			},
			'method': {
				'CCpfcTableRetrieveInstructions.Create': {
					'type': 'Function',
					'args': 'FileName as String, Position as IpfcPoint3D',
					'return': 'IpfcTableRetrieveInstructions',
					'info': 'Creates a data object containing instructions to retrieve a drawing table.   Pass this function to IpfcTableOwner.RetrieveTable(). or IpfcTableOwner.RetrieveTableByOrigin().',
					'com': 'pfcls.pfcTableRetrieveInstructions.1',
				},
			},
	}
	IpfcTabulatedCylinder = {
			'type': 'Classes',
			'parent': {
				'IpfcTransformedSurface': '',
			},
			'child': {
			},
			'properties': {
				'Profile': {
					'return': 'IpfcCurveDescriptor',
					'info': 'The geometry of the curve that is projected',
				},
			},
			'method': {
			},
	}
	IpfcTabulatedCylinderDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcTransformedSurfaceDescriptor': '',
			},
			'child': {
			},
			'properties': {
				'Profile': {
					'return': 'IpfcCurveDescriptor',
					'info': 'The geometry of the curve that is projected',
				},
			},
			'method': {
				'CCpfcTabulatedCylinderDescriptor.Create': {
					'type': 'Function',
					'args': 'Extents as IpfcSurfaceExtents, Orientation as IpfcSurfaceOrientation, Origin as IpfcTransform3D, Profile as IpfcCurveDescriptor',
					'return': 'IpfcTabulatedCylinderDescriptor',
					'info': 'This method creates a new TabulatedCylinderDescriptor object.',
					'com': 'pfcls.pfcTabulatedCylinderDescriptor.1',
				},
			},
	}
	IpfcTangentIndexDimensionSense = {
			'type': 'Classes',
			'parent': {
				'IpfcDimensionSense': '',
			},
			'child': {
			},
			'properties': {
				'TangentIndex': {
					'return': 'as Long',
					'info': 'The index of the tangent attachment point.',
				},
			},
			'method': {
				'CCpfcTangentIndexDimensionSense.Create': {
					'type': 'Function',
					'args': 'TangentIndex as Long',
					'return': 'IpfcTangentIndexDimensionSense',
					'info': 'Creates a new tangent index dimension sense object for use in creating a drawing dimension.',
					'com': 'pfcls.pfcTangentIndexDimensionSense.1',
				},
			},
	}
	IpfcTangentIndexDimSense = {
			'type': 'Classes',
			'parent': {
				'IpfcDimSense': '',
			},
			'child': {
			},
			'properties': {
				'TangentIndex': {
					'return': 'as Long',
					'info': '  ',
				},
			},
			'method': {
				'CCpfcTangentIndexDimSense.Create': {
					'type': 'Function',
					'args': 'TangentIndex as Long',
					'return': 'IpfcTangentIndexDimSense',
					'info': '  ',
					'com': 'pfcls.pfcTangentIndexDimSense.1',
				},
			},
	}
	IpfcTangentLeaderAttachment = {
			'type': 'Classes',
			'parent': {
				'IpfcDetailLeaderAttachment': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcTangentLeaderAttachment.Create': {
					'type': 'Function',
					'args': 'LeaderAttachs as IpfcAttachment',
					'return': 'IpfcTangentLeaderAttachment',
					'info': 'Creates tangent leader attachment. ',
					'com': 'pfcls.pfcTangentLeaderAttachment.1',
				},
			},
	}
	IpfcText = {
			'type': 'Classes',
			'parent': {
				'IpfcCurve': '',
			},
			'child': {
			},
			'properties': {
				'Point': {
					'return': 'IpfcPoint3D',
					'info': 'The point at which to start the text string',
				},
				'Style': {
					'return': 'IpfcTextStyle',
					'info': 'The text style',
				},
				'TextString': {
					'return': 'as String',
					'info': 'The text to display',
				},
			},
			'method': {
			},
	}
	IpfcTextDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcCurveDescriptor': '',
			},
			'child': {
			},
			'properties': {
				'Point': {
					'return': 'IpfcPoint3D',
					'info': 'The location of the text',
				},
				'Style': {
					'return': 'IpfcTextStyle',
					'info': 'The text style',
				},
				'TextString': {
					'return': 'as String',
					'info': 'The text string',
				},
			},
			'method': {
				'CCpfcTextDescriptor.Create': {
					'type': 'Function',
					'args': 'TextString as String, Point as IpfcPoint3D, Style as IpfcTextStyle',
					'return': 'IpfcTextDescriptor',
					'info': 'This method creates a new TextDescriptor object.',
					'com': 'pfcls.pfcTextDescriptor.1',
				},
			},
	}
	IpfcTextReference = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'TextIndexNumber': {
					'return': 'as Long',
					'info': 'The index of the text, on this line.',
				},
				'TextLineNumber': {
					'return': 'as Long',
					'info': 'The line number of the note.',
				},
				'TextRefNote': {
					'return': 'IpfcDetailNoteItem',
					'info': 'The note item containing the text.',
				},
			},
			'method': {
				'CCpfcTextReference.Create': {
					'type': 'Function',
					'args': 'inTextRefNote as IpfcDetailNoteItem, inTextLineNumber as Long, inTextIndexNumber as Long',
					'return': 'IpfcTextReference',
					'info': 'Creates a data object that identifies the text item used for a symbol definition which as a height type of SYMDEF_TEXT_RELATED. ',
					'com': 'pfcls.pfcTextReference.1',
				},
			},
	}
	IpfcTextStyle = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Angle': {
					'return': 'as Double',
					'info': 'The angle of rotation of the whole text, in counterclockwise degrees.  ',
				},
				'FontName': {
					'return': 'as String',
					'info': 'The font name.',
				},
				'Height': {
					'return': 'as Double',
					'info': 'The text height, in screen coordinates. ',
				},
				'IsMirrored': {
					'return': 'as Boolean [optional]',
					'info': 'If this is true, the text is mirrored. Otherwise, the text is not mirrored.',
				},
				'IsUnderlined': {
					'return': 'as Boolean [optional]',
					'info': 'If this is true, the text is underlined. Otherwise, the text is not underlined.',
				},
				'SlantAngle': {
					'return': 'as Double',
					'info': 'The slant angle of the text, in clockwise degrees.',
				},
				'Thickness': {
					'return': 'as Double',
					'info': 'The text thickness.',
				},
				'WidthFactor': {
					'return': 'as Double',
					'info': 'The ratio of the width of each character (including the gap) as a proportion of the height. ',
				},
			},
			'method': {
				'CCpfcTextStyle.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcTextStyle',
					'info': 'Creates a new text style object.',
					'com': 'pfcls.pfcTextStyle.1',
				},
			},
	}
	IpfcTIFFImageExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcRasterImageExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcTIFFImageExportInstructions.Create': {
					'type': 'Function',
					'args': 'ImageWidth as Double, ImageHeight as Double',
					'return': 'IpfcTIFFImageExportInstructions',
					'info': 'Creates a new instructions object used to export TIFF image.',
					'com': 'pfcls.pfcTIFFImageExportInstructions.1',
				},
			},
	}
	IpfcTorus = {
			'type': 'Classes',
			'parent': {
				'IpfcTransformedSurface': '',
			},
			'child': {
			},
			'properties': {
				'Radius1': {
					'return': 'as Double',
					'info': 'The distance from the center of the generating arc to the axis of revolution ',
				},
				'Radius2': {
					'return': 'as Double',
					'info': 'The radius of the generating arc',
				},
			},
			'method': {
			},
	}
	IpfcTorusDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcTransformedSurfaceDescriptor': '',
			},
			'child': {
			},
			'properties': {
				'Radius1': {
					'return': 'as Double',
					'info': 'The distance from the center of the generating arc to the axis of revolution ',
				},
				'Radius2': {
					'return': 'as Double',
					'info': 'The radius of the generating arc',
				},
			},
			'method': {
				'CCpfcTorusDescriptor.Create': {
					'type': 'Function',
					'args': 'Extents as IpfcSurfaceExtents, Orientation as IpfcSurfaceOrientation, Origin as IpfcTransform3D, Radius1 as Double, Radius2 as Double',
					'return': 'IpfcTorusDescriptor',
					'info': 'This method creates a new TorusDescriptor object.',
					'com': 'pfcls.pfcTorusDescriptor.1',
				},
			},
	}
	IpfcTransform3D = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Matrix': {
					'return': 'IpfcMatrix3D',
					'info': 'The transformation matrix',
				},
			},
			'method': {
				'CCpfcTransform3D.Create': {
					'type': 'Function',
					'args': 'Matrix as IpfcMatrix3D [optional]',
					'return': 'IpfcTransform3D',
					'info': 'Creates a Transform3D object. ',
					'com': 'pfcls.pfcTransform3D.1',
				},
				'GetOrigin': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcPoint3D',
					'info': 'Retrieves the point that represents the origin of the coordinate system.',
				},
				'GetXAxis': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcVector3D',
					'info': 'Retrieves the vector that represents the X-axis.',
				},
				'GetYAxis': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcVector3D',
					'info': 'Retrieves the vector that represents the Y-axis.',
				},
				'GetZAxis': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcVector3D',
					'info': 'Retrieves the vector that represents the Z-axis.',
				},
				'Invert': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Inverts the 4 x 4 matrix contained in the transform object.',
				},
				'SetOrigin': {
					'type': 'Sub',
					'args': 'Input as IpfcPoint3D',
					'return': '',
					'info': 'This method specifies the point that represents the origin of the coordinate system.',
				},
				'SetXAxis': {
					'type': 'Sub',
					'args': 'Input as IpfcVector3D',
					'return': '',
					'info': 'This method returns a vector that representing the X-axis of the coordinate system.',
				},
				'SetYAxis': {
					'type': 'Sub',
					'args': 'Input as IpfcVector3D',
					'return': '',
					'info': 'This method specifies a vector that represents the Y-axis of the coordinate system.',
				},
				'SetZAxis': {
					'type': 'Sub',
					'args': 'Input as IpfcVector3D',
					'return': '',
					'info': 'Specifies the vector that represents the Z-axis of the coordinate system.',
				},
				'TransformPoint': {
					'type': 'Function',
					'args': 'Input as IpfcPoint3D',
					'return': 'IpfcPoint3D',
					'info': 'Transforms the specified point using the   transformation matrix (both shift and rotation).',
				},
				'TransformVector': {
					'type': 'Function',
					'args': 'Input as IpfcVector3D',
					'return': 'IpfcVector3D',
					'info': 'Transforms the specified vector by the given transformation matrix (rotation only).',
				},
			},
	}
	IpfcTransformedSurface = {
			'type': 'Classes',
			'parent': {
				'IpfcSurface': '',
			},
			'child': {
				'IpfcPlane': '',
				'IpfcCylinder': '',
				'IpfcCone': '',
				'IpfcTorus': '',
				'IpfcRevolvedSurface': '',
				'IpfcRuledSurface': '',
				'IpfcTabulatedCylinder': '',
				'IpfcCylindricalSplineSurface': '',
				'IpfcSphericalSplineSurface': '',
				'IpfcForeignSurface': '',
			},
			'properties': {
				'CoordSys': {
					'return': 'IpfcTransform3D',
					'info': 'The origin and unit vectors of the surface',
				},
			},
			'method': {
			},
	}
	IpfcTransformedSurfaceDescriptor = {
			'type': 'Classes',
			'parent': {
				'IpfcSurfaceDescriptor': '',
			},
			'child': {
				'IpfcPlaneDescriptor': '',
				'IpfcCylinderDescriptor': '',
				'IpfcConeDescriptor': '',
				'IpfcTorusDescriptor': '',
				'IpfcRevolvedSurfaceDescriptor': '',
				'IpfcRuledSurfaceDescriptor': '',
				'IpfcTabulatedCylinderDescriptor': '',
				'IpfcCylindricalSplineSurfaceDescriptor': '',
				'IpfcSphericalSplineSurfaceDescriptor': '',
				'IpfcForeignSurfaceDescriptor': '',
			},
			'properties': {
				'CoordSys': {
					'return': 'IpfcTransform3D',
					'info': 'The origin and unit vectors of the surface object',
				},
			},
			'method': {
			},
	}
	IpfcTriangulationInstructions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'AngleControl': {
					'return': 'as Double',
					'info': '        The angle control to use for the exported facets. Value is from 0.0 to 1.0.      ',
				},
				'ChordHeight': {
					'return': 'as Double',
					'info': '        The chord height to use for the exported facets.      ',
				},
				'FacetControlOptions': {
					'return': 'IpfcFacetControlFlags',
					'info': '       Flags (FACET_STEP_SIZE_ADJUST and others) to control Facet export.     ',
				},
				'StepSize': {
					'return': 'as Double',
					'info': '       The step size to use for the exported facets.     ',
				},
			},
			'method': {
				'CCpfcTriangulationInstructions.Create': {
					'type': 'Function',
					'args': 'AngleControl as Double, ChordHeight as Double',
					'return': 'IpfcTriangulationInstructions',
					'info': '        Creates a new  object used to indicate the parameters used for faceting.      ',
					'com': 'pfcls.pfcTriangulationInstructions.1',
				},
			},
	}
	IpfcUDFAssemblyIntersection = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'ComponentPath': {
					'return': 'Iintseq',
					'info': 'The member identifier table that defines the location of the intersected part in the assembly that contains the UDF.',
				},
				'InstanceNames': {
					'return': 'Istringseq',
					'info': 'An array of names for the new instances of parts created to represent the intersection geometry.',
				},
				'VisibilityLevel': {
					'return': 'as Long',
					'info': 'The length of the IpfcComponentPath that corresponds to the visibility level of the intersected part in the assembly. If VisibilityLevel == the number of entries in the IpfcComponentPath sequence, the feature is visible in the part that it intersects. If VisibilityLevel is 0, the feature is visible at the level of the assembly containing the UDF. ',
				},
			},
			'method': {
				'CCpfcUDFAssemblyIntersection.Create': {
					'type': 'Function',
					'args': 'ComponentPath as Iintseq, VisibilityLevel as Long',
					'return': 'IpfcUDFAssemblyIntersection',
					'info': 'Creates an assembly intersection object, which is used to place a UDF programatically.',
					'com': 'pfcls.pfcUDFAssemblyIntersection.1',
				},
			},
	}
	IpfcUDFCustomCreateInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcUDFGroupCreateInstructions': '',
			},
			'child': {
			},
			'properties': {
				'AssemblyReference': {
					'return': 'IpfcComponentPath',
					'info': 'This property specifies the path to the placement model from the top-level assembly used by reference selections, if the placement model is not the top level assembly.  This is necessary whenever external references are used for placement, or whenever the reference selections are made from a context outside of the placement model.  ',
				},
				'DependencyType': {
					'return': 'IpfcUDFDependencyType',
					'info': 'The dependency of the UDF. The choices correspond to the choices available when you create the UDF interactively.  ',
				},
				'DimDisplayType': {
					'return': 'IpfcUDFDimensionDisplayType',
					'info': 'These values correspond to the options in Creo Parametric for determining the appearance in the model of UDF dimensions and pattern parameters that were not variable in the UDF, and therefore cannot be modified in the model. ',
				},
				'ExtReferences': {
					'return': 'IpfcUDFExternalReferences',
					'info': 'This property is deprecated.  To specify the path to the placement model from the top-level assembly used by reference selections use  the AssemblyReference property.  ',
				},
				'InstanceName': {
					'return': 'as String [optional]',
					'info': 'If the UDF contains a family table, this field can be used to select which instance in that table is to be selected. If the UDF contains no family table, or if the generic instance is to be selected, the string should be empty. ',
				},
				'Intersections': {
					'return': 'IpfcUDFAssemblyIntersections',
					'info': 'An array of intersections of the UDF and parts within the assembly.',
				},
				'Orientations': {
					'return': 'IpfcUDFOrientations',
					'info': "An array of orientations that provide the answers to Creo Parametric prompts that use a flip arrow. The order of orientations should correspond to the order in which Creo Parametric prompts for them when the UDF is created interactively. If you do not provide an orientation that Creo Parametric needs, it uses the default value ``no flip.''",
				},
				'Quadrants': {
					'return': 'IpfcPoint3Ds',
					'info': 'An array of points, which provide the X, Y, and Z coordinates that correspond to the picks answering the Creo Parametric prompts for the feature positions. The order of quadrants should correspond to the order in which Creo Parametric prompts for them when the UDF is created interactively. ',
				},
				'References': {
					'return': 'IpfcUDFReferences',
					'info': 'An array of element references.',
				},
				'Scale': {
					'return': 'as Double [optional]',
					'info': 'If the value of the ScaleType field is UDFSCALE_CUSTOM, this field is the user-defined scale factor. Otherwise, this field is ignored. ',
				},
				'ScaleType': {
					'return': 'IpfcUDFScaleType',
					'info': 'Specifies what should happen if the UDF used different length units than the model to which it is being applied.',
				},
				'VariantValues': {
					'return': 'IpfcUDFVariantValues',
					'info': 'An array of variable dimensions and pattern parameters',
				},
			},
			'method': {
				'CCpfcUDFCustomCreateInstructions.Create': {
					'type': 'Function',
					'args': 'Name as String',
					'return': 'IpfcUDFCustomCreateInstructions',
					'info': 'Creates an instructions object, which is used to place a UDF programmatically.',
					'com': 'pfcls.pfcUDFCustomCreateInstructions.1',
				},
			},
	}
	IpfcUDFDimension = {
			'type': 'Classes',
			'parent': {
				'IpfcBaseDimension': '',
			},
			'child': {
			},
			'properties': {
				'UDFDimensionName': {
					'return': 'as String',
					'info': 'The dimension name specified when the UDF was created.',
				},
			},
			'method': {
			},
	}
	IpfcUDFExternalReference = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'ExternalReferenceModel': {
					'return': 'IpfcComponentPath',
					'info': 'The external component in an assembly to which the reference is made.',
				},
			},
			'method': {
				'CCpfcUDFExternalReference.Create': {
					'type': 'Function',
					'args': 'Model as IpfcComponentPath',
					'return': 'IpfcUDFExternalReference',
					'info': 'Creates a UDFExternalReference object that is required when placing a UDF with external references.',
					'com': 'pfcls.pfcUDFExternalReference.1',
				},
			},
	}
	IpfcUDFGroupCreateInstructions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcUDFPromptCreateInstructions': '',
				'IpfcUDFCustomCreateInstructions': '',
			},
			'properties': {
				'Name': {
					'return': 'as String',
					'info': 'The name of the UDF to create.',
				},
				'ShowModifyNCSeqMenu': {
					'return': 'as Boolean',
					'info': 'Whether or not to display the NC Sequence menu for manufacturing UDFs.',
				},
				'ShowUDFEditMenu': {
					'return': 'as Boolean',
					'info': 'Whether or not to display the UDF Edit menu.',
				},
				'TurnOffFixModelUI': {
					'return': 'as Boolean',
					'info': 'Whether or not to allow user access to the <b>Fix Model</b> UI to fix UDF placement using Creo Parametric UI if programmatic placement fails.',
				},
				'TurnOffUDFRedefineMenu': {
					'return': 'as Boolean',
					'info': 'Whether or not to disable redefine menu for feature elements.',
				},
				'UseExistingTools': {
					'return': 'as Boolean',
					'info': '  ',
				},
			},
			'method': {
			},
	}
	IpfcUDFPromptCreateInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcUDFGroupCreateInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcUDFPromptCreateInstructions.Create': {
					'type': 'Function',
					'args': 'Name as String',
					'return': 'IpfcUDFPromptCreateInstructions',
					'info': 'Creates a UDFPromptCreateInstructions object, that makes Creo Parametric prompt the user to create a UDF.',
					'com': 'pfcls.pfcUDFPromptCreateInstructions.1',
				},
			},
	}
	IpfcUDFReference = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'IsExternal': {
					'return': 'as Boolean',
					'info': 'Determines whether reference is to current solid or to an external solid.',
				},
				'PromptForReference': {
					'return': 'as String',
					'info': 'String given to user if placing UDF interactively.',
				},
				'ReferenceItem': {
					'return': 'IpfcSelection',
					'info': 'Item used for reference.',
				},
			},
			'method': {
				'CCpfcUDFReference.Create': {
					'type': 'Function',
					'args': 'PromptForReference as String, ReferenceItem as IpfcSelection',
					'return': 'IpfcUDFReference',
					'info': 'Creates a UDF reference that is required when placing a UDF with references.',
					'com': 'pfcls.pfcUDFReference.1',
				},
			},
	}
	IpfcUDFVariantDimension = {
			'type': 'Classes',
			'parent': {
				'IpfcUDFVariantValue': '',
			},
			'child': {
			},
			'properties': {
				'DimensionValue': {
					'return': 'as Double',
					'info': 'The dimension value.',
				},
			},
			'method': {
				'CCpfcUDFVariantDimension.Create': {
					'type': 'Function',
					'args': 'Name as String, DimensionValue as Double',
					'return': 'IpfcUDFVariantDimension',
					'info': 'Specifies the value of the dimension the user must supply to place the UDF.',
					'com': 'pfcls.pfcUDFVariantDimension.1',
				},
			},
	}
	IpfcUDFVariantPatternParam = {
			'type': 'Classes',
			'parent': {
				'IpfcUDFVariantValue': '',
			},
			'child': {
			},
			'properties': {
				'PatternParam': {
					'return': 'as Long',
					'info': 'The parameter name',
				},
			},
			'method': {
				'CCpfcUDFVariantPatternParam.Create': {
					'type': 'Function',
					'args': 'Name as String, PatternParam as Long',
					'return': 'IpfcUDFVariantPatternParam',
					'info': 'Specifies the name of the pattern parameter the user must supply to place the UDF.',
					'com': 'pfcls.pfcUDFVariantPatternParam.1',
				},
			},
	}
	IpfcUDFVariantValue = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcUDFVariantDimension': '',
				'IpfcUDFVariantPatternParam': '',
			},
			'properties': {
				'Name': {
					'return': 'as String',
					'info': 'Name of variant value.',
				},
			},
			'method': {
				'GetType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcUDFVariantValueType',
					'info': 'Returns the variant value type, eithervariant dimension or variant parameter.',
				},
			},
	}
	IpfcUG3DExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExport3DInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcUG3DExportInstructions.Create': {
					'type': 'Function',
					'args': 'inConfiguration as IpfcAssemblyConfiguration, inGeometry as IpfcGeometryFlags',
					'return': 'IpfcUG3DExportInstructions',
					'info': 'Creates a new instructions object used to export a solid model to UG format.',
					'com': 'pfcls.pfcUG3DExportInstructions.1',
				},
			},
	}
	IpfcUICommand = {
			'type': 'Classes',
			'parent': {
				'IpfcObject': '',
				'IpfcActionSource': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'Designate': {
					'type': 'Sub',
					'args': 'MessageFile as String, Label as String, Help as String [optional], Description as String [optional]',
					'return': '',
					'info': 'Designates the command to appear as placeable in the Screen Customization dialog. This may not be used for existing Creo Parametric commands.',
				},
				'SetIcon': {
					'type': 'Sub',
					'args': 'IconFile as String',
					'return': '',
					'info': 'Designates the icon to be used with a user-created command. Adds the icon to the command of Creo Parametric.',
				},
			},
	}
	IpfcUICommandAccessListener = {
			'type': 'Classes',
			'parent': {
				'IpfcActionListener': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'OnCommandAccess': {
					'type': 'Function',
					'args': 'AllowErrorMessages as Boolean',
					'return': 'IpfcCommandAccess',
					'info': 'This is the callback method that is invoked to determine the accessibility of the action or option.',
				},
			},
	}
	IpfcUICommandActionListener = {
			'type': 'Classes',
			'parent': {
				'IpfcActionListener': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'OnCommand': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'This is the callback method that is invoked when a user executesa command that has been added to the Creo Parametric menu.',
				},
			},
	}
	IpfcUICommandBracketListener = {
			'type': 'Classes',
			'parent': {
				'IpfcActionListener': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'OnAfterCommand': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'This is the callback method that is invoked after the action is executed.',
				},
				'OnBeforeCommand': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': "This is the callback method that is invoked before the action is executed. In case the user cancel's the command, pfcExceptions::XCancelProEAction is caught and an error is returned.",
				},
			},
	}
	IpfcUnit = {
			'type': 'Classes',
			'parent': {
				'IpfcChild': '',
			},
			'child': {
			},
			'properties': {
				'ConversionFactor': {
					'return': 'IpfcUnitConversionFactor',
					'info': 'The relationship of the unit to its reference unit.',
				},
				'Expression': {
					'return': 'as String',
					'info': 'The user-friendly unit description: the name (i.e. "ksi") for ordinaryunits and the expression (i.e. N/m^3) for system-generated units.',
				},
				'IsStandard': {
					'return': 'as Boolean',
					'info': 'Whether the unit is system-defined (value true)or user-defined (value false).',
				},
				'Name': {
					'return': 'as String',
					'info': 'Specifies the name of the unit. ',
				},
				'ReferenceUnit': {
					'return': 'IpfcUnit',
					'info': 'The reference unit of the unit. ',
				},
				'Type': {
					'return': 'IpfcUnitType',
					'info': 'The type of quantity represented by the unit.',
				},
			},
			'method': {
				'Delete': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Delete the unit',
				},
				'Modify': {
					'type': 'Sub',
					'args': 'ConversionFactor as IpfcUnitConversionFactor, ReferenceUnit as IpfcUnit',
					'return': '',
					'info': "Modify a unit's definition by applying a new conversion factorand reference unit.",
				},
			},
	}
	IpfcUnitConversionFactor = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Offset': {
					'return': 'as Double [optional]',
					'info': 'The offset value applied to values in the reference unit.',
				},
				'Scale': {
					'return': 'as Double',
					'info': 'The scale applied to values in the reference unit to get thevalue in the actual unit.',
				},
			},
			'method': {
				'CCpfcUnitConversionFactor.Create': {
					'type': 'Function',
					'args': 'Scale as Double',
					'return': 'IpfcUnitConversionFactor',
					'info': 'Creates a new unit conversion factor object.',
					'com': 'pfcls.pfcUnitConversionFactor.1',
				},
			},
	}
	IpfcUnitConversionOptions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'DimensionOption': {
					'return': 'IpfcUnitDimensionConversion',
					'info': 'How the dimensions of the model should be converted. ',
				},
				'IgnoreParamUnits': {
					'return': 'as Boolean [optional]',
					'info': 'This boolean specifies whether to (or not to) ignore parameter units.  If null or true parameter values and units will not be changed when changing the model unit system.  If false, parameter units will be converted according to the rule specified for dimensions.',
				},
			},
			'method': {
				'CCpfcUnitConversionOptions.Create': {
					'type': 'Function',
					'args': 'DimensionOption as IpfcUnitDimensionConversion',
					'return': 'IpfcUnitConversionOptions',
					'info': 'Creates a new unit conversion options object.',
					'com': 'pfcls.pfcUnitConversionOptions.1',
				},
			},
	}
	IpfcUnitSystem = {
			'type': 'Classes',
			'parent': {
				'IpfcChild': '',
			},
			'child': {
			},
			'properties': {
				'IsStandard': {
					'return': 'as Boolean',
					'info': 'Specifies whether the unit system is system-defined (valuetrue) or user-defined (value false).',
				},
				'Name': {
					'return': 'as String',
					'info': 'Specifies the name of the unit system',
				},
				'Type': {
					'return': 'IpfcUnitSystemType',
					'info': 'Specifies the type of the unit system.',
				},
			},
			'method': {
				'Delete': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Delete the unit system',
				},
				'GetUnit': {
					'type': 'Function',
					'args': 'Type as IpfcUnitType',
					'return': 'IpfcUnit',
					'info': 'Retrieves the unit used by the unit system for a particulartype.',
				},
			},
	}
	IpfcUnsupportedAttachment = {
			'type': 'Classes',
			'parent': {
				'IpfcAttachment': '',
			},
			'child': {
			},
			'properties': {
				'AttachmentPoint': {
					'return': 'IpfcPoint3D',
					'info': 'The attachment point, in screen coordinates.',
				},
			},
			'method': {
			},
	}
	IpfcUploadBaseOptions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcCheckinOptions': '',
				'IpfcUploadOptions': '',
			},
			'properties': {
				'AutoresolveOption': {
					'return': 'IpfcServerAutoresolveOption',
					'info': 'Specifies the option for auto-resolving missing references.  The default is to not auto-resolve the references, which may result   in a conflict upon checkin.',
				},
				'DefaultFolder': {
					'return': 'as String [optional]',
					'info': 'Specifies the default folder location on the server for the automatic checkin operation',
				},
				'NonDefaultFolderAssignments': {
					'return': 'IpfcFolderAssignments',
					'info': 'Specifies the set of FolderAssignments for folder locations on the server in which specific objects    will be checked in',
				},
			},
			'method': {
			},
	}
	IpfcUploadOptions = {
			'type': 'Classes',
			'parent': {
				'IpfcUploadBaseOptions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcUploadOptions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcUploadOptions',
					'info': 'Creates a new IpfcUploadOptions object.',
					'com': 'pfcls.pfcUploadOptions.1',
				},
			},
	}
	IpfcVDA3DExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExport3DInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcVDA3DExportInstructions.Create': {
					'type': 'Function',
					'args': 'inConfiguration as IpfcAssemblyConfiguration, inGeometry as IpfcGeometryFlags',
					'return': 'IpfcVDA3DExportInstructions',
					'info': 'Creates a new instructions object used to export a solid model to VDA format.',
					'com': 'pfcls.pfcVDA3DExportInstructions.1',
				},
			},
	}
	IpfcVDAExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcGeomExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcVDAExportInstructions.Create': {
					'type': 'Function',
					'args': 'Flags as IpfcGeomExportFlags',
					'return': 'IpfcVDAExportInstructions',
					'info': 'Creates a new instructions object used to export a part or assembly in VDA format.',
					'com': 'pfcls.pfcVDAExportInstructions.1',
				},
			},
	}
	IpfcView = {
			'type': 'Classes',
			'parent': {
				'IpfcChild': '',
			},
			'child': {
			},
			'properties': {
				'IsCurrent': {
					'return': 'as Boolean',
					'info': 'The boolean status that indicates if the view is current or not.',
				},
				'Name': {
					'return': 'as String [readonly, optional]',
					'info': 'The name of the view',
				},
				'Transform': {
					'return': 'IpfcTransform3D',
					'info': 'The transformation matrix',
				},
			},
			'method': {
				'Reset': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'This method resets a previously set view orientation back to the default orientation.',
				},
				'Rotate': {
					'type': 'Sub',
					'args': 'Axis as IpfcCoordAxis, Angle as Double',
					'return': '',
					'info': 'This method rotates a specified object in respect to the X-, Y-, or Z- axis.Input arguments are angle and degree.',
				},
			},
	}
	IpfcView2D = {
			'type': 'Classes',
			'parent': {
				'IpfcChild': '',
			},
			'child': {
			},
			'properties': {
				'Display': {
					'return': 'IpfcViewDisplay',
					'info': 'The drawing view display settings.',
				},
				'IsBackground': {
					'return': 'as Boolean',
					'info': '  ',
				},
				'IsViewdisplayLayerDependent': {
					'return': 'as Boolean',
					'info': '  ',
				},
				'Name': {
					'return': 'as String',
					'info': 'The drawing view name.',
				},
				'Outline': {
					'return': 'IpfcOutline3D',
					'info': 'The outline of the drawing view, in screen coordinates.',
				},
				'Scale': {
					'return': 'as Double',
					'info': 'The drawing view scale.',
				},
			},
			'method': {
				'CheckIsDimensionDisplayed': {
					'type': 'Function',
					'args': 'Dim as IpfcBaseDimension',
					'return': ' as Boolean',
					'info': ' ',
				},
				'Delete': {
					'type': 'Sub',
					'args': 'DeleteChildren as Boolean [optional]',
					'return': '',
					'info': 'Deletes a specified drawing view.',
				},
				'GetIsScaleUserdefined': {
					'type': 'Function',
					'args': '',
					'return': ' as Boolean',
					'info': 'Identifies if the drawing view has a user-defined scale.',
				},
				'GetLayerDisplayStatus': {
					'type': 'Function',
					'args': 'Layer as IpfcLayer',
					'return': 'IpfcDisplayStatus',
					'info': 'Gets the display status of the specified layer in the drawing view.',
				},
				'GetModel': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcModel',
					'info': 'Get the model displayed in the drawing view.',
				},
				'GetSheetNumber': {
					'type': 'Function',
					'args': '',
					'return': ' as Long',
					'info': 'Gets the sheet where the view is located.',
				},
				'GetSimpRep': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSimpRep',
					'info': 'Get the Simplified Representation.',
				},
				'GetTransform': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcTransform3D',
					'info': 'Returns the coordinate transformation matrix for the drawing view.',
				},
				'Regenerate': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Regenerates the drawing view.',
				},
				'SetLayerDisplayStatus': {
					'type': 'Sub',
					'args': 'Layer as IpfcLayer, Status as IpfcDisplayStatus',
					'return': '',
					'info': 'Sets the display status for the layer in the drawing view.',
				},
				'Translate': {
					'type': 'Sub',
					'args': 'ByVector as IpfcVector3D',
					'return': '',
					'info': 'Moves the drawing view by the specified vector.',
				},
			},
	}
	IpfcView2DCreateInstructions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
				'IpfcGeneralViewCreateInstructions': '',
				'IpfcProjectionViewCreateInstructions': '',
			},
			'properties': {
			},
			'method': {
				'GetType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcView2DType',
					'info': 'Gets the type of view that this instructions object will create.',
				},
			},
	}
	IpfcViewDisplay = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'CableStyle': {
					'return': 'IpfcCableDisplayStyle',
					'info': 'The display style for cables.',
				},
				'RemoveQuiltHiddenLines': {
					'return': 'as Boolean',
					'info': 'true to remove quilt hidden lines, false otherwise.',
				},
				'ShowConceptModel': {
					'return': 'as Boolean',
					'info': 'true to include the skeleton model in the display, false otherwise.',
				},
				'ShowWeldXSection': {
					'return': 'as Boolean',
					'info': 'true to show weld cross sections in the display, false otherwise.',
				},
				'Style': {
					'return': 'IpfcDisplayStyle',
					'info': 'The display style for hidden lines.',
				},
				'TangentStyle': {
					'return': 'IpfcTangentEdgeDisplayStyle',
					'info': 'The display style for tangent lines.',
				},
			},
			'method': {
				'CCpfcViewDisplay.Create': {
					'type': 'Function',
					'args': 'Style as IpfcDisplayStyle, TangentStyle as IpfcTangentEdgeDisplayStyle, CableStyle as IpfcCableDisplayStyle, RemoveQuiltHiddenLines as Boolean, ShowConceptModel as Boolean, ShowWeldXSection as Boolean',
					'return': 'IpfcViewDisplay',
					'info': 'Creates a new view display object.',
					'com': 'pfcls.pfcViewDisplay.1',
				},
			},
	}
	IpfcViewOId = {
			'type': 'Classes',
			'parent': {
				'IpfcStringOId': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcViewOId.Create': {
					'type': 'Function',
					'args': 'Name as String',
					'return': 'IpfcViewOId',
					'info': 'Creates a new view identifier object using a supplied name.',
					'com': 'pfcls.pfcViewOId.1',
				},
			},
	}
	IpfcViewOwner = {
			'type': 'Classes',
			'parent': {
				'IpfcParent': '',
			},
			'child': {
				'IpfcModel': '',
			},
			'properties': {
			},
			'method': {
				'CurrentViewRotate': {
					'type': 'Sub',
					'args': 'Axis as IpfcCoordAxis, Angle as Double',
					'return': '',
					'info': 'Rotate the object in current view with respect to X, Y or Z axes.  ',
				},
				'GetCurrentView': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcView',
					'info': 'Returns a view object representing the current orientation of the model.',
				},
				'GetCurrentViewTransform': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcTransform3D',
					'info': "Retrieves the transformation for a model in the current view. The transformation is from the object's coordinate system to logical screen coordinates.  ",
				},
				'GetView': {
					'type': 'Function',
					'args': 'Name as String',
					'return': 'IpfcView',
					'info': 'Returns the specified view, given its name. ',
				},
				'ListViews': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcViews',
					'info': 'Retrieves the views associated with the object',
				},
				'RetrieveView': {
					'type': 'Function',
					'args': 'Name as String',
					'return': 'IpfcView',
					'info': 'Sets the current view to the orientationpreviously saved with a specified name.',
				},
				'SaveView': {
					'type': 'Function',
					'args': 'Name as String',
					'return': 'IpfcView',
					'info': 'This method saves the view with a specified name.',
				},
				'SetCurrentViewTransform': {
					'type': 'Sub',
					'args': 'TrForm as IpfcTransform3D',
					'return': '',
					'info': 'Sets the transformation of a model in the current view. This is the transformation between model coordinates and screen coordinates.  ',
				},
			},
	}
	IpfcVRMLDirectExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcVRMLExportInstructions': '',
			},
			'child': {
			},
			'properties': {
				'InputFile': {
					'return': 'as String',
					'info': 'The input file: specifies the file to be exported. This can be the name of a file in the current Creo Parametric directory or a fully qualified path.',
				},
			},
			'method': {
				'CCpfcVRMLDirectExportInstructions.Create': {
					'type': 'Function',
					'args': 'OutputFile as String, InputFile as String',
					'return': 'IpfcVRMLDirectExportInstructions',
					'info': 'Creates a new instructions object used to direct export VRML data from input to output file.',
					'com': 'pfcls.pfcVRMLDirectExportInstructions.1',
				},
			},
	}
	IpfcVRMLExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcExportInstructions': '',
			},
			'child': {
				'IpfcVRMLDirectExportInstructions': '',
				'IpfcVRMLModelExportInstructions': '',
			},
			'properties': {
				'OutputFile': {
					'return': 'as String',
					'info': '  ',
				},
			},
			'method': {
			},
	}
	IpfcVRMLModelExportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcVRMLExportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcVRMLModelExportInstructions.Create': {
					'type': 'Function',
					'args': 'OutputFile as String',
					'return': 'IpfcVRMLModelExportInstructions',
					'info': 'Creates a new instructions object used to export VRML data from a model.  ',
					'com': 'pfcls.pfcVRMLModelExportInstructions.1',
				},
			},
	}
	IpfcWindow = {
			'type': 'Classes',
			'parent': {
				'IpfcChild': '',
			},
			'child': {
			},
			'properties': {
				'GraphicsAreaHeight': {
					'return': 'as Double',
					'info': 'The height of the Creo Parametric graphics window without the border.',
				},
				'GraphicsAreaWidth': {
					'return': 'as Double',
					'info': 'The width of the Creo Parametric graphics window without the border',
				},
				'Height': {
					'return': 'as Double',
					'info': 'The window height. The size is normalized to values from 0 to 1.',
				},
				'Model': {
					'return': 'IpfcModel',
					'info': 'The Creo Parametric model that owns the window. This attribute can be null, if there no model that currently owns the window.',
				},
				'ScreenTransform': {
					'return': 'IpfcScreenTransform',
					'info': "The pan and zoom matrix of the window. When you set a window's ScreenTransform, the window updates immediately. Currently, the Get() method only retreives the ScreenTransform object for the current Creo Parametric window.",
				},
				'Width': {
					'return': 'as Double',
					'info': 'The width of the window. The size is normalized to values from 0 to 1.',
				},
				'XPos': {
					'return': 'as Double',
					'info': 'The X position of the window. The position is normalized to values from0 to 1.',
				},
				'YPos': {
					'return': 'as Double',
					'info': 'The Y position of the window. The position is normalized to values from0 to 1.',
				},
			},
			'method': {
				'Activate': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Activates the window.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Clears the Creo Parametric window.',
				},
				'Close': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Closes the window.',
				},
				'ExportRasterImage': {
					'type': 'Sub',
					'args': 'ImageFileName as String, Instructions as IpfcRasterImageExportInstructions',
					'return': '',
					'info': 'Outputs a standard Creo Parametric raster output file. ',
				},
				'GetBrowserSize': {
					'type': 'Function',
					'args': '',
					'return': ' as Double',
					'info': 'Returns the percentage of the graphics window covered by the embedded web browser.',
				},
				'GetId': {
					'type': 'Function',
					'args': '',
					'return': ' as Long',
					'info': 'Retrieves the current window identifier.',
				},
				'GetURL': {
					'type': 'Function',
					'args': '',
					'return': ' as String',
					'info': 'Returns the URL displayed in the embedded web browser.',
				},
				'Refresh': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Refreshes the screen. The function does not clearhighlights.  This is the most efficient function to use if youwant to clear "temporary" graphics only. ',
				},
				'Repaint': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Repaints the window. This function enables you to perform several operationsand update the view when necessary.',
				},
				'SetBrowserSize': {
					'type': 'Sub',
					'args': 'BrowserSize as Double',
					'return': '',
					'info': 'Sets the percentage of the graphics window covered by the embedded web browser.',
				},
				'SetURL': {
					'type': 'Sub',
					'args': 'URL as String',
					'return': '',
					'info': 'Sets the URL displayed in the embedded web browser.',
				},
			},
	}
	IpfcWindowOId = {
			'type': 'Classes',
			'parent': {
				'IpfcIntegerOId': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcWindowOId.Create': {
					'type': 'Function',
					'args': 'Id as Long',
					'return': 'IpfcWindowOId',
					'info': 'Creates a new window identifier object.',
					'com': 'pfcls.pfcWindowOId.1',
				},
			},
	}
	IpfcWireListImportInstructions = {
			'type': 'Classes',
			'parent': {
				'IpfcImportInstructions': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcWireListImportInstructions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcWireListImportInstructions',
					'info': 'Creates a new instructions object used to import (read) from a WIRELIST type file.',
					'com': 'pfcls.pfcWireListImportInstructions.1',
				},
			},
	}
	IpfcWorkspaceDefinition = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'WorkspaceContext': {
					'return': 'as String',
					'info': 'Context of the workspace ',
				},
				'WorkspaceName': {
					'return': 'as String',
					'info': 'Name of the workspace ',
				},
			},
			'method': {
				'CCpfcWorkspaceDefinition.Create': {
					'type': 'Function',
					'args': 'WorkspaceName as String, WorkspaceContext as String',
					'return': 'IpfcWorkspaceDefinition',
					'info': 'Creates a new IpfcWorkspaceDefinition object.',
					'com': 'pfcls.pfcWorkspaceDefinition.1',
				},
			},
	}
	IpfcWSExportOptions = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'IncludeSecondaryContent': {
					'return': 'as Boolean',
					'info': 'Flag to indicate whether or not to include secondary content             while exporting objects from a workspace.',
				},
			},
			'method': {
				'CCpfcWSExportOptions.Create': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcWSExportOptions',
					'info': 'Creates a new IpfcWSExportOptions object.  ',
					'com': 'pfcls.pfcWSExportOptions.1',
				},
			},
	}
	IpfcWSImportExportMessage = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Description': {
					'return': 'as String',
					'info': 'Specific description of the problem or information. ',
				},
				'FileName': {
					'return': 'as String',
					'info': 'The object name or pathname. ',
				},
				'MessageType': {
					'return': 'IpfcWSImportExportMessageType',
					'info': 'Severity of the message. ',
				},
				'Resolution': {
					'return': 'as String',
					'info': 'Resolution applied to resolve an overridable conflict             (Applicable when type returned is WSIMPEX_MSG_CONFLICT).            ',
				},
				'Succeeded': {
					'return': 'as Boolean',
					'info': 'Indicates whether the resolution succeded or not.             (Applicable when type returned is WSIMPEX_MSG_CONFLICT).            ',
				},
			},
			'method': {
			},
	}
	IpfcXSection = {
			'type': 'Classes',
			'parent': {
				'IpfcChild': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'Delete': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Deletes the cross-section from the model.',
				},
				'Display': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Shows the cross-section.',
				},
				'GetName': {
					'type': 'Function',
					'args': '',
					'return': ' as String',
					'info': 'Returns the name of the cross-section.',
				},
				'GetXSecType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcXSecType',
					'info': 'Gets details about the cross-section.',
				},
				'Regenerate': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Regenerates the cross-section.',
				},
				'SetName': {
					'type': 'Sub',
					'args': 'Name as String',
					'return': '',
					'info': 'Sets the cross-section name.',
				},
			},
	}
	IpfcXSecType = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'GetObjectType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcXSecCutobjType',
					'info': 'Gets the type of object intersected by the cross section.',
				},
				'GetType': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcXSecCutType',
					'info': 'Gets the type of intersection for the cross section.',
				},
			},
	}
	CMpfcArgument = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CreateIntArgValue': {
					'type': 'Function',
					'args': 'Value as Long',
					'return': 'IpfcArgValue',
					'info': 'Creates an integer argument value object.',
				},
				'CreateDoubleArgValue': {
					'type': 'Function',
					'args': 'Value as Double',
					'return': 'IpfcArgValue',
					'info': 'Creates a double argument value object.',
				},
				'CreateBoolArgValue': {
					'type': 'Function',
					'args': 'Value as Boolean',
					'return': 'IpfcArgValue',
					'info': 'Creates a boolean argument value object.',
				},
				'CreateASCIIStringArgValue': {
					'type': 'Function',
					'args': 'Value as String',
					'return': 'IpfcArgValue',
					'info': 'Creates an ASCII string argument value object.',
				},
				'CreateStringArgValue': {
					'type': 'Function',
					'args': 'Value as String',
					'return': 'IpfcArgValue',
					'info': 'Creates a string argument value object.',
				},
				'CreateSelectionArgValue': {
					'type': 'Function',
					'args': 'Value as IpfcSelection',
					'return': 'IpfcArgValue',
					'info': 'Creates a new selection argument value object.',
				},
				'CreateTransformArgValue': {
					'type': 'Function',
					'args': 'Value as IpfcTransform3D',
					'return': 'IpfcArgValue',
					'info': 'Creates a new transform argument value object.',
				},
				'FindArgumentByLabel': {
					'type': 'Function',
					'args': 'ArgList as IpfcArguments, Label as String',
					'return': 'IpfcArgument',
					'info': '  ',
				},
			},
			'com': 'pfcls.MpfcArgument.1',
	}
	CMpfcAssembly = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CreateComponentPath': {
					'type': 'Function',
					'args': 'Root as IpfcAssembly, Ids as Iintseq',
					'return': 'IpfcComponentPath',
					'info': 'Used to create an assembly component path. ',
				},
			},
			'com': 'pfcls.MpfcAssembly.1',
	}
	CMpfcExternal = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CreateIntExternalData': {
					'type': 'Function',
					'args': 'Value as Long',
					'return': 'IpfcExternalData',
					'info': 'Creates ExternalData object of type pfcExternalData.EXTDATA_INTEGER',
				},
				'CreateDoubleExternalData': {
					'type': 'Function',
					'args': 'Value as Double',
					'return': 'IpfcExternalData',
					'info': 'Creates ExternalData object of type pfcExternalData.EXTDATA_DOUBLE',
				},
				'CreateStringExternalData': {
					'type': 'Function',
					'args': 'Value as String',
					'return': 'IpfcExternalData',
					'info': 'Creates ExternalData object of type pfcExternalData.EXTDATA_String',
				},
			},
			'com': 'pfcls.MpfcExternal.1',
	}
	CMpfcInterference = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CreateSelectionEvaluator': {
					'type': 'Function',
					'args': 'Selections as IpfcSelectionPair',
					'return': 'IpfcSelectionEvaluator',
					'info': 'Used to create a IpfcSelectionEvaluator object, that represents the data necessary to call several methods related to the interface and clearance between the two selections.',
				},
				'CreateGlobalEvaluator': {
					'type': 'Function',
					'args': 'Assem as IpfcAssembly',
					'return': 'IpfcGlobalEvaluator',
					'info': 'Used to create a IpfcGlobalEvaluator object, that enables users to compute all of the interferences within the assembly.',
				},
			},
			'com': 'pfcls.MpfcInterference.1',
	}
	CMpfcModelItem = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CreateStringParamValue': {
					'type': 'Function',
					'args': 'Value as String',
					'return': 'IpfcParamValue',
					'info': 'Creates a parameter value with the specified string value.',
				},
				'CreateIntParamValue': {
					'type': 'Function',
					'args': 'Value as Long',
					'return': 'IpfcParamValue',
					'info': 'Creates a IpfcParamValue with the specified integer value.',
				},
				'CreateBoolParamValue': {
					'type': 'Function',
					'args': 'Value as Boolean',
					'return': 'IpfcParamValue',
					'info': 'Creates a IpfcParamValue object with the specified Boolean value.',
				},
				'CreateDoubleParamValue': {
					'type': 'Function',
					'args': 'Value as Double',
					'return': 'IpfcParamValue',
					'info': 'Creates a IpfcParamValue with the specified real value. ',
				},
				'CreateNoteParamValue': {
					'type': 'Function',
					'args': 'Value as Long',
					'return': 'IpfcParamValue',
					'info': 'Creates a note parameter.',
				},
			},
			'com': 'pfcls.MpfcModelItem.1',
	}
	CMpfcSelect = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CreateComponentSelection': {
					'type': 'Function',
					'args': 'Path as IpfcComponentPath',
					'return': 'IpfcSelection',
					'info': 'Used to create a IpfcSelection object, based on the path to a specified component.',
				},
				'CreateModelSelection': {
					'type': 'Function',
					'args': 'Model as IpfcModel',
					'return': 'IpfcSelection',
					'info': 'Used to create a IpfcSelection object, based on a IpfcModel',
				},
				'CreateModelItemSelection': {
					'type': 'Function',
					'args': 'SelItem as IpfcModelItem, Path as IpfcComponentPath [optional]',
					'return': 'IpfcSelection',
					'info': 'Used to create a IpfcSelection object, based on a IpfcModelItem and, optionally, its IpfcComponentPath in an assembly.',
				},
				'CreateSelectionFromString': {
					'type': 'Function',
					'args': 'SelectionString as String',
					'return': 'IpfcSelection',
					'info': 'Creates a new selection object, given a Web.Link style selection string.',
				},
			},
			'com': 'pfcls.MpfcSelect.1',
	}
	CMpfcSession = {
			'type': 'Classes',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'GetCurrentSession': {
					'type': 'Function',
					'args': '',
					'return': 'IpfcSession',
					'info': '  ',
				},
				'GetCurrentSessionWithCompatibility': {
					'type': 'Function',
					'args': 'compatibility as IpfcCreoCompatibility',
					'return': 'IpfcSession',
					'info': '  ',
				},
				'SetSessionUndo': {
					'type': 'Sub',
					'args': 'setFlag as Boolean',
					'return': '',
					'info': ' ',
				},
			},
			'com': 'pfcls.MpfcSession.1',
	}
class Collections(Enum):
	IpfcActionListeners = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcActionListener	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcActionListener	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcActionListeners	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcActionListener',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcActionListener	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcActionListeners.1',
	}
	IpfcActionSources = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcActionSource	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcActionSource	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcActionSources	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcActionSource',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcActionSource	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcActionSources.1',
	}
	IpfcActionTypes = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcActionType	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcActionType	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcActionTypes	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcActionType',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcActionType	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcActionTypes.1',
	}
	IpfcArguments = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcArgument	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcArgument	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcArguments	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcArgument',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcArgument	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcArguments.1',
	}
	IpfcAttachments = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcAttachment	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcAttachment	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcAttachments	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcAttachment',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcAttachment	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcAttachments.1',
	}
	IpfcBaseDimensions = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcBaseDimension	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcBaseDimension	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcBaseDimensions	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcBaseDimension',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcBaseDimension	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcBaseDimensions.1',
	}
	IpfcBSplinePoints = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcBSplinePoint	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcBSplinePoint	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcBSplinePoints	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcBSplinePoint',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcBSplinePoint	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcBSplinePoints.1',
	}
	IpfcColumnCreateOptions = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcColumnCreateOption	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcColumnCreateOption	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcColumnCreateOptions	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcColumnCreateOption',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcColumnCreateOption	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcColumnCreateOptions.1',
	}
	IpfcComponentConstraints = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcComponentConstraint	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcComponentConstraint	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcComponentConstraints	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcComponentConstraint',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcComponentConstraint	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcComponentConstraints.1',
	}
	IpfcComponentFeats = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcComponentFeat	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcComponentFeat	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcComponentFeats	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcComponentFeat',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcComponentFeat	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcComponentFeats.1',
	}
	IpfcComponentPaths = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcComponentPath	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcComponentPath	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcComponentPaths	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcComponentPath',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcComponentPath	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcComponentPaths.1',
	}
	IpfcContours = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcContour	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcContour	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcContours	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcContour',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcContour	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcContours.1',
	}
	IpfcCoonsCornerPoints = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'Item': {
					'type': 'Function',
					'args': '	Index1 as Long	, Index2 as Long	',
					'return': 'IpfcPoint3D',
					'info': 'Accesses an item by its index in the sequence.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index1 as Long	, Index2 as Long	, Item as IpfcPoint3D	',
					'return': '   ',
					'info': 'Assigns an item to the designated index in the sequence.',
				},
			},
			'com': 'pfcls.pfcCoonsCornerPoints.1',
	}
	IpfcCoonsUVDerivatives = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'Item': {
					'type': 'Function',
					'args': '	Index1 as Long	, Index2 as Long	',
					'return': 'IpfcVector3D',
					'info': 'Accesses an item by its index in the sequence.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index1 as Long	, Index2 as Long	, Item as IpfcVector3D	',
					'return': '   ',
					'info': 'Assigns an item to the designated index in the sequence.',
				},
			},
			'com': 'pfcls.pfcCoonsUVDerivatives.1',
	}
	IpfcCurveDescriptors = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcCurveDescriptor	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcCurveDescriptor	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcCurveDescriptors	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcCurveDescriptor',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcCurveDescriptor	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcCurveDescriptors.1',
	}
	IpfcCurves = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcCurve	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcCurve	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcCurves	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcCurve',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcCurve	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcCurves.1',
	}
	IpfcDatumAxisConstraints = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDatumAxisConstraint	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDatumAxisConstraint	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDatumAxisConstraints	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDatumAxisConstraint',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDatumAxisConstraint	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDatumAxisConstraints.1',
	}
	IpfcDatumAxisDimensionConstraints = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDatumAxisDimensionConstraint	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDatumAxisDimensionConstraint	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDatumAxisDimensionConstraints	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDatumAxisDimensionConstraint',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDatumAxisDimensionConstraint	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDatumAxisDimensionConstraints.1',
	}
	IpfcDatumCsysDimensionConstraints = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDatumCsysDimensionConstraint	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDatumCsysDimensionConstraint	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDatumCsysDimensionConstraints	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDatumCsysDimensionConstraint',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDatumCsysDimensionConstraint	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDatumCsysDimensionConstraints.1',
	}
	IpfcDatumCsysOrientMoveConstraints = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDatumCsysOrientMoveConstraint	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDatumCsysOrientMoveConstraint	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDatumCsysOrientMoveConstraints	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDatumCsysOrientMoveConstraint',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDatumCsysOrientMoveConstraint	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDatumCsysOrientMoveConstraints.1',
	}
	IpfcDatumCsysOriginConstraints = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDatumCsysOriginConstraint	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDatumCsysOriginConstraint	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDatumCsysOriginConstraints	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDatumCsysOriginConstraint',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDatumCsysOriginConstraint	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDatumCsysOriginConstraints.1',
	}
	IpfcDatumPlaneConstraints = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDatumPlaneConstraint	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDatumPlaneConstraint	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDatumPlaneConstraints	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDatumPlaneConstraint',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDatumPlaneConstraint	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDatumPlaneConstraints.1',
	}
	IpfcDatumPointDimensionConstraints = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDatumPointDimensionConstraint	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDatumPointDimensionConstraint	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDatumPointDimensionConstraints	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDatumPointDimensionConstraint',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDatumPointDimensionConstraint	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDatumPointDimensionConstraints.1',
	}
	IpfcDatumPointPlacementConstraints = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDatumPointPlacementConstraint	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDatumPointPlacementConstraint	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDatumPointPlacementConstraints	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDatumPointPlacementConstraint',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDatumPointPlacementConstraint	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDatumPointPlacementConstraints.1',
	}
	IpfcDependencies = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDependency	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDependency	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDependencies	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDependency',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDependency	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDependencies.1',
	}
	IpfcDetailItems = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDetailItem	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDetailItem	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDetailItems	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDetailItem',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDetailItem	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDetailItems.1',
	}
	IpfcDetailLeaderAttachments = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDetailLeaderAttachment	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDetailLeaderAttachment	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDetailLeaderAttachments	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDetailLeaderAttachment',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDetailLeaderAttachment	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDetailLeaderAttachments.1',
	}
	IpfcDetailSymbolDefItems = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDetailSymbolDefItem	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDetailSymbolDefItem	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDetailSymbolDefItems	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDetailSymbolDefItem',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDetailSymbolDefItem	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDetailSymbolDefItems.1',
	}
	IpfcDetailSymbolGroups = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDetailSymbolGroup	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDetailSymbolGroup	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDetailSymbolGroups	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDetailSymbolGroup',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDetailSymbolGroup	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDetailSymbolGroups.1',
	}
	IpfcDetailSymbolInstItems = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDetailSymbolInstItem	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDetailSymbolInstItem	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDetailSymbolInstItems	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDetailSymbolInstItem',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDetailSymbolInstItem	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDetailSymbolInstItems.1',
	}
	IpfcDetailTextLines = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDetailTextLine	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDetailTextLine	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDetailTextLines	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDetailTextLine',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDetailTextLine	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDetailTextLines.1',
	}
	IpfcDetailTexts = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDetailText	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDetailText	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDetailTexts	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDetailText',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDetailText	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDetailTexts.1',
	}
	IpfcDetailVariantTexts = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDetailVariantText	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDetailVariantText	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDetailVariantTexts	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDetailVariantText',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDetailVariantText	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDetailVariantTexts.1',
	}
	IpfcDimension2Ds = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDimension2D	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDimension2D	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDimension2Ds	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDimension2D',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDimension2D	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDimension2Ds.1',
	}
	IpfcDimensionAttachment = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcSelection',
					'info': 'Accesses an item by its index in the sequence.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcSelection	',
					'return': '   ',
					'info': 'Assigns an item to the designated index in the sequence.',
				},
			},
			'com': 'pfcls.pfcDimensionAttachment.1',
	}
	IpfcDimensionAttachments = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDimensionAttachment	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDimensionAttachment	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDimensionAttachments	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDimensionAttachment',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDimensionAttachment	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDimensionAttachments.1',
	}
	IpfcDimensions = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDimension	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDimension	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDimensions	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDimension',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDimension	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDimensions.1',
	}
	IpfcDimensionSenses = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDimensionSense	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDimensionSense	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDimensionSenses	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDimensionSense',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDimensionSense	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDimensionSenses.1',
	}
	IpfcDimSenses = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDimSense	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDimSense	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDimSenses	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDimSense',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDimSense	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDimSenses.1',
	}
	IpfcDisplayStatuses = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDisplayStatus	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDisplayStatus	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDisplayStatuses	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDisplayStatus',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDisplayStatus	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDisplayStatuses.1',
	}
	IpfcDrawingCreateErrors = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDrawingCreateError	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDrawingCreateError	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDrawingCreateErrors	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDrawingCreateError',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDrawingCreateError	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDrawingCreateErrors.1',
	}
	IpfcDrawingCreateOptions = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcDrawingCreateOption	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcDrawingCreateOption	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcDrawingCreateOptions	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcDrawingCreateOption',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcDrawingCreateOption	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcDrawingCreateOptions.1',
	}
	IpfcEdges = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcEdge	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcEdge	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcEdges	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcEdge',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcEdge	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcEdges.1',
	}
	IpfcEnvelope2D = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcOutline2D',
					'info': 'Accesses an item by its index in the sequence.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcOutline2D	',
					'return': '   ',
					'info': 'Assigns an item to the designated index in the sequence.',
				},
			},
			'com': 'pfcls.pfcEnvelope2D.1',
	}
	IpfcExternalDataClasses = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcExternalDataClass	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcExternalDataClass	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcExternalDataClasses	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcExternalDataClass',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcExternalDataClass	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcExternalDataClasses.1',
	}
	IpfcExternalDataSlots = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcExternalDataSlot	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcExternalDataSlot	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcExternalDataSlots	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcExternalDataSlot',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcExternalDataSlot	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcExternalDataSlots.1',
	}
	IpfcFacetControlFlags = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcFacetControlFlag	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcFacetControlFlag	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcFacetControlFlags	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcFacetControlFlag',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcFacetControlFlag	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcFacetControlFlags.1',
	}
	IpfcFamilyTableColumns = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcFamilyTableColumn	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcFamilyTableColumn	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcFamilyTableColumns	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcFamilyTableColumn',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcFamilyTableColumn	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcFamilyTableColumns.1',
	}
	IpfcFamilyTableRows = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcFamilyTableRow	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcFamilyTableRow	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcFamilyTableRows	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcFamilyTableRow',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcFamilyTableRow	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcFamilyTableRows.1',
	}
	IpfcFeatureGroups = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcFeatureGroup	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcFeatureGroup	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcFeatureGroups	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcFeatureGroup',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcFeatureGroup	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcFeatureGroups.1',
	}
	IpfcFeatureOperations = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcFeatureOperation	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcFeatureOperation	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcFeatureOperations	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcFeatureOperation',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcFeatureOperation	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcFeatureOperations.1',
	}
	IpfcFeatures = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcFeature	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcFeature	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcFeatures	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcFeature',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcFeature	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcFeatures.1',
	}
	IpfcFileOpenShortcuts = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcFileOpenShortcut	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcFileOpenShortcut	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcFileOpenShortcuts	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcFileOpenShortcut',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcFileOpenShortcut	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcFileOpenShortcuts.1',
	}
	IpfcFolderAssignments = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcFolderAssignment	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcFolderAssignment	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcFolderAssignments	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcFolderAssignment',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcFolderAssignment	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcFolderAssignments.1',
	}
	IpfcGeneralDatumPoints = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcGeneralDatumPoint	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcGeneralDatumPoint	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcGeneralDatumPoints	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcGeneralDatumPoint',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcGeneralDatumPoint	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcGeneralDatumPoints.1',
	}
	IpfcGlobalInterferences = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcGlobalInterference	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcGlobalInterference	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcGlobalInterferences	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcGlobalInterference',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcGlobalInterference	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcGlobalInterferences.1',
	}
	IpfcInertia = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'Item': {
					'type': 'Function',
					'args': '	Index1 as Long	, Index2 as Long	',
					'return': '   ',
					'info': 'Accesses an item by its index in the sequence.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index1 as Long	, Index2 as Long	, Item as Double	',
					'return': '   ',
					'info': 'Assigns an item to the designated index in the sequence.',
				},
			},
			'com': 'pfcls.pfcInertia.1',
	}
	IpfcLayers = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcLayer	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcLayer	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcLayers	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcLayer',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcLayer	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcLayers.1',
	}
	IpfcLeaderAttachments = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcLeaderAttachment	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcLeaderAttachment	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcLeaderAttachments	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcLeaderAttachment',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcLeaderAttachment	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcLeaderAttachments.1',
	}
	IpfcMaterials = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcMaterial	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcMaterial	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcMaterials	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcMaterial',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcMaterial	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcMaterials.1',
	}
	IpfcMatrix3D = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'Item': {
					'type': 'Function',
					'args': '	Index1 as Long	, Index2 as Long	',
					'return': '   ',
					'info': 'Accesses an item by its index in the sequence.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index1 as Long	, Index2 as Long	, Item as Double	',
					'return': '   ',
					'info': 'Assigns an item to the designated index in the sequence.',
				},
			},
			'com': 'pfcls.pfcMatrix3D.1',
	}
	IpfcMessageButtons = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcMessageButton	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcMessageButton	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcMessageButtons	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcMessageButton',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcMessageButton	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcMessageButtons.1',
	}
	IpfcModelDescriptors = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcModelDescriptor	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcModelDescriptor	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcModelDescriptors	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcModelDescriptor',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcModelDescriptor	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcModelDescriptors.1',
	}
	IpfcModelItems = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcModelItem	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcModelItem	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcModelItems	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcModelItem',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcModelItem	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcModelItems.1',
	}
	IpfcModelItemTypes = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcModelItemType	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcModelItemType	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcModelItemTypes	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcModelItemType',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcModelItemType	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcModelItemTypes.1',
	}
	IpfcModels = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcModel	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcModel	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcModels	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcModel',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcModel	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcModels.1',
	}
	IpfcOutline2D = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcPoint2D',
					'info': 'Accesses an item by its index in the sequence.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcPoint2D	',
					'return': '   ',
					'info': 'Assigns an item to the designated index in the sequence.',
				},
			},
			'com': 'pfcls.pfcOutline2D.1',
	}
	IpfcOutline3D = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcPoint3D',
					'info': 'Accesses an item by its index in the sequence.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcPoint3D	',
					'return': '   ',
					'info': 'Assigns an item to the designated index in the sequence.',
				},
			},
			'com': 'pfcls.pfcOutline3D.1',
	}
	IpfcParameters = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcParameter	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcParameter	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcParameters	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcParameter',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcParameter	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcParameters.1',
	}
	IpfcParameterSelectionContexts = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcParameterSelectionContext	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcParameterSelectionContext	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcParameterSelectionContexts	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcParameterSelectionContext',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcParameterSelectionContext	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcParameterSelectionContexts.1',
	}
	IpfcParamValues = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcParamValue	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcParamValue	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcParamValues	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcParamValue',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcParamValue	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcParamValues.1',
	}
	IpfcPDFOptions = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcPDFOption	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcPDFOption	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcPDFOptions	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcPDFOption',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcPDFOption	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcPDFOptions.1',
	}
	IpfcPoint2D = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': '   ',
					'info': 'Accesses an item by its index in the sequence.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as Double	',
					'return': '   ',
					'info': 'Assigns an item to the designated index in the sequence.',
				},
			},
			'com': 'pfcls.pfcPoint2D.1',
	}
	IpfcPoint2Ds = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcPoint2D	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcPoint2D	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcPoint2Ds	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcPoint2D',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcPoint2D	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcPoint2Ds.1',
	}
	IpfcPoint3D = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': '   ',
					'info': 'Accesses an item by its index in the sequence.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as Double	',
					'return': '   ',
					'info': 'Assigns an item to the designated index in the sequence.',
				},
			},
			'com': 'pfcls.pfcPoint3D.1',
	}
	IpfcPoint3Ds = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcPoint3D	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcPoint3D	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcPoint3Ds	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcPoint3D',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcPoint3D	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcPoint3Ds.1',
	}
	IpfcPrincipalAxes = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcVector3D',
					'info': 'Accesses an item by its index in the sequence.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcVector3D	',
					'return': '   ',
					'info': 'Assigns an item to the designated index in the sequence.',
				},
			},
			'com': 'pfcls.pfcPrincipalAxes.1',
	}
	IpfcRelationFunctionArguments = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcRelationFunctionArgument	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcRelationFunctionArgument	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcRelationFunctionArguments	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcRelationFunctionArgument',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcRelationFunctionArgument	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcRelationFunctionArguments.1',
	}
	IpfcSelections = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcSelection	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcSelection	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcSelections	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcSelection',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcSelection	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcSelections.1',
	}
	IpfcServers = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcServer	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcServer	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcServers	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcServer',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcServer	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcServers.1',
	}
	IpfcSimpRepItems = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcSimpRepItem	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcSimpRepItem	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcSimpRepItems	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcSimpRepItem',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcSimpRepItem	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcSimpRepItems.1',
	}
	IpfcSimpReps = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcSimpRep	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcSimpRep	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcSimpReps	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcSimpRep',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcSimpRep	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcSimpReps.1',
	}
	IpfcSplinePoints = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcSplinePoint	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcSplinePoint	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcSplinePoints	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcSplinePoint',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcSplinePoint	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcSplinePoints.1',
	}
	IpfcSplineSurfacePoints = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcSplineSurfacePoint	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcSplineSurfacePoint	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcSplineSurfacePoints	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcSplineSurfacePoint',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcSplineSurfacePoint	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcSplineSurfacePoints.1',
	}
	IpfcStdColors = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcStdColor	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcStdColor	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcStdColors	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcStdColor',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcStdColor	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcStdColors.1',
	}
	IpfcSurfaceDescriptors = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcSurfaceDescriptor	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcSurfaceDescriptor	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcSurfaceDescriptors	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcSurfaceDescriptor',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcSurfaceDescriptor	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcSurfaceDescriptors.1',
	}
	IpfcSurfaces = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcSurface	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcSurface	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcSurfaces	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcSurface',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcSurface	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcSurfaces.1',
	}
	IpfcSymbolDefAttachments = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcSymbolDefAttachment	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcSymbolDefAttachment	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcSymbolDefAttachments	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcSymbolDefAttachment',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcSymbolDefAttachment	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcSymbolDefAttachments.1',
	}
	IpfcTables = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcTable	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcTable	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcTables	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcTable',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcTable	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcTables.1',
	}
	IpfcTransform3Ds = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcTransform3D	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcTransform3D	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcTransform3Ds	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcTransform3D',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcTransform3D	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcTransform3Ds.1',
	}
	IpfcUDFAssemblyIntersections = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcUDFAssemblyIntersection	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcUDFAssemblyIntersection	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcUDFAssemblyIntersections	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcUDFAssemblyIntersection',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcUDFAssemblyIntersection	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcUDFAssemblyIntersections.1',
	}
	IpfcUDFDimensions = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcUDFDimension	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcUDFDimension	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcUDFDimensions	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcUDFDimension',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcUDFDimension	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcUDFDimensions.1',
	}
	IpfcUDFExternalReferences = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcUDFExternalReference	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcUDFExternalReference	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcUDFExternalReferences	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcUDFExternalReference',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcUDFExternalReference	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcUDFExternalReferences.1',
	}
	IpfcUDFOrientations = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcUDFOrientation	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcUDFOrientation	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcUDFOrientations	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcUDFOrientation',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcUDFOrientation	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcUDFOrientations.1',
	}
	IpfcUDFReferences = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcUDFReference	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcUDFReference	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcUDFReferences	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcUDFReference',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcUDFReference	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcUDFReferences.1',
	}
	IpfcUDFVariantValues = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcUDFVariantValue	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcUDFVariantValue	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcUDFVariantValues	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcUDFVariantValue',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcUDFVariantValue	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcUDFVariantValues.1',
	}
	IpfcUnits = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcUnit	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcUnit	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcUnits	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcUnit',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcUnit	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcUnits.1',
	}
	IpfcUnitSystems = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcUnitSystem	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcUnitSystem	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcUnitSystems	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcUnitSystem',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcUnitSystem	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcUnitSystems.1',
	}
	IpfcUVOutline = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcUVParams',
					'info': 'Accesses an item by its index in the sequence.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcUVParams	',
					'return': '   ',
					'info': 'Assigns an item to the designated index in the sequence.',
				},
			},
			'com': 'pfcls.pfcUVOutline.1',
	}
	IpfcUVParams = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': '   ',
					'info': 'Accesses an item by its index in the sequence.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as Double	',
					'return': '   ',
					'info': 'Assigns an item to the designated index in the sequence.',
				},
			},
			'com': 'pfcls.pfcUVParams.1',
	}
	IpfcUVParamsSequence = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcUVParams	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcUVParams	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcUVParamsSequence	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcUVParams',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcUVParams	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcUVParamsSequence.1',
	}
	IpfcUVVector = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': '   ',
					'info': 'Accesses an item by its index in the sequence.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as Double	',
					'return': '   ',
					'info': 'Assigns an item to the designated index in the sequence.',
				},
			},
			'com': 'pfcls.pfcUVVector.1',
	}
	IpfcVector2D = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': '   ',
					'info': 'Accesses an item by its index in the sequence.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as Double	',
					'return': '   ',
					'info': 'Assigns an item to the designated index in the sequence.',
				},
			},
			'com': 'pfcls.pfcVector2D.1',
	}
	IpfcVector3D = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': '   ',
					'info': 'Accesses an item by its index in the sequence.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as Double	',
					'return': '   ',
					'info': 'Assigns an item to the designated index in the sequence.',
				},
			},
			'com': 'pfcls.pfcVector3D.1',
	}
	IpfcVector3Ds = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcVector3D	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcVector3D	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcVector3Ds	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcVector3D',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcVector3D	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcVector3Ds.1',
	}
	IpfcView2Ds = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcView2D	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcView2D	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcView2Ds	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcView2D',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcView2D	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcView2Ds.1',
	}
	IpfcViews = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcView	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcView	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcViews	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcView',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcView	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcViews.1',
	}
	IpfcWindows = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcWindow	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcWindow	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcWindows	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcWindow',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcWindow	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcWindows.1',
	}
	IpfcWorkspaceDefinitions = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcWorkspaceDefinition	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcWorkspaceDefinition	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcWorkspaceDefinitions	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcWorkspaceDefinition',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcWorkspaceDefinition	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcWorkspaceDefinitions.1',
	}
	IpfcWSImportExportMessages = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcWSImportExportMessage	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcWSImportExportMessage	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcWSImportExportMessages	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcWSImportExportMessage',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcWSImportExportMessage	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcWSImportExportMessages.1',
	}
	IpfcXSections = {
			'type': 'Collections',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Count': {
					'return': 'as Long [readonly] ',
					'info': 'None',
				},
			},
			'method': {
				'Append': {
					'type': 'Sub',
					'args': '	Item as IpfcXSection	',
					'return': '   ',
					'info': 'Adds a new item to the end of the sequence.',
				},
				'Clear': {
					'type': 'Sub',
					'args': '		',
					'return': '   ',
					'info': 'Removes all items from the sequence.',
				},
				'Insert': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Item as IpfcXSection	',
					'return': '   ',
					'info': 'Inserts a new item in front of the         sequence item with the specified index. If the index is out of range,        the sequence is expanded to include this index.',
				},
				'InsertSeq': {
					'type': 'Sub',
					'args': '	AtIndex as Long	, Sequence as IpfcXSections	',
					'return': '   ',
					'info': 'Inserts a sequence of items         passed as the second argument in front of the sequence item with the         specified index. If the index is out of range, the sequence is expanded         to include this index.',
				},
				'Item': {
					'type': 'Function',
					'args': '	Index as Long	',
					'return': 'IpfcXSection',
					'info': 'Accesses an item by its index in the         sequence. If the index is out of range, this method throws the         cipXInvalidSeqIndex exception.',
				},
				'Remove': {
					'type': 'Sub',
					'args': '	FromIndex as Long	, ToIndex as Long	',
					'return': '   ',
					'info': 'Removes items in the specified range        from the sequence. The first argument specifies the index of the first         item to be removed. The second argument specifies the index of the item         immediately after the last in the range to be removed.',
				},
				'Set': {
					'type': 'Sub',
					'args': '	Index as Long	, Item as IpfcXSection	',
					'return': '   ',
					'info': 'Assigns an item to the designated index        in the sequence. If the index is out of range, the sequence is expanded         to include this index.',
				},
			},
			'com': 'pfcls.pfcXSections.1',
	}
class Exceptions(Enum):
	IpfcXBadArgument = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXInAMethod': '',
			},
			'child': {
			},
			'properties': {
				'ArgumentName': {
					'return': 'as String',
					'info': 'The name of the argument that caused the exception to be thrown',
				},
			},
			'method': {
			},
	}
	IpfcXBadExternalData = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXInAMethod': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXBadGetArgValue = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXInAMethod': '',
			},
			'child': {
			},
			'properties': {
				'Type': {
					'return': 'IpfcArgValueType',
					'info': 'The actual type of the argument value object.',
				},
			},
			'method': {
			},
	}
	IpfcXBadGetExternalData = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXPFC': '',
			},
			'child': {
			},
			'properties': {
				'Type': {
					'return': 'IpfcExternalDataType',
					'info': '  ',
				},
			},
			'method': {
			},
	}
	IpfcXBadGetParamValue = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXInAMethod': '',
			},
			'child': {
			},
			'properties': {
				'ValueType': {
					'return': 'IpfcParamValueType',
					'info': 'The parameter type that caused the exception to be thrown',
				},
			},
			'method': {
			},
	}
	IpfcXBadOutlineExcludeType = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXBadArgument': '',
			},
			'child': {
			},
			'properties': {
				'Type': {
					'return': 'IpfcModelItemType',
					'info': 'The exclusion type that caused the exception to be thrown',
				},
			},
			'method': {
			},
	}
	IpfcXCancelProEAction = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXPFC': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
				'CCpfcXCancelProEAction.Throw': {
					'type': 'Sub',
					'args': '',
					'return': '',
					'info': 'Call this method from the body of an appropriate IpfcActionListener method to cancel the impending Creo Parametric operation. ',
					'com': 'pfcls.pfcXCancelProEAction.1',
				},
			},
	}
	IpfcXCannotAccess = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXInAMethod': '',
			},
			'child': {
			},
			'properties': {
				'VariableName': {
					'return': 'as String',
					'info': 'The name of the variable value that cannot be accessed.',
				},
			},
			'method': {
			},
	}
	IpfcXCompatibilityNotSet = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXPFC': '',
			},
			'child': {
			},
			'properties': {
				'MethodName': {
					'return': 'as String',
					'info': ' ',
				},
			},
			'method': {
			},
	}
	IpfcXEmptyString = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXBadArgument': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXExternalDataBadDataArgs = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXExternalDataError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXExternalDataBadKeyByFlag = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXExternalDataError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXExternalDataClassOrSlotExists = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXExternalDataError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXExternalDataEmptySlot = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXExternalDataError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXExternalDataError = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXExternalDataInvalidObject = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXExternalDataError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXExternalDataInvalidObjType = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXExternalDataError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXExternalDataInvalidSlotName = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXExternalDataError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXExternalDataNamesTooLong = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXExternalDataError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXExternalDataSlotNotFound = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXExternalDataError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXExternalDataStreamTooLarge = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXExternalDataError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXExternalDataTKError = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXExternalDataError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXInAMethod = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXPFC': '',
			},
			'child': {
			},
			'properties': {
				'MethodName': {
					'return': 'as String',
					'info': 'The name of the method that caused the exception to be thrown',
				},
			},
			'method': {
			},
	}
	IpfcXInvalidEnumValue = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXInAMethod': '',
			},
			'child': {
			},
			'properties': {
				'Name': {
					'return': 'as String',
					'info': 'The name of the enumerated type  ',
				},
				'Value': {
					'return': 'as Long',
					'info': 'The invalid value',
				},
			},
			'method': {
			},
	}
	IpfcXInvalidFileName = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXInAMethod': '',
			},
			'child': {
			},
			'properties': {
				'FileName': {
					'return': 'as String',
					'info': 'The file name that triggered this exception.',
				},
			},
			'method': {
			},
	}
	IpfcXInvalidFileType = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXInAMethod': '',
			},
			'child': {
			},
			'properties': {
				'Extension': {
					'return': 'as String',
					'info': 'The file extension that the model descriptor contains which is invalid for the operation.',
				},
			},
			'method': {
			},
	}
	IpfcXInvalidModelItem = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXInAMethod': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXInvalidSelection = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXInAMethod': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXJLinkApplicationException = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXInAMethod': '',
			},
			'child': {
			},
			'properties': {
				'ExceptionDescription': {
					'return': 'as String',
					'info': 'The type of exception that was thrown from the invoked application.',
				},
			},
			'method': {
			},
	}
	IpfcXJLinkApplicationInactive = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXInAMethod': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXJLinkTaskExists = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXInAMethod': '',
			},
			'child': {
			},
			'properties': {
				'TaskId': {
					'return': 'as String',
					'info': '  ',
				},
			},
			'method': {
			},
	}
	IpfcXJLinkTaskNotFound = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXInAMethod': '',
			},
			'child': {
			},
			'properties': {
				'TaskId': {
					'return': 'as String',
					'info': '  ',
				},
			},
			'method': {
			},
	}
	IpfcXMethodForbidden = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXPFC': '',
			},
			'child': {
			},
			'properties': {
				'MethodName': {
					'return': 'as String',
					'info': ' ',
				},
			},
			'method': {
			},
	}
	IpfcXMethodNotLicensed = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXPFC': '',
			},
			'child': {
			},
			'properties': {
				'MethodName': {
					'return': 'as String',
					'info': ' ',
				},
			},
			'method': {
			},
	}
	IpfcXModelNotInSession = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXInAMethod': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXNegativeNumber = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXBadArgument': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXNumberTooLarge = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXBadArgument': '',
			},
			'child': {
			},
			'properties': {
				'MaxValue': {
					'return': 'as Double',
					'info': 'Maximum allowed value.',
				},
				'Value': {
					'return': 'as Double',
					'info': 'The number passed as an argument.',
				},
			},
			'method': {
			},
	}
	IpfcXPFC = {
			'type': 'Exceptions',
			'parent': {
			},
			'child': {
			},
			'properties': {
				'Message': {
					'return': 'as String',
					'info': 'Message associated with exception',
				},
			},
			'method': {
			},
	}
	IpfcXProdevError = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXInAMethod': '',
			},
			'child': {
			},
			'properties': {
				'DevelopFunctionName': {
					'return': 'as String',
					'info': 'The name of the Pro/DEVELOP function that caused the exception to be thrown',
				},
				'ErrorCode': {
					'return': 'as Long',
					'info': 'The Pro/DEVELOP error (integer); ',
				},
			},
			'method': {
			},
	}
	IpfcXProeWasNotConnected = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXInAMethod': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXSequenceTooLong = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXBadArgument': '',
			},
			'child': {
			},
			'properties': {
				'MaxLength': {
					'return': 'as Long',
					'info': 'The maximum length allowed for a sequence',
				},
			},
			'method': {
			},
	}
	IpfcXStringTooLong = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXBadArgument': '',
			},
			'child': {
			},
			'properties': {
				'MaxLength': {
					'return': 'as Long',
					'info': 'The maximum length allowed for a string',
				},
				'String': {
					'return': 'as String',
					'info': 'The string',
				},
			},
			'method': {
			},
	}
	IpfcXToolkitAbort = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitAmbiguous = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitAppBadDataPath = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitAppBadEncoding = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitAppCommunicationFailure = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitAppCreoBarred = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitAppExcessCallbacks = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitAppInitializationFailed = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitAppNewVersion = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitAppNoLicense = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitAppStartupFailed = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitAppTooOld = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitAppVersionMismatch = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitAuthenticationFailure = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitBadContext = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitBadDimAttach = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitBadInputs = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitBadSrfCrv = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitBsplMultiInnerKnots = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitBsplNonStdEndKnots = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitBsplUnsuitableDegree = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitBusy = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitCantAccess = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitCantModify = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitCantOpen = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitCantWrite = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitCheckLastError = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitCheckOmitted = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitCheckoutConflict = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
				'ConflictDescription': {
					'return': 'as String [readonly, optional]',
					'info': ' ',
				},
			},
			'method': {
			},
	}
	IpfcXToolkitCommError = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitContinue = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitCreateViewBadExplode = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitCreateViewBadModel = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitCreateViewBadParent = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitCreateViewBadSheet = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitCreateViewBadType = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitDeadLock = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitDllInactive = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXInAMethod': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitDllInitializeFailed = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXInAMethod': '',
			},
			'child': {
			},
			'properties': {
				'UserInitializeMessage': {
					'return': 'as String',
					'info': "The error string passed back from the application's user_initialize method.",
				},
				'UserInitializeReturn': {
					'return': 'as Long',
					'info': "The error return value passed back from the applications's user_initialize method.",
				},
			},
			'method': {
			},
	}
	IpfcXToolkitDrawingCreateErrors = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
				'CreatedDrawing': {
					'return': 'IpfcDrawing',
					'info': '  ',
				},
				'Errors': {
					'return': 'IpfcDrawingCreateErrors',
					'info': 'The array of drawing creation errors.',
				},
			},
			'method': {
			},
	}
	IpfcXToolkitEmpty = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitError = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXInAMethod': '',
			},
			'child': {
			},
			'properties': {
				'ToolkitFunctionName': {
					'return': 'as String',
					'info': 'The name of the Creo Parametric TOOLKIT function that caused the exception to be thrown',
				},
			},
			'method': {
				'GetErrorCode': {
					'type': 'Function',
					'args': '',
					'return': ' as Long',
					'info': 'Reterns an error code (integer value of the enumerated type ProError from Creo Parametric TOOLKIT).',
				},
			},
	}
	IpfcXToolkitFound = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitGeneralError = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitIncomplete = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitIncompleteTessellation = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitInUse = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitInvalidDir = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitInvalidFile = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitInvalidItem = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitInvalidMatrix = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitInvalidName = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitInvalidPtr = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitInvalidReference = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXInAMethod': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitInvalidType = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitLineTooLong = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitMaxLimitReached = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitMsgFmtError = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitMsgNotFound = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitMsgNoTrans = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitMsgTooLong = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitMsgUserQuit = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitNeedsUnlock = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitNoChange = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitNoCoordSystem = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitNoLicense = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitNoPermission = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitNotDisplayed = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitNotExist = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitNotFound = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitNotImplemented = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitNotValid = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitObsoleteFunc = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitOutdated = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitOutOfMemory = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitOutOfRange = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitPickAbove = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitRegenerateAgain = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitSuppressedParents = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitUnattachedFeats = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitUnavailableSection = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitUnrecognizedErrorCode = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitUnsupported = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXToolkitUserAbort = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXToolkitError': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXUnimplemented = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXInAMethod': '',
			},
			'child': {
			},
			'properties': {
			},
			'method': {
			},
	}
	IpfcXUnknownModelExtension = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXInAMethod': '',
			},
			'child': {
			},
			'properties': {
				'Extension': {
					'return': 'as String',
					'info': 'The extension that caused this exception to be thrown',
				},
			},
			'method': {
			},
	}
	IpfcXUnusedValue = {
			'type': 'Exceptions',
			'parent': {
				'IpfcXInAMethod': '',
			},
			'child': {
			},
			'properties': {
				'Value': {
					'return': 'as String',
					'info': '  ',
				},
			},
			'method': {
			},
	}
