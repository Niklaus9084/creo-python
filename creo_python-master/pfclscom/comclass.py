from win32com.client import Dispatch


class CCpfcACIS3DExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcACIS3DExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcAngleDimensionSense(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcAngleDimensionSense.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcAngularDimOptions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcAngularDimOptions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcAngularDimSense(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcAngularDimSense.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcAnnotationTextStyle(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcAnnotationTextStyle.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcAppInfo(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcAppInfo.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcArcDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcArcDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcArgument(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcArgument.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcArrowDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcArrowDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcASSEMTreeCFGImportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcASSEMTreeCFGImportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcAsyncConnection(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcAsyncConnection.1')
		return super().__new__(cls)
	def Connect(self, *args, **kwargs):
		return self.disp.Connect(*args, **kwargs)
	def ConnectById(self, *args, **kwargs):
		return self.disp.ConnectById(*args, **kwargs)
	def ConnectWS(self, *args, **kwargs):
		return self.disp.ConnectWS(*args, **kwargs)
	def GetActiveConnection(self, *args, **kwargs):
		return self.disp.GetActiveConnection(*args, **kwargs)
	def Spawn(self, *args, **kwargs):
		return self.disp.Spawn(*args, **kwargs)
	def Start(self, *args, **kwargs):
		return self.disp.Start(*args, **kwargs)


class CCpfcBitmapImageExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcBitmapImageExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcBOMExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcBOMExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcBSplineDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcBSplineDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcBSplinePoint(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcBSplinePoint.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcCableParamsFileInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcCableParamsFileInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcCableParamsImportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcCableParamsImportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcCADDSExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcCADDSExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcCatiaCGR3DExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcCatiaCGR3DExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcCATIAFacetsExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcCATIAFacetsExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcCATIAModel3DExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcCATIAModel3DExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcCatiaPart3DExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcCatiaPart3DExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcCatiaProduct3DExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcCatiaProduct3DExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcCATIASession3DExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcCATIASession3DExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcCGMFILEExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcCGMFILEExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcCheckinOptions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcCheckinOptions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcCheckoutOptions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcCheckoutOptions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcCircleDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcCircleDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcColorRGB(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcColorRGB.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcColumnCreateOption(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcColumnCreateOption.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcComponentConstraint(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcComponentConstraint.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcComponentDimensionShowInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcComponentDimensionShowInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcCompositeCurveDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcCompositeCurveDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcConeDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcConeDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcConfigImportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcConfigImportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcConnectionId(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcConnectionId.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcConnectorParamExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcConnectorParamExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcConnectorParamsImportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcConnectorParamsImportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcConstraintAttributes(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcConstraintAttributes.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcCoonsPatchDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcCoonsPatchDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcCreateNewSimpRepInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcCreateNewSimpRepInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcCustomCheckInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcCustomCheckInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcCustomCheckResults(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcCustomCheckResults.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcCylinderDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcCylinderDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcCylindricalSplineSurfaceDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcCylindricalSplineSurfaceDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDatumAxisConstraint(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumAxisConstraint.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDatumAxisDimensionConstraint(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumAxisDimensionConstraint.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDatumCsysDimensionConstraint(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumCsysDimensionConstraint.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDatumCsysOrientMoveConstraint(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumCsysOrientMoveConstraint.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDatumCsysOriginConstraint(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumCsysOriginConstraint.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDatumPlaneAngleConstraint(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumPlaneAngleConstraint.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDatumPlaneDefaultXConstraint(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumPlaneDefaultXConstraint.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDatumPlaneDefaultYConstraint(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumPlaneDefaultYConstraint.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDatumPlaneDefaultZConstraint(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumPlaneDefaultZConstraint.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDatumPlaneNormalConstraint(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumPlaneNormalConstraint.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDatumPlaneOffsetConstraint(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumPlaneOffsetConstraint.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDatumPlaneOffsetCoordSysConstraint(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumPlaneOffsetCoordSysConstraint.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDatumPlaneParallelConstraint(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumPlaneParallelConstraint.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDatumPlaneSectionConstraint(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumPlaneSectionConstraint.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDatumPlaneTangentConstraint(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumPlaneTangentConstraint.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDatumPlaneThroughConstraint(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumPlaneThroughConstraint.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDatumPointDimensionConstraint(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumPointDimensionConstraint.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDatumPointPlacementConstraint(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumPointPlacementConstraint.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDetailAttachment(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDetailAttachment.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDetailEntityInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDetailEntityInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDetailGroupInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDetailGroupInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDetailLeaders(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDetailLeaders.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDetailNoteInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDetailNoteInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDetailSymbolDefInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDetailSymbolDefInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDetailSymbolGroupInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDetailSymbolGroupInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDetailSymbolInstInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDetailSymbolInstInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDetailText(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDetailText.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDetailTextLine(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDetailTextLine.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDetailVariantText(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDetailVariantText.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDimensionAngleOptions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDimensionAngleOptions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDimTolISODIN(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDimTolISODIN.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDimTolLimits(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDimTolLimits.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDimTolPlusMinus(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDimTolPlusMinus.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDimTolSymmetric(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDimTolSymmetric.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDimTolSymSuperscript(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDimTolSymSuperscript.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDirectorySelectionOptions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDirectorySelectionOptions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDrawingDimCreateInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDrawingDimCreateInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDrawingDimensionShowInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDrawingDimensionShowInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDWG3DExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDWG3DExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDWGImport2DInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDWGImport2DInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDWGSetupExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDWGSetupExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDWGSetupImportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDWGSetupImportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDXF3DExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDXF3DExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDXFExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDXFExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcDXFImport2DInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDXFImport2DInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcEllipseDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcEllipseDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcEmptyDimensionSense(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcEmptyDimensionSense.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcEmptyDimSense(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcEmptyDimSense.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcEPSImageExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcEPSImageExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcExport2DOption(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcExport2DOption.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcFeatInfoExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcFeatInfoExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcFIATExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcFIATExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcFileOpenOptions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcFileOpenOptions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcFileOpenRegisterOptions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcFileOpenRegisterOptions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcFileOpenShortcut(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcFileOpenShortcut.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcFileSaveOptions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcFileSaveOptions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcFileSaveRegisterOptions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcFileSaveRegisterOptions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcFilletSurfaceDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcFilletSurfaceDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcFolderAssignment(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcFolderAssignment.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcForeignSurfaceDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcForeignSurfaceDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcFreeAttachment(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcFreeAttachment.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcGeneralViewCreateInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcGeneralViewCreateInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcGeometryFlags(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcGeometryFlags.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcGeomExportFlags(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcGeomExportFlags.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcIGES3DExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcIGES3DExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcIGES3DNewExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcIGES3DNewExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcIGESFileExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcIGESFileExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcIGESImport2DInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcIGESImport2DInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcIGESSectionImportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcIGESSectionImportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcImportFeatAttr(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcImportFeatAttr.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcInclusionFlags(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcInclusionFlags.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcIntfACIS(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcIntfACIS.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcIntfAI(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcIntfAI.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcIntfCatiaCGR(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcIntfCatiaCGR.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcIntfCatiaPart(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcIntfCatiaPart.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcIntfCatiaProduct(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcIntfCatiaProduct.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcIntfCDRS(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcIntfCDRS.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcIntfDXF(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcIntfDXF.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcIntfICEM(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcIntfICEM.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcIntfIges(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcIntfIges.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcIntfJT(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcIntfJT.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcIntfNeutral(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcIntfNeutral.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcIntfNeutralFile(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcIntfNeutralFile.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcIntfParaSolid(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcIntfParaSolid.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcIntfProductView(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcIntfProductView.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcIntfStep(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcIntfStep.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcIntfSTL(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcIntfSTL.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcIntfUG(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcIntfUG.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcIntfVDA(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcIntfVDA.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcIntfVRML(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcIntfVRML.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcInventorExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcInventorExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcJPEGImageExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcJPEGImageExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcJT3DExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcJT3DExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcLayerExportOptions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcLayerExportOptions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcLeaderAttachment(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcLeaderAttachment.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcLinAOCTangentDimensionSense(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcLinAOCTangentDimensionSense.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcLineAOCTangentDimSense(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcLineAOCTangentDimSense.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcLineDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcLineDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcMassProperty(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcMassProperty.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcMaterialExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcMaterialExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcMaterialProperty(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcMaterialProperty.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcMedusaExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcMedusaExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcMessageDialogOptions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcMessageDialogOptions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcMFGFeatCLExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcMFGFeatCLExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcMFGOperCLExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcMFGOperCLExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcModelCheckInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcModelCheckInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcModelDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcModelDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)
	def CreateFromFileName(self, *args, **kwargs):
		return self.disp.CreateFromFileName(*args, **kwargs)


class CCpfcModelInfoExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcModelInfoExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcModelItemOId(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcModelItemOId.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcModelOId(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcModelOId.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcMouseStatus(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcMouseStatus.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcNEUTRALFileExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcNEUTRALFileExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcNormalLeaderAttachment(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcNormalLeaderAttachment.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcNURBSSurfaceDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcNURBSSurfaceDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcOffsetAttachment(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcOffsetAttachment.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcParameterSelectionOptions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcParameterSelectionOptions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcParametricAttachment(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcParametricAttachment.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcParamOId(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcParamOId.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcParaSolid3DExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcParaSolid3DExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcPDFExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcPDFExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcPDFOption(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcPDFOption.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcPlaneDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcPlaneDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcPlotInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcPlotInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcPointDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcPointDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcPointDimensionSense(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcPointDimensionSense.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcPointDimSense(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcPointDimSense.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcPointToAngleDimensionSense(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcPointToAngleDimensionSense.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcPointToAngleDimSense(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcPointToAngleDimSense.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcPolygonDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcPolygonDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcPopupmenuOptions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcPopupmenuOptions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcPrinterInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcPrinterInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcPrinterPCFOptions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcPrinterPCFOptions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcPrintMdlOption(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcPrintMdlOption.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcPrintPlacementOption(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcPrintPlacementOption.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcPrintPrinterOption(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcPrintPrinterOption.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcPrintSize(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcPrintSize.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcProductViewExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcProductViewExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcProductViewExportOptions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcProductViewExportOptions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcProgramExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcProgramExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcProgramImportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcProgramImportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcProjectionViewCreateInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcProjectionViewCreateInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcRegenInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcRegenInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcRelationExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcRelationExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcRelationFunctionArgument(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcRelationFunctionArgument.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcRelationFunctionOptions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcRelationFunctionOptions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcRelationImportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcRelationImportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcRenderExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcRenderExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcRetrieveExistingSimpRepInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcRetrieveExistingSimpRepInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcRetrieveModelOptions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcRetrieveModelOptions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcRevolvedSurfaceDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcRevolvedSurfaceDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcRuledSurfaceDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcRuledSurfaceDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcScreenTransform(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcScreenTransform.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSelectionOptions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSelectionOptions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSelectionPair(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSelectionPair.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcShrinkwrapFacetedPartInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcShrinkwrapFacetedPartInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcShrinkwrapMergedSolidInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcShrinkwrapMergedSolidInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcShrinkwrapSTLInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcShrinkwrapSTLInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcShrinkwrapSurfaceSubsetInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcShrinkwrapSurfaceSubsetInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcShrinkwrapVRMLInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcShrinkwrapVRMLInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSimpRepAutomatic(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSimpRepAutomatic.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSimpRepBoundBox(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSimpRepBoundBox.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSimpRepCompItemPath(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSimpRepCompItemPath.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSimpRepDefaultEnvelope(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSimpRepDefaultEnvelope.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSimpRepExclude(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSimpRepExclude.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSimpRepFeatItemPath(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSimpRepFeatItemPath.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSimpRepGeom(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSimpRepGeom.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSimpRepGraphics(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSimpRepGraphics.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSimpRepInclude(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSimpRepInclude.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSimpRepItem(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSimpRepItem.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSimpRepLightWeightGraphics(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSimpRepLightWeightGraphics.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSimpRepNone(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSimpRepNone.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSimpRepReverse(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSimpRepReverse.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSimpRepSubstitute(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSimpRepSubstitute.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSimpRepSymb(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSimpRepSymb.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSliceExportData(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSliceExportData.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSphericalSplineSurfaceDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSphericalSplineSurfaceDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSplineDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSplineDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSplinePoint(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSplinePoint.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSplinePointDimensionSense(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSplinePointDimensionSense.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSplinePointDimSense(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSplinePointDimSense.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSplineSurfaceDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSplineSurfaceDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSplineSurfacePoint(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSplineSurfacePoint.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSpoolImportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSpoolImportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSTEP2DExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSTEP2DExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSTEP3DExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSTEP3DExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSTEPExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSTEPExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSTEPImport2DInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSTEPImport2DInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSTLASCIIExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSTLASCIIExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSTLBinaryExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSTLBinaryExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSubstAsmRep(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSubstAsmRep.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSubstEnvelope(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSubstEnvelope.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSubstInterchg(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSubstInterchg.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSubstPrtRep(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSubstPrtRep.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSurfaceExtents(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSurfaceExtents.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSWAsm3DExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSWAsm3DExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSWPart3DExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSWPart3DExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcSymbolDefAttachment(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSymbolDefAttachment.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcTableCell(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcTableCell.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcTableCreateInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcTableCreateInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcTableRetrieveInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcTableRetrieveInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcTabulatedCylinderDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcTabulatedCylinderDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcTangentIndexDimensionSense(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcTangentIndexDimensionSense.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcTangentIndexDimSense(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcTangentIndexDimSense.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcTangentLeaderAttachment(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcTangentLeaderAttachment.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcTextDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcTextDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcTextReference(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcTextReference.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcTextStyle(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcTextStyle.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcTIFFImageExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcTIFFImageExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcTorusDescriptor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcTorusDescriptor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcTransform3D(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcTransform3D.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcTriangulationInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcTriangulationInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcUDFAssemblyIntersection(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcUDFAssemblyIntersection.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcUDFCustomCreateInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcUDFCustomCreateInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcUDFExternalReference(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcUDFExternalReference.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcUDFPromptCreateInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcUDFPromptCreateInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcUDFReference(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcUDFReference.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcUDFVariantDimension(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcUDFVariantDimension.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcUDFVariantPatternParam(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcUDFVariantPatternParam.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcUG3DExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcUG3DExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcUnitConversionFactor(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcUnitConversionFactor.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcUnitConversionOptions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcUnitConversionOptions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcUploadOptions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcUploadOptions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcVDA3DExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcVDA3DExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcVDAExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcVDAExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcViewDisplay(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcViewDisplay.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcViewOId(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcViewOId.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcVRMLDirectExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcVRMLDirectExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcVRMLModelExportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcVRMLModelExportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcWindowOId(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcWindowOId.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcWireListImportInstructions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcWireListImportInstructions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcWorkspaceDefinition(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcWorkspaceDefinition.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CCpfcWSExportOptions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcWSExportOptions.1')
		return super().__new__(cls)
	def Create(self, *args, **kwargs):
		return self.disp.Create(*args, **kwargs)


class CMpfcArgument(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.MpfcArgument.1')
		return super().__new__(cls)
	def CreateASCIIStringArgValue(self, *args, **kwargs):
		return self.disp.CreateASCIIStringArgValue(*args, **kwargs)
	def CreateBoolArgValue(self, *args, **kwargs):
		return self.disp.CreateBoolArgValue(*args, **kwargs)
	def CreateDoubleArgValue(self, *args, **kwargs):
		return self.disp.CreateDoubleArgValue(*args, **kwargs)
	def CreateIntArgValue(self, *args, **kwargs):
		return self.disp.CreateIntArgValue(*args, **kwargs)
	def CreateSelectionArgValue(self, *args, **kwargs):
		return self.disp.CreateSelectionArgValue(*args, **kwargs)
	def CreateStringArgValue(self, *args, **kwargs):
		return self.disp.CreateStringArgValue(*args, **kwargs)
	def CreateTransformArgValue(self, *args, **kwargs):
		return self.disp.CreateTransformArgValue(*args, **kwargs)
	def FindArgumentByLabel(self, *args, **kwargs):
		return self.disp.FindArgumentByLabel(*args, **kwargs)


class CMpfcAssembly(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.MpfcAssembly.1')
		return super().__new__(cls)
	def CreateComponentPath(self, *args, **kwargs):
		return self.disp.CreateComponentPath(*args, **kwargs)


class CMpfcExternal(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.MpfcExternal.1')
		return super().__new__(cls)
	def CreateDoubleExternalData(self, *args, **kwargs):
		return self.disp.CreateDoubleExternalData(*args, **kwargs)
	def CreateIntExternalData(self, *args, **kwargs):
		return self.disp.CreateIntExternalData(*args, **kwargs)
	def CreateStringExternalData(self, *args, **kwargs):
		return self.disp.CreateStringExternalData(*args, **kwargs)


class CMpfcInterference(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.MpfcInterference.1')
		return super().__new__(cls)
	def CreateGlobalEvaluator(self, *args, **kwargs):
		return self.disp.CreateGlobalEvaluator(*args, **kwargs)
	def CreateSelectionEvaluator(self, *args, **kwargs):
		return self.disp.CreateSelectionEvaluator(*args, **kwargs)


class CMpfcModelItem(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.MpfcModelItem.1')
		return super().__new__(cls)
	def CreateBoolParamValue(self, *args, **kwargs):
		return self.disp.CreateBoolParamValue(*args, **kwargs)
	def CreateDoubleParamValue(self, *args, **kwargs):
		return self.disp.CreateDoubleParamValue(*args, **kwargs)
	def CreateIntParamValue(self, *args, **kwargs):
		return self.disp.CreateIntParamValue(*args, **kwargs)
	def CreateNoteParamValue(self, *args, **kwargs):
		return self.disp.CreateNoteParamValue(*args, **kwargs)
	def CreateStringParamValue(self, *args, **kwargs):
		return self.disp.CreateStringParamValue(*args, **kwargs)


class CMpfcSelect(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.MpfcSelect.1')
		return super().__new__(cls)
	def CreateComponentSelection(self, *args, **kwargs):
		return self.disp.CreateComponentSelection(*args, **kwargs)
	def CreateModelItemSelection(self, *args, **kwargs):
		return self.disp.CreateModelItemSelection(*args, **kwargs)
	def CreateModelSelection(self, *args, **kwargs):
		return self.disp.CreateModelSelection(*args, **kwargs)
	def CreateSelectionFromString(self, *args, **kwargs):
		return self.disp.CreateSelectionFromString(*args, **kwargs)


class CMpfcSession(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.MpfcSession.1')
		return super().__new__(cls)
	def GetCurrentSession(self, *args, **kwargs):
		return self.disp.GetCurrentSession(*args, **kwargs)
	def GetCurrentSessionWithCompatibility(self, *args, **kwargs):
		return self.disp.GetCurrentSessionWithCompatibility(*args, **kwargs)
	def SetSessionUndo(self, *args, **kwargs):
		return self.disp.SetSessionUndo(*args, **kwargs)


class CpfcActionListeners(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcActionListeners.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcActionSources(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcActionSources.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcActionTypes(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcActionTypes.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcArguments(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcArguments.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcAttachments(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcAttachments.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcBaseDimensions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcBaseDimensions.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcBSplinePoints(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcBSplinePoints.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcColumnCreateOptions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcColumnCreateOptions.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcComponentConstraints(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcComponentConstraints.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcComponentFeats(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcComponentFeats.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcComponentPaths(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcComponentPaths.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcContours(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcContours.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcCoonsCornerPoints(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcCoonsCornerPoints.1')
		return super().__new__(cls)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcCoonsUVDerivatives(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcCoonsUVDerivatives.1')
		return super().__new__(cls)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcCurveDescriptors(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcCurveDescriptors.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcCurves(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcCurves.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDatumAxisConstraints(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumAxisConstraints.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDatumAxisDimensionConstraints(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumAxisDimensionConstraints.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDatumCsysDimensionConstraints(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumCsysDimensionConstraints.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDatumCsysOrientMoveConstraints(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumCsysOrientMoveConstraints.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDatumCsysOriginConstraints(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumCsysOriginConstraints.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDatumPlaneConstraints(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumPlaneConstraints.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDatumPointDimensionConstraints(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumPointDimensionConstraints.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDatumPointPlacementConstraints(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDatumPointPlacementConstraints.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDependencies(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDependencies.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDetailItems(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDetailItems.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDetailLeaderAttachments(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDetailLeaderAttachments.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDetailSymbolDefItems(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDetailSymbolDefItems.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDetailSymbolGroups(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDetailSymbolGroups.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDetailSymbolInstItems(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDetailSymbolInstItems.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDetailTextLines(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDetailTextLines.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDetailTexts(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDetailTexts.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDetailVariantTexts(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDetailVariantTexts.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDimension2Ds(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDimension2Ds.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDimensionAttachment(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDimensionAttachment.1')
		return super().__new__(cls)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDimensionAttachments(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDimensionAttachments.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDimensions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDimensions.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDimensionSenses(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDimensionSenses.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDimSenses(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDimSenses.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDisplayStatuses(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDisplayStatuses.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDrawingCreateErrors(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDrawingCreateErrors.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcDrawingCreateOptions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcDrawingCreateOptions.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcEdges(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcEdges.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcEnvelope2D(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcEnvelope2D.1')
		return super().__new__(cls)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcExternalDataClasses(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcExternalDataClasses.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcExternalDataSlots(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcExternalDataSlots.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcFacetControlFlags(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcFacetControlFlags.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcFamilyTableColumns(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcFamilyTableColumns.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcFamilyTableRows(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcFamilyTableRows.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcFeatureGroups(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcFeatureGroups.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcFeatureOperations(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcFeatureOperations.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcFeatures(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcFeatures.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcFileOpenShortcuts(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcFileOpenShortcuts.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcFolderAssignments(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcFolderAssignments.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcGeneralDatumPoints(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcGeneralDatumPoints.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcGlobalInterferences(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcGlobalInterferences.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcInertia(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcInertia.1')
		return super().__new__(cls)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcLayers(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcLayers.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcLeaderAttachments(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcLeaderAttachments.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcMaterials(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcMaterials.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcMatrix3D(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcMatrix3D.1')
		return super().__new__(cls)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcMessageButtons(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcMessageButtons.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcModelDescriptors(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcModelDescriptors.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcModelItems(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcModelItems.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcModelItemTypes(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcModelItemTypes.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcModels(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcModels.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcOutline2D(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcOutline2D.1')
		return super().__new__(cls)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcOutline3D(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcOutline3D.1')
		return super().__new__(cls)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcParameters(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcParameters.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcParameterSelectionContexts(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcParameterSelectionContexts.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcParamValues(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcParamValues.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcPDFOptions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcPDFOptions.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcPoint2D(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcPoint2D.1')
		return super().__new__(cls)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcPoint2Ds(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcPoint2Ds.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcPoint3D(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcPoint3D.1')
		return super().__new__(cls)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcPoint3Ds(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcPoint3Ds.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcPrincipalAxes(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcPrincipalAxes.1')
		return super().__new__(cls)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcRelationFunctionArguments(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcRelationFunctionArguments.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcSelections(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSelections.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcServers(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcServers.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcSimpRepItems(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSimpRepItems.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcSimpReps(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSimpReps.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcSplinePoints(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSplinePoints.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcSplineSurfacePoints(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSplineSurfacePoints.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcStdColors(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcStdColors.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcSurfaceDescriptors(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSurfaceDescriptors.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcSurfaces(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSurfaces.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcSymbolDefAttachments(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcSymbolDefAttachments.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcTables(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcTables.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcTransform3Ds(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcTransform3Ds.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcUDFAssemblyIntersections(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcUDFAssemblyIntersections.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcUDFDimensions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcUDFDimensions.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcUDFExternalReferences(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcUDFExternalReferences.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcUDFOrientations(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcUDFOrientations.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcUDFReferences(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcUDFReferences.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcUDFVariantValues(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcUDFVariantValues.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcUnits(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcUnits.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcUnitSystems(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcUnitSystems.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcUVOutline(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcUVOutline.1')
		return super().__new__(cls)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcUVParams(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcUVParams.1')
		return super().__new__(cls)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcUVParamsSequence(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcUVParamsSequence.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcUVVector(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcUVVector.1')
		return super().__new__(cls)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcVector2D(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcVector2D.1')
		return super().__new__(cls)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcVector3D(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcVector3D.1')
		return super().__new__(cls)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcVector3Ds(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcVector3Ds.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcView2Ds(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcView2Ds.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcViews(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcViews.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcWindows(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcWindows.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcWorkspaceDefinitions(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcWorkspaceDefinitions.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcWSImportExportMessages(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcWSImportExportMessages.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CpfcXSections(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcXSections.1')
		return super().__new__(cls)
	def Append(self, *args, **kwargs):
		return self.disp.Append(*args, **kwargs)
	def Clear(self, *args, **kwargs):
		return self.disp.Clear(*args, **kwargs)
	def Count(self, *args, **kwargs):
		return self.disp.Count(*args, **kwargs)
	def Insert(self, *args, **kwargs):
		return self.disp.Insert(*args, **kwargs)
	def InsertSeq(self, *args, **kwargs):
		return self.disp.InsertSeq(*args, **kwargs)
	def Item(self, *args, **kwargs):
		return self.disp.Item(*args, **kwargs)
	def Remove(self, *args, **kwargs):
		return self.disp.Remove(*args, **kwargs)
	def Set(self, *args, **kwargs):
		return self.disp.Set(*args, **kwargs)


class CCpfcXCancelProEAction(object):
	def __new__(cls, *args, **kwargs):
		cls.disp = Dispatch('cls.pfcls.pfcXCancelProEAction.1')
		return super().__new__(cls)
	def Throw(self, *args, **kwargs):
		return self.disp.Throw(*args, **kwargs)
