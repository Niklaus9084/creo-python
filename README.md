# CreoPython

#### 介绍

利用 VBAPI 接口，使用 python 进行二次开发。

#### 说明

1.  ParameterOperationByExcel
    批量添加删除参数，交互截面使用 tkinter 开发
2.  creo_python-master
    包含多个小程序，交互界面采用 PyQt 开发
